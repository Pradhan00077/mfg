const tintColor = '#26BBF2';

export default {
  tintColor,
  tabIconDefault: '#8E8E93',
  tabIconSelected: tintColor,
  tabBar: '#F1F1F1',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
