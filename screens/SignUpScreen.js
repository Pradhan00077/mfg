import React from 'react';
import { Container, Item, Button } from 'native-base';
import { ImageEditor,ImageStore, View, Image, StyleSheet, TouchableOpacity, ScrollView, Platform, Text, Picker } from 'react-native';
import Spacer from '../components/UI/Spacer';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import { LinearGradient } from 'expo-linear-gradient';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import IOSPicker from 'react-native-ios-picker';

const name = /^[a-zA-Z]+(?:(?:|['_\. ])([a-zA-Z]*(\.\s)?[a-zA-Z])+)*$/;
const City = /^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$/;
const Country = /^[a-zA-Z]{2,}$/;
const mobile = /^(\+\d{1,3}[- ]?)?\d{10}$/;

let userImage = new FormData();

const numMonthDays = {
  '1': 31, '2': 28, '3': 31, '4': 30, '5': 31, '6': 30,'7': 31, '8': 31, '9': 30, '10': 31, '11': 30, '12': 31
}

class SignUp extends React.Component {
  constructor(props) {
    super(props);

    let currentDate = new Date();
    this.state = {
      userinfo: {
        Avatar: null,
        FirstName: '',
        FirstNameValidator: true,
        LastName: '',
        LastNameValidator: true,
        Address: '',
        AddressValidator: true,
        City: '',
        CityValidator: true,
        Country: '',
        CountryValidator: true,
        Mobile: '',
        MobileValidator: true,
        Gender: 'Male',
        Relationship: 'Mother',
        Birthday: '01/01/2000',
        currentDate: currentDate,
        uri: null,
        imageId: this.uniqueId(),
        month: new Date().getMonth(),
        day: new Date().getDay(),
        year: new Date().getFullYear() - 30,
      },
      imageSelected: false,
      isButtonDisabled: true,
      errorshow: false,
      error: false,

      month: 'Month',
      day: 'Day',
      year: 'Year',

      changedDay: "1",
      changedMonth: "1",
      changedYear: new Date().getFullYear() - 50,

      numberOfDays: "31",
      selectedDate: "",
      imgbase64:null,

      signUpFirstName: "",
      signUpLastName: "",
      signUpAddress: "",
      signUpCity: "",
      signUpCountry: "",
      signUpMobile: "",

    };
    //this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    this.validates = this.validates.bind(this);
    this.formsubmit = this.formsubmit.bind(this);

  }

  //CALLING COMPONENT ON BLUR FIELD
  async onBlurValidates(type) {

    if (type === 'FirstName') {
      if (this.state.userinfo.FirstName == '') {
        this.setState({ signUpFirstName: "First name can not be blank.", isButtonDisabled: true })
      }else if(this.state.userinfo.FirstName.trim(' ').length < 3){
        this.setState({ signUpFirstName: "Enter minimum 3 characters.", isButtonDisabled: true })
      }else{
        this.setState({ signUpFirstName: "" })
      }
    } else if (type === 'LastName') {
      if (this.state.userinfo.LastName == '') {
        this.setState({ signUpLastName: "Last name can not be blank.", isButtonDisabled: true })
      }else if(this.state.userinfo.LastName.trim(' ').length < 3){
        this.setState({ signUpLastName: "Enter minimum 3 characters.", isButtonDisabled: true })
      }else{
        this.setState({ signUpLastName: "" })
      }
    } else if (type === 'Address') {
      if (this.state.userinfo.Address == '') {
        this.setState({ signUpAddress: "Address can not be blank.", isButtonDisabled: true })
      }else if(this.state.userinfo.Address.trim(' ').length < 3){
        this.setState({ signUpAddress: "Enter minimum 3 characters.", isButtonDisabled: true })
      }else{
        this.setState({ signUpAddress: "" })
      }
    } else if (type === 'City') {
      if (this.state.userinfo.City == '') {
        this.setState({ signUpCity: "City can not be blank.", isButtonDisabled: true })
      }else if(this.state.userinfo.City.trim(' ').length < 3){
        this.setState({ signUpCity: "Enter minimum 3 characters.", isButtonDisabled: true })
      }else{
        this.setState({ signUpCity: "" })
      }
    } else if (type === 'Country') {
      if (this.state.userinfo.Country == '') {
        this.setState({ signUpCountry: "Country can not be blank.", isButtonDisabled: true })
      }else if(this.state.userinfo.Country.trim(' ').length < 3){
        this.setState({ signUpCountry: "Enter minimum 3 characters.", isButtonDisabled: true })
      }else{
        this.setState({ signUpCountry: "" })
      }
    } else if (type === 'Mobile') {
      if (this.state.userinfo.Mobile == '') {
        this.setState({ signUpMobile: "Mobile number can not be blank.", isButtonDisabled: true })
      }else if(this.state.userinfo.Mobile.trim(' ').length <= 9){
        this.setState({ signUpMobile: "Mobile number should be correct format.", isButtonDisabled: true })
      }else{
        this.setState({ signUpMobile: "" })
      }
    }
    

  }

  validates = (text, type) => {

    if (type === 'FirstName') {
      if ( text.trim(' ').length >= 3 ) {
        this.state.userinfo.FirstName = text;
        this.setState({ error: false, signUpFirstName: "" });
      } else {
        this.state.userinfo.FirstName = text;
        this.setState({ signUpFirstName: "" })
      }
    }
    else if (type === 'LastName') {
      if ( text.trim(' ').length >= 3) {
        this.state.userinfo.LastName = text;
        this.setState({ error: false, signUpLastName: "" });
      } else {
        this.state.userinfo.LastName = text;
        this.setState({ signUpLastName: "" })
      }
    }
    else if (type === 'Address') {
      if ( text.trim(' ').length >= 3) {
        this.state.userinfo.Address = text;
        this.setState({ error: false, signUpAddress: "" });
      }
      else {
        this.state.userinfo.Address = text;
        this.setState({ AddressValidator: false, signUpAddress: "" })
      }
    }
    else if (type === 'City') {
      if ( text.trim(' ').length >= 3) {
        this.state.userinfo.City = text;
        this.setState({ error: false, signUpCity: "" });
      }
      else {
        this.state.userinfo.City = text;
        this.setState({ CityValidator: false, signUpCity: "" })
      }
    }
    else if (type === 'Country') {
      if ( text.trim(' ').length >= 3) {
        this.state.userinfo.Country = text;
        this.setState({ error: false, signUpCountry: "" });
      }
      else {
        this.state.userinfo.Country = text;
        this.setState({ signUpCountry: "" })
      }
    }
    else if (type === 'Mobile') {
      if ( text.trim(' ').length >= 10) {
        this.state.userinfo.Mobile = text;
        this.setState({ error: false, signUpMobile: "" });
      } else {
        this.state.userinfo.Mobile = text;
        this.setState({ signUpMobile: "" })
      }
    }
    else if (type === 'Relationship') {
      this.state.userinfo.Relationship = text;
      this.setState({ error: false });
    }
    else if (type === 'Gender') {
      this.state.userinfo.Gender = text;
      this.setState({ error: false });
    }


    if( this.state.userinfo.FirstName !== '' &&
      this.state.userinfo.LastName !== '' &&
      this.state.userinfo.Relationship !== '' &&
      this.state.userinfo.Address !== '' &&
      this.state.userinfo.City !== '' &&
      this.state.userinfo.Country !== '' &&
      this.state.userinfo.Mobile !== '')
    {
      this.setState({ isButtonDisabled: false });
    } else {
      this.setState({ isButtonDisabled: true });
    }

  }

  formsubmit = () => {
    this.props.navigation.navigate('Signupchild', { userdata: this.state.userinfo })
  }


  // USING FUNCTION TO ASK PERMISSION FROM USER
  _checkPermissions = async () => {

    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    //  console.log("Camera status - ", status);
    this.setState({ camera: status });

    // take permission for Gallery, ask a CameraRoll
    const { statusRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    //  console.log("cameraRoll - ", statusRoll);
    this.setState({ cameraRoll: statusRoll });

  };

  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1); // substring return all chars except char at index=0;
  };

  uniqueId = () => {
    // create uniqueId for image as Alphabetical
    return (
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4()
    );
  };

 // USING FUNCTION TO CALL PERMISSION WITH DIRECTORY TO CHOOSE IMAGES
 findNewImage = async () => {

  this._checkPermissions();

  let result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: "Images",
    allowsEditing: true,
    aspect: [1, 1],
    quality: 1,
    isCamera: true,
  });

  if (!result.cancelled) {
    let resizedUri = await new Promise((resolve, reject) => {
      ImageEditor.cropImage(result.uri,
        {
          offset: { x: 0, y: 0 },
          size: { width: result.width, height: result.height },
          displaySize: { width: 100, height: 100 },
          resizeMode: 'contain',
        },
    (uri) => {
          ImageStore.getBase64ForTag(uri, (base64Data) => resolve(base64Data), (error) => reject(error))
        },
    () => reject(),
      );
    });
  this.state.userinfo['Avatar'] = resizedUri;
    this.state.userinfo['uri'] = result.uri;
    this.state.userinfo['imageId'] = this.uniqueId();
    this.setState({ imageSelected: true });
  }
};

  async onChangeDays(oMonthSel, oDaysSel, oYearSel) {

    let nDays = ''
    let oDaysSelLgth = ''
    
    if(oMonthSel === "month"){
      if(this.state.month === "") oMonthSel = this.state.changedMonth 
      else oMonthSel = (this.state.month == undefined)?0:this.state.month
    }
    if(oYearSel === "year"){
      if(this.state.year === "") oYearSel = this.state.changedYear 
      else oYearSel = (this.state.year == undefined)?0:this.state.year
    }
    if(oDaysSel === "day"){
      if(this.state.day === "") oDaysSel = this.state.changedDay 
      else oDaysSel = (this.state.day == undefined)?0:this.state.day
    }

    nDays = numMonthDays[oMonthSel];
    
    if (nDays == 28 && oYearSel % 4 == 0){
      nDays = nDays + 1;
    }
    this.setState({ numberOfDays: nDays })

    oDaysSelLgth = this.state.numberOfDays;
    let selectDate = oMonthSel + '/' + oDaysSel + '/' + oYearSel;

    this.setState({ day: oDaysSel, month: oMonthSel, year: oYearSel, selectedDate: selectDate })

    if( (oMonthSel != 0 && oMonthSel != "Month") && (oDaysSel != 0 && oDaysSel != "Day") && (oYearSel != 0 && oYearSel != "Year") ){

        this.state.userinfo.Birthday = selectDate;
        console.log("Date - ", selectDate )

        if( this.state.userinfo.FirstName !== '' &&
            this.state.userinfo.LastName !== '' &&
            this.state.userinfo.Relationship !== '' &&
            this.state.userinfo.Address !== '' &&
            this.state.userinfo.City !== '' &&
            this.state.userinfo.Country !== '' &&
            this.state.userinfo.Mobile !== '')
        {
          this.setState({ isButtonDisabled: false });
        } else {
          this.setState({ isButtonDisabled: true });
        }
    }else{
      this.setState({ isButtonDisabled: true });
    }

  }

  render() {

      
    let monthsForIos = [
      { label: '01', value: '1', },
      { label: '02', value: '2' },
      { label: '03', value: '3' },
      { label: '04', value: '4' },
      { label: '05', value: '5' },
      { label: '06', value: '6' },
      { label: '07', value: '7' },
      { label: '08', value: '8' },
      { label: '09', value: '9' },
      { label: '10', value: '10' },
      { label: '11', value: '11' },
      { label: '12', value: '12' }
    ];
    

    let Gender = [
      { value: 'Male' },
      { value: 'Female' }
    ];

    let Relationship = [
      { value: 'Mother' },
      { value: 'Father' },
    ];

    let year = [];
    let currentYear = new Date().getFullYear();
    for (let i = currentYear - 50; i <= currentYear; i++) {
      year.push({
        value: i,
      });
    }


    let days = [];
    let labelVal = ''
    let daysLength = ''
    
    if(this.state.numberOfDays == undefined)
      daysLength = "31"
    else
      daysLength = this.state.numberOfDays

    for (let i = 0; i < daysLength; i++) {
      days.push({
        value: i+1,
      });
    }

    const totDays = days.map((item, i) => {
      if((item.value).toString() < 10){
        labelVal =  "0"+(item.value).toString()
      }else{
        labelVal =  (item.value).toString()
      }
      return (
        <Picker.Item label={labelVal} value={item.value} key={i} />
      )
    })

    const totYears = year.map((item, i) => {
      return (
        <Picker.Item label={(item.value).toString()} value={item.value} key={i} />
      )
    })

    const options = {
      title: 'Select Avatar',
      customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    return (
      <Container>
        <ScrollView keyboardDismissMode='on-drag' style={styles.container} > 
            <View style={{ flex: 1 }}>
              <Spacer size={6} />
              <Text style={styles.title}>PARENT INFO</Text>
            </View>
            <View style={styles.mainContainer}>
              <View style={{ flex: 1 }}>
                <View style={styles.content}>
                  <Spacer size={10} />
                  <View>
                    {
                      (this.state.error) ?
                        (<Text style={{ alignSelf: 'center' }}>{errormessage}</Text>) :
                        (<View></View>)
                    }
                  </View>
                  <Spacer size={12} />

                  <View style={styles.notification}>
                    <View style={styles.firstColumn}>

                      <TextField
                        label='First Name'
                        keyboardType="default"
                        onChangeText={(value) => this.validates(value, 'FirstName')}
                        onBlur={() => this.onBlurValidates('FirstName')}
                        style={styles.labelName}
                        error={this.state.signUpFirstName}
                      />

                    </View>
                    <View style={styles.secondColumn}>
                      {/* USING FOLLOWING CONDITION TO SHOW SELECTED IMAGE OR BUTTON TO CHOOSE */}
                      {(this.state.imageSelected) ? (
                        //USING CODE TO DISPLAY IMAGE
                        <View>
                          <TouchableOpacity onPress={() => this.findNewImage()}>
                            <View style={[styles.Avatar, styles.AvatarContainer, { marginTop: 10, marginBottom: 15 },]} >
                              <Image source={{ uri: this.state.userinfo.uri }} style={styles.Avatar} />
                            </View>
                          </TouchableOpacity>
                        </View>

                      ) : (
                          <View>
                            <TouchableOpacity onPress={() => this.findNewImage()}>
                              <View style={[styles.Avatar, styles.AvatarContainer,{marginTop: 10, marginBottom: 15},]} >
                                <Image style={{ width: 15, height: 15 }} source={require('../assets/images/edit.png')} />
                              </View>
                            </TouchableOpacity>
                          </View>

                        )}
                      {/* CONDITION CLOSED */}
                    </View>
                  </View>

                  <View style={styles.notification}>
                    <View style={{ flex: 1, marginTop: -20 }}>
                      <TextField
                        label='Last Name'
                        keyboardType="default"
                        onChangeText={(value) => this.validates(value, 'LastName')}
                        onBlur={() => this.onBlurValidates('LastName')}
                        style={styles.labelName}
                        error={this.state.signUpLastName}
                      />
                    </View>
                  </View>

                  <View style={styles.itemView}>
                    <Item stackedLabel style={{
                      borderWidth: 1,
                      borderColor: '#999999'
                    }}>
                      <Dropdown containerStyle={styles.input}
                        label='Gender'
                        textColor='#000000'
                        baseColor="rgba(0, 0, 0, 0.54)"
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        labelFontSize={17}
                        value={this.state.userinfo.Gender}
                        data={Gender}
                        onChangeText={(value) => this.validates(value, 'Gender')}
                        style={styles.labelName}
                      />
                    </Item>
                  </View>

                  <View style={styles.itemView}>
                    <Item stackedLabel style={{
                      borderWidth: 1,
                      borderColor: '#999999'
                    }}>
                      <Dropdown containerStyle={styles.input}
                        label='Relationship'
                        textColor='#000000'
                        baseColor="rgba(0, 0, 0, 0.54)"
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        labelFontSize={17}
                        value={this.state.userinfo.Relationship}
                        data={Relationship}
                        onChangeText={(value) => this.validates(value, 'Relationship')}
                        style={styles.labelName}
                      />
                    </Item>
                  </View>

                  <Spacer size={15} />


                  <View style={[styles.notification]}>

                    <View style={[styles.birthday, { flex: 1, alignSelf: "flex-start" }]}>
                      <Text style={{ color: "rgba(0, 0, 0, 0.87)", fontSize: 12, lineHeight: 16 }} >
                        Birthday
                      </Text>
                    </View>

                  </View>


                  <View style={[styles.notification, { borderBottomColor: "rgba(0, 0, 0, 0.12)", borderBottomWidth: 1 }]}>

                    <View style={[styles.birthday, { flex: 0.43, borderRightWidth:1, borderRightColor: "rgba(0, 0, 0, 0.12)"}]}>
                      {
                        Platform.select({
                          ios:(
                            <IOSPicker 
                              mode='modal'
                              selectedValue={
                                (this.state.month < 10)?(
                                  (parseInt(this.state.month) == 0)?(
                                    "Month"
                                  ):(
                                    "0"+this.state.month
                                  )
                                ):(
                                  this.state.month
                                )
                              }
                              style={[styles.labelName, { height:30, width: "100%", paddingRight: 0, marginRight: 0, borderTopColor: "#FFFFFF"  }]}
                              onValueChange={(value) => this.onChangeDays(value,"day","year") }
                            >
                              <Picker.Item label="Month" value="0" />
                              <Picker.Item label="01" value="1" />
                              <Picker.Item label="02" value="2" />
                              <Picker.Item label="03" value="3" />
                              <Picker.Item label="04" value="4" />
                              <Picker.Item label="05" value="5" />
                              <Picker.Item label="06" value="6" />
                              <Picker.Item label="07" value="7" />
                              <Picker.Item label="08" value="8" />
                              <Picker.Item label="09" value="9" />
                              <Picker.Item label="10" value="10" />
                              <Picker.Item label="11" value="11" />
                              <Picker.Item label="12" value="12" />
                            </IOSPicker>
                          ),
                          android:(

                            <Picker
                              selectedValue={this.state.month}
                              style={[styles.labelName, { height:30, width: "100%", paddingRight: 0, marginRight: 0  }]}
                              onValueChange={(value) => this.onChangeDays(value,"day","year") }
                            >
                              <Picker.Item label="Month" value="0" />
                              <Picker.Item label="01" value="1" />
                              <Picker.Item label="02" value="2" />
                              <Picker.Item label="03" value="3" />
                              <Picker.Item label="04" value="4" />
                              <Picker.Item label="05" value="5" />
                              <Picker.Item label="06" value="6" />
                              <Picker.Item label="07" value="7" />
                              <Picker.Item label="08" value="8" />
                              <Picker.Item label="09" value="9" />
                              <Picker.Item label="10" value="10" />
                              <Picker.Item label="11" value="11" />
                              <Picker.Item label="12" value="12" />

                            </Picker>

                          )
                        })
                      }

                    </View>

                    <View style={[styles.birthday, { flex: 0.38, borderRightWidth:1, borderRightColor: "rgba(0, 0, 0, 0.12)" }]}>
                    {
                        Platform.select({
                          ios:(
                            <IOSPicker
                              mode='modal'
                              selectedValue={
                                (this.state.day < 10)?(
                                  (this.state.day == 0)?(
                                    "Day"
                                  ):(
                                    "0"+this.state.day
                                  )
                                ):(
                                  this.state.day
                                )
                              }
                              style={[styles.labelName, {height:30, borderTopColor: "#FFFFFF"}]}
                              onValueChange={(value) => this.onChangeDays("month",value,"year") }
                            >
                              <Picker.Item label="Day" value="0" />
                              {totDays}
                            </IOSPicker>
                            
                          ),
                          android:(

                            <Picker
                              selectedValue={this.state.day}
                              style={[styles.labelName, {height:30}]}
                              onValueChange={(value) => this.onChangeDays("month",value,"year") }
                            >
                              <Picker.Item label="Day" value="0" />
                              {totDays}
                            </Picker>

                          )
                        })
                      }

                    </View>

                    <View style={[styles.birthday, { flex: 0.39 }]}>
  
                    {
                        Platform.select({
                          ios:(
                            
                            <IOSPicker
                              mode='modal'
                              selectedValue={
                                (this.state.year == "0")?(
                                  "Year"
                                ):(
                                  this.state.year
                                )
                              }
                              style={[styles.labelName,{ height:30, borderTopColor: "#FFFFFF" }]}
                              onValueChange={(value) => this.onChangeDays("month","day",value) }
                            >
                              <Picker.Item label="Year" value="0" />
                              {totYears}
                            </IOSPicker>
                            
                          ),
                          android:(

                            <Picker
                              selectedValue={this.state.year}
                              style={[styles.labelName,{ height:30 }]}
                              onValueChange={(value) => this.onChangeDays("month","day",value) }
                            >
                              <Picker.Item label="Year" value="0" />
                              {totYears}
                            </Picker>

                          )
                        })
                      }
                      

                    </View>

                  </View>

                  <View style={styles.itemView}>

                    <TextField
                      label='Address'
                      keyboardType="default"
                      onChangeText={(value) => this.validates(value, 'Address')}
                      onBlur={() => this.onBlurValidates('Address')}
                      style={styles.labelName}
                      error={this.state.signUpAddress}
                    />

                  </View>
                  <View style={styles.itemView}>

                    <TextField
                      label='City'
                      keyboardType="default"
                      onChangeText={(value) => this.validates(value, 'City')}
                      onBlur={() => this.onBlurValidates('City')}
                      style={styles.labelName}
                      error={this.state.signUpCity}
                    />

                  </View>
                  <View style={styles.itemView}>

                    <TextField
                      label='Country'
                      keyboardType="default"
                      onChangeText={(value) => this.validates(value, 'Country')}
                      onBlur={() => this.onBlurValidates('Country')}
                      style={styles.labelName}
                      error={this.state.signUpCountry}
                    />

                  </View>
                  <View style={styles.itemView}>

                    <TextField
                      label='Mobile Phone'
                      keyboardType="numeric"
                      maxLength={10}
                      minLength={10}
                      onChangeText={(value) => this.validates(value, 'Mobile')}
                      onBlur={() => this.onBlurValidates('Mobile')}
                      style={styles.labelName}
                      error={this.state.signUpMobile}
                    />

                  </View>

                </View>
              </View>
            </View>
          
        </ScrollView>
        <KeyboardSpacer topSpacing ={15} />
        <View style={styles.cartButtonView} >
          <Button style={[styles.cartButton, this.state.isButtonDisabled ? styles.buttonDisabled : '']} onPress={this.formsubmit} disabled={this.state.isButtonDisabled} >
            <Image style={styles.imageStyle} source={require('../assets/images/Shape.png')} />
          </Button>
        </View>
      </Container>
    )
  }

};

SignUp.navigationOptions = {
  title: 'Sign Up',
  headerStyle: {
    backgroundColor: '#0099EF',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{
        flex: 1,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),
      }}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign: "center",
    flex: 1,
    fontSize: 20,
    lineHeight: 25,
    paddingRight: 42,
    ...Platform.select({
      ios: {
        fontFamily: 'SFProDisplayRegular'
      },
      android: {
        fontFamily: 'RobotoRegular_1'
      },
    }),

  },
};

const styles = StyleSheet.create({
  AvatarContainer: {
    justifyContent: 'center',
    alignItems: 'center',

  },
  Avatar: {
    backgroundColor: '#0099EF',
    borderRadius: 48,
    width: 94,
    height: 94,
  },
  mainContainer: {
    backgroundColor: '#ffffff',
    flex: 1,
    justifyContent: 'center',
    paddingRight: 10,
    paddingLeft: 10,
    marginBottom: 40,
    paddingTop: 10
  },
  container: {
    flex: 1,
    paddingTop: 15,
    //paddingBottom: 80,
    backgroundColor: '#ffffff',
  },
  profileImage: {
    width: 95,
    height: 95,
    borderRadius: 50,
    alignSelf: 'center',
    position: 'absolute',
    top: -50
  },

  title: {
    backgroundColor: '#EEEEEE',
    color: 'rgba(0, 0, 0, 0.87)',
    fontSize: 13,
    lineHeight: 18,
    fontWeight: 'normal',
    width: '100%',
    textAlign: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    ...Platform.select({
      ios: {
        fontFamily: 'SFProTextRegular',
        letterSpacing: -0.08
      },
      android: {
        fontFamily: 'RobotoRegular_1',
      },
    }),
  },

  text: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    flex: 0.8,
    margin: 0
  },
  buttonStyle: {
    backgroundColor: 'transparent',
    borderRadius: 4,
    borderWidth: 1,
    flex: 0.8,
    width: '80%',
    marginLeft: '10%',
    borderColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButtonText: {
    color: '#000000',
    fontSize: 12
  },
  content: {
    textAlign: 'center',
    alignItems: 'center',
    margin: 0,
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },

  notification: {
    flex: 1,
    flexDirection: 'row',
  },
  firstColumn: {
    flex: 0.6
  },
  secondColumn: {
    flex: 0.4,
    height: 90,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  thirdColumn: {
    flex: 1,
  },
  birthday: {
    flex: 0.33,
    marginBottom: 10
  },
  input: {
    backgroundColor: '#ffffff',
    width: '100%',
    paddingLeft: 0,
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.12)',
  },

  item: {
    maxHeight: 55,
    width: '100%',
    borderBottomWidth: 0
  },

  itemView: {
    width: '100%',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },

  cartButtonView: {
    position: "absolute",
    bottom: 18,
    right: 18,
    borderRadius: 33,
    width: 66,
    height: 66,
  },

  cartButton: {
    alignItems: 'center',
    width: 57,
    height: 57,
    borderRadius: 33,
    backgroundColor: '#0099EF',
    color: '#ffffff',
    alignSelf: 'flex-end',
    zIndex: 11,
    elevation: 10
  },
  buttonDisabled: {
    backgroundColor: '#cccccc',
  },

  imageStyle: {
    position: "relative",
    left: 20,
    top: 1,
    alignSelf: 'center',
    width: 21,
    height: 21
  },
  error: {
    borderBottomColor: 'red',
    borderBottomWidth: 2
  },

  labelName: {
    fontSize: 17,
    lineHeight: 22,
    color: "rgba(0, 0, 0, 0.87)",
    ...Platform.select({
      ios: {
        fontFamily: 'SFProTextRegular',
        letterSpacing: -0.41
      },
      android: {
        fontFamily: 'RobotoRegular_1',
      },
    }),

  },

});
export default SignUp;
