import React from 'react';
import { Container, Form, Item, Button, Label} from 'native-base';
import  { View, Image, StyleSheet, Platform, Animated, Text , TouchableOpacity, KeyboardAvoidingView, ScrollView, AsyncStorage} from 'react-native';
import Spacer from '../components/UI/Spacer';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextField } from 'react-native-material-textfield';
import { LinearGradient } from 'expo-linear-gradient';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

import DatePicker from 'react-native-datepicker'

import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
const name = /^[a-zA-Z]+(?:(?:|['_\. ])([a-zA-Z]*(\.\s)?[a-zA-Z])+)*$/;

const special = ['Zero','first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth'];
const deca = ['twent', 'thirt', 'fort', 'fift', 'sixt', 'sevent', 'eight', 'ninet'];

let updatedFieldsData = []

class AccountChild extends React.Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    let currentDate = new Date();
    childData = [],
    updatedFieldsData = [],

    this.config = {
      apiUrl: "https://konnect.myfirstgym.com/api/customer/my-account"
    }

    childinfo = {
      firstname:'',
      firstnameValidator:true,
      lastname:'',
      lastnameValidator:true,
      school:'',
      schoolValidator:true,
      gender:'',
      birthDate: "",
      currentDate: currentDate,
      imageId: this.uniqueId(),
      imageSelected: false,
    }
    
    childErrorsDatas = []
    childErrorsDataBoth = []

    childErrorsData = [{
      schoolError: "",
      //schoolData: ""
    }]

    this.state =  {
      addMoreChilderns: false,
      childsinfo:[],
      childinfo:childinfo,
      childCount:1,
      childCountCheck:1,
      disabled: false,
      isLoading:true,
      isButtonDisabled :true,
      errormessage:'',
      errorshow:false,
      error:false,
      month:'Month',
      day:'Day',
      year:'Year',
      schoolNew: true,
      schoolValue: " ",
      MedicalConditionNew:true,
      MedicalConditionValue: " ",
      errorNumber: 0,
      isButtonDisabled: false,
      childErrorsDataState: ''
    },
    this.animatedValue = new Animated.Value(0);
    this.onchangeText = this.onchangeText.bind(this);
    this.validates = this.validates.bind(this);
    this.formsubmit = this.formsubmit.bind(this);
    this.stringifyNumber= this.stringifyNumber.bind(this);
  };
  
  stringifyNumber=(n) =>{
    if (n < 20) return special[n];
    if (n%10 === 0) return deca[Math.floor(n/10)-2] + 'ieth';
    return deca[Math.floor(n/10)-2] + 'y-' + special[n%10];
  }

  IncrementChilds = () => {

      //childData.push(this.state.childinfo)
      this.setState({childCount: this.state.childCount+1, isButtonDisabled: true, addMoreChilderns: true, childinfo:childinfo});

  }

  onchangeText = (type, text, index) =>{
    if(type==='school'){
      childErrorsDataBoth[index]['schoolData'] = text
      this.setState({schoolNew: false, schoolValue: text})
    }else if(type==='MedicalCondition'){
        this.setState({MedicalConditionNew: false, MedicalConditionValue: text})
    }
  }

  validates = (typeData) =>{
    
    
    let splitedType = typeData.split("-")
    let chidID = splitedType[0]
    let errorNumberType = splitedType[1]

    if (childErrorsDataBoth[errorNumberType]['schoolData'] == "" || childErrorsDataBoth[errorNumberType]['schoolData'] == null) {

      childErrorsDataBoth[errorNumberType]['schoolError'] = "School can not be blank."
      this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
      
    }else if(childErrorsDataBoth[errorNumberType]['schoolData'].trim(' ').length < 3){
      
      childErrorsDataBoth[errorNumberType]['schoolError'] = "Enter minimum 3 characters."
      this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })

    }else{
      childErrorsDataBoth[errorNumberType]['schoolError'] = ""
      this.setState({ isButtonDisabled: false, errorNumber: errorNumberType })
    }

    //childErrorsData[errorNumberType]['schoolError'] = childErrorsDatas[0][errorNumberType]['schoolError'];


    console.log(errorNumberType, " DATA - ",childErrorsDataBoth )


    var updatedFieldss =  {
      "Id": chidID,
      "School": this.state.schoolValue,
      "MedicalCondition": this.state.MedicalConditionValue
    }
    updatedFieldsData.push(updatedFieldss)
  }

  async updateUserProfile(token) {
    console.log(updatedFieldsData);
    let responseJson = [];
    let self = this;
    let profileDatas = this.params.profileData
    let updatedFieldsDatas = updatedFieldsData

   // console.log("before paased", profileDatas );
   // console.log(" Childs - ", updatedFieldsDatas)

    
    let response = await fetch(this.config.apiUrl, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer 12345',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({

        "api_token": token,
        "Address": profileDatas.address,
        "City": profileDatas.city,
        "Country": profileDatas.country,
        "Mobile": profileDatas.mobile,
        "ChildInfo": updatedFieldsDatas

      }),
    })

    try {

      responseJson = await response.json();
      this.props.navigation.navigate('Profile', {childData:childData});
     // console.log("Response => ",responseJson);

    }catch (error) {
      alert(error);
      console.log("Response error - ",error);
    }

  }


  formsubmit = async () => {

    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.updateUserProfile(token);
      }
    });

  }

      
  // USING FUNCTION TO ASK PERMISSION FROM USER
  _checkPermissions = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    //  console.log("Camera status - ", status);
    this.setState({ camera: status });

    // take permission for Gallery, ask a CameraRoll
    const { statusRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    //  console.log("cameraRoll - ", statusRoll);
    this.setState({  cameraRoll: statusRoll   });
  };

  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1); // substring return all chars except char at index=0;
  };

  uniqueId = () => {
    // create uniqueId for image as Alphabetical
    return (
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4()
    );
  };

  // USING FUNCTION TO CALL PERMISSION WITH DIRECTORY TO CHOOSE IMAGES
  findNewImage = async () => {

    this._checkPermissions();

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: "Images",
      allowsEditing: true,
      quality: 1
    });

    if (!result.cancelled) {
      this.setState({
        imageSelected: true,
        imageId: this.uniqueId(),
        uri: result.uri
      });
    } else {
      this.setState({
        imageSelected: false
      });
    }

  };


  //FETCHING CHILDERNS DETAILS
  async fetchMyAccount(token) {
    let countChilds = 0;
    let allChildsInArr = []

    try {

        let response = await fetch(this.config.apiUrl+"?api_token="+token);
        let responseJson = [];
        try {
            responseJson = await response.json();
            console.log(responseJson.data.children);
        } catch (error) {
          alert(error)
            console.log(error);
            responseJson = [];
        }
        this.setState({ childsinfo: responseJson.data.children });

        this.state.childsinfo.map((item, i) =>{
          countChilds = countChilds+1;
          childData.push(item)
          childErrorsDataBoth[i] = {
            "schoolData": item.school,
            "schoolError": ""
          }
          this.setState({ schoolValue: item.school, MedicalConditionValue: item.medical_condition})
        })

        childErrorsDatas.push(this.state.childsinfo.map((item, i) =>{
          return {
            "schoolData": item.school,
            "schoolError": ""
          }
        }))

        this.setState({ childCountCheck: countChilds, childCount: countChilds, isButtonDisabled:false})

    } catch (error) {
      alert(error)
        console.log(error);
    }

  }

  componentDidMount(){
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
          this.fetchMyAccount(token);
      }
    });

    this.setState({isLoading:false});
  }

    
render(){

//  console.log(this.params.userdata);
var chilDataView=[];
var getChilddata=[];
  
    let gender = [
      { value: 'Male' },
      { value: 'Female' }
    ];
    let relation = [
      { value: 'Mother' },
      { value: 'Father' },
    ];
    let month = [
      { value: 'Jan' },
      { value: 'Feb' },
      { value: 'Mar' },
      { value: 'Apr' },
      { value: 'May' },
      { value: 'Jun' },
      { value: 'Jul' },
      { value: 'Aug' },
      { value: 'Sept' },
      { value: 'Oct' },
      { value: 'Nov' },
      { value: 'Dec' },
    ];
    let day = [];
    for (let i = 1; i < 32; i++) {
        day.push({
            value: i,
        });
    }
    let year = [];
    let currentYear = new Date().getFullYear();

    for (let i = currentYear - 50; i <= currentYear; i++) {
        year.push({
            value: i,
        });
    }

  const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  
  if( this.state.childCount > 0 ){

    this.state.childsinfo.map((item, i) =>{
      
      if(item.medical_condition == null){
        item.medical_condition = " ";
      }
      if(item.school == null){
        item.school = " ";
      }

      //console.log(i , "Data ", childErrorsDataBoth[i]['schoolError'] )

      getChilddata.push(

        <View key = {i}>
          {
            (i===(this.state.childCount-1))?
              (this.state.error)?
                (<Text style = {[ styles.fontFamily, {alignSelf:'center'}]}>{errormessage}</Text>):
                (<Text> </Text>)
                :
                (<Text> </Text>)
          }
              
        <Spacer size={6} />
        <Text style = {[ styles.fontFamily, styles.title]}>{ (this.state.childCount===1)? ( ' CHILD INFO'):((this.stringifyNumber(i+1)) +' CHILD INFO') }</Text>
        <View style ={styles.content}> 
        <Spacer size={12} />
          <View style={styles.notification}>
            <View style = {styles.firstColumn}>
              <TextField
                style = {styles.fontFamily}
                label='First Name'
                editable={false}
                //onChangeText={text => this.validates(text, 'firstname')}
                style={ styles.labelName }
                value={item.first_name}
              />
            </View>
            <View style = {styles.secondColumn}>
                {/* USING FOLLOWING CONDITION TO SHOW SELECTED IMAGE OR BUTTON TO CHOOSE */}
                {this.state.childinfo.imageSelected == true ? (
              //USING CODE TO DISPLAY IMAGE
                <View>
                  <TouchableOpacity /* onPress={this.findNewImage} */  underlayColor='tranaparent'>
                    <View style={[ styles.avatar, styles.avatarContainer, { marginBottom: 20 }, ]} >
                      <Image source={{ uri: this.state.uri }} style={styles.avatar} />
                    </View>
                  </TouchableOpacity>
                </View>
                ) : (
                <View>
                  {/* <TouchableOpacity onPress={this.findNewImage}> */}
                  <TouchableOpacity>
                    <View style={[ styles.avatar, styles.avatarContainer, { marginBottom: 20 }, ]} >
                      {(item.avatar !== "")?
                        (
                          <Image style={{backgroundColor:'#0099EF',borderRadius: 48,width: 96,height: 96}} source={{uri:item.avatar}}/>
                        ):(
                          <Image style={{width:15,height:15}} source={require('../assets/images/edit.png')}/>
                        )
                      }

                    </View>
                  </TouchableOpacity>
                </View>
              )}
              {/* CONDITION CLOSED */}
            </View>
          </View>
          <View style={styles.notification}>
            <View style = {{ flex: 1, marginTop: -20}}>
                <TextField
                  style ={styles.fontFamily}
                  label='Last Name'    
                  editable={false}                   
                  //onChangeText={text => this.validates(text, 'lastName')}
                  style={ styles.labelName }
                  value={item.last_name}
                />
            </View>
          </View>
          <View style = {styles.itemView}>
            <Item stackedLabel style={{borderWidth:1, borderColor:'#999999'}}>
              <Dropdown containerStyle={[styles.input]}
                label='Gender'
                textColor='#000000'
                baseColor='rgba(0, 0, 0, 0.54)'
                inputContainerStyle={{ borderBottomColor: 'transparent' }}
                labelFontSize={17}
                // data={gender}
                style={ styles.labelName }
                value={ item.gender }
                //value={(item.gender !== "" && this.state.childinfo.gender == "") ? item.gender : this.state.childinfo.gender}                
                //onChangeText={text => this.validates(text, 'gender')}
              />
            </Item>
          </View>
          <Spacer size={20} />
          
          <View style={styles.notification}>
            <View style = {styles.itemView}>
              <Label style={[ styles.labelName, styles.fontLineHeight, { width: "100%", alignSelf: "flex-start", color: "#575757", fontSize: 14 }]}>Birthday </Label>
                <DatePicker
                  style={[ {width: "100%", borderWidth: 0, backgroundColor:'transparent'}]}
                  date={ item.birth_date }
                  //date={(item.birth_date !== "" && this.state.childinfo.birthDate == "")? item.birth_date : this.state.childinfo.birthDate}
                  mode="date"
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  maxDate= {this.state.childinfo.currentDate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  disabled={true}
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: 0,
                      top: 4,
                    },
                    dateInput: {
                      borderTopWidth: 0,
                      borderLeftWidth: 0,
                      borderRightWidth: 0,
                      borderBottomWidth: 1,
                      textAlign: "left"
                    }
                  }}
                  //onDateChange={(date) => this.validates(date, 'birthday')}
                />
            </View>
          </View>

          <View style = {styles.itemView}>
              <TextField
                style = {styles.fontFamily}
                  label='School'
                  style={ styles.labelName }
                  value={(this.state.schoolNew)?(item.school):this.value}
                  onChangeText={(value) => this.onchangeText('school', value, i)} 
                  onBlur={ () => this.validates(item.id+"-"+i)}
                  //error={(this.state.errorNumber == i)?childErrorsDataBoth[i]['schoolError']:""}
              />
          </View>

          <View style = {styles.itemView}>
          <TextField
                  style ={styles.fontFamily}
                  label='Medical Condition'                  
                  onChangeText={(value) => this.onchangeText('MedicalCondition', value, i)} 
                  onBlur={ () => this.validates(item.id+"-"+i)}
                  style={ styles.labelName }
                  value={(this.state.MedicalConditionNew)?(item.medical_condition):this.value}
                />
          </View>
        </View>
      </View>
      )
    })
  }else{
    this.formsubmit()
  }

return(
  <Container>
      
      <ScrollView keyboardDismissMode='on-drag'>
      <View style ={styles.mainContainer}>
         {(!this.state.isLoading)?
         (
            <View style = {{flex:1}}>

              <KeyboardAvoidingView behavior="padding">
                <View>
                  {getChilddata}
                  {/* {chilDataView} */}
                </View>
              </KeyboardAvoidingView>

              <View>
              
              </View>

            </View>

         ):
         (
          <View style ={styles.content}> 
             <View style={styles.loading}>
                <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
              </View>
          </View>
         )
         }
      </View>
    </ScrollView>

    {(!this.state.isLoading)
        ?(
          <View style = { styles.cartButtonView } >
          <Button style = {[ styles.cartButton ] } onPress={this.formsubmit} disabled={this.state.isButtonDisabled}>
              <Image  style = {styles.imageStyle} source = {require('../assets/images/Shape.png')} />
          </Button>
        </View>
        ):
        (
          <View style={styles.loading}>
            <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
          </View>
        )
      }

      
    </Container>
    
    )
  } 

}
AccountChild.navigationOptions = {
  title: 'My Account',
    headerStyle: {
      backgroundColor: '#ffffff',
      borderBottomLeftRadius: 15, 
      borderBottomRightRadius: 15,
      height:60,
      borderBottomWidth: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          
          ...Platform.select({
            ios:{
              borderRadius: 15,
              marginTop:-15
            }
          }),
          
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      paddingRight: 42,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };

  const styles = StyleSheet.create({
    fontFamily:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    fontFamilyDisplay:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      opacity: 0.5,
      backgroundColor: 'black',
      justifyContent: 'center',
      alignItems: 'center'
  },
   
    spinnerTextStyle: {
      color: '#FFF',
    },
    avatarContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      
    },
    buttonDisabled:{
      backgroundColor:'#cccccc',
     },
    avatar: {
      backgroundColor:'#0099EF',
      borderRadius: 48,
      width: 94,
      height: 94,
    },
    mainContainer:{
      backgroundColor:'#ffffff',
      flex:1, 
      justifyContent: 'center',
      paddingRight:0,
      paddingLeft:0,
      marginBottom: 80
    },
    container: {
      flex: 1,
      paddingTop: 15,
      paddingBottom: 50,
      backgroundColor: '#ffffff',
    },
    profileImage:{
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center',
      position:'absolute',
      top:-50
    },
    title:{
      backgroundColor:'#EEEEEE',
      color:'rgba(0, 0, 0, 0.87)',
      fontSize:13,
      lineHeight:18,
      fontWeight:'normal',
      width:'100%',
      textAlign:'center',
      paddingTop:5,
      paddingBottom:5,
      textTransform: 'uppercase',
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular',
        },
        android: {
          fontFamily:'RobotoRegular_1',
        },
      }),
    },
    text:{
        fontWeight:'bold',
          fontSize:16,
          textAlign:'center',
            flex:0.8,
          margin:0
    },
    buttonStyle:{
      backgroundColor:'transparent',
      borderRadius:4,
      borderWidth:1,
      flex:0.8,
      width:'80%',
      marginLeft:'10%',
      borderColor:'#000000',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginButtonText:{
      color:'#000000',
      fontSize:12
    },
    content:{
      textAlign:'center',
      alignItems:'center',
      margin:0,
      flex:1,
      paddingLeft:25,
      paddingRight:25,
      paddingTop:15,
    },

    notification:{
      flex:1,
      flexDirection:'row',
    },
    firstColumn:{
      flex:0.6
    },
    secondColumn:{
      flex:0.4,
      height: 90,
      textAlign:'center',
      alignItems:'center',
      justifyContent:'center',
    },
    birthday:{
      flex:0.33,
      marginBottom:10
    },
    input:{
      backgroundColor:'#ffffff', 
      width:'100%',
      paddingLeft:0,
      borderBottomWidth:1,
      borderColor:'rgba(0, 0, 0, 0.12)',
    },
    item:{
      maxHeight:55,
      width:'100%',
      borderBottomWidth:0
    },
    itemView:{
      width:'100%',
    },
    headingView:{
      flexDirection:'row', 
      flexWrap:'wrap', 
      alignContent:'center',
      alignItems:'center',
      marginTop:10,
      flex:1,
      marginBottom:10,
      marginRight: 15
    },
    textHeading:{
      width:'100%',
      textAlign:'center',
      fontSize:17,
      lineHeight:22,
      alignContent:'center',
      alignSelf:'center',
      margin:0,
      position:'relative',
      color:'rgba(0, 0, 0, 0.87)'
    },
    tabBarInfoContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { width: 0, height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      alignItems: 'center',
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
    tabBarInfoText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      textAlign: 'center',
    },
    navigationFilename: {
      marginTop: 5,
    },

    cartButtonView: {
      position: "absolute",
      bottom: 18,
      right: 18,
      borderRadius:33,
      width:57,
      height:57,
    },
   
    cartButton:{
      alignItems:'center',
      width:57,
      height:57, 
      borderRadius:33,
      backgroundColor:'#0099EF',
      color:'#ffffff',
      alignSelf:'flex-end',
      zIndex: 11,
      elevation: 10
    },
  
    imageStyle:{
        position: "relative",
        left: 20,
        top: 1,
        alignSelf:'center', 
        width:18,
        height:18
    },

    labelName: {
      fontSize:17,
      lineHeight:22,
      color: "rgba(0, 0, 0, 0.87)",
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular',
          letterSpacing:-0.41
        },
        android: {
          fontFamily:'RobotoRegular_1',
        },
      }),
    },

  });
export default AccountChild;
