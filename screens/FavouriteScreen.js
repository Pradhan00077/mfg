import React, { Component } from 'react';
import { View, Image, TouchableHighlight, StyleSheet, Platform, ScrollView , AsyncStorage, RefreshControl } from 'react-native';
import { Container, Content,  TabHeading, Tab, Tabs, ScrollableTab, Text, Button } from 'native-base';
import Spacer from '../components/UI/Hr';

import ActivityIndicator from 'react-native-loading-spinner-overlay';
import { LinearGradient } from 'expo-linear-gradient';
export default class FavoriteScreen extends Component {

  constructor (props){
    super(props);
    this.navigate = this.props.navigation;
    this.params = this.props.navigation.state.params;
    this.state = {
      Fav:[],
      isLoading:true,
      hastoken:false,
      refreshing: false,
    };
  }

  
  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}

async fetchFavourite(api_token) {
  try {
      let response = await fetch('https://konnect.myfirstgym.com/api/customer/favourites?api_token='+api_token);
      let responseJson = [];
      try {
          responseJson = await response.json();
      } catch (error) {
          console.log(error);
          responseJson = [];
      }
      this.setState({ Fav: responseJson.data, isLoading:false, hastoken:true });
  } catch (error) {
    alert(error);
      console.log(error);
  }
}
componentDidMount() {
  AsyncStorage.getItem('api_token').then((token) => {
    if(token!==null){
      this.fetchFavourite(token); 
    }else{
      this.setState({isLoading:false,});
    }
  });
}

serviceItems = (itemID, itemName) =>{
  this.navigate.navigate('FavServicesItems', {serviceCatId:itemID, serviceCatName:itemName} )
}

_onRefresh = () => {
  this.setState({refreshing: true});
  
  AsyncStorage.getItem('api_token').then((token) => {
    if(token!==null){
      this.fetchFavourite(token).then(() => {
        this.setState({refreshing: false});
      });
    }else{
      this.setState({isLoading:false,});
    }
  });

}

  render(){
    let items=0
    datavalue =[],
    
     datavalue =  this.state.Fav.map((item, i) =>{
      items=items+1;

       return(
        <View  key = {item.id}>
          
          <TouchableHighlight style ={styles.mainEventsClass} underlayColor="white" onPress={() => this.serviceItems(item.id, item.name)} >

          <View style={{flexDirection:'row',flex:1, backgroundColor: "#FFFFFF",paddingTop:15,paddingBottom:15, borderTopRightRadius: 4, borderBottomRightRadius: 4}}>

            {/* <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.20, alignSelf:'center'}}>
                  <Text style={[styles.fontFamily, {textAlign:'center',lineHeight:20,  fontSize:14, color:'rgba(0, 0, 0, 0.87)', paddingLeft:3, paddingRight:3}]} >09:00AM</Text>
            </View> */}

            <View style ={{flex:0.74, paddingRight: 2 }}>
              <View style = {{paddingLeft:10}}>
                <Text style ={[styles.fontFamily, {fontSize:16, lineHeight:21, color:'rgba(0, 0, 0, 0.87)'}]}>{item.name}</Text>
                <Text style ={[styles.fontFamily, {fontSize:13,lineHeight:18, color:'rgba(0, 0, 0, 0.54)'}]} >{item.service_category.name}</Text>
              </View>
            </View>
            <View style ={{ flex:0.20, justifyContent:'center', alignItems: 'center'}}>
                <View style ={{flex:1, width:'100%', alignSelf:'center' , textAlign:'center', maxHeight:24, flex:1, justifyContent:'center', backgroundColor:'#25AE88', borderRadius:12,}}>
                    <Text style={[styles.fontFamily, styles.ageGroupbtn, { alignSelf:'center'}]}>{item.age_group}</Text>
                </View>
            </View>
            
            <View style ={{ flex:0.06, justifyContent:'center'}}>
              <View style ={{alignSelf:'center', justifyContent:'center', marginRight:5}}>
                <Image style={{width:8, height:'100%', resizeMode:'contain'}} source = {require('../assets/images/right_events.png')}/>
              </View>
            </View>

          </View>
        </TouchableHighlight>
      </View>
         )
        });

      if(this.state.hastoken){
        if(this.state.isLoading){
          return(
            <ScrollView style={styles.container}>
            <View style ={styles.mainContainer}>
            <View style={styles.loading}>
                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
                </View>
            </View>
        </ScrollView>
          )
        }else{
          
          return(
            
            <ScrollView style={styles.container}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
                    <View style ={styles.mainContainer12}>
                      
                        {
                          (items===0)?
                          (<View style={[styles.container, {flex:1, justifyContent:'center', flexDirection:'column'}]}>
                            <View style ={styles.mainContainer}><View style ={styles.content}><Text style ={[styles.fontFamily, styles.sorrymsg ]}>Sorry, You don't have any favourite items.</Text></View></View>
                          </View>):
                          (<View>{ datavalue }</View>)
                        }
                        
                      
                    </View>
                </ScrollView>
          )
        }
      }else if(!this.state.isLoading){
          //     this.fetchFavourite();
          return(
            <Container style={{flex:1,flexDirection:'row', alignItems:'center',justifyContent:'center',}}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
              <Content  >
                <View style ={[styles.content]}> 
                    <Text style = {[styles.fontFamilyDisplay, styles.maintext ]}>All your fitness favourites.</Text>
                    <Spacer size={10} />
                    <Text style={[styles.fontFamily, styles.subtext]}>View your go-to classes and book your next workout, instantly</Text>
                    <Spacer size={20} />
                    <Button block style ={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Home', {replace:false})} >
                      <Text uppercase={false} style={[styles.fontFamily, styles.loginButtonText]}> Login </Text>
                    </Button>
                </View>
              </Content>
            </Container>
          )

      }else{
        return(
          <ScrollView style={styles.container}>
            <View style ={styles.mainContainer}>
              <View style={styles.loading}>
                <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
              </View>
            </View>
          </ScrollView>
        )
      }
   }
  }

  FavoriteScreen.navigationOptions = {
    headerTitle:(
      <View style ={{flex:1}}><Text style = {{textAlign:'center', fontSize:20,lineHeight:25, alignSelf:'center', color:'#ffffff',...Platform.select({ios: { fontFamily:'SFProDisplayRegular' },  android: {  fontFamily:'RobotoRegular_1' }, }), }}>My Favourites</Text></View>
    ),
    headerStyle: {
      backgroundColor: '#E5E5E5',
      height:60,
      elevation: 0,
      borderBottomWidth: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ 
          flex: 1,
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          elevation: 5,
          ...Platform.select({
            ios:{
              borderRadius: 15,
              //height:130,
              marginTop:-15
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };



  const styles = StyleSheet.create({
    fontFamily:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    fontFamilyDisplay:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    ageGroupbtn:{
      marginLeft:0,
      marginRight:0,
      fontSize:10,
      lineHeight:25, 
      textAlign:'right', 
      color: '#ffffff'
    },
    sorrymsg:{
      alignSelf:'center',
      textAlign:'center', 
      flex:1, 
      margin:0, 
      fontSize:16,
      lineHeight:21,
      paddingBottom: 10, 
      paddingRight: 20, 
      paddingLeft: 20, 
      color:'rgba(0, 0, 0, 0.54)',
    },
    maintext:{
      fontWeight:'normal',
      fontStyle:'normal', 
      fontSize:20,
      lineHeight:25, 
      textAlign:'center', 
      flex:1, 
      margin:0,
      color:'rgba(0, 0, 0, 0.87)'
    },
    subtext:{
      textAlign:'center', 
      flex:1, 
      margin:0, 
      fontSize:16,
      lineHeight:21,
      paddingBottom: 10, 
      paddingRight: 20, 
      paddingLeft: 20, 
      color:'rgba(0, 0, 0, 0.54)',
    },

    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 0.5,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    
    spinnerTextStyle: {
      color: '#FFF',
    },
    
    mainContainer12: {
      backgroundColor:'#E5E5E5',
      flex:1, 
      justifyContent: 'center',
      paddingRight:15,
      paddingLeft:15,
      marginTop: 5,
      marginBottom: 10,
    },

    mainContainer:{
      backgroundColor:'#E5E5E5',
      flex:1, 
      justifyContent: 'center',
      paddingRight:15,
      paddingLeft:15,
      marginBottom: 30,
    },
    container: {
      flex: 1,
      paddingTop: 15,
      paddingBottom: 50,
      backgroundColor: '#E5E5E5',
    },
    mainEventsClass:{
      marginTop:5,
      marginBottom:5, 
      // paddingTop:15, 
      // paddingBottom:15, 
      borderColor:'#146FA9',
      borderLeftWidth:4,
      borderRadius:4, 
      shadowColor: 'rgba(0, 0, 0, 0.25)',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 1,
      elevation: 1,
      ...Platform.select({
        ios:{
          shadowColor:'#000000',
          shadowOffset:{width:0,height:1},
          shadowOpacity:0.5,
          shadowRadius:2
        }
      })
    },
    buttonStyle:{
      backgroundColor:'transparent',
      borderRadius:4,
      borderWidth:1,
      flex:0.8,
      width:'90%',
      alignSelf:'center',
      borderColor:'rgba(0, 0, 0, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      elevation:0
    },
    loginButtonText:{
      color:'rgba(0, 0, 0, 0.87)',
      fontSize:17,
      lineHeight:22,
      fontStyle:'normal',
      fontWeight:'normal',
    },
    content:{
      textAlign:'center',
      alignItems:'center',
      margin:0,
      flex:1,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  });
  