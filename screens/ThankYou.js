import React from 'react';
import {  View, StyleSheet, ScrollView, Platform, Text  } from 'react-native';
import { Button} from 'native-base';
import Spacer from '../components/UI/Hr';
import { LinearGradient } from 'expo-linear-gradient';
class ThankYou extends React.Component {
  
  constructor (props){
    super(props);
    this.state = {
      LoopNo:10
    };
  }

  render(){
    return(
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ 
          flex: 1,
          height:'100%'
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      >
      <ScrollView style={styles.container}>
        <View style ={styles.mainContainer}>
           <View style ={styles.content}> 
                  <Text style = {[ styles.textFont, {fontWeight:'normal', fontSize:16, textAlign:'center', flex:0.8, margin:0, marginBottom:30, color:'#ffffff'}]}>Thank you for buying our Gym classes</Text>
                 <Button block style ={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Profile')}>
                    <Text style={styles.loginButtonText}> Back to Profile </Text>
                 </Button>
             </View>
          </View>
        </ScrollView>
        </LinearGradient>
      )
    }
  }

  ThankYou.navigationOptions = {
    title:'Thank You',
    headerLeft: null,
    headerRight: "",
    headerStyle: {
      backgroundColor: '#00D2EF',
      height:60,
      borderBottomWidth: 0,
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ 
          flex: 1,
          ...Platform.select({
            ios:{
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };
const styles = StyleSheet.create({
  mainContainer:{
    flex:1, 
    justifyContent: 'center',
    paddingRight:15,
    paddingLeft:15,
    marginBottom: 30,
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
  },
  buttonStyle:{
    backgroundColor:'#ffffff',
    borderRadius:4,
    elevation:0
  },
  loginButtonText:{
    color:'#000000',
    fontSize:12
  },
  loginButtonText:{
    color:'#00A6EF',
    fontSize:20,
    lineHeight:25,
    fontStyle:'normal',
    fontWeight:'normal',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        //latterSpacing:0.38,
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  content:{
    textAlign:'center',
    alignItems:'center',
    margin:0,
    flex:1,    
    marginTop: 140
},
tabBarInfoContainer: {
  position: 'absolute',
  bottom: 0,
  left: 0,
  right: 0,
  ...Platform.select({
    ios: {
      shadowColor: 'black',
      shadowOffset: { width: 0, height: -3 },
      shadowOpacity: 0.1,
      shadowRadius: 3,
    },
    android: {
      elevation: 20,
    },
  }),
  alignItems: 'center',
  backgroundColor: '#fbfbfb',
  paddingVertical: 20,
},
tabBarInfoText: {
  fontSize: 17,
  color: 'rgba(96,100,109, 1)',
  textAlign: 'center',
},
navigationFilename: {
  marginTop: 5,
},
textFont: {
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
}

});
 export default ThankYou;
