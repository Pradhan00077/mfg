import React, { Component } from 'react';
import {TouchableOpacity, View, Image, TouchableHighlight, StyleSheet, Platform, ScrollView } from 'react-native';
import {  Text } from 'native-base';
import Spacer from '../components/UI/Hr';

import axios from 'axios';
import ActivityIndicator from 'react-native-loading-spinner-overlay';
import { LinearGradient } from 'expo-linear-gradient';

export default class ExploreServicesScreen extends Component {

  constructor (props){
    super(props);
    this.navigate = this.props.navigation;
    this.params = this.props.navigation.state.params;
    this.exploreServiceCatID = this.navigate.state.params.serviceCatId;
    this.config = {
        ListItemsByServicesCatAPI:'https://konnect.myfirstgym.com/api/schedules/filter'
    }
    this.state = {
      services:[],
      isLoading:true,
    };
  }

  
  fetchExploreByServicesId = async(service_id) =>  {
    let self = this;
      let response = axios.get(this.config.ListItemsByServicesCatAPI+"?service_category_id="+service_id)
      .then(function(response){
        self.setState({services:response.data.data, isLoading:false})
      })
      .catch(function (error){
        alert("Explore API Error...")
        console.log(error);
      });
  }
  
  componentDidMount() {
    //initialize your function
    this.fetchExploreByServicesId(this.exploreServiceCatID);
  }


  render(){
    let items=0
    datavalue =[],

      Object.keys(this.state.services).map((item, i) =>{
      let dateLoop = true;
      items = items+1;

      datavalue.push(this.state.services[item].map((data, x) => {
       
        let date = null;
        if(dateLoop){
          date = item;
          dateLoop = false;
        }

        return(          
          <View  key = {x}>

            {(date !== null)?
              (
                <View style = {{ backgroundColor:'#ffffff', flex:1, marginTop: 2, marginBottom: 2 }}>
                  <Spacer size={18} />
                    <Text style = {[styles.fontfam, {color:'rgba(0, 0, 0, 0.87)', height:22, fontSize:17, lineHeight:22,  alignSelf:'center' }]}>{date}</Text>
                  <Spacer size={18} />
                </View>
              ):(
                <View></View>
              )
            }
            <View style = {{ paddingLeft:15, paddingRight:15, }} >
            <TouchableHighlight style ={styles.mainEventsClass} underlayColor="white" onPress={() => this.navigate.navigate('EventsDescription', {serviceItemId: data.schedule_id, name: data.service_item_name, schedule_date_id: data.schedule_date_id })} >
                  <View style={{flexDirection:'row',flex:1, backgroundColor: "#FFFFFF",paddingTop:15,paddingBottom:15, borderTopRightRadius: 4, borderBottomRightRadius: 4}}>
                    <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.20, alignSelf:'center'}}>
                      <Text style={[styles.fontFamily, {textAlign:'center',lineHeight:20,  fontSize:14, color:'rgba(0, 0, 0, 0.87)', paddingLeft:3, paddingRight:3}]} >{ data.start_time.toUpperCase()}</Text>
                    </View>
                    <View style ={{flex:0.54}}>
                      <View style = {{paddingLeft:10}}>
                        <Text style ={[styles.fontFamily, {fontSize:16, lineHeight:21, color:'rgba(0, 0, 0, 0.87)'}]}>{data.service_item_name}</Text>
                        <Text style ={[styles.fontFamily, {fontSize:13,lineHeight:18, color:'rgba(0, 0, 0, 0.54)'}]} >{data.service_category}</Text>
                      </View>
                    </View>
                    <View style ={{ flex:0.20, justifyContent:'center'}}>
                      {/* <View style ={{alignSelf:'flex-end', flex:1, justifyContent:'center'}}>
                        <Text style={[styles.fontFamily, styles.ageGroupbtn, {backgroundColor:'#25AE88', borderRadius:12, paddingLeft:8, paddingRight:8}]}>{ data.age_group }</Text>
                      </View> */}

                      <View style ={{flex:1, width:'100%', alignSelf:'center' , textAlign:'center', maxHeight:24, flex:1, justifyContent:'center', backgroundColor:'#25AE88', borderRadius:12,}}>
                        <Text style={[styles.fontFamily, styles.ageGroupbtn, { alignSelf:'center'}]}>{ data.age_group }</Text>
                      </View>

                    </View>
                    <View style ={{ flex:0.06, justifyContent:'center'}}>
                    <View style ={{alignSelf:'center', justifyContent:'center', marginRight:5}}>
                      <Image style={{width:8, height:'100%', resizeMode:'contain'}} source = {require('../assets/images/right_events.png')}/>
                    </View>
                   </View>
                  </View>
                </TouchableHighlight>
            </View>
          </View>              
        )
  

      }));

  
    });


      if(this.state.isLoading){
        return(
          <ScrollView style={styles.container}>
              <View style ={styles.mainContainer}>
                  <View style={styles.loading}>
                      <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
                  </View>
              </View>
          </ScrollView>
        )

      }else{
        return(
          (items === 0)?
          (
            <ScrollView style={styles.container}>
              <View style ={styles.mainContainer}>

                <View style={[styles.container, {flex:1, justifyContent:'center', flexDirection:'column'}]}>
                  <View style ={styles.mainContainer}>
                    <View style ={styles.content}>
                      <Text style ={[styles.fontfam, styles.sorrymsg ]}>
                        Sorry, You don't have any Service items.
                      </Text>
                    </View>
                  </View>
                </View>
            
              </View>
            </ScrollView>
          ):(
            <ScrollView style={[styles.container, { paddingTop: 8, }]}>
              <View style ={styles.mainContainer1}>
                {datavalue}
              </View>
            </ScrollView>
          )
          
        )
      }

   }

   static navigationOptions = ({navigation}) => ({
        headerTitle: navigation.state.params.serviceCatName,
        headerRight: (
          <View style={{ marginRight: 20 }} >
            <TouchableOpacity style={{ borderBottomColor: "rgba(0, 0, 0, 0)", paddingTop: 6}} activeOpacity={1} underlayColor="transparent" onPress = {() => navigation.push('LocationFilterScreen', {location_Id:'', item_Id:'', category_Id:navigation.state.params.serviceCatId, Category_Name:navigation.state.params.serviceCatName,  guestSchedule:false})}  >
                <Image style={{ width: 20, height: 20 }} source={require('../assets/images/filter.png')} />
            </TouchableOpacity>
          </View>
        ),
        headerStyle: {
          backgroundColor: '#E5E5E5',
          height:60,
          elevation: 0,
          borderBottomWidth: 0
        },
        headerBackground: (
          <LinearGradient
            colors={['#0099EF', '#00D2EF']}
            style={{ flex: 1, 
            borderBottomLeftRadius: 15, 
            borderBottomRightRadius: 15,
            elevation: 5,
            ...Platform.select({
              ios:{
                borderRadius: 15,
                marginTop:-15,
                shadowOffset:{width:0, height:4},
                shadowColor:'#000000',
                shadowOpacity:1.0,
                shadowRadius:15
              }
            }),
            }}
            start={{x: 0, y: 0}}
            end={{x: 1.40, y: 0}}
          />
        ),
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
        fontWeight: 'normal',
        alignSelf: 'center',
        textAlign:"center", 
        flex:1 ,
        fontSize:20,
        lineHeight:25,
        ...Platform.select({
            ios: {
            fontFamily:'SFProDisplayRegular',
            paddingRight:0
            },
            android: {
            fontFamily:'RobotoRegular_1'
            },
        }),
        },
    });

}


const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#E5E5E5',
    flex:1, 
    justifyContent: 'center',
    paddingRight:15,
    paddingLeft:15,
    marginBottom: 30,
  },
  mainContainer1:{
    backgroundColor:'#E5E5E5',
    flex:1, 
    justifyContent: 'center',
    marginBottom: 20,
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 100,
    backgroundColor: '#E5E5E5',
  },
  mainEventsClass:{
    marginTop:4,
    marginBottom:4, 
    borderLeftColor:'#146FA9',
    borderLeftWidth:4,
    borderRadius:4, 
    elevation: 1,
    justifyContent:'center',
    ...Platform.select({
      ios:{
        shadowColor:'#000000',
        shadowOffset:{width:0,height:1},
        shadowOpacity:0.5,
        shadowRadius:2
      }
    })
  },
  sorrymsg:{
    alignSelf:'center',
    textAlign:'center', 
    flex:1, 
    margin:0, 
    fontSize:16,
    lineHeight:21,
    paddingBottom: 10, 
    paddingRight: 20, 
    paddingLeft: 20, 
    color:'rgba(0, 0, 0, 0.54)',
  },
  ageGroupbtn:{
    marginLeft:0,
    marginRight:0,
    fontSize:10,
    lineHeight:25, 
    textAlign:'right', 
    color: '#ffffff'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
},
 
  spinnerTextStyle: {
    color: '#FFF',
  },
  buttonStyle:{
    backgroundColor:'transparent',
    borderRadius:4,
    borderWidth:1,
    flex:0.8,
    width:'90%',
    marginLeft:'5%',
    borderColor:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButtonText:{
    color:'#000000',
    fontSize:12
  },
  content:{
    textAlign:'center',
    alignItems:'center',
    margin:0,
    flex:1,
    marginTop: 140
},
fontfam:{
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},  
tabBarInfoContainer: {
  position: 'absolute',
  bottom: 0,
  left: 0,
  right: 0,
  ...Platform.select({
    ios: {
      shadowColor: 'black',
      shadowOffset: { width: 0, height: -3 },
      shadowOpacity: 0.1,
      shadowRadius: 3,
    },
    android: {
      elevation: 20,
    },
  }),
  alignItems: 'center',
  backgroundColor: '#fbfbfb',
  paddingVertical: 20,
},
tabBarInfoText: {
  fontSize: 17,
  color: 'rgba(96,100,109, 1)',
  textAlign: 'center',
},
navigationFilename: {
  marginTop: 5,
},
});