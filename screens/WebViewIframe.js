import React from 'react';
import {AsyncStorage, AppRegistry, ListView, TouchableWithoutFeedback,TouchableOpacity, View, StyleSheet , Image, ScrollView, Platform, TouchableHighlight,Text  } from 'react-native';
import { WebView } from 'react-native-webview';
import { LinearGradient } from 'expo-linear-gradient';

import { HeaderBackButton } from 'react-navigation'
// ...
class WebViewIframe extends React.Component {
    constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    //  datasource rerendered when change is made (used to set swipeout to active)
    console.log(this.params.payment_url);
    paymentStatus=()=>{
        self.props.navigation.navigate('PaymentStatus', { replace: true });
    }  
    }
  render() {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={{ paddingLeft : 15 }} onPress={ this.paymentStatus } >
                        <Text style={[styles.selectedDaysBtns,{textAlign:"left",color:"rgba(0, 0, 0, 0.87)"}]}>
                          Once payment done. You need to click Here
                        </Text>
                      </TouchableOpacity>
            <WebView source={{ uri: this.params.payment_url }} />
        </View>
    );
  }

  static navigationOptions = ({ navigation }) => {
    return ({
  title: 'Payment',
  headerStyle: {
    backgroundColor: '#E5E5E5',
    height:50,
    elevation: 0,
    borderBottomWidth: 0,
  },
  headerLeft:(<HeaderBackButton tintColor='#ffffff' onPress={() => { navigation.navigate('Profile');}}/>),
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        elevation: 5,
        backgroundColor: '#E5E5E5',

        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),

      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8 ,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 0.38
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
});

}

}

var styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor:  '#ff00ff'
   },webView :{
     height: 320,
     width : 200
   }
});
export default WebViewIframe;