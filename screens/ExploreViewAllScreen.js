import React, { Component }  from 'react';
import { View, StyleSheet, ScrollView, Platform,FlatList, ImageBackground, TouchableHighlight, Image } from 'react-native';
import { Text, Container } from 'native-base';
import { LinearGradient } from 'expo-linear-gradient';
import axios from 'axios';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

class ExploreViewAllScreen extends Component {
  constructor(props) {
    super(props);
    this.navigate = this.props.navigation;
    this.config = {
      ListExploreByServicesAPI:'https://konnect.myfirstgym.com/api/customer/services/',
      ListExploreByLocServicesAPI:'https://konnect.myfirstgym.com/api/customer/locations/nearby'
    }
    this.exploreServiceID = this.navigate.state.params.service_id;

    this.state = {

      searchedString: "",
      searchedData: [],
      exploreData: [],
      servicesName: "",      
      isLoading:true,
      latitude:24.46555020,
      longitude: 54.38153140,

    }
  }

GetGridViewItem (item) {
  
  if(this.navigate.state.params.service_name === "AllLocations"){
     this.navigate.navigate('Location', {id:item.location_id} )
  }else{
    this.navigate.navigate('ExploreServices', {serviceCatId:item.id, serviceCatName:item.name} )
  }

}

fetchExploreByServicesId = async(service_id) =>  {
  let self = this;
    let response = axios.get(this.config.ListExploreByServicesAPI+service_id)
    .then(function(response){
      self.setState({exploreData:response.data.items, servicesName:response.data.name, isLoading:false })
    })
    .catch(function (error){
      alert("Explore API Error...")
      console.log(error);
    });
}

async fetchExploreByServicesIdForLoc(data) {
  let responseJson = []
  let self = this

  try {
      let response = await fetch(this.config.ListExploreByLocServicesAPI+'?latitude='+data.lat+'&longitude='+data.long);

      try {

          responseJson = await response.json();
          self.setState({exploreData:responseJson.data, isLoading:false })

      } catch (error) {

        alert("Explore API Error...")
        console.log(error);

      }
      
  } catch (error) {
    alert("Explore API Error...")
    console.log(error);
  }

}

componentDidMount() {

  if(this.navigate.state.params.service_name === "AllLocations"){
    
    // GETTING CURRENT LOCATION BY FOLLOWING CODE

    if(Platform.OS==='ios'){
        /*navigator.geolocation.getCurrentPosition(
        position => {
          const location = JSON.stringify(position);
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;

          //initialize your function
          this.fetchExploreByServicesIdForLoc({ lat: latitude, long: longitude});
          
        },
        error => {
          
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      )*/
      this.fetchExploreByServicesIdForLoc({ lat: this.state.latitude, long: this.state.longitude});
    }else if(Platform.OS==='android'){
      navigator.geolocation.getCurrentPosition(
        position => {
          const location = JSON.stringify(position);
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;

          //initialize your function
          this.fetchExploreByServicesIdForLoc({ lat: latitude, long: longitude});
          
        },
        error => {
          
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      )
    }
    
  }else{
    //initialize your function
    this.fetchExploreByServicesId(this.exploreServiceID);
  }

}

  render() {
    
    return (
    <Container>
        <ScrollView style={styles.container}>
          {(!this.state.isLoading) ? 
            (

              <View style ={styles.mainContainer}>
                  
                  {(this.navigate.state.params.service_name === "AllLocations")?
                    (

                      <FlatList
                        data={ this.state.exploreData }
                        renderItem={({item}) =>
                          
                          <View style={styles.GridViewBlockStyle}>

                            <TouchableHighlight onPress={this.GetGridViewItem.bind(this, item)} style={{width: "100%"}} >
                              <View>
                                <ImageBackground  source={{uri:item.photos[0].path}} style={{width: '100%', height: '100%'}} imageStyle={{ borderRadius:4 }}>
                                  <View style={[ styles.titleViewInGrid ]}>
                                      <Text onPress={this.GetGridViewItem.bind(this, item)} style={styles.GridViewInsideTextItemStyle} > {item.name} </Text>
                                  </View>
                                </ImageBackground>
                              </View>
                            </TouchableHighlight>

                          </View>

                        }
                        numColumns={2}
                      />

                    ):(
                    
                      <FlatList
                        data={ this.state.exploreData }
                        renderItem={({item}) =>

                          <View style={styles.GridViewBlockStyle}>
                            
                            <TouchableHighlight onPress={this.GetGridViewItem.bind(this, item)} style={{width: "100%"}} >
                              <View>
                                <ImageBackground  source={{uri:item.photos[0].path}} style={{width: '100%', height: '100%'}} imageStyle={{ borderRadius:4 }}>
                                  <View style={[ styles.titleViewInGrid ]}>
                                      <Text onPress={this.GetGridViewItem.bind(this, item)} style={styles.GridViewInsideTextItemStyle} > {item.name} </Text>
                                  </View>
                                </ImageBackground>
                              </View>
                            </TouchableHighlight>

                          </View>

                        }
                        numColumns={2}
                      />
                      
                    )
                  }

                  <View style={{ flex:1, height: 20 }}></View>

              </View>

            ):(
              <View style={styles.loading}>
                <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
              </View>
            )
          }
        </ScrollView>

    </Container>

    );
}

    
  static navigationOptions = ({navigation}) => ({

    title: (navigation.state.params.service_name === "AllLocations")? "Locations" : navigation.state.params.service_name,
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          ...Platform.select({
            ios:{
              borderRadius: 15,
              //height:130,
              marginTop:-15
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerStyle: {
      backgroundColor: '#ffffff',
      borderBottomLeftRadius: 15, 
      borderBottomRightRadius: 15,
      height:60,
      borderBottomWidth: 0
    },
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      paddingRight:44,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular',
          paddingRight:0
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },

  });

}


const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#ffffff',
    flex:1, 
    justifyContent: 'center',
    paddingRight:10,
    paddingLeft:10,
    marginBottom: 5
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#ffffff',
  },
  eventName:{
    fontSize:17, 
    lineHeight:22,
    color:'#565656',
    alignSelf:'flex-start'
  },
  viewStyle:
  {
    alignSelf:'flex-end', 
    fontSize:10,
    color:'#565656',
    marginTop:3
  },
  iconStyle:{
    width:8, 
    height:10, 
    alignSelf:'flex-end', 
    marginTop:22
  },
  listHead:
  {
    flexDirection:'row', 
    flexWrap:'wrap', 
    flex:1, 
    paddingLeft:10, 
    paddingRight:0, 
    paddingBottom:20,
    maxHeight: 10
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },

  itemCoverView1: {
    flex: 0.5,
    height: 150,
    backgroundColor: "steelblue",
    borderWidth: 1,
    borderColor: "black",
    marginTop: 10,
    marginRight: 10
  },
  itemCoverView2: {
      flex: 0.5,
      height: 150,
      backgroundColor: "steelblue",
      borderWidth: 1,
      borderColor: "black",
      marginTop: 10
  },

  GridViewBlockStyle: {
    flex:1,
    alignItems: 'center',
    height: 150,
    margin: 5,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  GridViewInsideTextItemStyle: {
    textTransform: 'uppercase',
    fontSize: 11,
    lineHeight: 12,
    color: '#ffffff',
    textAlign: "center",
    width: "100%",
    paddingTop: 13,
    paddingBottom: 12,
    paddingLeft: 5,
    paddingRight: 5,
    position: "relative",
    top: 0,
    zIndex: 111,

    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,


    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1',
      },
    }),
  },
   titleViewInGrid: {
    width: "100%",
    position: 'absolute',
    left: 0,
    bottom: 0,
    alignItems: 'center',
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    backgroundColor: "rgba(0, 0, 0, 0.3)",
   }



});
export default ExploreViewAllScreen;
