import React, { Component } from 'react';
import { TouchableOpacity, View, Image, StyleSheet, Platform, ScrollView } from 'react-native';
import { Container, Tab, Tabs, ScrollableTab, Text, Picker} from 'native-base';
import LocationSchedule from './locationSchedule';
import { LinearGradient } from 'expo-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
// import CalendarPicker from 'react-native-calendar-picker';
import DatePicker from 'react-native-datepicker'
let Location_Name='';
const Item = Picker.Item;

export default class Location extends Component {
  constructor (props){
    super(props);
    this.params = this.props.navigation.state.params;

    this.state = {
      branches:[],
      selectLocations: "Downtown Abu Dhabi",
      filterAgeGroup1: "3 years",
      filterAgeGroup2: "5 years",
      filterClassType: "Gymnastic",
      filterCalenderIconClick: false,
      filteredDate: new Date(),
      latitude: null,
      longitude: null,
      latitude:24.46555020,
      longitude: 54.38153140,
    };

    //Default visible isVisible
    this.isVisibleHeader = false;
    this.validates = this.validates.bind(this);
    Array.prototype.movingFirst = function(id){        
        let array = [];
        array = this;
        array = array.filter(x=>x.location_id === id);
        array = array.concat(this.filter(x=>x.location_id !== id));
        return array;
    }
  }

  validates = (text, type) => {
  }

  fixedEncodeURIComponent(str) {
      return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
          return '%' + c.charCodeAt(0).toString(16);
      });
  }


//WILL CALL FIRST BEFORE THE RENDER CODE
componentDidMount() {

  this.props.navigation.setParams({ onFullScreen: this.onFullScreen});

  // GETTING CURRENT LOCATION BY FOLLOWING CODE
  if(Platform.OS==='ios'){
    /* navigator.geolocation.getCurrentPosition(
    position => {
      const location = JSON.stringify(position);
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
        
        this.setState({ latitude: latitude });
        this.setState({ longitude: longitude });

        this.fetchBranch({ let: latitude, long: longitude});
    },
    error => {
    },
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  );*/
  this.fetchBranch({ let: this.state.latitude, long: this.state.longitude});
  }else if(Platform.OS==='android'){
    navigator.geolocation.getCurrentPosition(
    position => {
      const location = JSON.stringify(position);
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
        
        this.setState({ latitude: latitude });
        this.setState({ longitude: longitude });

        this.fetchBranch({ let: latitude, long: longitude});
    },
    error => {
    },
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  );
  }
}


fetchBranch = async(data) => {
  let branchresponseJson = []

  try {
      let branchResponse = await fetch("https://konnect.myfirstgym.com/api/customer/locations/nearby?latitude="+data.let+"&longitude="+data.long);
      try {
          branchresponseJson = await branchResponse.json();
          this.setState({branches:branchresponseJson.data.movingFirst(this.params.id) });
          Location_Name=this.state.branches[0]['name']
      } catch (error) {
        console.log(error);
      }
  } catch (error) {
      console.log(error);
  }
}

  render(){

    let selectLocations = [
      { value: 'Downtown Abu Dhabi' },
      { value: 'Abu Dhabi Ladies Club' },
      { value: 'Downtown Abu Dhabi' },
    ];

    let filterAgeGroup1 = [
      { value: '3 years' },
      { value: '4 years' },
      { value: '5 years' },
      { value: '6 years' }
    ];

    let filterAgeGroup2 = [
      { value: '3 years' },
      { value: '4 years' },
      { value: '5 years' },
      { value: '6 years' }
    ];

    let filterClassType = [
      { value: 'Gymnastic' },
      { value: 'Gymnastic' },
    ];
    
    const tabs = this.state.branches.map((item, i) =>{
      return(
        <Tab key={i} tabStyle= { { backgroundColor: 'transparent'}}  textStyle={{color: 'rgba(255, 255, 255, 0.5)'}} activeTabStyle={{backgroundColor: 'transparent' }} activeTextStyle={{color: '#ffffff', fontWeight:'normal'}} heading={item.name}>
          <ScrollView style={[styles.container]}>
              <View style ={styles.mainContainer1}>
                  <LocationSchedule apiid ={item.location_id } navigations ={ this.props.navigation }/>
              </View>
          </ScrollView>
        </Tab> 
      )
    })
   
    return(
            
      <Container style={{ backgroundColor: '#E5E5E5' }} >
        
        {(this.isVisibleHeader)? (

            <View style={ styles.filterContainer }>

              <View style={{ flex: 1, flexDirection: "row", alignContent: "stretch", maxHeight: 150,paddingBottom: 28 }}>

                <View style={{ flex: 0.85, }}>
                    <Dropdown containerStyle={{ backgroundColor:'#2eaaeb', width:'100%', borderWidth:1, borderColor:'#2eaaeb', borderRadius: 4, paddingLeft: 5, elevation: 6 }}
                      label= ""
                      textColor='#ffffff'
                      baseColor='#ffffff'
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      labelFontSize={0}
                      value={this.state.selectLocations}
                      data={selectLocations}
                      dropdownPosition={0}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={(value) => this.validates(value, 'selectLocations')}
                    />
                </View>

                <View style={{ flex: 0.15 }}>
                  <TouchableOpacity style={{ borderBottomColor: "rgba(0, 0, 0, 0)", paddingTop: 14, paddingRight: 2, alignItems: "flex-end" }} onPress={this. onFullScreen} >
                      <Image style={{width: 20, height: 20}} source={require('../assets/images/calendar.png')}/>
                  </TouchableOpacity>
                </View>

              </View>


              {/*<ScrollView style={{ width: "100%", height: 100, alignContent: "stretch", paddingTop: 5 }} >*/}
                
                <View style={{ flex: 1, flexDirection: "row", alignContent: "stretch", maxHeight: 150,paddingBottom: 30, }}>

                  <View style={{ flex: 0.45, }}>
                      <Dropdown containerStyle={[styles.tabbingFont, { backgroundColor:'#0099EF', width:'100%', borderBottomWidth:1, borderBottomColor:'rgba(0, 0, 0, 0.12)', paddingLeft: 5 }]}
                        label= "Age Group"
                        textColor='#000000'
                        baseColor='#ffffff'
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        labelFontSize={12}
                        value={this.state.filterAgeGroup1}
                        data={filterAgeGroup1}
                        dropdownPosition={0}
                        dropdownOffset={{ top: 8, left: 0 }}
                        onChangeText={(value) => this.validates(value, 'filterAgeGroup1')}
                      />
                  </View>

                  <View style={{ flex: 0.10, }}>
                    <Text style={{ color: "#ffffff", paddingTop: 15 }}> To </Text>
                  </View>

                  <View style={{ flex: 0.45, }}>
                      <Dropdown containerStyle={[styles.tabbingFont, {backgroundColor:'#0099EF', width:'100%', borderBottomWidth:1, borderBottomColor:'rgba(0, 0, 0, 0.12)', paddingLeft: 5 }]}
                        label= "Age Group"
                        textColor='#000000'
                        baseColor='#ffffff'
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        labelFontSize={0}
                        value={this.state.filterAgeGroup2}
                        data={filterAgeGroup2}
                        dropdownPosition={0}
                        dropdownOffset={{ top: 8, left: 0 }}
                        onChangeText={(value) => this.validates(value, 'filterAgeGroup2')}
                      />
                  </View>

                </View>

                <View style={{ flex: 1, flexDirection: "row", alignContent: "stretch", maxHeight: 150,paddingBottom: 20, }}>

                  <View style={{ flex: 1, }}>
                      <Dropdown containerStyle={[styles.tabbingFont, {backgroundColor:'#0099EF', width:'100%', borderBottomWidth:1, borderBottomColor:'rgba(0, 0, 0, 0.12)', paddingLeft: 5 , }]}
                        label= "Class Type"
                        textColor='#000000'
                        baseColor='#ffffff'
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        labelFontSize={12}
                        value={this.state.filterClassType}
                        data={filterClassType}
                        dropdownPosition={0}
                        dropdownOffset={{ top: 8, left: 0 }}
                        onChangeText={(value) => this.validates(value, 'filterClassType')}
                      />
                  </View>

                </View>

                <View style={[styles.CalenderContainer, {paddingBottom: 20,}]}>
                  <DatePicker
                    style={[styles.tabbingFont, {width: "100%", borderWidth: 0}]}
                    date={this.state.filteredDate}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MMMM-DD"
                    maxDate= {new Date()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                      },
                      dateInput: {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomWidth: 1,
                        textAlign: "left",
                      }
                    }}
                    onDateChange={(date) => this.validates(date, 'filteredDate')}
                  />
                </View>

                <View style={{ flex: 1, flexDirection: "row", alignContent: "stretch", maxHeight: 150 }}>

                  <View style={{ flex: 0.8, }}>
                    <TouchableOpacity onPress={this. onFullScreen} >
                        <Text style={[styles.tabbingFont, { textAlign: "right", color: "#ffffff" }]} > CANCEL </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 0.2, }}>
                    <TouchableOpacity onPress={this. onFullScreen} >
                        <Text style={[styles.tabbingFont, { textAlign: "left", paddingLeft: 5, color: "#ffffff" }]} > OK </Text>
                    </TouchableOpacity>
                  </View>

                </View>

              {/* </ScrollView> */}

            </View>

          ): ( <View></View> )

        }

        <LinearGradient colors={['#0099EF', '#00D2EF']}style ={{height:50, borderBottomLeftRadius: 15, borderBottomRightRadius: 15,
        
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),
        
        }} start={{x: 0, y: 0}} end={{x: 1, y: 0}}></LinearGradient>
          <Tabs tabBarUnderlineStyle={{ backgroundColor:'#ffffff'}} renderTabBar={()=> <ScrollableTab  style={{ backgroundColor:'transparent', zIndex:999, borderBottomLeftRadius: 15, borderBottomRightRadius: 15, paddingLeft: 20, paddingRight: 20, }}/>} style ={{marginTop:-50, backgroundColor:'transparent'}} >
              {tabs}
          </Tabs>
      </Container>

    );

  }

  onFullScreen = async() => {
    // CALL THE FILTER LOCATION SCREEN
    this.props.navigation.setParams({ fullscreen: !this.isVisibleHeader });
  }

  static navigationOptions = ({ navigation }) => {

    if (navigation.state.params && !navigation.state.params.fullscreen) {
      return{
        title: 'Location',
        headerRight: (
            <View style={{ marginRight: 20 }} >
                <TouchableOpacity activeOpacity={1} style={{ borderBottomColor: "rgba(0, 0, 0, 0)" }} onPress={() => navigation.push('LocationFilterScreen', {location_Id:navigation.state.params.id,Location_Name:Location_Name, item_Id:'', category_Id:'',  guestSchedule:false})} >
                    <Image style={{width: 20, height: 20}} source={require('../assets/images/filter.png')} />
                </TouchableOpacity>
            </View>
        ),
        headerStyle: {
          backgroundColor: '#E5E5E5',
          height:70,
          elevation: 0,
          borderBottomWidth: 0
        },
        headerBackground: (
          <LinearGradient
            colors={['#0099EF', '#00D2EF']}
            style={{ flex: 1 }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
          />
        ),
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          fontWeight: 'normal',
          alignSelf: 'center',
          textAlign:"center", 
          flex:1 ,
          fontSize:20,
          lineHeight:25,
          ...Platform.select({
            ios: {
              fontFamily:'SFProTextRegular'
            },
            android: {
              fontFamily:'RobotoRegular_1',
            },
          }),
          }
      }// RETURN CLOSED

    } else {      
      //Hide Header by returning null
      return { header: null };
    }//ELSE CLOSED

  }


}

  const styles = StyleSheet.create({
      mainContainer:{
        backgroundColor:'#E5E5E5',
        flex:1, 
        justifyContent: 'center',
        paddingRight:15,
        paddingLeft:15,
        marginBottom: 30,
      },
      mainContainer1:{
        backgroundColor:'#E5E5E5',
        flex:1, 
        justifyContent: 'center',
        marginBottom: 20,
      },
      tabbingFont:{
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular'
          },
          android: {
            fontFamily:'RobotoRegular_1',
          },
        }),
      },
      container: {
        flex: 1,
        paddingTop: 10,
        paddingBottom: 50,
        backgroundColor: '#E5E5E5',
      },
      mainEventsClass:{
        marginTop:10,
        marginBottom:10, 
        paddingTop:10, 
        paddingBottom:10, 
        borderColor:'#146FA9',
        borderLeftWidth:4,
        borderRadius:4, 
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 4
      },
      buttonStyle:{
        backgroundColor:'transparent',
        borderRadius:4,
        borderWidth:1,
        flex:0.8,
        width:'90%',
        marginLeft:'5%',
        borderColor:'#000000',
        justifyContent: 'center',
        alignItems: 'center',
      },
      loginButtonText:{
        color:'#000000',
        fontSize:12
      },
      content:{
        textAlign:'center',
        alignItems:'center',
        margin:0,
        flex:1,
        marginTop: 140
      },
      tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
          ios: {
            shadowColor: 'black',
            shadowOffset: { width: 0, height: -3 },
            shadowOpacity: 0.1,
            shadowRadius: 3,
          },
          android: {
            elevation: 20,
          },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
      },
      tabBarInfoText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
      },
      navigationFilename: {
        marginTop: 5,
      },
      filterContainer: {
        flex: 1,
        backgroundColor: '#0099EF',
        paddingTop: 40,
        paddingLeft: 15,
        paddingRight: 15
      },
      CalenderContainer: {
        flex: 1,
        backgroundColor: '#0099EF',
        color:'#ffffff',
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
      },


  });
