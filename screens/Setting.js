import React from 'react';
import { View, StyleSheet, ScrollView, Linking, Platform, AsyncStorage } from 'react-native';
import {Text, Button} from 'native-base';
import ToggleSwitch from 'toggle-switch-react-native';
import { Actions } from 'react-native-router-flux';
import Spacer from '../components/UI/Spacer';
import { LinearGradient } from 'expo-linear-gradient';
import axios from 'axios';

import { StackActions, NavigationActions } from 'react-navigation';

class Setting extends React.Component{

    constructor() {
        super();
        this.config = {
          userProfileSettingAPI : "https://konnect.myfirstgym.com/api/customer/settings",
          userProfileSettingUpdtAPI: "https://konnect.myfirstgym.com/api/customer/settings"
        }
        this.state = {
          allowPushEmailNotifications: false,
          allowPushSMSNotifications: false,
          allowPushNotifications: false,
          allowPushPromtClassNotifications: false,
        };
    }

    userLogout = () => {
      AsyncStorage.removeItem('api_token');
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Profile' })],
      });
      this.props.navigation.dispatch(resetAction);

      this.props.navigation.push('Loading', { logoutToken: true })
    }

    onToggleChange  = (id) => {
      {
        let allowNotify = '';
        let self = this;
        (id==='1')?
        (
          AsyncStorage.getItem('api_token').then((token) => {

            if(this.state.allowPushEmailNotifications){ allowNotify = false; }
            else{ allowNotify = true; }

            if(token!==null){
              let response = axios.post(this.config.userProfileSettingUpdtAPI, {
                api_token: token,
                name: "send_email_notifications",
                value: allowNotify,
              })
              .then(function (response) {
                self.setState({allowPushEmailNotifications: response['data'].settings.send_email_notifications })
              })
              .catch(function (error) {
                console.log("Update Response - ", error);
              });
            }  
          })

        ):(id==='2')?
        (           
          AsyncStorage.getItem('api_token').then((token) => {

            if(this.state.allowPushSMSNotifications){ allowNotify = false; }
            else{ allowNotify = true; }

            if(token!==null){
              let response = axios.post(this.config.userProfileSettingUpdtAPI, {
                api_token: token,
                name: "send_sms_notifications",
                value: allowNotify,
              })
              .then(function (response) {
                self.setState({allowPushSMSNotifications: response['data'].settings.send_sms_notifications })
              })
              .catch(function (error) {
                console.log("Update Response - ", error);
              });
            }  
          })
                  
        ):(id==='3')?
        (
          AsyncStorage.getItem('api_token').then((token) => {

            if(this.state.allowPushNotifications){ allowNotify = false; }
            else{ allowNotify = true; }

            if(token!==null){
              let response = axios.post(this.config.userProfileSettingUpdtAPI, {
                api_token: token,
                name: "send_push_notifications",
                value: allowNotify,
              })
              .then(function (response) {
                self.setState({allowPushNotifications: response['data'].settings.send_push_notifications })
              })
              .catch(function (error) {
                console.log("Update Response - ", error);
              });
            }  
          })
        
        ):(id==='4')?
        (
          AsyncStorage.getItem('api_token').then((token) => {

            if(this.state.allowPushPromtClassNotifications){ allowNotify = false; }
            else{ allowNotify = true; }

            if(token!==null){
              let response = axios.post(this.config.userProfileSettingUpdtAPI, {
                api_token: token,
                name: "prompt_to_signin_to_class",
                value: allowNotify,
              })
              .then(function (response) {
                self.setState({allowPushPromtClassNotifications: response['data'].settings.prompt_to_signin_to_class })
              })
              .catch(function (error) {
                console.log("Update Response - ", error);
              });
            }

          })          

        ):(<View><Text></Text></View>)

      }

    }


    componentDidMount(){
      
      let settingItem = [];
      let self = this;

      AsyncStorage.getItem('api_token').then((token) => {

        let settingItemResponse = axios.get(this.config.userProfileSettingAPI+"?api_token="+token)
        .then(function (settingItemResponse) {
          
          //----------------------------
          if(settingItemResponse["data"]["data"].send_email_notifications){
            self.setState({allowPushEmailNotifications: true })
          }
          //----------------------------
          if(settingItemResponse["data"]["data"].send_push_notifications){
            self.setState({allowPushNotifications: true })
          }
          //----------------------------
          if(settingItemResponse["data"]["data"].send_sms_notifications){
            self.setState({allowPushSMSNotifications: true })
          }
          //----------------------------
          if(settingItemResponse["data"]["data"].prompt_to_signin_to_class){
            self.setState({allowPushPromtClassNotifications: true })
          }

        })
        .catch(function (error) {
            console.log("Setting Item Error - ", error);
        })

      })

    }
  

  render(){
        return(
          <ScrollView style={styles.container}>
           <View style ={styles.mainContainer}>
            <View>
                <Text style = {[styles.mainNotification, styles.textFonts ]} >Notification</Text>
                <Spacer size ={20} />
                <View style={styles.notification}>
                    <View style = {styles.firstColumn}>
                        <Text style = {[styles.mainNotificationText, styles.textFonts ]}>Send me email confirmation</Text>
                        <Text style = {[styles.subNotificationText, styles.textFonts]}>Receive email confirmations when booking and purchasing in the app</Text>
                    </View>
                    <View style = {styles.secondColumn}>
                        <ToggleSwitch
                            style = {[ styles.textFonts, {elevation:20, borderColor:'#E5E5E5', borderWidth:1, alignSelf:'right'}]}
                            isOn={this.state.allowPushEmailNotifications}
                            onColor='#4CD964'
                            offColor='#E5E5E5'
                            size='small'
                            onToggle={  this.onToggleChange.bind(this,"1")}
                        />
                    </View>
                     
                </View>

                <View style={styles.notification}>
                    <View style = {styles.firstColumn}>
                        <Text style = {[styles.mainNotificationText, styles.textFonts]}>Email me SMS notification</Text>
                        <Text style = {[styles.subNotificationText, styles.textFonts]}>Receive notification from this business</Text>
                    </View>
                    <View style = {styles.secondColumn}>
                        <ToggleSwitch
                            isOn={this.state.allowPushSMSNotifications}
                            onColor='#4CD964'
                            offColor='#E5E5E5'
                            size='small'
                            onToggle={  this.onToggleChange.bind(this,"2")}
                        />
                    </View>
                     
                </View>

                <View style={styles.notification}>
                    <View style = {styles.firstColumn}>
                        <Text style = {[styles.mainNotificationText, styles.textFonts]}>Push Notification</Text>
                        <Text style = {[styles.subNotificationText, styles.textFonts]}>Receive push notification</Text>
                    </View>
                    <View style = {styles.secondColumn}>
                        <ToggleSwitch
                            isOn={this.state.allowPushNotifications}
                            onColor='#4CD964'
                            offColor='#E5E5E5'
                            size='small'
                            onToggle={  this.onToggleChange.bind(this,"3")}
                        />
                    </View>
                     
                </View>

                <View style={[styles.notification, {borderBottomWidth:0}]}>
                    <View style = {styles.firstColumn}>
                        <Text style = {[styles.mainNotificationText, styles.textFonts]}>Promt me to sign in to class</Text>
                        <Text style = {[styles.subNotificationText, styles.textFonts]}>Skip this line by signing in on mobile</Text>
                    </View>
                    <View style = {styles.secondColumn}>
                        <ToggleSwitch
                            isOn={this.state.allowPushPromtClassNotifications}
                            onColor='#4CD964'
                            offColor='#E5E5E5'
                            size='small'
                            onToggle={  this.onToggleChange.bind(this,"4")}
                        />
                    </View>
                     
                </View>
                <Text style = {[styles.mainNotification, styles.textFonts]} >Support</Text>
                <View style = {{marginBottom:20}}>
                  <Text style = {[styles.SupportBottomOnText, styles.textFonts]}  onPress={ ()=> Linking.openURL('https://myfirstgym.com/help-and-support/') } >Help Center</Text>
                  <Text style = {[styles.SupportBottomOnText, styles.textFonts]} onPress={ ()=> Linking.openURL('https://myfirstgym.com/privacy-policy/') }>My First Gym Privacy Policy</Text>
                  
                  <Text style = {[styles.SupportBottomOnText, styles.textFonts]} onPress={ ()=> Linking.openURL('https://myfirstgym.com/terms-and-conditions/') }>Terms and Conditions</Text>
                  <Text style = {[styles.SupportBottomOnText, styles.textFonts]} onPress={ ()=> Linking.openURL('https://myfirstgym.com/contact-us/') }>Tell Us What you think</Text>
                  <View  style = {[styles.SupportBottomOnText, styles.textFonts]}>
                    <Text>Version</Text>
                    <Text style = {[styles.versionText, styles.textFonts]}>1.0.6</Text>
                  </View>
                  <Text  style = {[styles.SupportBottomOnText, styles.textFonts]} onPress = {this.userLogout}>Logout</Text>
                </View>
            </View>
          </View>
        </ScrollView>
        )
    }
    
  };

  Setting.navigationOptions = {
    title: 'Settings',
    headerStyle: {
      backgroundColor: '#ffffff',
      borderBottomLeftRadius: 15, 
      borderBottomRightRadius: 15,
      height:50,
      borderBottomWidth: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          ...Platform.select({
            ios:{
              borderRadius: 15,
              marginTop:-15,
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:0.8 ,
      fontSize:20,
      lineHeight:25,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular',
          letterSpacing: 0.38
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };
  
  const styles = StyleSheet.create({
    mainContainer:{
      backgroundColor:'#ffffff',
      flex:1, 
      justifyContent: 'center',
    },
    container: {
      flex: 1,
      paddingTop: 15,
      paddingBottom: 70,
      backgroundColor: '#ffffff',
    },
      notification:{
        paddingLeft:25,
        paddingRight:15,
        flex:1,
        flexDirection:'row',
        marginBottom:20,
        borderBottomWidth:1,
        borderBottomColor:'rgba(0, 0, 0, 0.1)',
        paddingTop:10,
        paddingBottom:10,
      },
      marginBottomOnText:{
        marginBottom:10,
        paddingLeft:25,
        paddingRight:15,
        flex:1,
        flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'rgba(0, 0, 0, 0.1)',
        paddingTop:10,
        paddingBottom:10,
        fontSize:15,
        color:'rgba(0, 0, 0, 0.87)',
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular'
          },
          android: {
            fontFamily:'RobotoRegular_1'
          },
        }),
      },
      SupportBottomOnText:{
        fontSize:15,
        lineHeight: 20,
        color:'rgba(0, 0, 0, 0.87)',
        paddingLeft:25,
        paddingRight:15,
        borderBottomWidth:1,
        borderBottomColor:'rgba(0, 0, 0, 0.1)',
        paddingTop:10,
        paddingBottom:10,
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular',
            letterSpacing: -0.24
          },
          android: {
            fontFamily:'RobotoRegular_1'
          },
        }),
      },
      mainNotification:{
        fontSize:17,
        lineHeight: 22,
        color:'rgba(0, 0, 0, 0.87)',
        paddingLeft:15,
        paddingRight:15,
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular',
            letterSpacing: -0.41
          },
          android: {
            fontFamily:'RobotoRegular_1'
          },
        }),
    },
      mainNotificationText:{
          fontSize:15,
          lineHeight: 20,
          color:'rgba(0, 0, 0, 0.87)',
          ...Platform.select({
            ios: {
              fontFamily:'SFProTextRegular',
              letterSpacing: -0.24
            },
            android: {
              fontFamily:'RobotoRegular_1'
            },
          }),
      },
      subNotificationText:{
        fontSize:13,
        lineHeight: 18,
        color:'rgba(0, 0, 0, 0.54)',
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular',
            letterSpacing: -0.08
          },
          android: {
            fontFamily:'RobotoRegular_1'
          },
        }),
      },
      firstColumn:{
        flex:0.85
      },
      secondColumn:{
        flex:0.15,
        textAlign:'right',
        marginTop:10
      },
      versionText:{
        fontSize:13,
        lineHeight: 18,
        paddingLeft:3,
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular',
            letterSpacing: -0.08
          },
          android: {
            fontFamily:'RobotoRegular_1'
          },
        }),
      },
     text:{
          fontWeight:'bold',
           fontSize:16,
            textAlign:'center',
             flex:0.8,
            margin:0
        },
    buttonStyle:{
        backgroundColor:'transparent',
        borderRadius:4,
        borderWidth:1,
        flex:0.8,
        width:'80%',
        marginLeft:'10%',
        borderColor:'#000000',
        justifyContent: 'center',
        alignItems: 'center',
      },
      loginButtonText:{
        color:'#000000',
        fontSize:12
      },
      content:{
        textAlign:'center',
        alignItems:'center',
        margin:0,
        flex:1,
    },
    tabBarInfoContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { width: 0, height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      alignItems: 'center',
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
    tabBarInfoText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      textAlign: 'center',
    },
    navigationFilename: {
      marginTop: 5,
    },
    textFonts: {
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    }

  });
  
  export default Setting;
  