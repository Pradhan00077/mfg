import React from 'react';
import { View, StyleSheet , Image, Platform, ScrollView, AsyncStorage } from 'react-native';
import {Container, Content, Text} from 'native-base';
import Spacer from '../components/UI/Spacer';
import { LinearGradient } from 'expo-linear-gradient';
class MyPoints extends React.Component{
  constructor(props){
    super(props);
    this.config = {
      apiUrl: "https://konnect.myfirstgym.com/api/customer/points"
  }
    this.state={
      points: [],
      point: [
        {
            ClientSourceName: 'Points Added From: Client/Source Name',
            Time: "10:54 pm",
            Day:'07 Jun 2019',
            ClosingPoint: 200,
            EarnPoint: 20
        },{
            ClientSourceName: 'Points Added From: Client/Source Name',
            Time: "10:56 pm",
            Day:'07 Jun 2019',
            ClosingPoint: 220,
            EarnPoint: 10
        },{
          ClientSourceName: 'Points Added From: Client/Source Name',
          Time: "02:04 pm",
          Day:'06 July 2019',
          ClosingPoint: 224,
          EarnPoint: 45
        },{
          ClientSourceName: 'Points Added From: Client/Source Name',
          Time: "10:04 am",
          Day:'06 Jun 2019',
          ClosingPoint: 230,
          EarnPoint: 30
        },{
            ClientSourceName: 'Points removed From: Client/Source Name',
            Time: "1:14 pm",
            Day:'06 Jun 2019',
            ClosingPoint: 200,
            EarnPoint: -10
        }
      ]
    }
  }

  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}

async fetchMyPoints(token) {
  try {

      let response = await fetch(this.config.apiUrl+"?api_token="+token);
      let responseJson = [];
      try {
          responseJson = await response.json();
          console.log(responseJson);
      } catch (error) {
          console.log(error);
          responseJson = [];
      }
      this.setState({ points: responseJson.data });
  } catch (error) {
      console.log(error);
  }
}


componentDidMount() {
  AsyncStorage.getItem('api_token').then((token) => {
    if(token!==null){
      this.fetchMyPoints(token);
    }
  })

}


  render(){
    let dataPoint=[];
    this.state.point.map((item, i) =>{
      let date=null;
      dataPoint.push(
        <View key ={i}>

            <View style ={styles.rootheading}>
              <Text style={ styles.pointsDate }>{item.Day}</Text>
            </View>

            <View style ={(item.EarnPoint>0)? styles.itemRow :  styles.itemRow1 }>

                <View style = {{flex:0.15}}>
                  {
                    (item.EarnPoint>0)?
                    (
                      <Image style={styles.settingcardIcon} source={require('../assets/images/points-1.png')}/>
                    )
                    :(
                      <Image style={styles.settingcardIcon} source={require('../assets/images/points-2.png')}/>
                    )
                  }
                </View>

                <View style = {{flex:0.50}}>
                  
                  <View style={{flexDirection:'row', flexWrap:'wrap', flex:1 }}>
                    <Text style ={styles.mainHead}>{item.ClientSourceName}</Text>
                  </View>

                  <View style={{flexDirection:'row', flexWrap:'wrap', flex:1, marginTop:6, justifyContent:'center', textAlignVertical:'center', }}>

                    <Text style ={[styles.textFont, {fontSize:11, lineHeight:13, flex:0.3, color:'rgba(0, 0, 0, 0.38)'}]} >{item.Time}</Text>
                    <Text style ={[ styles.textFont, {fontSize:11, lineHeight:13, flex:0.7, textAlign:'right', color:'rgba(0, 0, 0, 0.38)'}]} >Closing Points: {item.ClosingPoint}</Text>

                  </View>
                </View>

                <View style = {{flex:0.35}}>
                  {
                    (item.EarnPoint>0)?
                    (
                      <Text style={[styles.pointStyle, {color:'#25AE88'}]}>+{item.EarnPoint} points</Text>
                    )
                    :(
                      <Text style={[styles.pointStyle]}>{item.EarnPoint} points</Text>
                    )
                  }
                </View>

            </View>

          </View>
      )
    })

    return(
      <ScrollView style={styles.container}>
        <View style ={styles.mainContainer}>
            {dataPoint}
        </View>
      </ScrollView>
    );
  }
}

MyPoints.navigationOptions = {
  title: 'My Points',
  headerStyle: {
    backgroundColor: '#E5E5E5',
    height:60,
    elevation: 0,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        elevation: 4,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            //height:130,
            marginTop:-15
          }
        }),
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8 ,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 0.38
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),

  },
};

const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#E5E5E5',
    flex:1, 
    justifyContent: 'center',
    marginBottom: 0,
  },
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
settingcardIcon:{
  width:40,
  height:40,
  zIndex:99,
  alignSelf:'flex-start',
  paddingTop: 16,
  paddingBottom: 16,
},
itemRow:{
  flexDirection:'row', 
  flexWrap:'wrap', 
  flex:1, 
  backgroundColor: "#FFFFFF",
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 16,
  paddingBottom: 16,

},
itemRow1:{
  flexDirection:'row', 
  flexWrap:'wrap', 
  flex:1, 
  backgroundColor: "#FFFFFF",
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 16,
  paddingBottom: 16,
},
rootheading:{
  backgroundColor: "#F4F4F4",
  flexDirection:'row', 
  flexWrap:'wrap', 
  flex:1,
  color:'rgba(0, 0, 0, 0.54)',
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 7,
  paddingBottom: 7,

  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
mainHead:{
  fontSize:11,
  lineHeight: 13,
  color:'rgba(0, 0, 0, 0.87)', 
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular',
      letterSpacing: 0.07
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
pointStyle:{
  fontSize:15,
  fontWeight:'normal',
  lineHeight: 20,
  color:'rgba(0, 0, 0, 0.87)',

  textAlign:'right',
  justifyContent:'center',
  textAlignVertical:'center',
  paddingTop: 16,
  paddingBottom: 15,
  //marginTop:4,

  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular',
      letterSpacing: -0.24
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
textFont:{
  fontSize: 11,
  lineHeight: 13,
  color: "rgba(0, 0, 0, 0.38)",
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular',
      letterSpacing: 0.07
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
pointsDate: {
  fontSize: 12,
  lineHeight: 16,
  color: "rgba(0, 0, 0, 0.54)"
}


});
export default MyPoints;
