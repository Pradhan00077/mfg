import React, { Component } from 'react';
import { View, Image, TouchableHighlight, StyleSheet, Platform, ScrollView, AsyncStorage, RefreshControl } from 'react-native';
import { Container,Content, TabHeading, Tab, Tabs, ScrollableTab, Text, Button } from 'native-base';
import Spacer from '../components/UI/Hr';
import { LinearGradient } from 'expo-linear-gradient';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

export default class ScheduleScreen extends Component {
  constructor (props){
    super(props);
    this.navigate = this.props.navigation;
    this.params = this.props.navigation.state.params;  
    this.state = {
      schedule: {},
      isLoading: true,
      hastoken:false,
      refreshing: false,
      purchase:this.params?(this.params.purchase?this.params.purchase:false):false,
    };
  }

  async fetchSchedule(token) { 
    try {
        let response = await fetch('https://konnect.myfirstgym.com/api/customer/schedules?api_token='+token);
        let responseJson = [];
        try {
            responseJson = await response.json();
        } catch (error) {
            console.log(error);
            responseJson = [];
        }
        this.setState({ schedule: responseJson.data,  isLoading:false, hastoken: true  });
    } catch (error) {
        alert(error)
        console.log(error);
    }
  }
  componentDidMount() {
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.fetchSchedule(token);
    }else{
      this.setState({isLoading:false,});
    }    
    });
  }


  _onRefresh = () => {
    this.setState({refreshing: true});

    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.fetchSchedule(token).then(() => {
          this.setState({refreshing: false});
        });
      }else{
        this.setState({isLoading:false,});
      }
    });
  
  }

  render() {
    let itemsupcoming=0
    let itemshistory=0
    upcomingEvents =[];
    historyEvents =[];
    if(Object.keys(this.state.schedule).length>0){
      let scheduleDate = this.state.schedule.upcoming_schedules;
      let scheduleDatehistory = this.state.schedule.schedule_history;
      if(Object.keys(scheduleDate).length>0){
        Object.keys(scheduleDate).map((item, i) =>{
          let first = true;
          upcomingEvents.push(scheduleDate[item].map((rowhistory,h)=>{     
            let date='';0
            if(first){
              date = item;
              first=false;
            }
            else{
              data=null;
            }    
        itemsupcoming=itemsupcoming+1;
        return(
          <View  key = {h}>
              {
                (date)?
                (
                  <View style={{ backgroundColor: "#ffffff", marginBottom: 4, marginTop: 4, elevation:1 }}>

                    <Text style = {[styles.fontFamily, {color:'rgba(0, 0, 0, 0.87)',height:22,justifyContent:'center', fontSize:17, lineHeight:22, backgroundColor:'#ffffff', flex:1, alignSelf:'center', textAlignVertical:'center'}]}>{date}</Text>

                  </View>
                ):
                (

                  <Text style = {[styles.fontFamily, {color:'rgba(0, 0, 0, 0.87)', height:0, fontSize:0, lineHeight:0, backgroundColor:'#ffffff', flex:1, alignSelf:'center'}]}>{date}</Text>

                )
              }

            <View style={{ paddingRight:15, paddingLeft:15, }}>

            <TouchableHighlight style ={styles.mainEventsClass} underlayColor="white" onPress={() => this.navigate.navigate('EventsDescription', {serviceItemId:rowhistory.schedule_id, name:rowhistory.service_item_name, schedule_date_id:rowhistory.schedule_date_id})} >
              
            <View style={{flexDirection:'row',flex:1, backgroundColor: "#FFFFFF",paddingTop:15,paddingBottom:15, borderTopRightRadius: 4, borderBottomRightRadius: 4 }}>

              <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.20, alignSelf:'center'}}>
              <Text style={[styles.fontFamily, {textAlign:'center',lineHeight:20,  fontSize:14, color:'rgba(0, 0, 0, 0.87)', paddingLeft:3, paddingRight:3}]} >{rowhistory.start_time.toUpperCase()}</Text>
              </View>
              <View style ={{flex:0.54}}>
                <View style = {{paddingLeft:10}}>
                  <Text style ={[styles.fontFamily, {fontSize:16, lineHeight:21, color:'rgba(0, 0, 0, 0.87)'}]}>{rowhistory.service_item_name}</Text>
                  <Text style ={[styles.fontFamily, {fontSize:13,lineHeight:18, color:'rgba(0, 0, 0, 0.54)'}]} >{rowhistory.service_category}</Text>
                </View>
              </View>
              <View style ={{ flex:0.20, justifyContent:'center'}}>
                  <View style ={{flex:1, width:'100%', alignSelf:'center' , textAlign:'center', maxHeight:24, flex:1, justifyContent:'center', backgroundColor:'#25AE88', borderRadius:12,}}>
                        <Text style={[styles.fontFamily, styles.ageGroupbtn, { alignSelf:'center'}]}>{ rowhistory.age_group }</Text>
                  </View>
              </View>
              <View style ={{ flex:0.06, justifyContent:'center'}}>
                <View style ={{alignSelf:'center', justifyContent:'center', marginRight:5}}>
                  <Image style={{width:8, height:'100%', resizeMode:'contain'}} source = {require('../assets/images/right_events.png')}/>
                </View>
              </View>
            </View>
          </TouchableHighlight>
          
          </View>
        </View>
          )
        }));
        });
        }
      if(Object.keys(scheduleDatehistory).length>0){
         Object.keys(scheduleDatehistory).map((item, j) =>{
         let first = true;
         historyEvents.push(scheduleDatehistory[item].map((row,i)=>{     
          let date='';
           if(first){
             date = item;
             first=false;
           }
           else{
             data=null;
           }    
    
          itemshistory=itemshistory+1;
           return(
            <View  key = {i}>
                {
                  (date)?
                  (
                    <View style={{ backgroundColor: "#ffffff", marginBottom: 4, marginTop: 4, elevation:1 }}>
                      <Text style = {[styles.fontFamily, {color:'rgba(0, 0, 0, 0.87)',height:22,justifyContent:'center', fontSize:17, lineHeight:25, backgroundColor:'#ffffff', flex:1, alignSelf:'center', textAlignVertical:'center'}]}>{date}</Text>
                    </View>
                  ):
                  (<Text style = {[styles.fontFamily, {color:'rgba(0, 0, 0, 0.87)', height:0, fontSize:0, lineHeight:0, backgroundColor:'#ffffff', flex:1, alignSelf:'center'}]}>{date}</Text>)
                }

              <View style={{ paddingRight:15, paddingLeft:15, }}>

                <TouchableHighlight style ={styles.mainEventsClass} underlayColor="white" onPress={() => this.navigate.navigate('EventsDescription', {serviceItemId:row.schedule_id, name:row.service_item_name, schedule_date_id:row.schedule_date_id})} >

                <View style={{flexDirection:'row',flex:1, backgroundColor: "#FFFFFF",paddingTop:15,paddingBottom:15, borderTopRightRadius: 4, borderBottomRightRadius: 4 }}>

                  <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.20, alignSelf:'center', textAlign:'center'}}>
                    <Text style={[styles.fontFamily, {textAlign:'center',lineHeight:20,  fontSize:14, color:'rgba(0, 0, 0, 0.87)', paddingLeft:3, paddingRight:3}]} >{row.start_time.toUpperCase()}</Text>
                  </View>
                  <View style ={{flex:0.54}}>
                    <View style = {{paddingLeft:10}}>
                      <Text style ={[styles.fontFamily, {fontSize:16, lineHeight:21, color:'rgba(0, 0, 0, 0.87)'}]}>{row.service_item_name}</Text>
                      <Text style ={[styles.fontFamily, {fontSize:13,lineHeight:18, color:'rgba(0, 0, 0, 0.54)'}]} >{row.service_category}</Text>
                    </View>
                  </View>
                  <View style ={{ flex:0.20, justifyContent:'center'}}>
                      <View style ={{flex:1, width:'100%', alignSelf:'center' , textAlign:'center', maxHeight:24, flex:1, justifyContent:'center', backgroundColor:'#25AE88', borderRadius:12,}}>
                            <Text style={[styles.fontFamily, styles.ageGroupbtn, { alignSelf:'center'}]}>{row.age_group }</Text>
                      </View>
                  </View>
                  <View style ={{ flex:0.06, justifyContent:'center'}}>
                    <View style ={{alignSelf:'center', justifyContent:'center', marginRight:5}}>
                      <Image style={{width:8, height:'100%', resizeMode:'contain'}} source = {require('../assets/images/right_events.png')}/>
                    </View>
                   </View>
                </View>
              </TouchableHighlight>
            </View>
          </View>
             )
            })
         );
            });
          }

    }

    return (
      <Container style={{ backgroundColor:'#E5E5E5' }}>
        <LinearGradient colors={['#0099EF', '#00D2EF']}style ={{height:50, borderBottomLeftRadius: 15, borderBottomRightRadius: 15, 
        
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        })

        }} start={{x: 0, y: 0}} end={{x: 1, y: 0}}></LinearGradient>
        <Tabs tabBarUnderlineStyle={{ backgroundColor:'#ffffff'}} initialPage={this.state.purchase?2:0} page={this.state.purchase?1:0} renderTabBar={()=><ScrollableTab  style={{ borderBottomLeftRadius: 15, borderBottomRightRadius: 15,backgroundColor:'transparent', zIndex:999 }} />} style ={{marginTop:-50}} >

        <Tab  tabStyle= { { backgroundColor: 'transparent'}}  textStyle={{color: 'rgba(255, 255, 255, 0.5)'}} activeTabStyle={{backgroundColor: 'transparent' }} activeTextStyle={{color: '#ffffff', fontWeight:'normal'}} heading="Upcoming">
                {
                  (this.state.hastoken)?
                    (
                      (this.state.isLoading)?
                      (
                          <ScrollView style={styles.container}>
                            <View style ={styles.mainContainer}>
                            <View style={styles.loading}>
                                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
                                </View>
                            </View>
                          </ScrollView>
                      ):
                      (
                        <ScrollView style={styles.container} 
                          refreshControl={
                            <RefreshControl
                              refreshing={this.state.refreshing}
                              onRefresh={this._onRefresh}
                            />
                          }
                        >
                              {
                                (itemsupcoming===0)?
                                (
                                  <View style ={styles.mainContainer}>
                                    <View style={[styles.container, {flex:1, justifyContent:'center', flexDirection:'column'}]}>
                                      <View style ={styles.mainContainer}>
                                        <View style ={styles.content}>
                                          <Text style ={[styles.fontFamily, styles.sorrymsg ]}>Sorry, You don't have any Schedules upcoming services.</Text>
                                        </View>
                                      </View>
                                    </View>
                                  </View>
                                ):(

                                <View style ={styles.mainContainer1}>{upcomingEvents}</View>
                                
                                )
                              }
                        </ScrollView>
                      )
                    ):
                    (!this.state.isLoading)?
                    (<Container style={{flex:1,flexDirection:'row', alignItems:'center',justifyContent:'center',}}>
                    <Content  >
                        <View style ={[styles.content]}> 
                            <Text style = {[styles.fontFamilyDisplay, styles.maintext ]}>Keep track of where you’re going.</Text>
                            <Spacer size={10} />
                            <Text style={[styles.fontFamily, styles.subtext]}>Easily access all your upcoming events.</Text>
                            <Spacer size={20} />
                            <Button block style ={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Home', {replace:false})} >
                              <Text uppercase={false} style={[styles.fontFamily, styles.loginButtonText]}> Login </Text>
                            </Button>
                        </View>
                    </Content>
                  </Container>
                  ):
                    (
                      <ScrollView style={styles.container}>
                            <View style ={styles.mainContainer}>
                            <View style={styles.loading}>
                                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
                                </View>
                            </View>
                          </ScrollView>
                    )
                }
            </Tab>  
            <Tab tabStyle= { { backgroundColor: 'transparent'}}  textStyle={{color: 'rgba(255, 255, 255, 0.5)'}} activeTabStyle={{backgroundColor: 'transparent' }} activeTextStyle={{color: '#ffffff', fontWeight:'normal'}} heading="History">
            {
                  (this.state.hastoken)?
                    (
                      (this.state.isLoading)?
                      (
                          <ScrollView style={styles.container}>
                            <View style ={styles.mainContainer}>
                            <View style={styles.loading}>
                                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
                                </View>
                            </View>
                          </ScrollView>
                      ):
                      (
                        <ScrollView style={styles.container}
                          refreshControl={
                            <RefreshControl
                              refreshing={this.state.refreshing}
                              onRefresh={this._onRefresh}
                            />
                          }
                        >
                          
                              {
                                (itemshistory===0)?
                                (
                                <View style ={styles.mainContainer}>
                                  <View style={[styles.container, {flex:1, justifyContent:'center', flexDirection:'column'}]}>
                                    <View style ={styles.mainContainer}>
                                      <View style ={styles.content}>
                                        <Text style ={[styles.fontFamily, styles.sorrymsg ]}>Sorry, You don't have any Schedules history services.</Text>
                                      </View>
                                    </View>
                                  </View>
                                </View>
                              ):
                                (<View  style ={styles.mainContainer1 }>{ historyEvents }</View>)
                              }
                              
                        </ScrollView>
                      )
                    ):
                    (
                      <Container style={{flex:1,flexDirection:'row', alignItems:'center',justifyContent:'center',}}>
                        <Content  >
                            <View style ={[styles.content]}> 
                                <Text style = {[styles.fontFamilyDisplay, styles.maintext ]}> Keep track of where you’re going.</Text>
                                <Spacer size={10} />
                                <Text style={[styles.fontFamily, styles.subtext]}> Easily access all your upcoming events.</Text>
                                <Spacer size={20} />
                                <Button block style ={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Home', {replace:false})} >
                                  <Text uppercase={false} style={[styles.fontFamily, styles.loginButtonText]}> Login </Text>
                                </Button>
                            </View>
                        </Content>
                      </Container>
                    )
                }
            </Tab>
        </Tabs>
      </Container>
    );
  }
}

ScheduleScreen.navigationOptions = {
  headerLeft:null,
  headerTitle:(
    <View style ={{flex:1}}><Text style = {{textAlign:'center', fontSize:20,lineHeight:25, alignSelf:'center', color:'#ffffff',...Platform.select({ios: { fontFamily:'SFProDisplayRegular' },  android: {  fontFamily:'RobotoRegular_1' }, }), }}>My Schedules</Text></View>
  ),
    headerStyle: {
      height:50,
      elevation: 0,
      backgroundColor:'#E5E5E5',
      borderBottomWidth: 0,
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1 }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      paddingLeft: 34,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };

  
  const styles = StyleSheet.create({
    fontFamily:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },

    fontFamilyDisplay:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    maintext:{
      fontWeight:'normal',
      fontStyle:'normal', 
      fontSize:20,
      lineHeight:25, 
      textAlign:'center', 
      flex:1, 
      margin:0,
      color:'rgba(0, 0, 0, 0.87)'
    },
    subtext:{
      textAlign:'center', 
      flex:1, 
      margin:0, 
      fontSize:16,
      lineHeight:21,
      paddingBottom: 10, 
      paddingRight: 20, 
      paddingLeft: 20, 
      color:'rgba(0, 0, 0, 0.54)',
    },
    sorrymsg:{
      alignSelf:'center',
      textAlign:'center', 
      flex:1, 
      margin:0, 
      fontSize:16,
      lineHeight:21,
      paddingBottom: 10, 
      paddingRight: 20, 
      paddingLeft: 20, 
      color:'rgba(0, 0, 0, 0.54)',
    },
    ageGroupbtn:{
      marginLeft:0,
      marginRight:0,
      fontSize:10,
      lineHeight:25, 
      textAlign:'right', 
      color: '#ffffff'
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      opacity: 0.5,
      backgroundColor: 'black',
      justifyContent: 'center',
      alignItems: 'center'
  },
   
    spinnerTextStyle: {
      color: '#FFF',
    },
    mainContainer:{
      backgroundColor:'#E5E5E5',
      flex:1, 
      justifyContent: 'center',
      paddingRight:15,
      paddingLeft:15,
      marginBottom: 30,
    },
    mainContainer1:{
      backgroundColor:'#E5E5E5',
      flex:1, 
      justifyContent: 'center',
      marginBottom: 5,
    },
    container: {
      flex: 1,
      paddingTop: 10,
      paddingBottom: 100,
      backgroundColor: '#E5E5E5',
    },
    mainEventsClass:{
      marginTop:4,
      marginBottom:4, 
      borderLeftColor:'#146FA9',
      borderLeftWidth:4,
      borderRadius:4, 
      justifyContent:'center',
      // paddingTop:15,
      // paddingBottom:15,
      ...Platform.select({
        ios: {
          shadowColor: 'rgba(0, 0, 0, 0.25)',
          shadowOffset: { width: 0, height: 0 },
          shadowOpacity: 1,
        },
        android: {
          elevation: 1,
        },
      }),
    },
    buttonStyle:{
      backgroundColor:'transparent',
      borderRadius:4,
      borderWidth:1,
      flex:0.8,
      width:'90%',
      alignSelf:'center',
      borderColor:'rgba(0, 0, 0, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      elevation:0
    },
    loginButtonText:{
      color:'rgba(0, 0, 0, 0.87)',
      fontSize:17,
      lineHeight:22,
      fontStyle:'normal',
      fontWeight:'normal',
    },
    content:{
      textAlign:'center',
      alignItems:'center',
      margin:0,
      flex:1,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  });
  