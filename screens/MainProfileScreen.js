import React from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, ScrollView, Platform, TouchableHighlight } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, sliderItemWidth } from '../components/MainLocationSlider/Styles_slider';
import SliderCarousel from '../components/MainLocationSlider/Setting';
import Spacer from '../components/UI/Spacer';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

import { LinearGradient } from 'expo-linear-gradient';
import { StackActions, NavigationActions } from 'react-navigation';

class MainScreen extends React.Component {

    constructor(props) {
        super(props);
        this.config = {
          apiUrl: 'https://konnect.myfirstgym.com/api/customer/locations/nearby'
        }
        this.state = {
            locations: [],
            isLoading:true,
            locationError: false,
            locationErrorMsg: '',
            latitude:24.46555020,
            longitude: 54.38153140,
        };
    }

    fixedEncodeURIComponent(str) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    }

    async fetchBranches(data) {
        try {
          let response = await fetch(this.config.apiUrl+'?latitude='+data.lat+'&longitude='+data.long);
            let responseJson = [];
            try {
                responseJson = await response.json(); 
                this.setState({ locations: responseJson.data, isLoading:false });
            } catch (error) {
                console.log(error);
                responseJson = [];
            }
            
        } catch (error) {
            console.log(error);
        }
    }

    componentDidMount() {
      // GETTING CURRENT LOCATION BY FOLLOWING CODE
      if(Platform.OS==='ios'){
        /* navigator.geolocation.getCurrentPosition(
           position => {
             const location = JSON.stringify(position);
             const latitude = position.coords.latitude;
             const longitude = position.coords.longitude;
             
             this.fetchBranches({ lat: latitude, long: longitude});
           },
           error => {
             this.setState({ locationError: true, locationErrorMsg: error.message, isLoading:false })
           },
           { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
         )*/
         this.fetchBranches({ lat: this.state.latitude, long: this.state.longitude});
     }else if(Platform.OS==='android'){
       navigator.geolocation.getCurrentPosition(
         position => {
           const location = JSON.stringify(position);
           const latitude = position.coords.latitude;
           const longitude = position.coords.longitude;
           
           this.fetchBranches({ lat: latitude, long: longitude});
         },
         error => {
           this.setState({ locationError: true, locationErrorMsg: error.message, isLoading:false })
         },
         { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
       )
     }
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Profile' })],
      });
      this.props.navigation.dispatch(resetAction);
      this.props.navigation.push('Loading');
    }


renderListComponent = ({ item }) => (
 
  <TouchableHighlight onPress = {() => this.props.navigation.navigate('Location', {id:item.location_id})} underlayColor="white">
    <SliderCarousel title={item.name} path={item.photos[0].path} className={"cls_"+item.location_id} />
  </TouchableHighlight>
);

render() {
    return (
      <ScrollView style={styles.container}>
        <View style ={styles.mainContainer}>
            <View style ={styles.mainView}>
              <Text style = {styles.headingText}>LOCATIONS NEAR YOU</Text>
              <Spacer size={10} />
            </View>
            {
              (!this.state.isLoading)?

              (
                <View>
                  {
                    (this.state.locationError)?(
                      <Text style={{ textAlign: "center" }}> { this.state.locationErrorMsg } </Text>
                    ):(

                      <Carousel
                        containerCustomStyle={{ backgroundColor: 'white', marginLeft:0, borderRadius:4 }}
                        contentContainerCustomStyle={{ backgroundColor: '#ffffff', height: 160 }}
                        data={this.state.locations}
                        renderItem={this.renderListComponent}
                        sliderWidth={sliderWidth}
                        itemWidth={sliderItemWidth}
                        activeSlideAlignment={'start'}
                        inactiveSlideOpacity={1}
                        inactiveSlideScale={0.85}
                        loop = {true}
                      />
  
                    )
                  }
                </View>

              ) : (
                <View style={[styles.loading, {backgroundColor:'transparent'}]}>
                  <Text></Text>
                </View>
              )
            }
            
            <View style ={styles.mainView}>
              <Spacer size={10} />
              <Text style = {styles.headingText}>DISCOVER</Text>
              <Spacer size={10} />

              <TouchableHighlight onPress={() => this.props.navigation.push('Explore')} style={{width: "100%"}} >
                <View>
                  <ImageBackground
                    source={ require('../assets/images/explore.jpg')}
                    style={styles.imgBackground}
                    imageStyle={{ borderRadius: 4 }}
                  >
                    <Text style={[styles.headingText,styles.actionText]} onPress={()=>this.props.navigation.navigate('Explore')}>
                      EXPLORE
                    </Text>
                  </ImageBackground>
                </View>
              </TouchableHighlight>

              <Spacer size={10} />
              <Text style = {styles.headingText}>SCHEDULE</Text>
              
              <Spacer size={10} />

              <TouchableHighlight onPress={() => this.props.navigation.navigate('Schedule')} style={{width: "100%"}} >
                <View>
                  <ImageBackground
                      source={ require('../assets/images/schedule.jpg')}
                      style={styles.imgBackground}
                      imageStyle={{ borderRadius: 4 }}
                    >
                      <Text style={[styles.headingText, styles.actionText]} onPress={() => this.props.navigation.navigate('Schedule')} >SCHEDULE</Text>
                  </ImageBackground>
                </View>
              </TouchableHighlight>

              <Spacer size={10} />

          </View>
          </View>
        </ScrollView>
    );
}
}
MainScreen.navigationOptions = {
  headerTitle:(
    <View style ={{flex:1}}><Text style = {{textAlign:'center', fontSize:20,lineHeight:25, alignSelf:'center', color:'#ffffff',...Platform.select({ios: { fontFamily:'SFProDisplayRegular' },  android: {  fontFamily:'RobotoRegular_1' }, }), }}>Home</Text></View>
  ),
  headerLeft: null,
  gesturesEnabled: false,
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    alignSelf: 'center',
    textAlign:"center", 
    flex:1,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  headerStyle:{
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        height:60,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
};

const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#ffffff',
    flex:1, 
    justifyContent: 'center',
    paddingRight:10,
    paddingLeft:10,
    marginBottom: 30
  },
  loading: {
    opacity: 0.5,
    backgroundColor: 'black',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#ffffff',
  },
  mainView:{
    alignItems: 'center', 
  },
  headingText:{
    fontSize:17,
    lineHeight:22,
   fontFamily:'SFPro_Text_Regular',
    color:'rgba(0, 0, 0, 0.87)',
    fontStyle:'normal',
    fontWeight:'normal',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing:-0.41
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  imgBackground:{
    height: 160,
    width: '100%',
    position: 'relative', // because it's parent
    textAlign:'center',
    alignContent:'center',
    alignItems:'center',
    borderRadius:4
  },
  actionText:{
    position: 'absolute', // child
    bottom: 10, // position where you want
    textAlign:'center',
    paddingTop:8,
    paddingBottom:8,
    paddingLeft:20,
    paddingRight:20,
    borderWidth:1,
    borderColor:'white',
    backgroundColor:'white',
    color:'#014863',
    fontSize: 17,
    lineHeight:22,
    fontStyle:'normal',
    fontWeight:'normal',
    borderRadius: 3,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1',
        fontWeight:'600'
      },
    }),
  },
  adStyle:{
    backgroundColor:'#cccccc',
    padding:20,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    width:'100%',
  },
  imgStyle:{
    borderRadius:0.5,
    width:'100%', 
    position: 'relative',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },

});

export default MainScreen;