import React, { Component }  from 'react';
import { TouchableOpacity, TextInput, View, StyleSheet, Image, ScrollView, Platform, TouchableHighlight, RefreshControl } from 'react-native';
import { Text, Container, Content } from 'native-base';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, sliderItemWidth, sliderItemHorizontalMargin } from '../components/ExploreSlider/Styles_slider';
import SliderCarousel from '../components/ExploreSlider/Setting';
import Spacer from '../components/UI/Spacer';
import axios from 'axios';
import { LinearGradient } from 'expo-linear-gradient';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

class ExploreScreen extends Component {

  constructor(props){
    super(props);
    this.config={
      AddExploreAPI:'https://konnect.myfirstgym.com/api/explore'
    }
    this.state={
      exploreData :{},
      searchedData: [],
      searchedString: '',
      latitude:24.46555020,
      longitude: 54.38153140,
      isLoading:true,
      isSearched:false,
      SearchisLoading:true,
      refreshing:false,
      serviceId:0,
      serviceName:'classes',
    }
  }
  fetchSearchedExplore = async() =>  {
    if(this.state.searchedString.length>0){
    try {
        let explrSrchResponse = await fetch("https://konnect.myfirstgym.com/api/search?search="+this.state.searchedString);
        let explrSrchResponseJson = [];
        try {
            explrSrchResponseJson = await explrSrchResponse.json();
        } catch (error) { 
            explrSrchResponseJson = [];           
        }

        if( explrSrchResponseJson != null){
          this.setState({searchedData: explrSrchResponseJson, isLoading:false, isSearched : true});
          if(this.state.searchedData['locations'] || this.state.searchedData['services']){
              if(this.state.searchedData['locations'].length>0 || this.state.searchedData['services'].length>0){
              this.setState({SearchisLoading:false});
            }
            else{
              this.setState({SearchisLoading:true });
            }
             
          }
          else{
            this.setState({SearchisLoading:true });
          }
          
        }else{
          this.setState({ isLoading:false, isSearched : false });
        }

    } catch (error) {
      alert(error)
        console.log(error);
    }
  }
    else{
      alert("Please add some text in searchbox first.");
    }
  }
  fetchExplore = async(lat, long) =>  {
    let self = this;
      let response = axios.get(this.config.AddExploreAPI+'?latitude='+lat+'&longitude='+long)
      .then(function (response) {
        self.setState({exploreData:response.data, isLoading:false})
      })
      .catch(function (error) {
        alert(error)
        console.log(error);
      });
  }
  componentDidMount() {
    //initialize your function
    this.props.navigation.setParams({ onClickSearched: this.fetchSearchedExplore, onTextInputChangeSearch : this.searchInputOnChange});

    // GETTING CURRENT LOCATION BY FOLLOWING CODE

    if(Platform.OS==='ios'){
      /* navigator.geolocation.getCurrentPosition(
         navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
          
          this.setState({ latitude: latitude });
          this.setState({ longitude: longitude });
      },
      error => {
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
       )*/
       this.fetchExplore(this.state.latitude, this.state.longitude);
   }else if(Platform.OS==='android'){
    navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
          
          this.setState({ latitude: latitude });
          this.setState({ longitude: longitude });
      },
      error => {
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );

    this.fetchExplore(this.state.latitude, this.state.longitude);
   }
  }

  _onRefresh = () => {
    this.setState({refreshing: true});

    if(this.state.isSearched && this.state.searchedString.length>0){
      this.fetchSearchedExplore().then(() => {
        this.setState({refreshing: false});
      });
    }else{
      this.fetchExplore(this.state.latitude, this.state.longitude).then(() => {
        this.setState({refreshing: false});
      });
    }
  
  }

  searchInputOnChange = async (event) => {
    this.setState({searchedString: event.nativeEvent.text});
  }

  onClickSearched() {
    
    if(this.state.searchedString.length>0){
      this.setState({ isLoading: true });
      this.fetchSearchedExplore();
    }
    else{
      alert("Please add some text in searchbox first");
    }
    
  }

  renderListComponentLocation = ({ item }) => (
    <TouchableHighlight onPress = {() => this.props.navigation.navigate('Location', {id:item.id})} underlayColor="white" style={{  height: "100%" }} >
      <SliderCarousel title={item.name} path={item.photo} className = {'location'} />
    </TouchableHighlight>
  );

  renderListComponent = ({ item }) => (
    <TouchableHighlight onPress = {() => this.props.navigation.navigate('ExploreServices', {serviceCatId:item.id, serviceCatName:item.name})} underlayColor="white" style={{  height: "100%" }} >
      <SliderCarousel title={item.name} path={item.photos[0].path} className = {'others'} />
    </TouchableHighlight>
  );
  
  //FOLLOWING COMPONENT USING FOR UNDEFIEND SERVICES ID'S 
  renderListComponentForUndIDs = ({ item }) => (
      <SliderCarousel title={item.name} path={item.photos[0].path} className = {'others'} />
  );


  
  render() {

    let newExplorer = [];
    let SearchedItem =[];
    let newSearchitem = [];
    let itemLocationLength = ''

    let exploreData = Object.keys(this.state.exploreData).map((item, i) =>{
    let itemsexplore=  itemsexplore+1;
    
    
    if(item==='locations'){
      
      itemLocationLength = (this.state.exploreData[item]).length

      return(
      <View key ={i}>
        
        <Spacer size={10} />

        <View style={styles.listHead}>
          <View style={{ flex:1 }}>

            <View style={[{ alignSelf: "center" }, styles.iosZindex]}>
              <Text style={styles.eventName}>{item}</Text>          
            </View>
            
            <View style={[{ alignSelf: "flex-end"}, styles.iosZindex, styles.iosZindex1]}>              
              <Text style={[styles.viewStyle, { paddingTop: 1 }]} onPress = {() => this.props.navigation.navigate('ExploreViewAll', {service_id: 1, service_name: 'AllLocations' })}>
                View All <Image  style = {[styles.iconStyle ]} source = {require('../assets/images/arrow.png')} />
              </Text>

            </View>

          </View>
        </View>

        <Spacer size={10} />
        
        <View>
          <Carousel
            contentContainerCustomStyle={{ overflow: 'hidden', width: sliderItemWidth * (itemLocationLength) + (17), height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
            data={this.state.exploreData[item]}
            renderItem={this.renderListComponentLocation}
            sliderWidth={sliderWidth}
            itemWidth={sliderItemWidth}
            activeSlideAlignment={'start'}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
          />
        </View>
        
        <Spacer size={15} />

      </View>
      )
    }else{
      
      let itemsLength = ''

      newExplorer= this.state.exploreData[item].map((data, p)=>{

        itemsLength = (data.items).length

      return( 
        <View key={p}>
        <Spacer size={10} />
        <View style={styles.listHead}>
          <View style={{ flex:1 }}>

            <View style={[{ alignSelf: "center" }, styles.iosZindex]}>
              <Text style={styles.eventName}>{data.name}</Text>
            </View>

            <View style={[{ alignSelf: "flex-end"}, styles.iosZindex, styles.iosZindex1]}>
              {(data.id === undefined)?
                (
                  <Text style={[styles.viewStyle, { justifyContent: "center" }]} >View All <Image  style = {[styles.iconStyle ]} source = {require('../assets/images/arrow.png')} /></Text>
                ):(
                  <Text style={[styles.viewStyle, { justifyContent: "center" }]} onPress={() => this.props.navigation.navigate('ExploreViewAll', {service_id: data.id, service_name: data.name })} >
                    View All <Image  style = {[styles.iconStyle]} source = {require('../assets/images/arrow.png')} />
                  </Text>
                )
              }

            </View>

          </View>          
        </View>

        <Spacer size={10} />

        <View >

          {(data.id === undefined)?
            (
              <Carousel
                contentContainerCustomStyle={{ overflow: 'hidden', width: sliderItemWidth * (itemsLength) + (17), height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
                data={data.items}
                renderItem={this.renderListComponentForUndIDs}
                sliderWidth={sliderWidth}
                itemWidth={sliderItemWidth}
                activeSlideAlignment={'start'}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
              />
            ):(
              <Carousel
                contentContainerCustomStyle={{ overflow: 'hidden', width: sliderItemWidth * (itemsLength) + (17), height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
                data={data.items}
                renderItem={this.renderListComponent}
                sliderWidth={sliderWidth}
                itemWidth={sliderItemWidth}
                activeSlideAlignment={'start'}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
              />
            )
          }
      </View>
      
      <Spacer size={15} />
        
      </View>
      )
    });
    }
   })

   SearchedItem = Object.keys(this.state.searchedData).map((item, i) =>{
     
    let itemsexplore=  itemsexplore+1;

    if(item==='locations'){
      
      itemLocationLength = (this.state.searchedData[item]).length

      return(
        <View key ={"id="+i}>
        <Spacer size={10} />

        <View style={styles.listHead}>
          <View style={{ flex:1 }}>

            <View style={[{ alignSelf: "center" }, styles.iosZindex]}>
              <Text style={styles.eventName}>{item}</Text>          
            </View>
            
            <View style={[{ alignSelf: "flex-end"}, styles.iosZindex, styles.iosZindex1]}>
              <Text style={[styles.viewStyle, { paddingTop: 1 }]} onPress = {() => this.props.navigation.navigate('ExploreViewAll', {service_id: 1, service_name: 'AllLocations' })}>
                View All <Image  style = {[styles.iconStyle ]} source = {require('../assets/images/arrow.png')} />
              </Text>

            </View>

          </View>
        </View>

        <Spacer size={10} />

        <View>
          <Carousel
            contentContainerCustomStyle={{zIndex:1, overflow: 'hidden', width: sliderItemWidth * (itemLocationLength) + (17), height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
            data={this.state.searchedData[item]}
            renderItem={this.renderListComponentLocation}
            sliderWidth={sliderWidth}
            itemWidth={sliderItemWidth}
            activeSlideAlignment={'start'}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
          />
        </View>
        <Spacer size={15} />
      </View>
      )
    }else{
      
      let itemsLength = ''

      newSearchitem= this.state.searchedData[item].map((data, p)=>{
        
        itemsLength = (data.items).length

      return( 
        <View key={p}>
        <Spacer size={10} />
        
        <View style={styles.listHead}>
          <View style={{ flex:1 }}>

            <View style={[{ alignSelf: "center" }, styles.iosZindex]}>
              <Text style={styles.eventName}>{data.name}</Text>
            </View>

            <View style={[{ alignSelf: "flex-end"}, styles.iosZindex, styles.iosZindex1]}>
              {(data.id === undefined)?
                (
                  <Text style={[styles.viewStyle, { justifyContent: "center" }]} >View All <Image  style = {[styles.iconStyle ]} source = {require('../assets/images/arrow.png')} /></Text>
                ):(
                  <Text style={[styles.viewStyle, { justifyContent: "center" }]} onPress={() => this.props.navigation.navigate('ExploreViewAll', {service_id: data.id, service_name: data.name })} >
                    View All <Image  style = {[styles.iconStyle]} source = {require('../assets/images/arrow.png')} />
                  </Text>
                )
              }

            </View>

          </View>          
        </View>

        <Spacer size={10} />

        <View>
          {(data.id === undefined)?
            (
              <Carousel
                contentContainerCustomStyle={{ overflow: 'hidden', width: sliderItemWidth * (itemsLength) + (17), height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
                data={data.items}
                renderItem={this.renderListComponentForUndIDs}
                sliderWidth={sliderWidth}
                itemWidth={sliderItemWidth}
                activeSlideAlignment={'start'}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
              />
            ):(
              <Carousel
                contentContainerCustomStyle={{ overflow: 'hidden', width: sliderItemWidth * (itemsLength) + (17), height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
                data={data.items}
                renderItem={this.renderListComponent}
                sliderWidth={sliderWidth}
                itemWidth={sliderItemWidth}
                activeSlideAlignment={'start'}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
              />
            )
          }
        </View>

        <Spacer size={15} />
      </View>
      )
    });
    }
    
   })

    return (
        <Container>
          <ScrollView style={styles.container}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View style ={styles.mainContainer}>
              { (!this.state.isLoading) ? (
                  (this.state.isSearched && this.state.searchedString.length>0)?
                  ( (!this.state.SearchisLoading) ?
                    (
                        <View>
                        {SearchedItem}
                        {newSearchitem}
                      </View>  
                    )
                   :
                   (
                    <View style={[{flex:1, justifyContent:'center', flexDirection:'column', height:'100%', textAlignVertical:'center'}]}>
                    <Text style ={[styles.fontFamily, styles.sorrymsg, {textAlignVertical:'center', textAlign:'center', alignSelf:'center'} ]}>Don't found any Item.</Text>
                  </View>
                   )
                   ):
                  (<View>
                    {exploreData}
                    {newExplorer}
                  </View>)
                      
                ) : (
                  
                    <View style={styles.loading}>
                      <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
                    </View>
                  
                )
              }
            </View>
          </ScrollView>
        </Container>
    );
  }

    
  static navigationOptions = ({navigation}) => ({
    headerTitle: (  
      <View style={{alignContent:'center', justifyContent:'center', marginBottom:10, flex:1, position:'absolute', width:'90%', marginLeft:'5%'}}>

        <View style = {{flex:1, alignItems:'center', marginTop:0, marginBottom:10}}>
          <Text style={{fontSize:20, lineHeight:25, alignSelf: 'center', textAlign:"center", color:'#ffffff'}}>
            Explore
          </Text>
        </View>

        <View style = {{flex:1, alignItems:'center', backgroundColor: "rgba(255, 255, 255, 0.3)", borderRadius: 4, height: 45, marginLeft: 16, marginRight: 16 }}>

            <TextInput underlineColorAndroid = "transparent" placeholder = "Search" placeholderTextColor = "#ffffff"  style={{ textAlign: 'center', width:"100%", color: "#ffffff", fontSize:16,lineHeight:21 , paddingTop:10, paddingBottom:10, paddingRight:15, paddingLeft:15, height:'100%',  backgroundColor:'rgba(255, 255, 255, 0.3)',  ...Platform.select({ios: { fontFamily:'SFProTextRegular'  }, android: {  fontFamily:'RobotoRegular_1' }, }), }} onChange={navigation.getParam('onTextInputChangeSearch')}/>

            <TouchableOpacity onPress={navigation.getParam('onClickSearched')} style={{ width: 33, alignSelf: "flex-end", position: "relative", top: -32 }} >

              <Image style={{ width: 20, height: 20 }} source={require('../assets/images/searchIcon.png')} />

            </TouchableOpacity>

        </View>

      </View>
    ),
    headerLeft:null,
    headerRight:null,
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          elevation: 2,
          ...Platform.select({
            ios:{
              borderRadius: 15,
              //height:130,
              marginTop:-15
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerStyle:{ 
      height:120,
      backgroundColor:'#E5E5E5',
      elevation: 0,
      borderBottomWidth: 0
    },
    headerTintColor: 'transparent',
    headerTitleStyle: {
      fontWeight: 'bold',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      position: "absolute",
      top: 32,
      left: 80,
    },

  });

}


const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#E5E5E5',
    flex:1, 
    justifyContent: 'center',
    paddingRight:10,
    paddingLeft:10,
    marginBottom: 10
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
},
iosZindex:{
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular',
      zIndex:999, 
      position:'absolute'
    },
    android: {
      fontFamily:'RobotoRegular_1',
      zIndex:999, 
    },
  }),
},
iosZindex1:{
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular',
      zIndex:9999, 
      position:'absolute'
    },
    android: {
      fontFamily:'RobotoRegular_1',
      position:'absolute',
    },
  }),
},
fontFamily:{
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
fontFamilyDisplay:{
  ...Platform.select({
    ios: {
      fontFamily:'SFProDisplayRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
sorrymsg:{
  alignSelf:'center',
  textAlign:'center', 
  flex:1, 
  margin:0, 
  fontSize:16,
  lineHeight:21,
  paddingBottom: 10, 
  paddingRight: 20, 
  paddingLeft: 20, 
  color:'rgba(0, 0, 0, 0.54)',
},
  spinnerTextStyle: {
    color: '#FFF',
  },
  content:{
    textAlign:'center',
    alignItems:'center',
    margin:0,
    flex:1,

},
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#E5E5E5',
  },
  eventName:{
    fontSize:17, 
    lineHeight:22,
    color:'#565656',
    alignSelf:'flex-start',
    textTransform: 'capitalize',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
      },
      android: {
        fontFamily:'RobotoRegular_1',
      },
    }),
  },
  viewStyle:
  {
    alignSelf:'flex-end',
    color:'#565656',
    marginTop:3,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        fontSize:13,
        lineHeight: 18
      },
      android: {
        fontFamily:'RobotoRegular_1',
        fontSize:12,
        lineHeight: 16
      },
    }),
  },
  iconStyle:{
    width:10, 
    height:13, 
    alignSelf:'flex-end',
  },
  listHead:
  {
    flexDirection:'row', 
    flexWrap:'wrap', 
    flex:1, 
    paddingLeft:10, 
    paddingRight:0, 
    paddingBottom:20,
    height: 10,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },

  secondcolumn:{
    flex:0.4,
    textAlign: "right",
    paddingRight: 5,
    marginTop: 2
  },
  thirdcolumn:{
    flex:0.05,
    justifyContent:'flex-end',
    marginTop: -1,
    paddingBottom: 3,
  },

});
export default ExploreScreen;
