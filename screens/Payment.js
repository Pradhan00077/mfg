import React from 'react';
import {ActivityIndicator, View, StyleSheet , Image, TouchableHighlight, ImageBackground, ScrollView, Platform, AsyncStorage, RefreshControl } from 'react-native';
import {Text} from 'native-base';
import Spacer from '../components/UI/Spacer';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, sliderItemWidth } from '../components/ExploreSlider/Styles_slider';
import SliderCarousel from '../components/PaymentCardSlider/Setting';
import Hr from '../components/UI/hr1';
import { LinearGradient } from 'expo-linear-gradient';
class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.config={
      SavedCardsListAPI:'https://konnect.myfirstgym.com/api/customer/credit-cards'
    }
    this.state = {
      fetchCardsList: [],
      refreshing:false,
      isLoading: true
  } 
}

renderListComponent = ({ item }) => (
 // <SliderCarousel title={item.card_no} path={item.path} className={item.exp_date} />

 <View style={{ flex:1 }}>
    <TouchableHighlight onPress={() => this.props.navigation.navigate('payByexistingcard', {cardId: item.id})} underlayColor="transparent">
      <ImageBackground source={require('../assets/images/creditcard.png')} style = {{width:'100%',minHeight:175, flexGrow:1,borderRadius:12}} resizeMode="stretch">
        <Spacer size={80} />

        <View style ={{marginLeft:15, flexDirection:'row',flexWrap:'wrap' , flex:1}}>
          <View style ={{flexDirection:'row' , flex:1, position:'absolute'}}>
              <Text style ={[styles.ExpdateText]} >Exp   {item.exp_month+"/"+ item.exp_year.substr(item.exp_year.length - 2)}</Text>
          </View>
          <Text style ={[styles.cardNumberText]} >....  ....  ....  </Text>
          <Text style ={[styles.balanceText]} >{item.card_number.substr(item.card_number.length - 4)}</Text>
        </View>

      </ImageBackground>
    </TouchableHighlight>
    </View>

);

  
  //FETCHING THE SAVED CARDS DETAILS
  async fetchCardsList(api_token) {
    let responseJson = [];

    try{
        let response = await fetch(this.config.SavedCardsListAPI+'?api_token='+api_token);
        try {
          responseJson = await response.json();
          this.setState({ fetchCardsList: responseJson.data, isLoading:false });
        }catch (error){
          alert(error)
          console.log(error);
        }
    }catch (error){
      alert(error);
      console.log(error);
    }
  }

componentDidMount() {

  //initialize your function
  AsyncStorage.getItem('api_token').then((token) => {
    if(token!==null){
      this.fetchCardsList(token); 
    }else{
      this.setState({isLoading:false});
    }
  })

}
_onRefresh = () => {
  this.setState({refreshing: true});

  AsyncStorage.getItem('api_token').then((token) => {
    if(token!==null){
      this.fetchCardsList(token).then(() => {
        this.setState({refreshing: false});
      }); 
    }else{
      this.setState({isLoading:false});
    }
  })
}

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loading}>
          <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
        </View>
      )
    } else{
      return (
        <ScrollView style={styles.container}>
          <View style ={styles.mainContainer}>
          <Spacer size={20} />
        <View style = {{flex:1}}>
          <View style ={styles.headingView} >
              <Text style={[styles.smallFont, styles.textFonts]} >Total amount to be paid</Text>
              <Spacer size={10} />
              <Text style={[styles.textHeading, styles.textFonts]} >AED { parseFloat(this.params.payMoney).toFixed(2)}</Text>
          </View>

          {
            (this.state.fetchCardsList != null)?(
              <View>
                <Spacer size={30} />
                  <View style ={styles.headingView} >
                    <Hr text="SAVED CARDS" style = {[ styles.textFonts, {textTransform:'uppercase', fontSize: 12, lineHeight: 14, color: "#666666" }]} />
                  </View>
                <Spacer size={30} />
              </View>              
              ):(
              <View></View>
            )
          }
          
          <View style = {{paddingLeft:10}}>
            {(this.state.fetchCardsList != "")?
              (
                <View style = {styles.rootView}>
                  <View>
                    <Carousel
                      //contentContainerCustomStyle={{ height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
                      data={this.state.fetchCardsList}
                      renderItem={this.renderListComponent}
                      sliderWidth={sliderWidth}
                      itemWidth= {sliderItemWidth*2}
                      activeSlideAlignment={'start'}
                      inactiveSlideOpacity={1}
                      inactiveSlideScale={0.90}
                    />
                  </View>
                </View>
              ):(
                <View style={{ minHeight:22 }}></View>
              )
            }
          </View>

          <Spacer size={30} />
            <Hr text="OTHER PAYMENT MODES" style = {[ styles.textFonts, {textTransform:'uppercase', fontSize: 12, lineHeight: 14, color: "#666666"}]} />
          <Spacer size={30} />
          <TouchableHighlight onPress={() => this.props.navigation.navigate('Paymentcard')} underlayColor="#cccccc">
          <View style={styles.listItemsDesign}>
              <View style={styles.serviceImage} ><Image  style = {{width:20, height:16}} source = {require('../assets/images/dc_card.png')} /></View>
              <View style={styles.serviceName}><Text style={[styles.textFont, styles.textFonts, {fontSize: 16, lineHeight: 28, color: "rgba(0, 0, 0, 0.87)"}]}>Debit/Credit Card</Text></View>
              <View style={styles.serviceMoney}><Image  style = {{width:8, height:10}} source = {require('../assets/images/arrow.png')} /></View>
          </View>
          </TouchableHighlight>
          <View style = {[styles.listItemsDesign]}>
              <View style={styles.serviceImage} ><Image  style = {{width:20, height:16}} source = {require('../assets/images/net_banking.png')} /></View>
              <View style={styles.serviceName}><Text style={[styles.textFont, styles.textFonts, {fontSize: 16, lineHeight: 28, color: "rgba(0, 0, 0, 0.87)"}]}>Net Banking</Text></View>
              <View style={styles.serviceMoney}><Image  style = {{width:8, height:10}} source = {require('../assets/images/arrow.png')} /></View>
          </View>
          <View style = {[styles.listItemsDesign]}>
              <View style={styles.serviceImage} ><Image  style = {{width:20, height:16}} source = {require('../assets/images/paypal.png')} /></View>
              <View style={[styles.serviceName, {borderBottomWidth:0}]}><Text style={[styles.textFont, styles.textFonts, {fontSize: 16, lineHeight: 28, color: "rgba(0, 0, 0, 0.87)"}]}>Paypal</Text></View>
              <View style={[styles.serviceMoney, {borderBottomWidth:0}]}><Image  style = {{width:8, height:10}} source = {require('../assets/images/arrow.png')} /></View>
          </View>
          <Spacer size={30} />
       </View>
       </View>
       </ScrollView>
  
  );
    }

}
}

Payment.navigationOptions = {
  title: 'Payment Method',
  headerStyle: {
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height:60,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            //height:130,
            marginTop:-15
          }
        }),
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8 ,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 0.38
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
};

const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#ffffff',
    flex:1, 
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 60,
    backgroundColor: '#ffffff',
  },
  rootView:{
      borderRadius:12,
      position:'relative',
      alignSelf:'center',
      color:'#ffffff',
      paddingBottom:15,
      flex:1,
      flexDirection:'row',
      flexWrap:'wrap',
  },
  
  headingView:{
    flexDirection:'row', 
    flexWrap:'wrap', 
    textAlign:'center',
    alignContent:'center',
    alignItems:'center',
    margin:0,
    flex:1,
  },
  textHeading:{
    width:'100%',
    textAlign:'center',
    fontSize:24,
    color:'rgba(0, 0, 0, 0.87)',
    ...Platform.select({
      ios: {
        lineHeight:29,
      },
      android: {
        lineHeight:28,
      },
    }),
  },
  smallFont:{
    fontSize:12,
    lineHeight:14,
    width:'100%',
    textAlign:'center',
    color:'#666666',
    ...Platform.select({
      ios: {
        letterSpacing: 0.38
      },
    }),
  },
  listItemsDesign:{
    paddingLeft:15,
    paddingRight:15,
    backgroundColor:'#ffffff',
    flexWrap:'wrap',
    flexDirection:'row',
    paddingTop:15,
    paddingBottom:15,
    borderColor:'#D1D1D1',
  },
  serviceImage:{flex:0.1,paddingTop:4, marginRight:10, paddingBottom:8},
  serviceName:{flex:0.8, alignItems:'flex-start', borderBottomWidth:1, borderBottomColor:'#D1D1D1', paddingBottom:8, marginRight:0},
  serviceMoney:{flex:0.1, textAlign:'right', alignItems:'flex-end', fontSize:13, marginLeft:10, paddingTop:8, borderBottomWidth:1, borderBottomColor:'#D1D1D1', paddingBottom:8, marginLeft:0},
  marginTopData:{
    marginTop:15,
    marginRight:10
  },
  textFont:{
    fontSize:16,
    lineHeight:20,
    color:'rgba(0, 0, 0, 0.87)',
  },
  imgStyle:{
    borderRadius:15,
    width:'100%', 
    position: 'relative',
    minHeight:200,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  textFonts: {
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },

  ExpdateText:{
    fontSize:16,
    lineHeight: 24,
    textAlign:'right',
    color:'#ffffff',
    ...Platform.select({
      ios: {
        fontWeight:"600",
        letterSpacing: 0.1
      },
      android: {
        fontWeight:"normal"
      },
    }),
    paddingLeft: 10
  },
  balanceText:{
      fontSize:18,
      lineHeight: 24,
      textAlign:'right',
      color:'#ffffff',
      paddingTop:28,
      paddingLeft: 0
    },
    cardNumberText:{
      fontSize:36,
      textAlign:'right',
      color:'#ffffff',
      marginRight: 6,
      paddingTop: 1,
      paddingLeft: 10
    },

});
export default Payment;
