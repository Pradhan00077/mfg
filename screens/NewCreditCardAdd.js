import React from 'react';
import PropTypes from 'prop-types';
import {
  Container, Content, Text, Form, Item, Input, Button, View, Label } from 'native-base';
import { StyleSheet, Platform, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Messages from '../components/UI/Messages';
import Spacer from '../components/UI/Spacer';
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';


class NewCreditCard extends React.Component {
  static propTypes = {
    member: PropTypes.shape({
      email: PropTypes.string,
    }),
    error: PropTypes.string,
    success: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    onFormSubmit: PropTypes.func.isRequired,
  }

  static defaultProps = {
    success: null,
    error: null,
    member: {},
  }

  constructor(props) {
    super(props);
    this.state = {

      cc_Number: "",
      cvv_Number: "",
      name: "",
      cc_Month: "01",
      cc_Year: new Date().getFullYear(),
      billing_Address: "",
      city: "",
      postal_Code: "",
      validated_Account_Password: "",
      
      cc_Number_error: "",
      cvv_Number_error: "",
      name_error: "",
      billing_Address_error: "",
      city_error: "",
      postal_Code_error: "",
      validated_Account_Password_error: "",

      modalVisible: false,
      isLoading: false,
      isError: false,
    };

    this.config = {
      cardAddAPI: "https://konnect.myfirstgym.com/api/customer/credit-cards"
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
  }

  //CALLING COMPONENT ON BLUR FIELD
  async handleOnBlurChange(name) {
  
    if(name === "cc_Number"){
      if(this.state.cc_Number.length < 10){
        this.setState({ cc_Number_error: "Minimum 10 digit should be.", isError: true })
      }else{
        this.setState({ cc_Number_error: "" })
      }
    }else if(name === "cvv_Number"){
      if(this.state.cvv_Number.length < 3){
        this.setState({ cvv_Number_error: "Minimum 3 digit should be.", isError: true })
      }else{
        this.setState({ cvv_Number_error: "" })
      }
    }else if(name === "name"){
      if(this.state.name.length < 3){
        this.setState({ name_error: "Minimum 3 character should be.", isError: true })
      }else{
        this.setState({ name_error: "" })
      }
    }else if(name === "billing_Address"){
      if(this.state.billing_Address.length < 3){
        this.setState({ billing_Address_error: "Minimum 3 character should be.", isError: true })
      }else{
        this.setState({ billing_Address_error: "" })
      }
    }else if(name === "city"){
      if(this.state.city.length < 3){
        this.setState({ city_error: "Minimum 3 character should be.", isError: true })
      }else{
        this.setState({ city_error: "" })
      }
    }else if(name === "postal_Code"){
      if(this.state.postal_Code.length < 3){
        this.setState({ postal_Code_error: "Minimum 3 digit should be.", isError: true })
      }else{
        this.setState({ postal_Code_error: "" })
      }
    }

  }

  // CALL COMPONENT ON CHANGE TEXT
  handleChange = (name, val) => {
    this.setState({ [name]: val, isError: false, [name+"_error"]: "" })
  }

  handleSubmit = () => {
     const { onFormSubmit } = this.props;

    return onFormSubmit(this.state)
      .then(() => setTimeout(() => { Actions.pop(); Actions.login(); }, 1000))
      .catch(() => {});
  }

  // cc_Number name cc_Expiration billing_Address city  postal_Code validated_Account_Password
  
  async addNewCard(token) {

    let self = this;
    let responseJson = [];
    let errormessage1 = "";

    let response = await fetch(this.config.cardAddAPI+"?api_token="+token, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        api_token: token,
        card_holder_name: this.state.name,
        card_number: this.state.cc_Number,
        exp_month: this.state.cc_Month,
        exp_year: this.state.cc_Year,
        cvv: this.state.cvv_Number,
        billing_address: this.state.billing_Address,
        city: this.state.city,
        postal_code: this.state.postal_Code
      })
    })

    try {

      responseJson = await response.json();
      
      if( responseJson.hasOwnProperty('errors') ){

        if ( responseJson.errors.hasOwnProperty('card_number') ){

          errormessage1 = responseJson.errors.card_number[0];
          self.setState({ isError: true, isLoading: false, cc_Number_error: errormessage1, postal_Code_error: "", city_error: "", billing_Address_error: "", name_error: "", cvv_Number_error: ""})

        }else if ( responseJson.errors.hasOwnProperty('cvv') ){

          errormessage1 = responseJson.errors.cvv[0];
          self.setState({ isError: true, isLoading: false, cvv_Number_error: errormessage1, postal_Code_error: "", city_error: "", billing_Address_error: "", name_error: "", cc_Number_error: ""})

        }else if ( responseJson.errors.hasOwnProperty('card_holder_name') ){

          errormessage1 = responseJson.errors.card_holder_name[0];
          self.setState({ isError: true, isLoading: false, name_error: errormessage1, postal_Code_error: "", city_error: "", billing_Address_error: "", cvv_Number_error: "", cc_Number_error: ""})

        }else if ( responseJson.errors.hasOwnProperty('billing_address') ){

          errormessage1 = responseJson.errors.billing_address[0];
          self.setState({ isError: true, isLoading: false, billing_Address_error: errormessage1, postal_Code_error: "", city_error: "", name_error: "", cvv_Number_error: "", cc_Number_error: ""})

        }else if ( responseJson.errors.hasOwnProperty('city') ){

          errormessage1 = responseJson.errors.city[0];
          self.setState({ isError: true, isLoading: false, city_error: errormessage1, postal_Code_error: "", billing_Address_error: "", name_error: "", cvv_Number_error: "", cc_Number_error: ""})

        }else if ( responseJson.errors.hasOwnProperty('postal_code') ){

          errormessage1 = responseJson.errors.postal_code[0];
          self.setState({ isError: true, isLoading: false, postal_Code_error: errormessage1, city_error: "", billing_Address_error: "", name_error: "", cvv_Number_error: "", cc_Number_error: ""})

        }else{
          self.setState({ isError: false, isLoading: false, postal_Code_error: "", city_error: "", billing_Address_error: "", name_error: "", cvv_Number_error: "", cc_Number_error: "" })
        }

      }else{
        
        self.setState({ isError: false, isLoading: false, postal_Code_error: "", city_error: "", billing_Address_error: "", name_error: "", cvv_Number_error: "", cc_Number_error: "" })

        // USING FUNCTION TO REDIRECT ON CREDIT CARD
        this.redirectScreenToCredit()
      }

    } catch (error) {

      console.log("Response error - ",error)
      this.setState({isLoading:false})

    }

  }

  // ON CLICK OF SUBMIT BUTTON 
  addNewCardSubmitBtn() {

    if(this.state.cc_Number.length < 10){
      this.setState({ cc_Number_error: "Minimum 10 digit should be.", isError: true })
    }else if(this.state.cvv_Number.length < 3){
      this.setState({ cvv_Number_error: "Minimum 3 digit should be.", isError: true })
    }else if(this.state.name.length < 3){
      this.setState({ name_error: "Minimum 3 character should be.", isError: true })
    }else if(this.state.billing_Address.length < 3){
      this.setState({ billing_Address_error: "Minimum 3 character should be.", isError: true })
    }else if(this.state.city.length < 3){
      this.setState({ city_error: "Minimum 3 character should be.", isError: true })
    }else if(this.state.postal_Code.length < 3){
      this.setState({ postal_Code_error: "Minimum 3 digit should be.", isError: true })
    }else{
      this.setState({ cc_Number_error: "", cvv_Number_error: "", name_error: "", billing_Address_error: "", city_error:"", postal_Code_error: "" })
      
      this.setState({isLoading:true});
      //initialize your function
      AsyncStorage.getItem('api_token').then((token) => {
        if(token!==null){
          this.addNewCard(token);
        }else{
          this.setState({isLoading:false});
        }
      })

    }
    
    
  }

  
  // USING FUNCTION TO REDIRECT ON CREDIT CARD
  redirectScreenToCredit() {
    this.props.navigation.navigate('Creditcard');
  }


  render() {
    const { loading, error, success } = this.props;
    const { email } =this.state;

    let month = [
      { value: '01' },
      { value: '02' },
      { value: '03' },
      { value: '04' },
      { value: '05' },
      { value: '06' },
      { value: '07' },
      { value: '08' },
      { value: '09' },
      { value: '10' },
      { value: '11' },
      { value: '12' },
    ];
        
    let year = [];
    let currentYear = new Date().getFullYear();
    for (let i = 0; i < 5; i++) {
      year.push({
          value: currentYear+i,
      });
    }

    return (
      <Container>
         <View style ={{flex:1, textAlign:'center', backgroundColor:'#ffffff'}}>

          <Content>

            <Spacer  size = {20}/>

              <View style ={{width:'100%', height:30, color:'#959595',backgroundColor:'#EEEEEE', textAlign:'center', justifyContent:'center'}}>
                <Text style={[ styles.labelName, styles.newCreditCardLable]}>NEW CREDIT CARD</Text>
              </View>

            {/* <Spacer  size = {10}/> */}

          <View style = {{position:'relative', alignSelf:'center',padding:15, width:'100%', }}>

            <Spacer  size = {5}/>

            {error && <Messages message={error} />}
            {success && <Messages type="success" message={success} />}

            <Form>

              <View style={{flexDirection:'row', flexWrap:'wrap', flex:1, fontSize:15}}>

                <View style={{ width:"100%" }}>
                  <View style={{ flexDirection:'row', flexWrap:'wrap', flex:1 }}>

                    <View style={{ flex:0.7, marginRight: 10 }}>
                      <TextField
                        label='CC Number'
                        keyboardType="numeric"
                        autoCapitalize="none"
                        maxLength={16}
                        minLength={10}
                        onChangeText={v => this.handleChange('cc_Number', v)}
                        onBlur={() => this.handleOnBlurChange('cc_Number')}
                        style={[ styles.labelName ]}
                        tintColor = "rgba(0, 0, 0, 0.87)"
                        baseColor = "rgba(0, 0, 0, 0.87)"
                        error	= {this.state.cc_Number_error}
                      />
                    </View>

                    <View style={{ flex:0.3 }}>
                      <TextField
                        label='CVV'
                        keyboardType="numeric"
                        autoCapitalize="none"
                        maxLength={3}
                        minLength={3}
                        onChangeText={v => this.handleChange('cvv_Number', v)}
                        onBlur={() => this.handleOnBlurChange('cvv_Number')}
                        style={[ styles.labelName ]}
                        tintColor = "rgba(0, 0, 0, 0.87)"
                        baseColor = "rgba(0, 0, 0, 0.87)"        
                        error	= {this.state.cvv_Number_error}
                      />
                    </View>

                  </View>
                </View>

                <View style={{ width:"100%" }}>
                  <TextField
                    label='Name'
                    keyboardType="default"
                    autoCapitalize="none"
                    minLength={3}
                    onChangeText={v => this.handleChange('name', v)}
                    onBlur={() => this.handleOnBlurChange('name')}
                    style={[ styles.labelName ]}
                    tintColor = "rgba(0, 0, 0, 0.87)"
                    baseColor = "rgba(0, 0, 0, 0.87)"
                    error	= {this.state.name_error}
                  />
                </View>
                
                <View style={{ width:"100%" }}>

                  <View style={{ flexDirection:'row', flexWrap:'wrap', flex:1 }}>
                    <Text style={[ styles.labelName, { marginTop: 20 } ]} >CC Expiration </Text>
                  </View>

                  <View style={{ flexDirection:'row', flexWrap:'wrap', flex:1 }}>

                    <View style={{ flex:0.45, marginRight: 10 }}>
                      <Dropdown 
                        textColor='rgba(0, 0, 0, 0.87)'
                        tintColor = "rgba(0, 0, 0, 0.87)"
                        baseColor = "rgba(0, 0, 0, 0.87)"
                        inputContainerStyle={{ borderBottomWidth: 1, borderBottomColor: "rgba(0, 0, 0, 0.37)" }}
                        labelFontSize={12}
                        value={this.state.cc_Month}
                        data={month}
                        dropdownOffset={{ top: 10, left: 0 }}
                        onChangeText={v => this.handleChange('cc_Month', v)}
                      />
                    </View>

                    <View style={{ flex:0.5 }}>
                      <Dropdown 
                        textColor='rgba(0, 0, 0, 0.87)'
                        tintColor = "rgba(0, 0, 0, 0.87)"
                        baseColor = "rgba(0, 0, 0, 0.87)"
                        inputContainerStyle={{ borderBottomWidth: 1, borderBottomColor: "rgba(0, 0, 0, 0.37)" }}
                        labelFontSize={12}
                        value={this.state.cc_Year}
                        data={year}
                        dropdownOffset={{ top: 10, left: 0 }}
                        onChangeText={v => this.handleChange('cc_Year', v)}
                      />
                    </View>

                  </View>
                  
                </View>
                
                <View style={{ width:"100%" }}>
                  <TextField
                    label='Billing Address'
                    keyboardType="default"
                    autoCapitalize="none"
                    maxLength={200}
                    minLength={3}
                    onChangeText={v => this.handleChange('billing_Address', v)}
                    onBlur={() => this.handleOnBlurChange('billing_Address')}
                    style={[ styles.labelName ]}
                    tintColor = "rgba(0, 0, 0, 0.87)"
                    baseColor = "rgba(0, 0, 0, 0.87)"
                    error	= {this.state.billing_Address_error}
                  />
                </View>
                
                <View style={{ width:"100%" }}>
                  <TextField
                    label='City'
                    keyboardType="default"
                    autoCapitalize="none"
                    minLength={3}
                    onChangeText={v => this.handleChange('city', v)}
                    onBlur={() => this.handleOnBlurChange('city')}
                    style={[ styles.labelName ]}
                    tintColor = "rgba(0, 0, 0, 0.87)"
                    baseColor = "rgba(0, 0, 0, 0.87)"
                    error	= {this.state.city_error}
                  />
                </View>
                
                <View style={{ width:"100%" }}>
                  <TextField
                    label='Postal Code'
                    keyboardType="numeric"
                    autoCapitalize="none"
                    maxLength={6}
                    minLength={5}
                    onChangeText={v => this.handleChange('postal_Code', v)}
                    onBlur={() => this.handleOnBlurChange('postal_Code')}
                    style={[ styles.labelName ]}
                    tintColor = "rgba(0, 0, 0, 0.87)"
                    baseColor = "rgba(0, 0, 0, 0.87)"
                    error	= {this.state.postal_Code_error}
                  />
                </View>
                
                <View style={{ width:"100%" }}>
                  <Label style={[ styles.labelName, { paddingTop: 18 } ]}>
                    Validate your account password
                  </Label>

                  <TextField
                    keyboardType="default"
                    autoCapitalize="none"
                    onChangeText={v => this.handleChange('validated_Account_Password', v)}
                    style={[ styles.labelName ]}
                    inputContainerStyle= {{ paddingTop: 10, height: 44 }}   
                    tintColor = "rgba(0, 0, 0, 0.87)"
                    error	= {this.state.validated_Account_Password_error}
                  />
                </View>
                
                
                </View>
              <Spacer size={20} />
              
              <View padder style={{alignItems:'flex-end'}}>

                <Button block style ={styles.buttonStyleTransparent} onPress={() => this.addNewCardSubmitBtn()} disabled={this.state.isError}>
                  <Text style={styles.loginButtonText}>{(this.state.isLoading)?"Loding...":"update card"}</Text>
                </Button>

              </View>

            </Form>
            </View>
            
          </Content>
        </View>
      </Container>
    );
  }
}

NewCreditCard.navigationOptions = {
  title: 'Credit Card',
  headerStyle: {
    backgroundColor: '#FFFFFF',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height:60,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            //height:130,
            marginTop:-15
          }
        }),
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8 ,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 0.38
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }), 
  },
};

const styles = StyleSheet.create({

  labelColor:{
    color:'#ffffff'
  },
  labelForgetColor:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:2,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  welcomeText:{
    fontSize:30,
    textAlign: 'center',
    flex:1, 
    color:'white',
  },
  normalText:{
    fontSize:12,
    textAlign: 'center',
    flex:1, 
    color:'white',
  },
  buttonStyle:{
    backgroundColor:'white',
    borderWidth:1,
    borderRadius:4,
    borderColor:'#ffffff'
  },

  loginButtonText:{
    color:'#ffffff',
    fontSize:12,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  buttonStyleTransparent:{
    backgroundColor:'#25AE88',
    borderRadius:166,
    marginTop:20,
  },
  labelName: {
    color: "rgba(0, 0, 0, 0.87)",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  newCreditCardLable:{
    textAlign:'center',
    fontSize:13,
    lineHeight: 18,
    letterSpacing: -0.08,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },

  formLabelName: {
    fontSize:15,
    lineHeight: 20,
    paddingBottom: 8,
    paddingTop: 10,
    ...Platform.select({
      ios: {
        letterSpacing: -0.24
      },
    }),
  },
  formLabelName1: {
    paddingBottom: 8,
    paddingTop: 10,
    fontSize:12,
    lineHeight: 16,
  }

});
export default NewCreditCard;
