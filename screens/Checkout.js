import React from 'react';
import {AsyncStorage, AppRegistry, ListView, TouchableWithoutFeedback,TouchableOpacity, View, StyleSheet , Image, ScrollView, Platform, TouchableHighlight  } from 'react-native';
import {Text, Button, Form, Item, Input, Container} from 'native-base';
import Spacer from '../components/UI/Spacer';
import { Actions } from 'react-native-router-flux';
import ActivityIndicator from 'react-native-loading-spinner-overlay';
import { LinearGradient } from 'expo-linear-gradient';
import Swipeout from 'react-native-swipeout';
import Modal from "react-native-modal";
import axios from 'axios';
import Checkbox from 'react-native-modest-checkbox';
let rows = [];
var ds = new ListView.DataSource({rowHasChanged: (row1, row2) => true});
class Checkout extends React.Component{
  constructor(props) {
    super(props);
    //  datasource rerendered when change is made (used to set swipeout to active)
    
    this.state = {
      promo:'',      
      dataSource: ds.cloneWithRows(rows),
      sectionID: null,
      rowID: null,
      isLoading:true,
      checkoutData:[],
      CheckoutGet:'',
      totalAmout:0,
      tax:0,
      appCredit:0,
      Subtotal:0,
      PromoCodeApplied:false,
      PromoCode:0,
      PromoCodeErrorMessage:'',
      loggedInUserChilds: [],
      loggedInUserChildsCount: 1,
      isCheckedChilds: false,
      isCheckedAllChilds: false,
      isCheckedChilds: [],
      selectedUserChilds: [],
      isModalUsersChilds: false,
      showWebView: false,
      payment_url:''
    };

    this.validates = this.validates.bind(this);
    this.itemsConcatation = this.itemsConcatation.bind(this);
    this.removeCartItem =this.removeCartItem.bind(this);
    this.promocode = this.promocode.bind(this);
  }

  validates = ( type, promo,) => {
    if(type==='promo'){
      this.setState({promo:promo});
    }
  }

  fetchPromoCode = () => {
    let self = this;
        let response = axios.post('https://konnect.myfirstgym.com/api/customer/promo-code/validate', {
          code: self.state.promo,
        })
        .then(function (response) {
          console.log(response['data']['success']);
          if(response['data']['success']){
            self.setState({PromoCode:response['data']['value'],  PromoCodeApplied:true });
            self.setState({PromoCode:self.state.totalAmout*self.state.PromoCode/100, PromoCodeErrorMessage:'Promo Code applied successfully '})
          }
          else{
            self.setState({PromoCode:0, PromoCodeErrorMessage:'Invalid promo code', PromoCodeApplied:true });

          }
        })
        .catch(function (error) {
          alert(error);
          console.log(error);
        });
  }

  promocode=() =>{
    this.fetchPromoCode();
  }


   _renderRow(rowData, sectionID, rowID) {
    return (
      <Swipeout
        close={!(this.state.sectionID === sectionID && this.state.rowID === rowID)}
        left={rowData.left}
        right={rowData.right}
        rowID={rowID}
        sectionID={sectionID}
        autoClose={rowData.autoClose}
        backgroundColor={rowData.backgroundColor}
        onOpen={(sectionID, rowID) => {
          this.setState({
            sectionID,
            rowID,
          })
        }}
        onClose={() => console.log('===close') }
        scroll={event => console.log('scroll event') }
      >
        <TouchableWithoutFeedback onPress={() => console.log('press children')}>
          <View style = {[styles.listItemsDesign]} >

            <View style={styles.serviceImage} >
              <Image  style = {{width:6, height:6}} source = {require('../assets/images/Ellipse.png')} />
            </View>
            <View style={styles.serviceName}>
              <Text style={[styles.textFont, styles.textFonts]}> {rowData.serviceName} </Text>
            </View>
            <View style={styles.serviceMoney}>
              <Text style={[styles.priceFont, styles.textFonts]}> {rowData.serviceMoney}</Text>
            </View>

          </View>
        </TouchableWithoutFeedback>
        

      </Swipeout>
    );
  }


   fetchCheckout(token) {
    return new Promise(async (resolve)=>{
    try {
        let response = await fetch('https://konnect.myfirstgym.com/api/customer/checkout?api_token='+token);
        let responseJson = [];
        try {
            responseJson = await response.json();
            resolve(responseJson)
        } catch (error) {
            console.log(error);
            responseJson = [];
            resolve([])
        }
        this.setState({ CheckoutGet: responseJson});
    } catch (error) {
        alert(error)
        console.log(error);
        resolve([]);
    }
  })
  }
  async doCheckout() {
    
    return new Promise(async (resolve)=>{
      try {
        let self =this;
           
            let res = await AsyncStorage.getItem('ServiceItems').then(async (servicesData) => {
            ServiceItems=[];
            if(servicesData!==null){
              newServicesData = JSON.parse(servicesData);      
              if(Object.keys(newServicesData).length>0){    
                newServicesData.data.map((item, i) =>{
                  item.items = item.items.filter(x=>x.isSelect === true);   
                  item.items.map((itemList, j)=>{
                    if(itemList.isSelect)
                      ServiceItems.push(itemList.id)
                  })
                })
              }
              let token =  await AsyncStorage.getItem('api_token');
              let payLoad ={ 
                "api_token": token,
                "item_id": ServiceItems[0], //comma separated ids
                "child_id":self.state.selectedUserChilds[0],
                "promo_code":self.state.promo, // optional
                };
                console.log(payLoad);
              
              let response = await axios.post('https://konnect.myfirstgym.com/api/customer/checkout', payLoad)
                .then(function (response) {
                  console.log(response['data']['url']);       
                  return   response['data']['url'];        
                  
                })
                .catch(function (error) {
                  //alert(error);
                  console.log(error);
                });
                console.log(response);
                self.props.navigation.navigate('WebViewIframe', { payment_url:response,replace: true });
                 resolve(response);                
              }  
            });
            
  //self.setState({payment_url:res});
  //self.setState({ showWebView: true});
        } catch (error) {
          // alert(error)
          console.log(error);
          resolve([]);
        }
        
  })
  }



  //USING COMPONENT TO CHECK OR UNCHECK BY ALL CHECK OPTION
  allCheckedChilds = () => {
    let checkedChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(this.state.isCheckedAllChilds){
        item.isSelect= false;
      }else{
        item.isSelect= true;
      }
    })

    this.setState({ isCheckedChilds: checkedChilds });
    this.setState((prevState) => ({ isCheckedAllChilds: !this.state.isCheckedAllChilds }));
  }

  //USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE CHILDS
  childsCheckbox = (itemID) => {
    let checkedChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(itemID===item.id){
        item.isSelect=!item.isSelect;
      }
    })

    this.setState({ isCheckedChilds: checkedChilds });
    this.setState((prevState) => ({ isCheckedAllChilds: false }));
  }

  // USING COMPONENT ON CLICK OF OK BUTTON TO BOOK CLASS
  selectedUserChilds = () => {
    let selectedUserChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(item.isSelect === true){
        selectedUserChilds[item.id] = item.id;
      }
    })
   
    selectedUserChilds = selectedUserChilds.filter(Boolean);
    this.setState({ selectedUserChilds: selectedUserChilds });
    this.doCheckout();
    this.setState({ isModalVisible: true });
    this.setState({ isModalUsersChilds: !this.state.isModalUsersChilds });
  }


  modalForUserChilds = () => {
    checkedChilds = this.state.loggedInUserChilds[0];
    checkedChilds.map((item, i)=>{
      item.isSelect= false;
    })
    this.setState({ isCheckedChilds: checkedChilds, isCheckedAllChilds: false });

    this.setState({ isModalUsersChilds: !this.state.isModalUsersChilds });
  }

async componentDidMount(){

  let token =  await AsyncStorage.getItem('api_token');
  if(token!==null){
    await this.fetchCheckout(token);

  AsyncStorage.getItem('ServiceItems').then(async (servicesData) => {
    if(servicesData!==null){
      newServicesData = JSON.parse(servicesData);
      this.setState({ checkoutData : newServicesData})
      this.itemsConcatation();
      this.setState({isLoading:false, dataSource: ds.cloneWithRows(rows)});
    }  
  });
// USING ASYNC STORAGE FUNCTION TO SET ITEM CHILDERNS IN STATE    
    AsyncStorage.getItem('@UserChilderns:key').then((children) => {
      let totChildCnt = 1;

      if (children !== null) {
        let ArrayChilderns = JSON.parse(children);
        // We have data!!
        this.setState({ loggedInUserChilds: ArrayChilderns })

        ArrayChilderns[0].map((item, i) =>{
          totChildCnt = totChildCnt+i;
          item.isSelect=false;
        })

        this.setState({ loggedInUserChildsCount: totChildCnt })

      }
      
    });
}
}
itemsConcatation=() =>{
  // console.log(this.state.checkoutData);
  if(Object.keys(this.state.checkoutData).length>0){
    rows=[];
    this.state.checkoutData.data.map((item, i) =>{
      item.items = item.items.filter(x=>x.isSelect === true);   
       item.items.map((itemList, j)=>{
         this.setState({totalAmout : this.state.totalAmout+itemList.price});
        rows.push(
          {
          "id":itemList.id,
          'serviceName':itemList.name,
          'serviceMoney': "AED "+ parseFloat(itemList.price).toFixed(2),
          'right': [
            {
              'component': <Image style={{width:20, height:30, alignSelf:'center', marginTop:10, marginBottom:10, backgroundColor:'transparent'}} source={require('../assets/images/deleteButton.png')} />,
              onPress: () =>this.removeCartItem(itemList.id),
              'type': 'primary',
              'backgroundColor': '#EB4242',
            }
          ],
         }
          )
    })
  })
  
}
this.setState({tax : (this.state.totalAmout*this.state.CheckoutGet.tax_rate)/100});
this.setState({appCredit : this.state.CheckoutGet.applied_credit});
this.setState({Subtotal:this.state.totalAmout-this.state.tax});
}

removeCartItem =(id) =>{
  let afterremoved={};
  if(Object.keys(this.state.checkoutData).length>0){
    this.state.checkoutData.data.forEach( async (item, i) =>{
      let index = item.items.findIndex(x=>x.id === id);

      if(index != -1){
        afterremoved = item.items[index];
        item.items[index].isSelect = false;
       await this.setState({totalAmout:this.state.totalAmout-afterremoved.price})
       await this.setState({tax : this.state.totalAmout*this.state.CheckoutGet.tax_rate/100});
       await this.setState({Subtotal:this.state.totalAmout-this.state.tax});
       if(this.state.PromoCodeApplied){
        this.fetchPromoCode();
       }
       
      }          
  })
  }                    
  this.saveItem('ServiceItems', JSON.stringify(this.state.checkoutData));
  rows = rows.filter(x=>x.id !== id);
  this.setState({dataSource: ds.cloneWithRows(rows)})
}

async saveItem(item, selectedValue) {
  try {
    await AsyncStorage.setItem(item, selectedValue);
    console.log("Successfull Added into Local Storage");
  } catch (error) {
    console.error('AsyncStorage error: ' + error.message);
  }
}

  render(){
    

    return(
      <Container  style={{ backgroundColor:'#E5E5E5' }}>
      {
        (this.state.isLoading)?
        (
          <ScrollView style={styles.container}>
              <View style ={styles.mainContainer}>
              <View style={styles.loading}>
                    <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
                  </View>
              </View>
            </ScrollView>
        )
        :
        (
          <ScrollView style={styles.container}>
          <View style ={styles.mainContainer}>
       <Spacer size={15} />
       <View style = {{flex:1}}>
          <Spacer size={5} />
  
            <ListView scrollEnabled  dataSource={this.state.dataSource}  renderRow={this._renderRow.bind(this)}  style={styles.listview}  />
  
          <Spacer size={15} />
          {
            Platform.select({
              android:(
                <TouchableHighlight underlayColor="rgba(0, 0, 0, 0.54)" onPress={() => this.props.navigation.navigate('Buyservices', { Checkout:true})}  underlayColor="transparent" >
            <View style ={styles.headingView} >
                  <Text style={[styles.textHeading, styles.textFonts]} >Buy More Services</Text>
                  <View style = {{width:20, height:20, borderRadius:10, backgroundColor:'#0099EF', marginLeft:10, position:'absolute', right:'25%', textAlign:'center',
        justifyContent:'center'}}><Image  style = {{width:10, height:10,  alignSelf:'center'}} source = {require('../assets/images/buyServices.png')} /></View>
            </View>
          </TouchableHighlight>
              ),
              ios:(
                <TouchableHighlight underlayColor="rgba(0, 0, 0, 0.54)" onPress={() => this.props.navigation.navigate('Buyservices', { Checkout:true})}  underlayColor="transparent" >
            <View style ={styles.headingView} >
                  <Text style={[styles.textHeading, styles.textFonts]} >Buy More Services</Text>
                  <View style = {{width:20, height:20, borderRadius:10, backgroundColor:'#0099EF', marginLeft:10, position:'absolute', right:'20%', textAlign:'center',
        justifyContent:'center'}}><Image  style = {{width:10, height:10,  alignSelf:'center'}} source = {require('../assets/images/buyServices.png')} /></View>
            </View>
          </TouchableHighlight>
              )
            })
          }
          
          <Spacer size={15} />
          <View style ={styles.listItemsDesign} >
              <View style = {{flex:0.3}}><Text style={[styles.textFont, styles.textFonts, { fontSize: 17, lineHeight: 22 }]}>Promo Code</Text></View>
              <View style = {[{flex:0.5, marginTop:-40 }, {...Platform.select({ios:{textAlignVertical:'bottom',marginTop:-30, lineHeight:15}})}]}>
                <Item stackedLabel style = {[{ borderBottomColor:'rgba(0, 0, 0, 0.1)', borderBottomWidth:1}, {...Platform.select({ios:{ height:20}})}]} >
                  <Input
                    style={[ styles.textFonts, {color:'#000000', textAlignVertical:'bottom', width: "100%"}, {...Platform.select({ios:{ height:20}})}]}
                    placeholder = ''
                    placeholderTextColor="#000000"
                    autoCapitalize="none"
                    value={this.state.promo}
                    keyboardType="default"
                    onChangeText={promocode => this.validates( 'promo', promocode,)}
                  />
                </Item>
              </View>
              <View style = {{flex:0.2}}>
                <TouchableHighlight style= {styles.applyView} onPress={() =>this.promocode()} underlayColor='#ffffff'>
                    <View ><Text style={[styles.textApply, styles.textFonts]}>APPLY</Text></View>
                </TouchableHighlight>
               </View>
          </View>
          <Spacer size={5} />
          <View style ={[styles.listItemsDesign, {borderBottomWidth:0}]} >
              <View><Text style={[styles.textFonts, {fontSize:17, lineHeight:22, color:'rgba(0, 0, 0, 0.87)', fontWeight:'bold'}]}>Bill Details</Text></View>
          </View>
          <View style = {[styles.billDetails]}>
                  <View style={styles.serviceName}><Text style={[styles.textFont, styles.textFonts]}>Subtotal</Text></View>
                  <View style={styles.serviceMoney}><Text style={[styles.textFont, styles.textFonts]}>AED { parseFloat(this.state.Subtotal).toFixed(2)}</Text></View>
              </View>
              <View style = {[styles.billDetails]}>
                  <View style={styles.serviceName}><Text style={[styles.textFont, styles.textFonts]}>Tax</Text></View>
                  <View style={styles.serviceMoney}><Text style={[styles.textFont, styles.textFonts]}>AED { parseFloat(this.state.tax).toFixed(2)}</Text></View>
              </View>
              <View style = {[styles.billDetails]}>
                  <View style={styles.serviceName}><Text style={[styles.textFont, styles.textFonts]}>Applied Credit</Text></View>
                  <View style={styles.serviceMoney}><Text style={[styles.textFont, styles.textFonts]}>AED { parseFloat(this.state.appCredit).toFixed(2)}</Text></View>
              </View>
              {
                (this.state.PromoCodeApplied)?
                (
                  <View style = {[styles.billDetails, {color:'green'}]}>
                    <View style={styles.serviceName}><Text style={[styles.textFont, styles.textFonts, {fontSize:12, color:'green'}]}>{this.state.PromoCodeErrorMessage}</Text></View>
                    <View style={styles.serviceMoney}><Text style={[styles.textFont, styles.textFonts, {fontSize:12, color:'green'}]}>AED { parseFloat(this.state.PromoCode).toFixed(2)}</Text></View>
                  </View>
                ):(
                  <Text></Text>
                )
              }

              
              <Spacer size={60} />
              <View style = {[styles.listItemsDesign]}>
                  <View style={styles.Pay}><Text style={[ styles.textFonts, {fontSize:17, lineHeight:22, color:'rgba(0, 0, 0, 0.87)', fontWeight:'bold'}]}>To Pay</Text></View>
                  <View style={styles.payMoney}><Text style={[ styles.textFonts, {fontSize:17, lineHeight:22, color:'rgba(0, 0, 0, 0.87)', fontWeight:'bold'}]}>AED { parseFloat(this.state.totalAmout-this.state.appCredit-this.state.PromoCode).toFixed(2)}</Text></View>
              </View>
              <Spacer size={20} />
              
                <View style={{width:'90%', alignContent:'center', alignSelf:'center'}}>
                    <Button block style ={styles.buttonStyle} onPress = { this.modalForUserChilds} >
                      <Text style={[styles.loginButtonText, styles.textFonts]}>Pay Now</Text>
                    </Button>
                </View>
                
              <Spacer size={30} />

              </View>
                          {/* MODAL USING TO DISPLAY ALL USER CHILDS ON CLICK OF BOOK NOW BUTTON  onPress={this.doCheckout}*/}

            <Modal isVisible= {this.state.isModalUsersChilds} style={[ styles.modal, { width: "80%", marginLeft: "10%", height: 210, padding: 20, paddingTop: 16, paddingBottom: 16 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >


              <View style={[ styles.modalSelectDayViews ]}>
                  <View style={{ flex: 1, alignSelf: "flex-start" }} >
                      <Text style={ styles.selectedDaysTitle }> Select Childerns </Text>
                  </View>
              </View>

                        
              <View style= {[ styles.modalSelectDayViews, { borderBottomWidth: 1, borderBottomColor: "rgba(0, 0, 0, 0.54)", marginBottom: 10, paddingBottom: 5 } ]} >

                  
                
                  <View style={{flex: 0.13, height: 35, paddingBottom:5, paddingRight: 5, paddingTop:5 }} >
                    <Checkbox 
                      checked={this.state.isCheckedAllChilds}
                      checkedImage={require('../assets/images/box-checked.png')}
                      uncheckedImage={require('../assets/images/box.png')}
                      checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                      onChange={this.allCheckedChilds} 
                    />
                  </View>
              </View>
              <ScrollView style={{ width: "100%", height: 40, alignContent: "stretch" }} >
                {
                 (this.state.loggedInUserChilds && this.state.loggedInUserChilds.length > 0)? ( 
                    this.state.loggedInUserChilds[0].map((item, i) =>{
                      let itemID = item.id
                        
                      return(
                        <View style={[ styles.modalSelectDayViews ]} >
                            <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                                <Text style={ styles.modalSelectDayTitles }>
                                  {item.name}
                                </Text>
                            </View>

                            <View style={{flex: 0.13, height: 35, paddingBottom:5, paddingRight: 5, paddingTop:5 }} >
                              {
                                (item.isSelect)?
                                (
                                  <Checkbox 
                                    checked={true}
                                    checkedImage={require('../assets/images/box-checked.png')}
                                    uncheckedImage={require('../assets/images/box.png')}
                                    checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                                    onChange={this.childsCheckbox.bind(this, itemID)}
                                  />
                                ):(
                                  <Checkbox 
                                    checked={false}
                                    checkedImage={require('../assets/images/box-checked.png')}
                                    uncheckedImage={require('../assets/images/box.png')}
                                    checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                                    onChange={this.childsCheckbox.bind(this, itemID)}
                                  />
                                )
                              }
                            </View>
                        </View>
                      )
                    })
                  ):(
                    <View></View>
                  )
                }
              </ScrollView>  
              <View style={[ styles.modalSelectDayViews, { maxHeight: 25, marginTop: 10, paddingTop: 5, alignItems: "center" } ]}>

                  {/* <View style={{ flex: 0.55,alignSelf: "flex-start" }} >
                      
                  </View> */}

                      <TouchableOpacity style={{ paddingRight : 15 }} onPress={ this.modalForUserChilds } >
                        <Text style={[styles.selectedDaysBtns,{textAlign:"right",color:"rgba(0,0,0,0.54)"}]}>
                          CANCEL
                        </Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={{ paddingLeft : 15 }} onPress={ this.selectedUserChilds } >
                        <Text style={[styles.selectedDaysBtns,{textAlign:"left",color:"rgba(0, 0, 0, 0.87)"}]}>
                          OK
                        </Text>
                      </TouchableOpacity>


                  {/* <View style={{ flex: 0.45, alignSelf:"flex-start" }} >
                      
                  </View> */}
                
              </View>


            </Modal>
              </View>
              </ScrollView>
        )
      }

</Container>
    )
  }
}
Checkout.navigationOptions = {
  title: 'Checkout',
  headerStyle: {
    backgroundColor: '#E5E5E5',
    height:50,
    elevation: 0,
    borderBottomWidth: 0,
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        elevation: 5,
        backgroundColor: '#E5E5E5',

        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),

      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8 ,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 0.38
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
};

const styles = StyleSheet.create({
  mainContainer:{
    flex:1, 
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#E5E5E5',
  },
  headingView:{
    flexDirection:'row', 
    flexWrap:'wrap', 
    textAlign:'center',
    alignContent:'center',
    alignItems:'center',
    margin:0,
    flex:1,
  },
  textHeading:{
    width:'100%',
    textAlign:'center',
    fontSize:17,
    lineHeight:22,
    alignContent:'center',
    alignSelf:'center',
    margin:0,
    position:'relative',
    letterSpacing: -0.41,
    color:'rgba(0, 0, 0, 0.54)'
  },
  listItemsDesign:{
    backgroundColor:'#ffffff',
    flexWrap:'wrap',
    flexDirection:'row',
    flex:1,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:10,
    paddingRight:10,
    borderBottomWidth:1,
    borderBottomColor:'rgba(0, 0, 0, 0.1)'
  },
  listItemsDesign1:{
    backgroundColor:'#ffffff',
    flexWrap:'wrap',
    flexDirection:'row',
    borderBottomWidth:1,
    borderBottomColor:'rgba(0, 0, 0, 0.1)'
  },
  billDetails:{
    paddingLeft:15,
    paddingRight:15,
    backgroundColor:'#ffffff',
    flexWrap:'wrap',
    flexDirection:'row',
    paddingTop:7,
    paddingBottom:7
  },
  listItemsDesignAction:{backgroundColor:'#0099EF'},
  serviceImage:{flex:0.05,paddingTop:8},
  serviceName:{flex:0.65, alignItems:'flex-start',},
  serviceMoney:{flex:0.3, textAlign:'right', alignItems:'flex-end', fontSize:13},
  Pay:{flex:0.5, alignItems:'flex-start',},
  payMoney:{flex:0.5, textAlign:'right', alignItems:'flex-end', fontSize:13},
  textFont:{
    fontSize:15,
    lineHeight: 20,
    color: "rgba(0, 0, 0, 0.87)",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing: -0.24
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  priceFont:{    
    ...Platform.select({
      ios: {
        fontSize:13,
        lineHeight: 18,
        fontFamily:'SFProTextRegular',
        letterSpacing: -0.08,
        color: "rgba(0, 0, 0, 0.87)"
      },
      android: {
        fontSize:14,
        lineHeight: 20,
        fontFamily:'RobotoRegular_1',
        color: "rgba(0, 0, 0, 0.87)"
      },
    }),

  },
  cartButton:{
    alignItems:'center',
    width:58,
    textAlign:'center', 
    height:58, 
    borderRadius:29,
    backgroundColor:'#FFFFFF',
    color:'#ffffff', 
    position:'absolute', 
    alignSelf:'flex-end', 
    bottom:10, 
    right:10
  },
  imageStyle:{
    marginLeft:12 ,
    alignSelf:'center', 
    width:35, 
    height:35, 
    borderRadius:10
  },
  textApply:{
      fontSize:11,
      alignItems:'flex-end',
      lineHeight:13,
      textAlign:'center',
      color:'#C4C4C4',
      borderWidth:1,
      borderColor:'#E8E8E8',
      paddingTop:7,
      paddingBottom:7,
      paddingLeft:10,
      paddingRight:10,
      borderRadius:16
    },
    applyView:{
      alignItems:'flex-end',
      alignContent:'flex-end',
      alignSelf:'flex-end',
      flex:0.3,
    },
    buttonStyle:{
      backgroundColor:'#25AE88',
      borderRadius:150,
      shadowColor: 'rgb(0, 0, 0)',
      shadowOpacity: 0.25,
      shadowOffset: { width: 2, height: 4 },
      shadowRadius: 2,
      elevation: 1,
    },
    loginButtonText:{
      color:'#FFFFFF',
      fontSize:17,
      lineHeight:22
    },
    marginTopData:{
      marginTop:15,
      marginRight:10
    },
    tabBarInfoContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { width: 0, height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      alignItems: 'center',
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
    tabBarInfoText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      textAlign: 'center',
    },
    navigationFilename: {
      marginTop: 5,
    },
    textFonts:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    

  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,
    backgroundColor: "#ffffff",
    position: "absolute",
    top:150,
    elevation: 10,
    borderRadius: 5
  },
  modalHeading: {
    fontSize: 26,
    textAlign: "center",    
  },
  modalMessage: {
    textAlign: "center",
    fontSize: 18,
    justifyContent: "center",
    color: "gray",
  },
  closeModalBtnView: {
    position: "absolute",
    top: 5,
    right: 0,
    width: 50,
    paddingRight: 5,
    backgroundColor: "#ffffff",

  },
  
  closeModalBtn: {
    backgroundColor: "#ffffff",
    alignSelf: "center",
    borderWidth: 1,
    borderColor: "#FFFFFF",
    elevation:0,
    paddingLeft: 24
  },

  closeModalBtnX: {
    paddingTop: 17,
    fontSize:20,
    fontWeight: "bold",
    color: "gray",
    borderWidth: 1,
    borderColor: "#FFFFFF",
    elevation: 0,
    backgroundColor: "#ffffff"
  },

  closeModalBtnImg: {
    width: 15,
    marginRight: 5,
    marginLeft: 5,
  },

  modalSelectDayViews: {
    flex: 1,
    flexDirection: "row",
    alignContent: "stretch",
    
  },
});
export default Checkout;
