import React from 'react';
import PropTypes from 'prop-types';
import { Text, Form, Item, Input, Button, Picker, View, Label} from 'native-base';
import {StyleSheet, ScrollView, Platform, ActivityIndicator, AsyncStorage} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Messages from '../components/UI/Messages';
import Spacer from '../components/UI/Spacer';
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';
import { LinearGradient } from 'expo-linear-gradient';
class UpdateCreditCard extends React.Component {

  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.config={
      apiUrl:'https://konnect.myfirstgym.com/api/customer/credit-cards/'+this.params.cardId,
      SavedCardsListAPI:'https://konnect.myfirstgym.com/api/customer/credit-cards',
    }
    this.state = {
      month:'Jun',
      year:'2023',
      cardnumber:'',
      cvc:'',
      cardholderName:'',
      selectedMonth: '',
      selectedYear: '',
      billing_address: '',
      city: '',
      postal_code: '',
      errorshow:false,
      error:false,
      isLoading: true,
      fetchCardsList: [],
      errorMessage: '',
    };

    this.validates = this.validates.bind(this);
    this.formsubmit = this.formsubmit.bind(this);
  }

  validates = (val, name) => this.setState({ [name]: val })

  async fetchedCardUpdate(token) {

    let self = this;
    let responseJson = [];

    let response = await fetch(this.config.apiUrl+"?api_token="+token, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        api_token: token,
        card_number: self.state.cardnumber,
        cvv: self.state.cvc,
        card_holder_name : self.state.cardholderName,
        exp_month : self.state.selectedMonth,
        exp_year : self.state.selectedYear,
        billing_address : self.state.billing_address,
        city : self.state.city,
        postal_code : self.state.postal_code
      })
    })

    try {

      responseJson = await response.json();
      console.log("API - ", responseJson)

      // USING FUNCTION TO REDIRECT ON CREDIT CARD
      this.redirectScreenToCredit()

    } catch (error) {

        console.log("Response error - ",error)

    }

  }

  // USING FUNCTION TO REDIRECT ON CREDIT CARD
  redirectScreenToCredit() {
    this.props.navigation.navigate('Creditcard');
  }


  formsubmit =() =>{

    if(this.state.cardnumber == ''){
     errormessage='Card Number is Required.';
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.cardnumber.length <= 10){
      errormessage='Minimum 10 digit should be in Card Number.';
      this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.cvc == ''){
     errormessage = 'CVC Number is Required.';
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.cvc.length < 3){
     errormessage = "Minimum 3 digit should be in CVC Number.";
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.cardholderName == ''){
     errormessage='Cardholder Name is Required.';
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.cardholderName.length < 3){
      errormessage = "Minimum 3 character should be in Cardholder Name.";
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.billing_address == ''){
     errormessage='Billing Address is Required.';
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.billing_address.length < 3){
     errormessage = "Minimum 3 character should be in Billing Address.";
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.city == ''){
     errormessage='City is Required.';
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.city.length < 3){
     errormessage = "Minimum 3 character should be in city.";
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.postal_code == ''){
     errormessage='Postal Code is Required';
     this.setState({error:true, errorMessage: errormessage });
    }else if(this.state.postal_code.length < 3){
     errormessage = "Minimum 3 digit should be in Postal Code.";
     this.setState({error:true, errorMessage: errormessage });
    } else {

      this.setState({error:false});

      //initialize your function
      AsyncStorage.getItem('api_token').then((token) => {
        if(token!==null){
          this.fetchedCardUpdate(token); 
          console.log("Call Token")
        }else{
          this.setState({isLoading:false,});
          console.log("Nothing")
        }
      })

    }
  }
 
  //FETCHING THE SAVED CARDS DETAILS
  async fetchCardsDetails(api_token) {
    let responseJson = [];
    let matchedRespJson = [];
    let self = this;

    try{
      let response = await fetch(this.config.SavedCardsListAPI+'?api_token='+api_token);
      try {
        responseJson = await response.json();

        matchedRespJson.push(responseJson.data.map((items, i)=>{
          if(self.params.cardId == items.id){
            return items;
          }
        }))

        matchedRespJson = matchedRespJson[0].filter(Boolean);
        matchedRespJson = matchedRespJson[0];

        this.setState({ fetchCardsList: matchedRespJson, cardnumber: matchedRespJson.card_number, cvc: matchedRespJson.cvv, cardholderName: matchedRespJson.card_holder_name, selectedMonth: matchedRespJson.exp_month, selectedYear: matchedRespJson.exp_year, billing_address: matchedRespJson.billing_address, city: matchedRespJson.city, postal_code: matchedRespJson.postal_code,  isLoading:false });

      }catch (error){
        alert(error)
        console.log(error);
      }
    }catch (error){
      alert(error);
      console.log(error);
    }
  }
  
  componentDidMount() {

    //initialize your function
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.fetchCardsDetails(token); 
      }else{
        this.setState({isLoading:false,});
      }
    })

  }



  render() {
    // const { loading, error, success } = this.props;
    // const { email } = this.state;
    let month = [{
      value: '01',
    }, {
      value: '02',
    }, {
      value: '03',
    }, {
      value: '04',
    }, {
      value: '05',
    }, {
      value: '06',
    }, {
      value: '07',
    }, {
      value: '08',
    }, {
      value: '09',
    }, {
      value: '10',
    }, {
      value: '11',
    }, {
      value: '12',
    },
  ];

  var currentYear = new Date().getFullYear();
  let _years = []  
  for(let i=0; i<5; i++){
    _years.push({
      value: currentYear+i
    })
  }

  if(this.state.isLoading){
    return(
      <View style={styles.loading}>
        <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
      </View>
    )
  }else{
    return (

      <ScrollView style={styles.container}>
        <View style ={styles.mainContainer}>
          <View style = {{flex:1}}>
            <Spacer  size = {10}/>
            <View>
                {
                  (this.state.error)?
                  (<Text style = {{alignSelf:'center', color: "rgb(213, 0, 0)"}}>{errormessage}</Text>):
                  (<Text> </Text>)
                }
              </View>
            <Form>
              <View style={{flexDirection:'row', flexWrap:'wrap', flex:1}}>
                
                  <Item stackedLabel style={{ borderBottomWidth:0 ,borderRadius:4, flex:0.64, marginRight: '2%', alignSelf:'flex-start'}}>
                    <Label style={ styles.labelName }>Card Number</Label>
                    <Input
                      style={{borderRadius:5, color:'#959595', textAlignVertical:'center', backgroundColor:'#F0F0F0', marginRight:10, borderWidth:0, maxWidth:'100%',maxHeight:40,borderBottomWidth:0, paddingLeft:10}}
                      placeholder = '7419 9412 5910 0212'
                      placeholderTextColor="#959595"
                      autoCapitalize="none"
                      maxLength={16}
                      minLength={10}
                      value={this.state.cardnumber}
                      keyboardType="numeric"
                      onChangeText={(text) => this.validates(text, 'cardnumber')}
                    />
                  </Item>
                  <Item stackedLabel style={{ borderBottomWidth:0 ,borderRadius:4, flex:0.34, marginLeft: '2%', alignSelf:'flex-end'}}>
                    <Label style ={[ styles.labelName, { marginLeft: '3%' }]}>CVC</Label>
                    <Input style={{borderRadius:5, color:'#959595', textAlignVertical:'center', backgroundColor:'#F0F0F0', borderWidth:0, maxWidth:'100%',maxHeight:40,borderBottomWidth:0,  paddingLeft:10}}
                      placeholder = '123'
                      placeholderTextColor="#959595"
                      autoCapitalize="none"
                      maxLength={3}
                      minLength={3}
                      value={this.state.cvc}
                      keyboardType="numeric"
                      onChangeText={(text) => this.validates(text, 'cvc')}
                    />
                  </Item>
                  <Item stackedLabel style={{ borderBottomWidth:0 ,borderRadius:4, width: '98%', alignSelf:'flex-end', marginTop:10}}>
                    <Label style={ styles.labelName }>Card Holder Name </Label>
                    <Input
                  style={{borderRadius:5, color:'#959595', textAlignVertical:'center', backgroundColor:'#F0F0F0', borderWidth:0, maxWidth:'100%',maxHeight:40,borderBottomWidth:0, paddingLeft:10}}
                      placeholder = 'joshua Hernandez'
                      placeholderTextColor="#959595"
                      autoCapitalize="none"
                      minLength={3}
                      value={this.state.cardholderName}
                      keyboardType="default"
                      onChangeText={(text) => this.validates(text, 'cardholderName')}
                    />
                  </Item>

                </View>
                
                
                <View style={{ flex: 1, flexDirection: "row", flexWrap:'wrap' }}>
                    
                  <View style={{ width: "100%", paddingBottom: 8 }}>
                    <Label style={[ styles.labelName, { marginBottom: 0 } ]}>Expiration Date </Label>
                  </View>


                  <View style={{ flex: 0.4, flexDirection: "row", flexWrap:'wrap', borderColor: "#F0F0F0", borderWidth: 1, backgroundColor: "#F0F0F0", borderRadius: 5, marginRight: 10, paddingRight:2 }}>

                    <Picker
                      selectedValue={this.state.selectedMonth}
                      style={[ styles.textFont, {height: 40, width: "100%" ,  color: "rgba(0, 0, 0, 0.87)" }]}
                      onValueChange={(itemValue, itemIndex) => this.setState({selectedMonth: itemValue})}
                    >
                      {
                        month.map((item, i) => {
                          return(
                            <Picker.Item  label={(item.value).toString()} value={(item.value).toString()} key={i} />
                          );
                        })
                      }
                    </Picker>

                  </View>
                  
                  <View style={{flex: 0.3, flexDirection: "row", flexWrap:'wrap', borderColor: "#F0F0F0", borderWidth: 1, backgroundColor: "#F0F0F0", borderRadius: 5, marginLeft: 20, marginRight: 10, paddingRight: 2 }}>

                    <Picker
                      selectedValue={this.state.selectedYear}
                      style={[ styles.textFont, { height: 40, width: "100%" , color: "rgba(0, 0, 0, 0.87)" }]}
                      onValueChange={(itemValue, itemIndex) => this.setState({selectedYear: itemValue})}
                    >
                      {
                        _years.map((items, i) => {
                          return(
                            <Picker.Item  label={(items.value).toString()} value={(items.value).toString()} key={i} />
                          );
                        })
                      }
                    </Picker>

                  </View>
                    
                </View>
                
                <View style={{flexDirection:'row', flexWrap:'wrap', flex:1}}>
                
                  <Item stackedLabel style={{ borderBottomWidth:0 ,borderRadius:4, width: '98%', alignSelf:'flex-end', marginTop:10}}>
                    <Label style={ styles.labelName }>Billing Address </Label>
                    <Input style={{borderRadius:5, color:'#959595', textAlignVertical:'center', backgroundColor:'#F0F0F0', borderWidth:0, maxWidth:'100%',maxHeight:40,borderBottomWidth:0, paddingLeft:10}}
                      placeholder = 'Abu Dhabi'
                      placeholderTextColor="#959595"
                      autoCapitalize="none"
                      value={this.state.billing_address}
                      keyboardType="default"
                      onChangeText={(text) => this.validates(text, 'billing_address')}
                    />
                  </Item>

                </View>
                
                <View style={{flexDirection:'row', flexWrap:'wrap', flex:1}}>
                
                  <Item stackedLabel style={{ borderBottomWidth:0 ,borderRadius:4, width: '98%', alignSelf:'flex-end', marginTop:10}}>
                    <Label style={ styles.labelName }>City</Label>
                    <Input style={{borderRadius:5, color:'#959595', textAlignVertical:'center', backgroundColor:'#F0F0F0', borderWidth:0, maxWidth:'100%',maxHeight:40,borderBottomWidth:0, paddingLeft:10}}
                      placeholder = 'Abu Dhabi'
                      placeholderTextColor="#959595"
                      autoCapitalize="none"
                      value={this.state.city}
                      keyboardType="default"
                      onChangeText={(text) => this.validates(text, 'city')}
                    />
                  </Item>

                </View>

                
                <View style={{flexDirection:'row', flexWrap:'wrap', flex:1}}>
                
                  <Item stackedLabel style={{ borderBottomWidth:0 ,borderRadius:4, width: '98%', alignSelf:'flex-end', marginTop:10}}>
                    <Label style={ styles.labelName }>Postal Code</Label>
                    <Input style={{borderRadius:5, color:'#959595', textAlignVertical:'center', backgroundColor:'#F0F0F0', borderWidth:0, maxWidth:'100%',maxHeight:40,borderBottomWidth:0, paddingLeft:10}}
                      placeholder = '234232'
                      placeholderTextColor="#959595"
                      autoCapitalize="none"
                      maxLength={6}
                      minLength={5}
                      value={this.state.postal_code}
                      keyboardType="numeric"
                      onChangeText={(text) => this.validates(text, 'postal_code')}
                    />
                  </Item>

                </View>



                <View padder style={{alignItems:'flex-end'}}>
                  <Button block style ={styles.buttonStyleTransparent} onPress={this.formsubmit}>
                      <Text style={styles.loginButtonText}> Save </Text>
                  </Button>
                </View>

            </Form>
            </View>
          </View>
     </ScrollView>
    );
  }
  
  }
}
UpdateCreditCard.navigationOptions = {
  title: 'Credit Card',
  headerStyle: {
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height:60,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 0.38
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
    fontWeight:'normal',
    fontSize:20,
    lineHeight:25,

  },
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor:'#ffffff',
    flex:1, 
    justifyContent: 'center',
    paddingRight:10,
    paddingLeft:10,
    marginBottom: 30
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#ffffff',
  },
  input: {
    backgroundColor:'#ffffff', 
    width:'100%',
    paddingLeft:0,
    borderBottomWidth:0
  },
  labelColor: {
    color:'#ffffff'
  },
  labelForgetColor: {
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:2,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  welcomeText:{
    fontSize:30,
    textAlign: 'center',
    flex:1, 
    color:'white',
  },
  normalText:{
    fontSize:12,
    textAlign: 'center',
    flex:1, 
    color:'white',
  },
  buttonStyle:{
    backgroundColor:'white',
    borderWidth:1,
    borderRadius:4,
    borderColor:'#ffffff'
  },
  loginButtonText:{
    color:'#ffffff',
    fontSize:17,
    lineHeight:22,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  buttonStyleTransparent:{
    backgroundColor:'#147BDF',
    borderRadius:150,
    marginTop:160,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  labelName: {
    fontSize: 13,
    lineHeight: 16,
    color: "rgba(0, 0, 0, 0.87)",
    fontWeight: 'normal',
    marginBottom: 8,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing: 0.75
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  textFont: {
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  }

});



export default UpdateCreditCard;
