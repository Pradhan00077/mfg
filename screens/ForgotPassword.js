import React from 'react';
import {
  Container, Content, Text, Form, Item, Input, Button, View, } from 'native-base';
import {Dimensions, Image, StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import Spacer from '../components/UI/Spacer';
import { TextField } from 'react-native-material-textfield';

const window = Dimensions.get('window');
import axios from 'axios';

const Loading=() =><View><Text style={{alignSelf:'center', color:'white', fontWeight:'bold'}}>Loading...</Text></View>;
let email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

class ForgotPassword extends React.Component {
  
  constructor(props) {
    super(props);
    this.config={
      apiUrl:'https://konnect.myfirstgym.com/api/customer/reset-password'
    }
    this.state = {
      Apiresponse:'',
      check:false,
      email: '',
      emailValidator:true,
      errorshow:false,
      error:false,
      errormessage:'',
      email_error: ""
    };
    
    this.validates = this.validates.bind(this);
    this.formsubmit = this.formsubmit.bind(this);
  }

  async fetchForgotPassword(emailAddress) {
    let self = this;
    let errormessage1= ''
      let response = await fetch(this.config.apiUrl, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: emailAddress
        }),
      })

      try {
        
        responseJson = await response.json();
        console.log(" Response Json - ", responseJson)

        if( responseJson.hasOwnProperty('errors') ){

          errormessage1= responseJson.errors.email[0];
          this.setState({emailValidator:false, Apiresponse: "", error:true, check:false, email_error: errormessage1 }) 

        }else{

          if( responseJson.success){
            self.setState({Apiresponse: responseJson.message, error:false, check:false, email_error:""});
          }else {
            errormessage1= responseJson.message;
            this.setState({emailValidator:false, error:true, check:false, Apiresponse: "", email_error: errormessage1 }) 
          }

        }

      } catch (error) {
        alert(error)
        console.log("Response error - ",error);
      }
  }
  
  validates = (text, type) => { 
    if(type==='username'){
      this.setState({ email:text, Apiresponse: "" })
    }
  }

  formsubmit =() =>{
    this.setState({check:true})
    this.fetchForgotPassword(this.state.email)
  }

  render() {
    return (
      <Container>
         <View style ={{backgroundColor:'#2eaaeb',flex:1, justifyContent: 'center',
                alignItems: 'center',alignContent:'center', textAlign:'center'}}>

          <KeyboardAvoidingView  behavior="padding" >

            <Content padder>
                
              {
                Platform.select({
                  ios: (
                    <Spacer size={window.height/12} />
                  ),
                  android: (
                    <Spacer size={window.height/7} />
                  )
                })
              }

              <Image style={[ styles.logoAdjustment ]} source ={require('../assets/images/mgflogo1.png')} resizeMode='contain' />
              
              <Spacer size={window.height/18} />
              
              <Text style ={styles.welcomeText} >Welcome!</Text>              
              <Text style ={styles.normalText} >Reset Your Password and start exploring.</Text>

              {
                Platform.select({
                  ios: (
                    <Spacer size={window.height/34} />
                  ),
                  android: (
                    <Spacer size={window.height/26} />
                  )
                })
              }

                <View>
                  {
                    (this.state.Apiresponse !== '')?
                    (
                      <View>
                        <Text style = {{alignSelf:'center', color:'green'}}>{this.state.Apiresponse}</Text>
                      </View>
                    ):(
                      <View></View>
                    )
                  }
                </View>

              <Form>
               
                  <TextField
                    label='E-mail'
                    onChangeText={(text) => this.validates(text, 'username')} 
                    style={[ styles.labelName ]}
                    tintColor = "#FFFFFF"
                    baseColor = "#FFFFFF"
                    keyboardType='email-address'
                    autoCapitalize='none'
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    returnKeyType='done'
                    error	= {this.state.email_error}
                  />

                <Spacer size={20} />
                <View>
                  <Button block style={[styles.buttonStyle, this.state.check? styles.disablebtn:null]} onPress={this.formsubmit} disabled = { this.state.check} >
                      <Text style={styles.loginButtonText}> {this.state.check?'Loading...':' Reset Password'}</Text>
                  </Button>
                </View>
              </Form>
            </Content>
            
          </KeyboardAvoidingView>

        </View>
      </Container>
    );
  }
}

ForgotPassword.navigationOptions = {
  header: null,
  headerStyle: {
    backgroundColor: '#0099EF',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
  },
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign:"center", 
    flex:1 ,
    fontSize:20,
    lineHeight:25
  },
};

const styles = StyleSheet.create({

  labelColor:{
    color:'#ffffff'
  },
  errormsg:{
    alignSelf:'center' ,
    color: "red",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },

  logoAdjustment: {
    ...Platform.select({
      ios: {
        width:270, height: 150, alignSelf:'center'
      },
      android: {
        width:300, height: 160, alignSelf:'center'
      },
    }),
  },

  labelForgetColor:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:2,
  },
  disablebtn:{
    backgroundColor:'#cccccc'
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  welcomeText:{
    fontSize:30,
    textAlign: 'center',
    flex:1, 
    color:'white',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  normalText:{
    fontSize:16,
    textAlign: 'center',
    flex:1, 
    color:'#FFFFFF',
    paddingLeft: 15,
    paddingRight: 15,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  buttonStyle:{
    backgroundColor:'white',
    borderWidth:0,
    borderRadius:4,
    elevation:0
  },

  loginButtonText:{
    color:'#2eaaeb',
    fontSize:15,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  labelName: {
    color: "#ffffff",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  }

});


export default ForgotPassword;
