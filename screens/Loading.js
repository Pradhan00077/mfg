import React, { Component } from 'react';
import { View, StyleSheet, Platform, ScrollView  } from 'react-native';
import { Text } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import { LinearGradient } from 'expo-linear-gradient';
export default class Loading extends Component {

  constructor (props){
    super(props);
    this.navigate = this.props.navigation;
    this.params = this.props.navigation.state;
    this.state={
      logoutTokenGet: (this.params.params)?true:false
    }
  }

componentDidMount() {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Favourite' })],
  });
  this.props.navigation.dispatch(resetAction);
if(this.state.logoutTokenGet){
  this.props.navigation.navigate('Home', { logoutUser: true });
}
else{
  this.props.navigation.navigate('Main', {login:true})
}
  
}

  render(){
          return(
            <ScrollView style={styles.container}>
            {/* <View style ={styles.mainContainer}>
            <View style={styles.loading}>
                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#ffffff"  />
                </View>
            </View> */}
        </ScrollView>
          )
  }
}
Loading.navigationOptions = {
    headerTitle:(
      <View style ={{flex:1}}><Text style = {{textAlign:'center', fontSize:20,lineHeight:25, alignSelf:'center', color:'#ffffff',...Platform.select({ios: { fontFamily:'SFProDisplayRegular' },  android: {  fontFamily:'RobotoRegular_1' }, }), }}>Home</Text></View>
    ),
    headerStyle: {
      backgroundColor: '#E5E5E5',
      height:60,
      elevation: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ 
          flex: 1,
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          elevation: 5
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };



  const styles = StyleSheet.create({

    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 0.5,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContainer:{
      backgroundColor:'transparent',
      flex:1, 
      justifyContent: 'center',
      paddingRight:15,
      paddingLeft:15,
      marginBottom: 30,
    },
    container: {
      flex: 1,
      paddingTop: 15,
      paddingBottom: 50,
      backgroundColor: 'transparent',
    },
    spinnerTextStyle: {
      color: '#FFF',
    },

  });
  