import React from 'react';
import { View, StyleSheet, Image, ScrollView, Platform, Dimensions, AsyncStorage } from 'react-native';
import { Container, Text, Button } from 'native-base';
import Spacer from '../components/UI/Spacer';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
const screenHeight = Math.round(Dimensions.get('window').height);
import ActivityIndicator from 'react-native-loading-spinner-overlay';

class BuyServices extends React.Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      ServiceItems: {},
      isLoading: true,
      Checkout: true,
      buttonDisable: true
    }
  }
  async fetchBuyServices(token) {
    try {
      let response = await fetch('https://konnect.myfirstgym.com/api/customer/items?api_token=' + token);
      let responseJson = [];
      try {
        responseJson = await response.json();
      } catch (error) {
        console.log(error);
        responseJson = [];
      }
      this.setState({ ServiceItems: responseJson });
      this.state.ServiceItems.data.map((item, i) => {
        item.items.map((itemlist, j) => {
          itemlist.isSelect = false;
        })
      });
      this.setState({ isLoading: false, Checkout: false });
    } catch (error) {
      alert(error)
      console.log(error);
    }
  }

  Selecteditem = (cat_name, id) => {
    let Services = [];
    Services = this.state.ServiceItems;
    Services.data.map((item, i) => {
      if (cat_name === item.name) {
        item.items.map((itemlist, j) => {
          if (id === itemlist.id) {
            itemlist.isSelect = !itemlist.isSelect;
          }
          else{
            itemlist.isSelect=false;
          }
        })
      }
    })
    this.setState({ ServiceItems: Services })
  }

  componentDidMount() {

    AsyncStorage.getItem('api_token').then((token) => {
      if (token !== null) {
        this.fetchBuyServices(token);
      }
    });

  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  checkout = () => {
    this.saveItem('ServiceItems', JSON.stringify(this.state.ServiceItems));
    let isSelected = false;
    this.state.ServiceItems.data.forEach((element) => {
      if(element.items.filter(x => x.isSelect).length > 0){
          isSelected = true;
      }
    });

    if (isSelected) {
      this.props.navigation.navigate('Checkout', { replace: true });
    }
    else {
      alert("Please select atleast one service.");
    }

  }

  fromCheckout = () => {
    AsyncStorage.getItem('ServiceItems').then((servicesData) => {
      if (servicesData !== null) {
        newServicesData = JSON.parse(servicesData);
        this.setState({ ServiceItems: newServicesData });
        this.setState({ isLoading: false, Checkout: false });
      }
      else {
        AsyncStorage.getItem('api_token').then((token) => {
          if (token !== null) {
            this.fetchBuyServices(token);
          }
        });
      }
    });
  }

  render() {

    let buyServices = [];
    if (Object.keys(this.state.ServiceItems).length > 0) {
      this.state.ServiceItems.data.map((item, i) => {
        let title_cat = true;
        buyServices.push(item.items.map((itemlist, j) => {
          let title = '';
          let cat_name = item.name;
          if (title_cat) {
            title = item.name;
            title_cat = false;
          }
          else {
            title = null;
          }
          return (
            <View key={j}>
              {
                (title) ?
                  (
                    <View>
                      <Spacer size={15} />
                      <View style={styles.headingView} >
                        <Text style={styles.textHeading} >{title}</Text>
                      </View>
                    </View>
                  ) :
                  (

                    <Text style={[styles.textFonts, { height: 0 }]}>{title}</Text>

                  )
              }
              <TouchableHighlight style={[styles.listItemsDesign, itemlist.isSelect ? styles.selectedItemColor : styles.unSelectedItemColor]} underlayColor="#ffffff" onPress={() => this.Selecteditem(cat_name, itemlist.id)}>
                {
                  (itemlist.isSelect) ?
                    (
                      <LinearGradient
                        locations={[0, 0.4]}
                        colors={['#00D2EF', '#0099EF']}
                        style={{
                          flex: 1,
                          borderRadius: 4,
                        }}
                      >
                        <View style={styles.rowStyle}>
                          <View style={[styles.serviceName]}><Text style={[styles.textFont, styles.textFonts, { color: itemlist.isSelect ? '#ffffff' : '#000000' }]}>{itemlist.name}</Text></View>
                          <View style={[styles.serviceMoney]}><Text style={[styles.priceFont, styles.textFonts, { color: itemlist.isSelect ? '#ffffff' : '#000000' }]}>AED {parseFloat(itemlist.price).toFixed(2)}</Text></View>
                        </View>
                      </LinearGradient>
                    ) :
                    (
                      <View style={styles.rowStyle}>
                        <View style={[styles.serviceName]}><Text style={[styles.textFont, styles.textFonts, { color: itemlist.isSelect ? '#ffffff' : '#000000' }]}>{itemlist.name}</Text></View>
                        <View style={[styles.serviceMoney]}><Text style={[styles.priceFont, styles.textFonts, { color: itemlist.isSelect ? '#ffffff' : '#000000' }]}>AED {parseFloat(itemlist.price).toFixed(2)}</Text></View>
                      </View>
                    )
                }
              </TouchableHighlight>
            </View>
          );
        }));
      })
    }

    return (
      <Container style={{ backgroundColor:'#E5E5E5' }}>
        {
          (this.state.isLoading) ?
            (

              <ScrollView style={styles.container}>
                <View style={styles.mainContainer}>
                  <View style={styles.loading}>
                    <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff" />
                  </View>
                </View>
              </ScrollView>

            ) :
            (
              <ScrollView style={styles.container}>
                <View style={styles.mainContainer}>
                  <View style={{ flex: 1 }}>
                    <View>{buyServices}</View>
                    <Spacer size={5} />
                  </View>
                </View>
              </ScrollView>

            )
        }
        {
          (this.state.isLoading) ?
            (
              <ScrollView style={styles.container}>
                <View style={styles.mainContainer}>
                  <View style={styles.loading}>
                    <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff" />
                  </View>
                </View>
              </ScrollView>
            ) :
            (
              <View style={styles.cartButtonView} >
                <Button style={styles.cartButton} onPress={() => this.checkout()} underlayColor='#ffffff'>
                  <Image style={styles.imageStyle} source={require('../assets/images/cart.png')} />
                </Button>
              </View>
            )
        }
      </Container>
    )
  }
}
BuyServices.navigationOptions = {
  title: 'Buy Services',
  headerStyle: {
    backgroundColor: '#E5E5E5',
    height: 60,
    borderBottomWidth: 0,
    elevation: 0,
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{
        flex: 1,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        elevation: 5,
        
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),

      }}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign: "center",
    flex: 1,
    fontSize: 20,
    lineHeight: 25,
    paddingRight: 35,
    ...Platform.select({
      ios: {
        fontFamily: 'SFProDisplayRegular',
        paddingRight:0
      },
      android: {
        fontFamily: 'RobotoRegular_1'
      },
    }),
  },
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 20
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#E5E5E5',
  },
  headingView: {
    height: 24,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    flexWrap: 'wrap',
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
    margin: 0,
    flex: 1,
    marginBottom: 10
  },
  textHeading: {
    width: '100%',
    textAlign: 'center',
    fontSize: 15,
    lineHeight: 20,
    alignContent: 'center',
    alignSelf: 'center',
    margin: 0,
    position: 'relative',
    color: 'rgba(0, 0, 0, 0.87)',
    ...Platform.select({
      ios: {
        fontFamily: 'SFProTextRegular',
        letterSpacing: -0.24
      },
      android: {
        fontFamily: 'RobotoRegular_1'
      },
    }),
  },
  serviceContent: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10
  },
  listItemsDesign: {
    backgroundColor: '#ffffff',
    color: '#ffffff',
    flexWrap: 'wrap',
    flexDirection: 'row',
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
    marginLeft: 15,
    marginRight: 15,

    borderRadius: 4,
  },
  selectedItemColor: {
    // backgroundColor:'#0099EF',
    borderRadius: 4
  },
  unSelectedItemColor: {
    backgroundColor: '#ffffff',
  },
  rowStyle: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    flex: 1,
    height: '100%',
    padding: 15,
  },
  listItemsDesignAction: {
    backgroundColor: '#0099EF',
    color: '#ffffff'
  },
  serviceName: {
    flex: 0.7,
    alignItems: 'flex-start',
  },
  serviceMoney: { flex: 0.3, textAlign: 'right', alignItems: 'flex-end', fontSize: 13 },
  textFont: {
    fontSize: 17,
    lineHeight: 22,
    letterSpacing: -0.41,
  },
  priceFont: {
    fontSize: 13,
    lineHeight: 18,
    letterSpacing: -0.08,
  },

  cartButtonView: {
    position: "absolute",
    bottom: 18,
    right: 18,
    borderRadius: 33,
    width: 66,
    height: 66,
    
  },

  cartButton: {
    alignItems: 'center',
    width: 66,
    height: 66,
    borderRadius: 33,
    backgroundColor: '#FFFFFF',
    color: '#ffffff',
    alignSelf: 'flex-end',
    zIndex: 11,
    elevation: 10,
    ...Platform.select({
      ios:{
        shadowOffset:{  width: 8,  height: 8,  },
        shadowColor: ' rgba(0, 0, 0, 0.24)',
        shadowOpacity: 0.8,
        shadowRadius: 10,
      }
    }),
  },

  imageStyle: {
    position: "relative",
    left: 20,
    top: 1,
    alignSelf: 'center',
    width: 24,
    height: 24
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  textFonts: {
    ...Platform.select({
      ios: {
        fontFamily: 'SFProTextRegular'
      },
      android: {
        fontFamily: 'RobotoRegular_1'
      },
    }),
  }
});
export default BuyServices;
