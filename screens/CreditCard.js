import React from 'react';
import { ActivityIndicator, View, Image, StyleSheet, TouchableHighlight, ImageBackground, Platform, AsyncStorage, RefreshControl } from 'react-native';
import {Container, Content,Text} from 'native-base';
import Spacer from '../components/UI/Spacer';
import { LinearGradient } from 'expo-linear-gradient';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, sliderItemWidth } from '../components/ExploreSlider/Styles_slider';

class CreditCard extends React.Component{

  constructor(props){
    super(props);
    this.config={
      SavedCardsListAPI:'https://konnect.myfirstgym.com/api/customer/credit-cards'
    }
    this.state={
      fetchCardsList: [],
      refreshing:false,
      isLoading: true
    }
  }

   //FOLLOWING COMPONENT USING FOR UNDEFIEND SERVICES ID'S 
   renderListComponent = ({ item }) => (
    <View style={{ flex:1 }}>
    <TouchableHighlight onPress={() => this.props.navigation.navigate('UpdateCreditcard', {cardId: item.id})} underlayColor="white">
      <ImageBackground source={require('../assets/images/creditcard.png')} style = {{width:'100%',minHeight:200, flexGrow:1,borderRadius:12}} resizeMode="stretch">
        <Spacer size={110} />

        <View style ={{marginLeft:15, flexDirection:'row',flexWrap:'wrap' , flex:1}}>
          <View style ={{flexDirection:'row' , flex:1, position:'absolute'}}>
              <Text style ={[styles.ExpdateText]} >Exp   {item.exp_month+"/"+ item.exp_year.substr(item.exp_year.length - 2)}</Text>
          </View>
          <Text style ={[styles.cardNumberText]} >....  ....  ....  </Text>
          <Text style ={[styles.balanceText]} >{item.card_number.substr(item.card_number.length - 4)}</Text>
        </View>

      </ImageBackground>
    </TouchableHighlight>
    </View>
   );
  
  //FETCHING THE SAVED CARDS DETAILS
  async fetchCardsList(api_token) {
    let responseJson = [];

    try{
        let response = await fetch(this.config.SavedCardsListAPI+'?api_token='+api_token);
        try {
          responseJson = await response.json();
          this.setState({ fetchCardsList: responseJson.data, isLoading:false });
        }catch (error){
          alert(error)
          console.log(error);
        }
    }catch (error){
      alert(error);
      console.log(error);
    }
  }


  componentDidMount() {

    //initialize your function
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.fetchCardsList(token); 
      }else{
        this.setState({isLoading:false});
      }
    })

  }

  _onRefresh = () => {
    this.setState({refreshing: true});

    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.fetchCardsList(token).then(() => {
          this.setState({refreshing: false});
        }); 
      }else{
        this.setState({isLoading:false});
      }
    })
  }

  render(){
  
   // console.log("sliderItemWidth - ", sliderItemWidth-sliderItemHorizontalMargin * 2)

    if(this.state.isLoading){
      return(
        <View style={styles.loading}>
          <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
        </View>
      )
    }else{
      return(
        <Container>
        <View style ={{flex:1, textAlign:'center'}}>
        <Content 
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <View>
            {(this.state.fetchCardsList != "")?
              (
                <View style = {styles.rootView}>
                  <View>
                    <Carousel
                      //contentContainerCustomStyle={{ height: sliderItemWidth-sliderItemHorizontalMargin * 2 }}
                      data={this.state.fetchCardsList}
                      renderItem={this.renderListComponent}
                      sliderWidth={sliderWidth}
                      itemWidth= {sliderItemWidth*2}
                      activeSlideAlignment={'start'}
                      inactiveSlideOpacity={1}
                      inactiveSlideScale={0.90}
                    />
                  </View>
                </View>
              ):(
                <View style={{ minHeight:22 }}></View>
              )
            }
            <TouchableHighlight  style = {styles.addNewVardView} onPress={() => this.props.navigation.navigate('NewCreditcard')} underlayColor="transparent">
              <View style={styles.content} >
                <Image style={styles.settingcardIcon}  source = {require('../assets/images/buy-services.png')}/>
              </View>
            </TouchableHighlight>
          </View>
        </Content>
        </View>
      </Container>
      );
    }

  }
  
}

CreditCard.navigationOptions = {
  title: 'Credit Card',
  headerStyle: {
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height:60,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        elevation:2,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),
        
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    alignSelf: 'center',
    textAlign:"center", 
    fontSize:20,
    lineHeight:25,
    flex:0.8,
    fontWeight:'normal',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }), 
  },  
};


const styles = StyleSheet.create({
rootView:{
  minHeight:200,
    borderRadius:12,
    position:'relative',
    width:'90%',
    alignSelf:'center',
    marginTop:22,
    color:'#ffffff',
    paddingBottom: 2,
},
  buttonView:{
    minHeight:30,
    position:'relative',
    width:'95%',
    alignSelf:'center',
    marginTop:50,
    marginBottom:15,
},
addNewVardView:{
  marginTop:20,
  backgroundColor:'#cccccc',
  minHeight:200,
  borderRadius:12,
  position:'relative',
  width:'90%',
  alignSelf:'center',
  paddingBottom:15,
  justifyContent:'center',
  alignItems: 'center',
},
profileName:{
    fontSize:22,
    textAlign:'left',
    color:'#ffffff'
},  
cardColor:{
    color:'#ddd9d9',
    fontStyle: 'italic'
},
ExpdateText:{
  fontSize:16,
  lineHeight: 24,
  textAlign:'right',
  color:'#ffffff',
  ...Platform.select({
    ios: {
      fontWeight:"600",
      letterSpacing: 0.1
    },
    android: {
      fontWeight:"normal"
    },
  }),
  paddingLeft: 10
},
balanceText:{
    fontSize:18,
    lineHeight: 24,
    textAlign:'right',
    color:'#ffffff',
    paddingTop:28,
    paddingLeft: 0
  },
  cardNumberText:{
    fontSize:36,
    textAlign:'right',
    color:'#ffffff',
    marginRight: 6,
    paddingTop: 1,
    paddingLeft: 10
  },
balanceBoldText:{
    fontSize:26,
    fontWeight:'bold',
    textAlign:'right',
    textTransform: 'uppercase',
    color:'#ffffff'
  },
  settingcardIcon:{
    width:40,
    height:40,
    zIndex:99,
    marginTop:80
  },
  content:{
    textAlign:'center',
    alignItems:'center',
    margin:0,
    flex:1,
    
},
buttonStyleTransparent:{
  backgroundColor:'#cccccc',
  borderRadius:20,
},
transparentButtonText:{
  color:'#ffffff'
},
});

// CreditCard.propTypes = {
//   member: PropTypes.shape({}),
//   logout: PropTypes.func.isRequired,
// };

// CreditCard.defaultProps = {
//   member: {},
// };

export default CreditCard;
