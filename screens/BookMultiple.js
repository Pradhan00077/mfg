import React, { Component }  from 'react';
import { View, StyleSheet, Image, ScrollView, TouchableHighlight, TouchableOpacity,AsyncStorage, Platform, DatePickerIOS, DatePickerAndroid } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import Spacer from '../components/UI/Spacer';
import Modal from "react-native-modal";
import CalendarPicker from 'react-native-calendar-picker';
import { LinearGradient } from 'expo-linear-gradient';
import axios from 'axios';
import Checkbox from 'react-native-modest-checkbox';
import DatePicker from 'react-native-datepicker';
import * as Calendar from 'expo-calendar';
import * as Permissions from 'expo-permissions';

class BookMultiple extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.config= {

      addMultipleBookingAPI:  'https://konnect.myfirstgym.com/api/customer/schedules/'+this.params.itemID+'/date/'+this.params.itemDateID+'/book-multiple',
      deleteMultipleBookingAPI:  'https://konnect.myfirstgym.com/api/customer/schedules/'+this.params.itemID+'/date/'+this.params.itemDateID+'/cancel',

    }
    this.state = {
      booking: true,
      isModalVisible: false,
      isModalAttention: false,
      isModalAtToSucs: false,
      isModalSelectDay: false,
      isModalStartDate: false,
      isModalEndDate: false,
      
      onChangedStartDate: null,
      onChangedEndDate: null,
      changedStartDate: null,
      changedEndDate: null,
      
      bookingStartDate: "Munday 01 July",
      bookingEndDate: "Sunday 14 July",

      selectedDaysState: "All",
      selectedDaysInArray: [0, 1, 2, 3, 4, 5, 6],

      isCheckedAll: false,
      isCheckedDays0: false,
      isCheckedDays1: false,
      isCheckedDays2: false,
      isCheckedDays3: false,
      isCheckedDays4: false,
      isCheckedDays5: false,
      isCheckedDays6: false,

      loggedInUserChilds: [],
      loggedInUserChildsCount: 1,
      isCheckedAllChilds: false,
      isCheckedChilds: [],
      selectedUserChilds: [],
      isModalUsersChilds: false,      
      minEndDate: "",
      maxStartDate: "",
      
      changedEndDateForIos: "",
      changedEndDateToDisForIos: "Munday 01 August",
      minEndDateForIos: "",

      changedStartDateForIos: "",
      changedStartDateToDisForIos: "Munday 01 July",
      maxStartDateForIos: "",

      onChangedStartDateToBook: null,
      onChangedEndDateToBook: null,
    }
  }


  allCheckedDays = () => {

    if (this.state.isCheckedAll == false) {

      this.setState((prevState) => ({ isCheckedAll: true }));
      this.setState((prevState) => ({ isCheckedDays0: true }));
      this.setState((prevState) => ({ isCheckedDays1: true }));
      this.setState((prevState) => ({ isCheckedDays2: true }));
      this.setState((prevState) => ({ isCheckedDays3: true }));
      this.setState((prevState) => ({ isCheckedDays4: true }));
      this.setState((prevState) => ({ isCheckedDays5: true }));
      this.setState((prevState) => ({ isCheckedDays6: true }));

    }else{

      this.setState((prevState) => ({ isCheckedAll: !prevState.isCheckedAll }));
      this.setState((prevState) => ({ isCheckedDays0: !prevState.isCheckedDays0 }));
      this.setState((prevState) => ({ isCheckedDays1: !prevState.isCheckedDays1 }));
      this.setState((prevState) => ({ isCheckedDays2: !prevState.isCheckedDays2 }));
      this.setState((prevState) => ({ isCheckedDays3: !prevState.isCheckedDays3 }));
      this.setState((prevState) => ({ isCheckedDays4: !prevState.isCheckedDays4 }));
      this.setState((prevState) => ({ isCheckedDays5: !prevState.isCheckedDays5 }));
      this.setState((prevState) => ({ isCheckedDays6: !prevState.isCheckedDays6 }));

    }

  }

  //SELECTED DAYS 
  CheckedSelectedDays = () => {
    
    let totDaysSelected = [];

    if(this.state.isCheckedAll){
      this.setState({ selectedDaysState: "All" });
      this.setState({ selectedDaysInArray: this.state.selectedDaysInArray });
    }else{

      if(this.state.isCheckedDays0){ totDaysSelected.push({ id: 0, value : "Sun" }) }
      if(this.state.isCheckedDays1){ totDaysSelected.push({ id: 1, value : "Mon" }) }
      if(this.state.isCheckedDays2){ totDaysSelected.push({ id: 2, value : "Tus" }) }
      if(this.state.isCheckedDays3){ totDaysSelected.push({ id: 3, value : "Wed" }) }
      if(this.state.isCheckedDays4){ totDaysSelected.push({ id: 4, value : "Thu" }) }
      if(this.state.isCheckedDays5){ totDaysSelected.push({ id: 5, value : "Fri" }) }
      if(this.state.isCheckedDays6){ totDaysSelected.push({ id: 6, value : "Sat" }) }


      let getValInArr = [];
      let selectedDaysInArr = [];

      totDaysSelected.map((item, i) => {
        getValInArr[i] = item.value;
        selectedDaysInArr[i] = item.id;
      })

      this.setState({ selectedDaysInArray: selectedDaysInArr });

      addSelectedDateInText = getValInArr.join("-");
      this.setState({ selectedDaysState: addSelectedDateInText });
    }
    
    this.setState({ isModalSelectDay: !this.state.isModalSelectDay });

  }

  //USING FOLLOWING FUNCTIONS FOR START AND END DATE
  onDateChangeForStartDate = (date) => { this.setState({ onChangedStartDate: date }) }
  onSelectChangeStartDate = () => {
    if(this.state.onChangedStartDate == ''){
      const customDate = new Date();
      this.setState({ changedStartDate: customDate })   
    }else{
      this.setState({ changedStartDate: this.state.onChangedStartDate }) 
    }
    
    this.setState({ isModalStartDate: false });
  } 

  onDateChangeForEndDate = (date) => { this.setState({ onChangedEndDate: date }) }
  onSelectChangeEndDate = () => {
    if(this.state.onChangedEndDate == ''){
      const customDate = new Date;
      this.setState({ changedEndDate: customDate })   
    }else{
      this.setState({ changedEndDate: this.state.onChangedEndDate }) 
    }    
    this.setState({ isModalEndDate: false });
  } 


  //USING COMPONENT TO ADD EVENT IN CALENDAR AS REMINDER
   AddReminderCmpt = async()=> {
    let self =this;
     
    const  newStatus  = await Permissions.askAsync( Permissions.CALENDAR ); 
    if(newStatus.status === 'granted') {      
      const status  = await Permissions.getAsync( Permissions.CALENDAR );
      if (status.status === 'granted') {
        let startDate = self.state.onChangedStartDateToBook ? self.state.onChangedStartDateToBook : self.params.itemDate;
        let endDate = self.state.onChangedEndDateToBook ? self.state.onChangedEndDateToBook : self.params.itemDate; 
        let InputPayLoadCal={
            title : self.params.itemName,//self.state.scheduleitem['schedule'].class.service_category.name,
            startDate: startDate,//'2019-08-09T06:05:00.000Z',
            endDate: endDate,
            location :self.params.itemLoc,//self.state.scheduleitem['schedule'].branches[0].detail.address,
            notes  : self.params.itemDesc,//self.state.scheduleitem['schedule'].class.description, 
            timeZone: "GMT+5:30"
        };
        console.log(InputPayLoadCal);
        let result = await Calendar.createEventAsync("100",InputPayLoadCal );
        await Calendar.openEventInCalendar(result);
        //console.log(CalOpen);
        self.setState({ isModalVisible: !self.state.isModalVisible });
        //console.log('ALL status - ', result );
      }else {
      alert("Calendar Event can not be add!")
    }

    }else {
      alert("Calendar Event can not be add!")
    }
  };
  BookingClickAction =() =>{
    this.setState({ booking: !this.state.booking });
    this.setState({ isModalAtToSucs: false }); 
  };

  // MODAL POPUPS ACTION 
  cancelButtonAction = () => { this.setState({ isModalAttention: !this.state.isModalAttention }); }  
  AttentionButtonAction = () => { this.setState({ isModalAttention: !this.state.isModalAttention }); }
  
  SelectDayButtonAction = () => { this.setState({ isModalSelectDay: !this.state.isModalSelectDay }); }
  

  async StartDateButtonActionForIos(date) {

    //this.setState({ isModalStartDate: !this.state.isModalStartDate }); 

    let getDates = ''
    let currentDate = ''
    let months = ''
    let numMonths = ''
    let numDays = ''
    let days = ''
    let years = ''
    let weekDays = ''
    let self = this
    
      console.log("date start - ", date)

      try {

        let day = date.split("-");
          day = day[2]
        let month = date.split("-");
          month = month[1]
        let year = date.split("-");
          year = year[0]
  
        if (day != "" && month != "" && year != "" ) {
          
          days = day
          numDays = parseInt(day) + 1
          years = year
          numMonths = parseInt(month)
  
          if(month == "01"){  months = 'January' }
          else if(month == "02"){  months = 'February' }
          else if(month == "03"){  months = 'March' }
          else if(month == "04"){  months = 'April' }
          else if(month == "05"){  months = 'May' }
          else if(month == "06"){  months = 'June' }
          else if(month == "07"){  months = 'July' }
          else if(month == "08"){  months = 'August' }
          else if(month == "09"){  months = 'September' }
          else if(month == "10"){  months = 'October' }
          else if(month == "11"){  months = 'November' }
          else if(month == "12"){  months = 'December' }
  
          currentDate = months+' '+ days +', '+years;
  
          let minDates = year+'-'+ numMonths +'-'+numDays;
  
          var getDate = new Date(currentDate);
          getDates = getDate.getDay();
  
          if(getDates == 0){  weekDays = 'Sunday' }
          else if(getDates == 1){  weekDays = 'Monday' }
          else if(getDates == 2){  weekDays = 'Tuesday' }
          else if(getDates == 3){  weekDays = 'Wednesday' }
          else if(getDates == 4){  weekDays = 'Thursday' }
          else if(getDates == 5){  weekDays = 'Friday' }
          else if(getDates == 6){  weekDays = 'Saturday' }
          self.setState({ onChangedStartDateToBook: date, changedStartDate: getDate, changedStartDateForIos: date, changedStartDateToDisForIos: weekDays + ' ' + days + ' ' + months, maxStartDateForIos: minDates })
        
          //self.setState({ changedStartDate: getDate, changedStartDateForIos: date, changedStartDateToDisForIos: weekDays + ' ' + days + ' ' + months, maxStartDateForIos: minDates })
        }
  
      } catch ({code, message}) {
        console.warn('Cannot open date picker', message);
      }



  }


  async EndDateButtonActionForIos(date) {

    //this.setState({ isModalStartDate: !this.state.isModalStartDate }); 

    let getDates = ''
    let currentDate = ''
    let months = ''
    let numMonths = ''
    let numDays = ''
    let days = ''
    let years = ''
    let weekDays = ''
    let self = this
    
      console.log("date start - ", date)

      try {

        let day = date.split("-");
          day = day[2]
        let month = date.split("-");
          month = month[1]
        let year = date.split("-");
          year = year[0]
  
        if (day != "" && month != "" && year != "" ) {
          
          days = day
          numDays = parseInt(day) - 1
          years = year
          numMonths = parseInt(month)
  
          if(month == "01"){  months = 'January' }
          else if(month == "02"){  months = 'February' }
          else if(month == "03"){  months = 'March' }
          else if(month == "04"){  months = 'April' }
          else if(month == "05"){  months = 'May' }
          else if(month == "06"){  months = 'June' }
          else if(month == "07"){  months = 'July' }
          else if(month == "08"){  months = 'August' }
          else if(month == "09"){  months = 'September' }
          else if(month == "10"){  months = 'October' }
          else if(month == "11"){  months = 'November' }
          else if(month == "12"){  months = 'December' }
  
          currentDate = months+' '+ days +', '+years;
  
          let minDates = year+'-'+ numMonths +'-'+numDays;
  
          var getDate = new Date(currentDate);
          getDates = getDate.getDay();
  
          if(getDates == 0){  weekDays = 'Sunday' }
          else if(getDates == 1){  weekDays = 'Monday' }
          else if(getDates == 2){  weekDays = 'Tuesday' }
          else if(getDates == 3){  weekDays = 'Wednesday' }
          else if(getDates == 4){  weekDays = 'Thursday' }
          else if(getDates == 5){  weekDays = 'Friday' }
          else if(getDates == 6){  weekDays = 'Saturday' }
          self.setState({ onChangedEndDateToBook: date, changedEndDate: getDate, changedEndDateForIos: date, changedEndDateToDisForIos: weekDays + ' ' + days + ' ' + months, minEndDateForIos: minDates })
        
          //self.setState({ changedEndDate: getDate, changedEndDateForIos: date, changedEndDateToDisForIos: weekDays + ' ' + days + ' ' + months, minEndDateForIos: minDates })
        }
  
      } catch ({code, message}) {
        console.warn('Cannot open date picker', message);
      }



  }


  StartDateButtonAction = async() => {

    //this.setState({ isModalStartDate: !this.state.isModalStartDate }); 

    let getDates = ''
    let currentDate = ''
    let months = ''
    let numMonths = ''
    let numDays = ''
    let days = ''
    let years = ''
    let weekDays = ''
    let self = this;
    
    console.log("date start - ", this.state.minEndDate)

    try {
      const {action, year, month, day} = await DatePickerAndroid.open(
        (this.state.minEndDate != '')?
        {
          date: new Date(),
          maxDate:  new Date(this.state.minEndDate),
        }:{
          date: new Date()
        }
      )

      if (action !== DatePickerAndroid.dismissedAction) {
        
        days = day
        numDays = day + 1
        years = year
        numMonths = month + 1

        if (days < 10) {  days = '0' + days; }
        if (numDays < 10) {  numDays = '0' + numDays; }
        if (numMonths < 10) {  numMonths = '0' + numMonths; }

        if(month == 0){  months = 'January' }
        else if(month == 1){  months = 'February' }
        else if(month == 2){  months = 'March' }
        else if(month == 3){  months = 'April' }
        else if(month == 4){  months = 'May' }
        else if(month == 5){  months = 'June' }
        else if(month == 6){  months = 'July' }
        else if(month == 7){  months = 'August' }
        else if(month == 8){  months = 'September' }
        else if(month == 9){  months = 'October' }
        else if(month == 10){  months = 'November' }
        else if(month == 11){  months = 'December' }

        currentDate = months+' '+ days +', '+years;

        let minDates = year+'-'+ numMonths +'-'+numDays;

        var getDate = new Date(currentDate);
        getDates = getDate.getDay();

        if(getDates == 0){  weekDays = 'Sunday' }
        else if(getDates == 1){  weekDays = 'Monday' }
        else if(getDates == 2){  weekDays = 'Tuesday' }
        else if(getDates == 3){  weekDays = 'Wednesday' }
        else if(getDates == 4){  weekDays = 'Thursday' }
        else if(getDates == 5){  weekDays = 'Friday' }
        else if(getDates == 6){  weekDays = 'Saturday' }
        self.setState({ onChangedStartDateToBook: year + "-" + month + "-" +day, changedStartDate: weekDays + ' ' + days + ' ' + months, maxStartDate: minDates })
        //self.setState({ changedStartDate: weekDays + ' ' + days + ' ' + months, maxStartDate: minDates })
      }

    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }

  }
  
  EndDateButtonAction = async() => {

    //this.setState({ isModalEndDate: !this.state.isModalEndDate }); 

    let getDates = ''
    let currentDate = ''
    let months = ''
    let numMonths = ''
    let numDays = ''
    let days = ''
    let years = ''
    let weekDays = ''
    let self = this ;
    
    console.log("date start - ", this.state.maxStartDate)
    
    try {
      const {action, year, month, day} = await DatePickerAndroid.open(
        (this.state.maxStartDate != '')?
        {
          date: new Date(),
          minDate: new Date(this.state.maxStartDate),
        }:{
          date: new Date()
        }
      )

      if (action !== DatePickerAndroid.dismissedAction) {
        
        days = day
        numDays = day - 1
        years = year
        numMonths = month + 1

        if (days < 10) {  days = '0' + days; }
        if (numDays < 10) {  numDays = '0' + numDays; }
        if (numMonths < 10) {  numMonths = '0' + numMonths; }

        if(month == 0){  months = 'January' }
        else if(month == 1){  months = 'February' }
        else if(month == 2){  months = 'March' }
        else if(month == 3){  months = 'April' }
        else if(month == 4){  months = 'May' }
        else if(month == 5){  months = 'June' }
        else if(month == 6){  months = 'July' }
        else if(month == 7){  months = 'August' }
        else if(month == 8){  months = 'September' }
        else if(month == 9){  months = 'October' }
        else if(month == 10){  months = 'November' }
        else if(month == 11){  months = 'December' }

        currentDate = months+' '+ days +', '+years;
        let minDates = year+'-'+ numMonths +'-'+numDays;
        var getDate = new Date(currentDate);
        getDates = getDate.getDay();

        if(getDates == 0){  weekDays = 'Sunday' }
        else if(getDates == 1){  weekDays = 'Monday' }
        else if(getDates == 2){  weekDays = 'Tuesday' }
        else if(getDates == 3){  weekDays = 'Wednesday' }
        else if(getDates == 4){  weekDays = 'Thursday' }
        else if(getDates == 5){  weekDays = 'Friday' }
        else if(getDates == 6){  weekDays = 'Saturday' }
        
        var compltDate = weekDays + ' ' + days + ' ' + months;
self.setState({ onChangedEndDateToBook: year + "-" + month + "-" +day, changedEndDate: compltDate, minEndDate: minDates })
        //self.setState({ changedEndDate: compltDate, minEndDate: minDates })
      }

    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  
  }
  

  //FOLLOWING FUNCTION USING API CALL ON CLICK OF OK BUTTON FROM ATTENTION POP-UP
  AttentionToSuccessBtnAction = (state) => {
    let self = this;
    AsyncStorage.getItem('api_token').then((token) => {

      if(token!==null){
        let response = axios.delete(this.config.deleteMultipleBookingAPI+"?api_token="+token)
        .then(function (response) {
        
        //  self.setState({ isModalAtToSucs: !self.state.isModalAtToSucs });
          self.setState({ isModalAttention: false,isModalAtToSucs: true, booking: !self.state.booking });

        })        
        .catch(function (error) {
          console.log(error);
        });
      }
    
    });

  }

  //FOLLOWING FUNCTION USING API CALL ON CLICK OF BOOK MULTIPLE BUTTON 
  BookingModalAction = () => {
     let startDate = this.state.onChangedStartDateToBook ? this.state.onChangedStartDateToBook : '';
    let endDate = this.state.onChangedEndDateToBook ? this.state.onChangedEndDateToBook : ''; 

    //let startDate = this.state.changedStartDate ? this.state.changedStartDate.toString() : '';
    //let endDate = this.state.changedEndDate ? this.state.changedEndDate.toString() : ''; 
    let self = this;

    AsyncStorage.getItem('api_token').then((token) => {

      if(token!==null){
        if(this.state.loggedInUserChildsCount === 1 ){
            
          let response = axios.post(this.config.addMultipleBookingAPI, {
            api_token: token,
            selected_days: this.state.selectedDaysInArray,
            start_date:'',// startDate,
            end_date: '',//endDate,
            children: this.state.loggedInUserChilds[0][0].id
          })
          .then(function (response) {
            self.setState({ isModalVisible: !self.state.isModalVisible, booking: false });
          })
          .catch(function (error) {
            console.log(error);
          });
          
        }else{
          
          let response = axios.post(this.config.addMultipleBookingAPI, {
            api_token: token,
            selected_days: this.state.selectedDaysInArray,
            start_date: '',// startDate,
            end_date: '',//endDate,
            children: this.state.selectedUserChilds.toString()
          })
          .then(function (response) {
            self.setState({ isModalVisible: !self.state.isModalVisible });
            self.setState({ booking: false });
          })
          .catch(function (error) {
            console.log(error);
          });

        }

      }

    });

  }

  // USING FUNCTION TO CLOSE SUCCESS MESSAGE POP-UP
  BookingModalCloseAction =() =>{
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };


  componentDidMount() {

    // USING ASYNC STORAGE FUNCTION TO SET ITEM CHILDERNS IN STATE    
    AsyncStorage.getItem('@UserChilderns:key').then((children) => {
      let totChildCnt = 1;

      if (children !== null) {
        let ArrayChilderns = JSON.parse(children);
        // We have data!!
        this.setState({ loggedInUserChilds: ArrayChilderns })

        ArrayChilderns[0].map((item, i) =>{
          totChildCnt = totChildCnt+i;
          item.isSelect=false;
        })        
        this.setState({ loggedInUserChildsCount: totChildCnt })

      }
      
    });

  }
  

  //USING COMPONENT TO CHECK OR UNCHECK BY ALL CHECK OPTION
  allCheckedChilds = () => {
    let checkedChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(this.state.isCheckedAllChilds){
        item.isSelect= false;
      }else{
        item.isSelect= true;
      }
    })

    this.setState({ isCheckedChilds: checkedChilds });
    this.setState((prevState) => ({ isCheckedAllChilds: !this.state.isCheckedAllChilds }));
  }

  //USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE CHILDS
  childsCheckbox = (itemID) => {
    let checkedChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(itemID===item.id){
        item.isSelect=!item.isSelect;
      }
    })

    this.setState({ isCheckedChilds: checkedChilds });
    this.setState((prevState) => ({ isCheckedAllChilds: false }));
  }

  // USING COMPONENT ON CLICK OF OK BUTTON TO BOOK CLASS
  selectedUserChilds = () => {
    let selectedUserChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(item.isSelect === true){
        selectedUserChilds[item.id] = item.id;
      }
    })
   
    selectedUserChilds = selectedUserChilds.filter(Boolean);
    this.setState({ selectedUserChilds: selectedUserChilds });
    this.BookingModalAction();
    this.setState({ isModalUsersChilds: !this.state.isModalUsersChilds });
  }


  modalForUserChilds = () => {
    checkedChilds = this.state.loggedInUserChilds[0];
    checkedChilds.map((item, i)=>{
      item.isSelect= false;
    })
    this.setState({ isCheckedChilds: checkedChilds, isCheckedAllChilds: false });

    this.setState({ isModalUsersChilds: !this.state.isModalUsersChilds });
  }



  render() {

    const { changedStartDate, changedEndDate } = this.state;

    let startDate = changedStartDate ? changedStartDate.toString() : '';
    startDate = startDate.split("00:00:00");
    startDate = startDate[0];
    if(startDate == ''){
      startDate = this.state.bookingStartDate+" ";
    }

    let endDate = changedEndDate ? changedEndDate.toString() : '';
    endDate = endDate.split("00:00:00");
    endDate = endDate[0];
    if(endDate == ''){
      endDate = this.state.bookingEndDate+" ";
    }
    
    return (
    <Container style={styles.container}>
      <Content style = {{flex:1, flexDirection:'column',backgroundColor:'#ffffff'}}>
        <View  style = {{flex:1, flexDirection:'column'}} >
          <Spacer size={25} />

            <TouchableHighlight style ={styles.mainEventsRow} underlayColor="white" onPress={this.SelectDayButtonAction} >
              <View style = {styles.eachRow}>
                  <View style={styles.firstcolumn}>
                    <Text style = {styles.rootText} >Selected Day</Text>
                  </View>
                  <View style={styles.secondcolumn}>
                    <Text style = {styles.calenderText}>{this.state.selectedDaysState}</Text>
                  </View>
                  <View style={styles.thirdcolumn}>
                  <Image style={{ width:7.5, height: 12 }} source={require('../assets/images/right_events.png')}/>
                  </View>
              </View>
            </TouchableHighlight>

            {
              Platform.select({
                ios:(
                  <TouchableHighlight style ={styles.mainEventsRow} underlayColor="white" >
                    <View style = {styles.eachRow}>
                        <View style={[styles.firstcolumn, { flex: 0.50}]}>
                          <Text style = {styles.rootText}>Start Date</Text>
                        </View>

                        <View style={[styles.secondcolumn, { flex: 0.55}]}>

                          {/* <Text style = {styles.calenderText}>( {startDate} )</Text> */}
                          
                          <DatePicker
                            customStyles = {{ borderWidth: 0, maxHeight: 5 }}
                            style={{width: "100%", alignSelf: "flex-end", maxHeight: 5, borderWidth: 0,  color:'rgba(0, 0, 0, 0.87)', fontSize: 11, lineHeight: 13, backgroundColor:'#ffffff'}}
                            date= ""
                            mode="date"
                            placeholder={ this.state.changedStartDateToDisForIos }
                            placeholderTextColor = "#000000"
                            // format="YYYY-MM-DD"
                            // minDate="2016-05-01"
                            maxDate= {this.state.minEndDateForIos}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon = {false}
                            customStyles={{
                              dateInput: {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                                borderBottomWidth: 0,
                                textAlign: "right",
                                backgroundColor:'transparent',
                                marginTop: -25,
                              },
                              placeholderText: {
                                color: "#000000",
                                backgroundColor:'transparent'
                              },
                              backgroundColor:'transparent'
                            }}
                            
                            tintColor = "red"
                            baseColor = "green"    
                            onDateChange={(date) => {this.StartDateButtonActionForIos(date)}}
                          />

                        </View>

                        <View style={styles.thirdcolumn}>
                          <Image style={{ width:7.4, height: 12 }} source={require('../assets/images/right_events.png')}/>
                        </View>
                        
                    </View>
                  </TouchableHighlight>
    
                ),
                android:(
                  <TouchableHighlight style ={styles.mainEventsRow} underlayColor="white" onPress={this.StartDateButtonAction} >
                    <View style = {styles.eachRow}>
                        <View style={styles.firstcolumn}>
                          <Text style = {styles.rootText}>Start Date</Text>
                        </View>
                        <View style={styles.secondcolumn}>
                          <Text style = {styles.calenderText}>( {startDate} )</Text>
                        </View>
                        <View style={styles.thirdcolumn}>
                          <Image style={{ width:7.4, height: 12 }} source={require('../assets/images/right_events.png')}/>
                        </View>
                    </View>
                  </TouchableHighlight>

                )
              })
            }


            {
              Platform.select({
                ios:(
                  <TouchableHighlight style ={styles.mainEventsRow} underlayColor="white" >
                    <View style = {styles.eachRow}>
                        <View style={[styles.firstcolumn, { flex: 0.50}]}>
                          <Text style = {styles.rootText}>End Date</Text>
                        </View>

                        <View style={[styles.secondcolumn, { flex: 0.55 }]}>

                          {/* <Text style = {styles.calenderText}>( {startDate} )</Text> */}
                          
                          <DatePicker
                            customStyles = {{ borderWidth: 0, borderColor: "#FFFFFF", maxHeight: 5 }}
                            style={{width: "100%", alignSelf: "flex-end", maxHeight: 5, borderWidth: 0, borderColor: "#FFFFFF",  color:'rgba(0, 0, 0, 0.87)', fontSize: 11, lineHeight: 13, backgroundColor:'#ffffff'}}
                            date= ""
                            mode="date"
                            placeholder={ this.state.changedEndDateToDisForIos }
                            placeholderTextColor = "#000000"
                            minDate= {this.state.maxStartDateForIos}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon = {false}
                            customStyles={{
                              dateInput: {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                                borderBottomWidth: 0,
                                textAlign: "right",
                                backgroundColor:'transparent',
                                marginTop: -25,
                              },
                              placeholderText: {
                                color: "#000000",
                                backgroundColor:'transparent'
                              },
                              backgroundColor:'transparent'
                            }}
                            
                            tintColor = "red"
                            baseColor = "green"    
                            onDateChange={(date) => {this.EndDateButtonActionForIos(date)}}
                          />

                        </View>

                        <View style={styles.thirdcolumn}>
                          <Image style={{ width:7.4, height: 12 }} source={require('../assets/images/right_events.png')}/>
                        </View>
                        
                    </View>
                  </TouchableHighlight>

                ),
                android:(
                        
                  <TouchableHighlight style ={styles.mainEventsRow} underlayColor="white" onPress={this.EndDateButtonAction} >
                    <View style = {styles.eachRow}>
                        <View style={styles.firstcolumn}>
                          <Text style = {styles.rootText}>End Date</Text>
                        </View>
                        <View style={styles.secondcolumn}>
                          <Text style = {styles.calenderText}>( {endDate} )</Text>
                        </View>
                        <View style={styles.thirdcolumn}>
                          <Image style={{ width:7.4, height: 12 }} source={require('../assets/images/right_events.png')}/>
                        </View>
                    </View>
                  </TouchableHighlight>

                )
              })
            }

            <TouchableHighlight style ={styles.mainEventsRow} underlayColor="white">
              <View style = {styles.eachRow}>
                  <View style={styles.firstcolumn}>
                    <Text style = {styles.rootText}>Total Reservation:</Text>
                  </View>
                  <View style={styles.secondcolumn}>
                    <Text style = {styles.calenderText}>1</Text>
                  </View>
                  <View style={styles.thirdcolumn}></View>
              </View>
            </TouchableHighlight>

            <View style = {styles.buttonView}>
            { (this.state.booking) ?
                (
                  <View>
                    { (this.state.loggedInUserChildsCount > 1 ) ?
                      (
                        <Button block style ={styles.buttonStyle} onPress = {this.modalForUserChilds} >
                          <Text style={[ styles.bookMultipleBtn ]}> BOOK MULTIPLE </Text>
                        </Button>
                      ):(
                        <Button block style ={styles.buttonStyle} onPress = {this.BookingModalAction} >
                          <Text style={[ styles.bookMultipleBtn ]}> BOOK MULTIPLE </Text>
                        </Button>
                      )
                    }

                  </View>
                )
              :
              (
                <View>
                  <Button block style ={[ styles.cancelButtonStyle ]} onPress={this.cancelButtonAction}>
                    <Text style={ styles.bookMultipleBtn }> CANCEL BOOKING </Text>
                  </Button>
                </View>
              )
            }


            {/* MODAL USING TO DISPLAY SUCCESS POPUP ON CLICK OF BOOKING NOW */}

            
            <Modal isVisible={this.state.isModalVisible} style={[ styles.modal, { paddingLeft: 10, paddingRight: 10 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={{ flex: 1 }}>
                
                <View style={styles.rightImgView} >
                  <Image  style = {styles.rightImgSelf} source = {require('../assets/images/success_icon.png')} />
                </View>

                <View style={ styles.closeModalBtnView }>
                  <Button style={[styles.closeModalBtn ]} onPress={this.BookingModalCloseAction}>
                    <Text style={[ styles.closeModalBtnX,{ position:'absolute' }]}>X</Text> 
                  </Button>
                </View>

                <View style={{ flex: 1, position: "relative", top: 70, left: 0 }} >
                  <Text style={[ styles.modalHeading, styles.headingfont] } >SUCCESS!</Text>
                  <View style={{ marginLeft: 35, marginRight: 35 }}>
                    <Text style={[styles.buttonFontfanm, styles.modalMessage ]}>You are now signed up for { this.params.itemName }.</Text>
                  </View>
                  
                  <TouchableHighlight style={ styles.reminderModalBtn }  onPress={this.AddReminderCmpt} >
                    <Text style={[styles.buttonFontfanm, { alignSelf:"center", color: "#fff" }]}> ADD REMINDER </Text> 
                  </TouchableHighlight>
                </View>

              </View>

            </Modal>

            {/* MODAL USING TO DISPLAY ATTENTION POPUP ON CLICK OF CANCEL BOOKING */}

            <Modal isVisible={this.state.isModalAttention} style={[ styles.modal, { height: 200, padding: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={{ alignContent: "stretch" }}>

                <View style={{ flex: 1, flexDirection: "row"}}>
                  <View style={{ height: 100, }} >

                    <Text style={{ color:"#727272", alignSelf:"center", fontSize:20, paddingTop:15 }}>
                      ATTENTION!
                    </Text>

                    <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                      <Text style={[ styles.modalMessage , { color: "#727272", fontSize: 14 } ]}>
                        Do you really want ot cancel { this.params.itemName }?
                      </Text>
                    </View>
                    
                  </View>
                </View>


                <View style={{ flex: 1, flexDirection: "row", position: "relative", top: 0, marginTop: 75 }} >

                  <View style={{ flex:0.5, marginLeft: 0 , marginRight: 8, marginTop: 0, marginBottom: 0 }} >
                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.AttentionButtonAction } >
                      <Text style={{ width: "100%", textAlign:"center", padding: 10, }}> NO </Text> 
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex:0.5, marginLeft: 8 , marginRight: 0, marginTop: 0, marginBottom: 0 }}>
                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.AttentionToSuccessBtnAction} >
                      <Text style={{ width: "100%", textAlign:"center", padding: 10 }}> YES </Text> 
                    </TouchableOpacity>
                  </View>

                </View>

              </View>
            </Modal>


            {/* MODAL USING TO DISPLAY SUCCESS POPUP FROM ATTENTION ON CLICK OF YES BOOKING */}

            <Modal isVisible={this.state.isModalAtToSucs} style={[ styles.modal, { height: 210, paddingTop: 0 , paddingBottom: 20 , paddingRight: 20 , paddingLeft: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={{ alignContent: "stretch" }}>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, alignSelf: "flex-end" }} >

                    <TouchableOpacity onPress={ this.AttentionToSuccessBtnAction } >
                      <Text style={{ textAlign:"right", color: "#rgba(135, 135, 135, 0.7)" }}> X </Text> 
                    </TouchableOpacity>

                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row"}}>
                  <View style={{ height: 100, }} >

                    <Text style={{ color:"#727272", alignSelf:"center", fontSize:20, }}>
                        SUCCESS!
                    </Text>

                    <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                      <Text style={[ styles.modalMessage , { color: "#727272", fontSize: 14 } ]}>
                        You have successfully canceled { this.params.itemName }.
                      </Text>
                    </View>
                    
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row", marginTop: 60 }} >
                  <View style={{ flex: 1 }} >

                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.BookingClickAction } >
                      <Text style={{ width: "100%", textAlign:"center", padding: 10, }}> OK </Text> 
                    </TouchableOpacity>

                  </View>
                </View>

              </View>
            </Modal>


            {/* MODAL USING TO DISPLAY SUCCESS POPUP FROM ATTENTION ON CLICK OF YES BOOKING */}


            <Modal isVisible= {this.state.isModalSelectDay} style={[ styles.modal, { width: "90%", marginLeft: "5%", height: 310, paddingTop: 20 , paddingBottom: 20 , paddingRight: 20 , paddingLeft: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >


              <View style={[ styles.modalSelectDayViews, { marginBottom: 6 } ]}>
                  <View style={{ flex: 1, alignSelf: "flex-start" }} >
                      <Text style={ styles.selectedDaysTitle }> Selected Day </Text>
                  </View>
              </View>

                        
              <View style= {[ styles.modalSelectDayViews, { borderBottomWidth: 1, borderBottomColor: "rgba(0, 0, 0, 0.54)", marginBottom: 15, paddingBottom: 10 } ]} >
                  <View style={{flex: 0.87, alignSelf: "flex-start", }} >
                      <Text style={ styles.modalSelectDayTitles }>
                        All Day 
                      </Text>
                  </View>
                
                  <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                    
                      <Checkbox 
                        checked={this.state.isCheckedAll} 
                        label=""
                        checkedImage={require('../assets/images/box-checked.png')}
                        uncheckedImage={require('../assets/images/box.png')}
                        checkboxStyle={{width: 18, height: 18 }}
                        onChange={this.allCheckedDays}
                      />
                      
                  </View>
              </View>


              <ScrollView style={{ width: "100%", height: 100, alignContent: "stretch" }} >
                
                <View style={[ styles.modalSelectDayViews ]} >
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Sunday
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                      
                        <Checkbox 
                          checked={this.state.isCheckedDays0}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays0: !this.state.isCheckedDays0 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>
                </View>

                <View style={[ styles.modalSelectDayViews ]}>
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Monday 
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                        
                        <Checkbox 
                          checked={this.state.isCheckedDays1}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays1: !this.state.isCheckedDays1 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>                
                </View>

                <View style={[ styles.modalSelectDayViews ]}>
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Tuesday 
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                        
                        <Checkbox 
                          checked={this.state.isCheckedDays2}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays2: !this.state.isCheckedDays2 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>                
                </View>

                <View style={[ styles.modalSelectDayViews ]}>
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Wednesday 
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                       
                        <Checkbox 
                          checked={this.state.isCheckedDays3}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays3: !this.state.isCheckedDays3 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>                
                </View>
                
                <View style={[ styles.modalSelectDayViews ]}>
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Thursday 
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                        
                        <Checkbox 
                          checked={this.state.isCheckedDays4}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays4: !this.state.isCheckedDays4 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>                
                </View>
                
                <View style={[ styles.modalSelectDayViews ]}>
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Friday
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                        
                        <Checkbox 
                          checked={this.state.isCheckedDays5}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays5: !this.state.isCheckedDays5 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>                
                </View>
                
                <View style={[ styles.modalSelectDayViews ]}>
                    <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                        <Text style={ styles.modalSelectDayTitles }>
                          Saturday
                        </Text>
                    </View>
                  
                    <View style={{flex: 0.13, maxWidth: 20,  marginRight: 15, alignSelf: "flex-start" }} >
                        
                        <Checkbox 
                          checked={this.state.isCheckedDays6}
                          label=""
                          checkedImage={require('../assets/images/box-checked.png')}
                          uncheckedImage={require('../assets/images/box.png')}
                          checkboxStyle={{width: 18, height: 18 }}
                          onChange={() => {
                            this.setState({ isCheckedDays6: !this.state.isCheckedDays6 })
                            this.setState({ isCheckedAll: false });
                          }}
                        />

                    </View>                
                </View>
              
              </ScrollView>  

              <View style={[ styles.modalSelectDayViews, { marginTop: 20, alignItems: "center" } ]}>

                  {/* <View style={{ flex: 0.55,alignSelf: "flex-start" }} >
                  </View> */}
                  <TouchableOpacity style={{ paddingRight : 15 }} onPress={ this.SelectDayButtonAction } >
                    <Text style={[styles.selectedDaysBtns,{textAlign:"right",color:"rgba(0, 0, 0, 0.54)"}]}>
                      CANCEL
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{ paddingLeft : 15 }} onPress={ this.CheckedSelectedDays } >
                    <Text style={[styles.selectedDaysBtns,{textAlign:"left",color:"rgba(0, 0, 0, 0.87)"}]}>
                      OK
                    </Text>
                  </TouchableOpacity>

                  {/* <View style={{ flex: 0.45, alignSelf:"flex-start" }} >
                  </View> */}
                
              </View>


            </Modal>


            {/* MODAL USING TO DISPLAY Calendar Picker POPUP  */}

            <Modal isVisible={this.state.isModalStartDate} style={[ styles.modal, { width: "90%", height: 350, marginLeft: "5%", paddingTop: 20 , paddingBottom: 20 , paddingRight: 20 , paddingLeft: 20 }]}  animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={[ styles.modalSelectDayViews, { marginBottom: 220 } ]}>
                  <View style={{ flex: 1, alignSelf: "flex-start", flexDirection: "row" }} >
                      
                    <CalendarPicker
                      onDateChange={this.onDateChangeForStartDate}
                      weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                      nextTitle = '>'
                      previousTitle = '<'
                      allowRangeSelection={false}
                      selectedDayTextColor="#fff"
                      selectedDayStyle={{
                        backgroundColor: "#00D2EF",
                        fontWeight: "500",
                        elevation: 3,
                      }}
                      todayBackgroundColor="#0099EF"
                      todayTextStyle={{
                        color: "#fff",
                        fontWeight: "500",
                        elevation: 3,
                      }}
                      width= {300}
                      textStyle={{
                        fontFamily: 'SFPro_Text_Regular',
                      }}
                    />
                  </View>
              </View>

              
              <View style={[ styles.modalSelectDayViews ]}>

                  <View style={{ flex: 0.8,alignSelf: "flex-start" }} >
                      <TouchableOpacity style={{ paddingRight : 5 }} onPress={ this.StartDateButtonAction } >
                        <Text style={[styles.selectedDaysBtns, { textAlign:"right", color: "rgba(0, 0, 0, 0.54)" }]}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>

                  <View style={{ flex: 0.2, alignSelf:"flex-start" }} >
                      <TouchableOpacity style={{ paddingLeft : 5 }} onPress={ this.onSelectChangeStartDate } >
                        <Text style={[styles.selectedDaysBtns, { textAlign:"left", color: "rgba(0, 0, 0, 0.87)" }]}> OK </Text>
                      </TouchableOpacity>
                  </View>
                
              </View>
              
              

            </Modal>


            {/* MODAL USING TO DISPLAY Calendar Picker POPUP  */}

            <Modal isVisible={this.state.isModalEndDate} style={[ styles.modal, { width: "90%", height: 350, marginLeft: "5%", paddingTop: 20 , paddingBottom: 20 , paddingRight: 20 , paddingLeft: 20 }]}  animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={[ styles.modalSelectDayViews, { marginBottom: 220 } ]}>
                  <View style={{ flex: 1, alignSelf: "flex-start", flexDirection: "row" }} >
                      
                    <CalendarPicker
                        onDateChange={this.onDateChangeForEndDate}
                        weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                        nextTitle = '>'
                        previousTitle = '<'
                        allowRangeSelection={false}
                        selectedDayTextColor="#fff"
                        selectedDayStyle={{
                          backgroundColor: "#00D2EF",
                          fontWeight: "500",
                          elevation: 3,
                        }}
                        todayBackgroundColor="#0099EF"
                        todayTextStyle={{
                          color: "#fff",
                          fontWeight: "500",
                          elevation: 3,
                        }}
                        width={300}
                        textStyle={{
                          fontFamily: 'SFPro_Text_Regular',
                        }}
                    />
                      
                  </View>
              </View>

              
              <View style={[ styles.modalSelectDayViews ]}>

                  <View style={{ flex: 0.8,alignSelf: "flex-start" }} >
                      <TouchableOpacity style={{ paddingRight : 5 }} onPress={ this.EndDateButtonAction } >
                        <Text style={[styles.selectedDaysBtns, { textAlign:"right", color: "rgba(0, 0, 0, 0.54)" }]}> CANCEL </Text>
                      </TouchableOpacity>
                  </View>

                  <View style={{ flex: 0.2, alignSelf:"flex-start" }} >
                      <TouchableOpacity style={{ paddingLeft : 5 }} onPress={ this.onSelectChangeEndDate } >
                        <Text style={[styles.selectedDaysBtns, { textAlign:"left", color: "rgba(0, 0, 0, 0.87)" }]}> OK </Text>
                      </TouchableOpacity>
                  </View>
                
              </View>
              
              

            </Modal>

            {/* MODAL USING TO DISPLAY ALL USER CHILDS ON CLICK OF BOOK NOW BUTTON */}

            <Modal isVisible= {this.state.isModalUsersChilds} style={[ styles.modal, { width: "80%", marginLeft: "10%", height: 210, padding: 20, paddingTop: 16, paddingBottom: 16 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >


            <View style={[ styles.modalSelectDayViews ]}>
                <View style={{ flex: 1, alignSelf: "flex-start" }} >
                    <Text style={ styles.selectedDaysTitle }> Select Childerns </Text>
                </View>
            </View>

                      
            <View style= {[ styles.modalSelectDayViews, { borderBottomWidth: 1, borderBottomColor: "rgba(0, 0, 0, 0.54)", marginBottom: 10, paddingBottom: 5 } ]} >

                <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                  <Text style={ styles.modalSelectDayTitles }>
                    All Childerns
                  </Text>
                </View>
              
                <View style={{flex: 0.13, height: 35, paddingBottom:5, paddingRight: 5, paddingTop:5 }} >
                  <Checkbox 
                    checked={this.state.isCheckedAllChilds}
                    label=""
                    checkedImage={require('../assets/images/box-checked.png')}
                    uncheckedImage={require('../assets/images/box.png')}
                    checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                    onChange={this.allCheckedChilds}
                  />
                </View>
            </View>


            <ScrollView style={{ width: "100%", height: 40, alignContent: "stretch" }} >

              {/* {userChilds} */}

              {
              (this.state.loggedInUserChilds && this.state.loggedInUserChilds.length > 0)? (
                  

                  this.state.loggedInUserChilds[0].map((item, i) =>{
                    let itemID = item.id
                      
                    return(
                      <View key ={i} style={[ styles.modalSelectDayViews ]} >
                          <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                              <Text style={ styles.modalSelectDayTitles }>
                                {item.name}
                              </Text>
                          </View>

                          <View style={{flex: 0.13, height: 35, paddingBottom:5, paddingRight: 5, paddingTop:5 }} >
                            {
                              (item.isSelect)?
                              (
                                <Checkbox 
                                  checked={true}
                                  label=""
                                  checkedImage={require('../assets/images/box-checked.png')}
                                  uncheckedImage={require('../assets/images/box.png')}
                                  checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                                  onChange={this.childsCheckbox.bind(this, itemID)}
                                />
                              ):(
                                <Checkbox 
                                  checked={false}
                                  label=""
                                  checkedImage={require('../assets/images/box-checked.png')}
                                  uncheckedImage={require('../assets/images/box.png')}
                                  checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                                  onChange={this.childsCheckbox.bind(this, itemID)}
                                />
                              )
                            }

                          </View>
                      </View>
                    )
                  
                  })

                ):(
                  <View></View>
                )
              }

            </ScrollView>  

            <View style={[ styles.modalSelectDayViews, { maxHeight: 25, marginTop: 10, paddingTop: 5, alignItems: "center" } ]}>

              <TouchableOpacity style={{ paddingRight : 15 }} onPress={ this.modalForUserChilds } >
                <Text style={[styles.selectedDaysBtns,{textAlign:"right", color:"rgba(0, 0, 0, 0.54)"}]}>
                  CANCEL
                </Text>
              </TouchableOpacity>

              <TouchableOpacity style={{ paddingLeft : 15 }} onPress={ this.selectedUserChilds } >
                <Text style={[styles.selectedDaysBtns,{textAlign:"left", color:"rgba(0, 0, 0, 0.87)"}]}>
                  OK
                </Text>
              </TouchableOpacity>    
            </View>
            </Modal>



            </View>
        </View>
      </Content>
    </Container>
    );
  }

 

  static navigationOptions =({navigation})=> ({ 
    title: navigation.state.params.itemName,
    headerStyle: {
      height:65,
      elevation: 0,
      zIndex:999,
      backgroundColor:'#ffffff',
      borderBottomWidth: 0
    },
    headerBackground:  (
      <View>
      <LinearGradient
          colors={['#0099EF', '#00D2EF']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          style={{ borderBottomLeftRadius: 15, borderBottomRightRadius: 15, elevation: 4,
          
            ...Platform.select({
              ios:{
                borderRadius: 15,
                marginTop:-15
              }
            }),

          }}
        >
          <Image  style={{ width: '100%', height:'100%', zIndex:1, opacity:0.1 }} source = {require('../assets/images/multiple-header.png')} />
        
      </LinearGradient>
      </View>
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      marginRight: 44,
      paddingRight: 10
    }
  
  })

}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'#ffffff',
    flex:1,
  },
  mainEventsRow:{
    paddingRight:25,
    paddingLeft:25,
    borderBottomWidth:1,
    borderBottomColor:'rgba(0, 0, 0, 0.1)'
  },
  eachRow:{
    flexDirection:'row',
    flexWrap:'wrap',
    flex:1,
    justifyContent:'center',
    paddingTop:15,
    paddingBottom:15
  },
  firstcolumn:{flex:0.65},
  secondcolumn:{
    flex:0.4,
    textAlign: "right",
    paddingRight: 5,
    marginTop: 2
  },
  thirdcolumn:{
    flex:0.05,
    justifyContent:'flex-end',
    marginTop: -1,
    paddingBottom: 3,
  },
  buttonView:{
    flex:1,
    marginRight:15,
    marginLeft:15,
    ...Platform.select({
      ios: {
        marginTop:120,
      },
      android: {
        marginTop:150,
      },
    }),
  },
  buttonStyle:{
    // paddingBottom:15,
    // paddingTop:15,
    backgroundColor:'#25AE88',
    borderRadius:2,
  },
  bookMultipleBtn: {
    color: "#ffffff",
    fontSize: 17,
    lineHeight: 18,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        //letterSpacing: 0.41,
        //marginBottom: 15
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  rootText:{
    fontSize:13,
    lineHeight:18,
    color:'rgba(0, 0, 0, 0.87)',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing: -0.08
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  calenderText:{
    fontSize:11,
    lineHeight:13,
    color:'rgba(0, 0, 0, 0.87)',
    textAlign: "right",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing: 0.07
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  cancelButtonStyle:{
    backgroundColor:'#EB4242',
  },
  
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,
    backgroundColor: "#ffffff",
    position: "absolute",
    top:150,
    elevation: 10,
    borderRadius: 5
  },
  modalHeading: {
    fontSize: 18,
    textAlign: "center",    
  },
  modalMessage: {
    textAlign: "center",
    fontSize: 16,
    justifyContent: "center",
    color: "gray",
  },
  
  closeModalBtnView: {
    position: "absolute",
    top: 5,
    right: 0,
    width: 50,
    paddingRight: 5,
  },
  
  closeModalBtn: {
    alignSelf: "center",
    borderWidth: 1,
    borderColor: "#FFFFFF",
    elevation:0,
    paddingLeft: 24
  },

  closeModalBtnX: {
    paddingTop: 17,
    fontSize:20,
    fontWeight: "bold",
    color: "gray",
    borderWidth: 1,
    borderColor: "#FFFFFF",
    elevation: 0,
    backgroundColor: "#ffffff"
  },

  closeModalBtnImg: {
    width: 15,
    marginRight: 5,
    marginLeft: 5,
  },

  rightImgView: {
    position: "absolute",
    top: 0,
    right: 0,
    width: "100%",
    // alignSelf: "center",
    // textAlign: "center"
  },

  rightImgSelf: {
    alignSelf:'center',
    backgroundColor:'transparent',
    borderColor: "#ffffff",    
    borderWidth: 8,
    borderRadius: 100,
    height: 90,
    width: 90,
    position: 'relative',
    top: -40,
  },
  
  reminderModalBtn: {
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,

    backgroundColor:  "#25AE88",
    padding: 15,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#25AE88"
  },
  

  modalSelectDayViews: {
    flex: 1,
    flexDirection: "row",
    alignContent: "stretch"
  },
  modalSelectDayTitles: {
    textAlign:"left",
    color:"#727272",
    fontSize: 13,
    lineHeight: 18,
    paddingBottom:5,
    paddingTop:5,
    //paddingLeft: 10
  },
  selectedDaysTitle: {
    fontSize: 17,
    lineHeight: 22,
    color: "rgba(0, 0, 0, 0.87)",
    textAlign:"center",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing:-0.41
      },
      android: {
        fontFamily:'RobotoRegular_1',
      },
    }),
  },

  selectedDaysBtns: {
    fontSize: 13,
    lineHeight: 18, 
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing:-0.41
      },
      android: {
        fontFamily:'RobotoRegular_1',
      },
    }),
  }

});
export default BookMultiple;
