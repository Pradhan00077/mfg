import React from 'react';
import {Content, Form, Item, Input, Label, View, Button, Container} from 'native-base';
import {Dimensions, Image, StyleSheet, ScrollView, Text, AsyncStorage, KeyboardAvoidingView, Platform} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Spacer from '../components/UI/Spacer';
const window = Dimensions.get('window');

import { StackActions, NavigationActions } from 'react-navigation';

let email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ; 
let pwd= /^\w(\w|[!@#$%]){4,20}/;

class LoginScreen extends React.Component {

  constructor(props){
    super(props);
    this.config={
      apiUrl:'https://konnect.myfirstgym.com/api/customer/auth/login'
    }
    this.state={
      Apiresponse :[],
      check:false,
      email: '',
      password:'',
      errorshow:false,
      error:false,
      isdisabled:false,
      errormessage:'',
      password_error: "",
      email_error: "",
    };
    this.validates = this.validates.bind(this);
    this.formsubmit = this.formsubmit.bind(this);
    this.emailRef = this.updateRef.bind(this, 'email');
    this.passwordRef = this.updateRef.bind(this, 'password');
  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue); 
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }


  async fetchLogin(email, password) {
    let email_error_msg = ""
    let password_error_msg = ""
    let self = this;
    let responseJson = [];
    let childernsResp = [];

    let response = await fetch(this.config.apiUrl+"", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })

    try {
      responseJson = await response.json();

      if( responseJson.hasOwnProperty('errors') ){

        if(responseJson.errors.hasOwnProperty('email')){
          
          email_error_msg = responseJson.errors.email[0]
          self.setState({ isdisabled: false, error:true, email_error: email_error_msg, password_error: "" })
          
        }else if ( responseJson.errors.hasOwnProperty('password') ){

          password_error_msg = responseJson.errors.password[0];
          self.setState({ isdisabled: false, error:true, email_error: "", password_error: password_error_msg})
            
        }
        
      }else if(responseJson.hasOwnProperty('email') ){

        email_error_msg = responseJson.email;
        self.setState({ isdisabled: false, error:true, email_error: email_error_msg, password_error: ""}) 

      }else{

        self.setState({ error:false, email_error: "", password_error: "" }) 

        let childToBeSaved = responseJson['data'].children;
        if(childToBeSaved == ""){
          childernsResp.push(0)
        }else{
          childernsResp.push( childToBeSaved )
        }
        
        self.setState({Apiresponse: responseJson['data'], isdisabled: false});
        self.saveItem('api_token', self.state.Apiresponse.token);
        self.saveItem('member_id', self.state.Apiresponse.member_id);
        self.saveItem('membership_status', self.state.Apiresponse.membership_status);
        self.saveItem('@UserChilderns:key', JSON.stringify(childernsResp) );
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
        this.props.navigation.dispatch(resetAction);
        self.props.navigation.navigate('MainProfile', {api_token:self.state.Apiresponse.token, replace:true})
        
      }
      
    } catch (error) {
        alert(error)
        responseJson = []
        self.setState({isdisabled:false})
        console.log("Response error - ",error);
    }
    
  }

  
  validates = (text, type) => { 
    if(type==='username'){
      this.setState({ email:text })
    }
    else if(type === 'password'){
      this.setState({ password:text })
    }  
  }

  formsubmit =() =>{
    this.setState({isdisabled:true})
    this.fetchLogin(this.state.email, this.state.password)
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    
    return (
    <Container style={styles.container} >
      <ScrollView keyboardDismissMode='on-drag'>

          <KeyboardAvoidingView behavior="position" >
           <View style={styles.mainContainer}>
                  
              {
                Platform.select({
                  ios: (
                    <Spacer size={window.height/10} />
                  ),
                  android: (
                    <Spacer size={window.height/7} />
                  )
                })
              }
                  <Image style={[ styles.logoAdjustment ]} source = {require('../assets/images/mgflogo1.png')} resizeMode='contain' />

              {
                Platform.select({
                  ios: (
                    <Spacer size={window.height/21} />
                  ),
                  android: (
                    <Spacer size={window.height/18} />
                  )
                })
              }

              <Text style={styles.welcomeText} >Welcome!</Text>
              <Text style={styles.normalText} >Sign up and start exploring.</Text>

              {
                Platform.select({
                  ios: (
                    <Spacer size={window.height/28} />
                  ),
                  android: (
                    <Spacer size={window.height/18} />
                  )
                })
              }


              <View>

                <Form style={{ flex: 1, flexDirection: "column", alignItems: "stretch",  }} >

                  <View>
                    
                      <TextField
                        label='Username or E-mail'
                        ref={this.emailRef}
                        onChangeText={(text) => this.validates(text, 'username')}
                        style={[ styles.labelName ]}
                        tintColor = "#FFFFFF"
                        baseColor = "#FFFFFF"
                        keyboardType='email-address'
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        error	= {this.state.email_error}
                      />

                  </View>

                  <View>
                    
                      <TextField
                        label='Password'
                        ref={this.passwordRef}
                        onChangeText={(text) => this.validates(text, 'password')}
                        style={[ styles.labelName ]}
                        tintColor = "#FFFFFF"
                        baseColor = "#FFFFFF"
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        clearTextOnFocus={true}
                        secureTextEntry={true}
                        maxLength={35}
                        returnKeyType='done'
                        error	= {this.state.password_error}
                      />
                    
                      <Text onPress={() => this.props.navigation.navigate('ForgotPassword')} style ={ (this.state.password_error != "")?styles.labelForgetColor1:styles.labelForgetColor} >Forget Password?</Text>

                  </View>

                  {
                    Platform.select({
                      ios: (
                        <Spacer size={16} />
                      ),
                      android: (
                        <Spacer size={20} />
                      )
                    })
                  }


                  <View>
                    <Button block style ={[styles.buttonStyle, this.state.isdisabled? styles.disablebtn:null]}  onPress={this.formsubmit} disabled = { this.state.isdisabled}>
                        <Text style={styles.loginButtonText} uppercase={false}>{this.state.isdisabled?'Loading...':'Login'}</Text>
                    </Button>
                  </View>
                </Form>
                
                <Spacer size={30} />
              </View>

           </View>            
          </KeyboardAvoidingView>

      </ScrollView>
    
    </Container>
    );
  }
}

LoginScreen.navigationOptions = {
  header: null,
  headerStyle: {
    backgroundColor: '#0099EF',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
  },
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign:"center", 
    flex:1 ,
    fontSize:20,
    lineHeight:25
  },
};
const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    //aspectRatio: 1,
    backgroundColor:'#2eaaeb',
  },
  mainContainer:{
    backgroundColor:'#2eaaeb',
    paddingRight:30,
    paddingLeft:30,
    width: "100%",
    height: "100%",
  },
  
  logoAdjustment: {
    ...Platform.select({
      ios: {
        width:270, alignSelf:'center', height:150
      },
      android: {
        width:300, alignSelf:'center', height:160
      },
    }),
  },

  
  inputField:{
    color:'#ffffff', 
    textAlignVertical:'bottom',
    fontSize:17,
    lineHeight:22,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  labelColor:{
    color:'#ffffff'
  },
  disablebtn:{
    backgroundColor:'#cccccc'
  },

  labelForgetColor1:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },

  labelForgetColor:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        backgroundColor: "transparent"
      },
      android: {
        fontFamily:'RobotoRegular_1',
        backgroundColor: "transparent"
      },
    }),
  },
  welcomeText:{
    fontSize:34,
    lineHeight:41,
    textAlign: 'center',
    flex:1, 
    color:'white',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  normalText:{
    fontSize:16,
    lineHeight:17,
    textAlign: 'center',
    flex:1, 
    color:'white',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  buttonStyle:{
    backgroundColor:'white',
    borderWidth:1,
    borderRadius:4,
    borderColor:'#ffffff',
    elevation:0
  },

  loginButtonText:{
    color:'#00A6EF',
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
    fontWeight:'300'
  },
  error:{
    borderBottomColor:'red',
    borderBottomWidth:2
  },
  labelName: {
    color: "#ffffff",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  }

});
export default LoginScreen;