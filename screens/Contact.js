import React from 'react';
import { StyleSheet, View, Text, Image, Alert, TouchableOpacity, Picker, Platform } from 'react-native';
import { Container } from 'native-base';
import MapView from 'react-native-maps';
import IOSPicker from 'react-native-ios-picker';

import { Dropdown } from 'react-native-material-dropdown';

import { LinearGradient } from 'expo-linear-gradient';

export default class Contact extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
          latitude: 24.46555020,
          longitude: 54.38153140,
          location: '',
          locationRsp:[],
          floatingBtnDisplay: false,
          tracksViewChanges: false,
          selectedLocationForIos: ""
        };
    }
  

  onSelectLocation = (itemValue, itemIndex) =>{

    console.log("Index - ", itemIndex, " Value - ", itemValue)

    this.setState({location: itemValue});
    let getLatLong = itemValue.split('-');

    this.state.locationRsp.map((item, i) =>{
      let locationLatLong = parseFloat(item.latitude, 10)+"-"+parseFloat(item.longitude, 10)
      if( locationLatLong == itemValue ){
        this.setState({ selectedLocationForIos: item.name });
      }
    })

    this.setState({ latitude: parseFloat(getLatLong[0], 10) });
    this.setState({ longitude: parseFloat(getLatLong[1], 10) });
  };

  floatingBtnClickAction = () => {
    this.setState({floatingBtnDisplay: !this.state.floatingBtnDisplay});
  }


  //WILL CALL COMPONENT TO GET LOCATIONS
  fetchLocations = async(data) => {
    let locationResponseJSON = []

    try {
        let branchResponse = await fetch("https://konnect.myfirstgym.com/api/customer/locations/nearby?latitude="+data.let+"&longitude="+data.long);
        try {
          locationResponseJSON = await branchResponse.json();
          locationResponseJSON.data.map((item, i) => {
            if(((locationResponseJSON.data).length - 1) == i){
              this.setState({ selectedLocationForIos: item.name });
            }
          })

          this.setState({locationRsp: locationResponseJSON.data })

        } catch (error) {
          console.log(error);
        }
    } catch (error) {
        console.log(error);
    }
  }


  //WILL CALL FIRST BEFORE THE RENDER CODE
  componentDidMount() {

    // GETTING CURRENT LOCATION BY FOLLOWING CODE
    if(Platform.OS==='ios'){
      /* navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        this.fetchLocations({ let: parseFloat(latitude, 10), long: parseFloat(longitude, 10)});
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );*/
    this.fetchLocations({ let: parseFloat(this.state.latitude, 10), long: parseFloat(this.state.longitude, 10)});
    }else if(Platform.OS==='android'){
      navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        this.fetchLocations({ let: parseFloat(latitude, 10), long: parseFloat(longitude, 10)});
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    }
   
  }

  get loadMap() {
    return (
      <MapView style={ styles.mapView } 
        region={{
          latitude: parseFloat(this.state.latitude, 10),
          longitude: parseFloat(this.state.longitude, 10),
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        <MapView.Marker
          tracksViewChanges={this.state.tracksViewChanges}
          //zIndex={9}
          coordinate={{
          longitude: parseFloat(this.state.latitude, 10),
          latitude: parseFloat(this.state.longitude, 10),
          }}
          centerOffset={{ x: 0, y: -20 }}
          title={"My First Gym"}
        />
      </MapView>
    )
  }

  render(){

    //Lat-Long like : 24.46555020 - 54.38153140
    const dropdownData=[
      {"name":"Downtown Abu Dhabi", "value": "24.46555020-54.38153140"},
      {"name":"Abu Dhabi Ladies Club", "value": "23.39292420-55.46929790"},
    ]

    console.log("Modal - ", this.state.selectedLocationForIos)
    return(
        <Container>

            <View style={ styles.mapContainer }>
                {this.loadMap}
            </View>
            
            <View style={ styles.mapLocationDropDown }>
                
              <View style={[ styles.mapLocationDropDown, { flex: 1, flexDirection: "row", backgroundColor: "#ffffff", zIndex: 9999, elevation: 10 , position:'relative'}]} >

                <View style={{ flex: 0.08, paddingTop: 18, }} >
                  <Image style={{alignSelf:'flex-end', width: 9, height: 14 }} source ={require('../assets/images/location.png')} />
                </View>

                <View style={{ flex: 0.90, marginLeft: 6 }} >
                  
                  {
                    Platform.select({
                      ios:(
                        <IOSPicker
                          mode = "modal"
                          selectedValue={this.state.selectedLocationForIos}
                          style={[ styles.textFont, { width: "100%", color: "#00D2EF",  paddingLeft: 0, paddingRight: 5, paddingBottom: 16, paddingTop: 16, minHeight: 25 }]}
                          onValueChange={ this.onSelectLocation }>
                          {
                            this.state.locationRsp.map((item, i) =>{
                              return(
                                <Picker.Item key={i} label={item.name} value={ parseFloat(item.latitude, 10)+"-"+parseFloat(item.longitude, 10) } />
                              );
                            })
                          }
                        </IOSPicker>

                      ),
                      android:(
                        <Picker
                          selectedValue={this.state.location}
                          style={[ styles.textFont, { width: "100%", color: "#00D2EF",  paddingLeft: 0, paddingRight: 5,  paddingTop:50 }]}
                          onValueChange={ this.onSelectLocation }>
                          {
                            this.state.locationRsp.map((item, i) =>{
                              return(
                              <Picker.Item key={i} label={item.name} value={ parseFloat(item.latitude, 10)+"-"+parseFloat(item.longitude, 10)} />
                              );
                            })
                          }
                        </Picker>
                      )
                    })
                  }


                </View>

              </View>

            </View>
            
            <View style={ styles.mapContact} >
                
                <View style={ styles.mapContactBottomBtn1 } >
                    <TouchableOpacity>
                        {
                          Platform.select({
                            ios:(
                              <View style={{ flex: 1, flexDirection: "row", alignSelf: 'center', justifyContent: "center" }}>

                                <View style={{ flex: 0.35, alignItems: "flex-end", paddingTop: 17, paddingBottom: 17,  }}>
                                  <Image style={{ width: 20, height: 20, paddingRight: 0, alignSelf: "flex-end", }} source={require('../assets/images/google-map.png')} /> 
                                </View>

                                <View>
                                  <Text style={[ styles.textFont, { flex: 0.65, alignSelf: 'center', justifyContent: "center", color: "#00D2EF", paddingTop: 18, paddingBottom: 14, paddingRight: 15, paddingLeft: 5, }]}>
                                    GOOGLE MAPS 
                                  </Text>
                                </View>

                              </View>
  
                            ),
                            android:(
                              <Text style={[ styles.textFont, { alignSelf: "center", color: "#00D2EF", paddingTop: 15, paddingBottom: 15, paddingRight: 15, paddingLeft: 15, }]}>
                                <Image  style={{ width: 20, height: 20, paddingRight: 2 }} source={require('../assets/images/google-map.png')} /> GOOGLE MAPS 
                              </Text>
                            )
                          })
                        }

                    </TouchableOpacity>
                </View>

                {(this.state.floatingBtnDisplay) ? (

                  <View style={[ styles.mapContactBottomBtn2, { backgroundColor: "#147BDF" } ]} >
                  
                    <TouchableOpacity style={[ styles.mapFloatingBtnAction ]} onPress={ this.floatingBtnClickAction } >
                      <Image  style={{ width: 21, height: 20 }} source={require('../assets/images/email_call.png')} />
                    </TouchableOpacity>
                    
                  </View>

                ) : (

                  <View style={ styles.mapContactBottomBtn2 } >

                    <TouchableOpacity style={[ styles.mapFloatingBtnAction]} onPress={ this.floatingBtnClickAction } >
                      <Image  style={{ width: 21, height: 20 }} source={require('../assets/images/email_call_blue.png')} />
                    </TouchableOpacity>

                  </View>

                )}

            </View>

            {(this.state.floatingBtnDisplay) ? (

                <View style={ styles.mapFloatingButtons }>


                  <View style={ styles.floatingBtn1 } >
                        <View style={ styles.emailFloatingName } >
                          <Text style={[styles.textFont, { textAlign: "center", color: "#00D2EF" }]} > Call Us </Text>
                        </View>

                        <View style={ styles.emailFloatingBtn } >
                          <TouchableOpacity style={styles.mapFloatingBtnAction} onPress={()=>{alert("Call Us")}}>
                            <Image  style={{ width: 21, height: 20 }} source={require('../assets/images/call_us.png')} />
                          </TouchableOpacity>
                        </View>
                  </View>

                  <View style={ styles.floatingBtn2 } >
                        <View style={ styles.emailFloatingName } >
                          <Text style={[styles.textFont,{ textAlign: "center", color: "#00D2EF" }]} > Email Us </Text>
                        </View>

                        <View style={ styles.emailFloatingBtn } >
                          <TouchableOpacity style={styles.mapFloatingBtnAction} onPress={()=>{alert("Email Us")}}>
                            <Image  style={{ width: 21, height: 20 }} source={require('../assets/images/email_us.png')} />
                          </TouchableOpacity>
                        </View>
                  </View>
                  
                  <View style={ styles.floatingBtn3 } >
                        <View style={ styles.emailFloatingName } >
                          <Text style={[styles.textFont, { textAlign: "center", color: "#00D2EF" }]} > Website </Text>
                        </View>

                        <View style={ styles.emailFloatingBtn } >
                          <TouchableOpacity style={styles.mapFloatingBtnAction} onPress={()=>{alert("Website")}}>
                            <Image  style={{ width: 21, height: 20 }} source={require('../assets/images/website.png')} />
                          </TouchableOpacity>
                        </View>
                  </View>


                </View>

            ) : (
              <View></View>
            )

          }            

        </Container>

    );  
  }
}

Contact.navigationOptions = {
  title: 'Contact',
  headerStyle: {
    backgroundColor: '#0099EF',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height:60,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),

      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
};
  

const styles = StyleSheet.create({
    mapContainer:{
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    mapView: {
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0
    },
    
    mapLocationDropDown: {
        position: "absolute",
        top: 15,
        left: 0,
        borderRadius: 5,
        width: "90%",
        marginLeft: "5%",
    },
    locationDropDownSlct:{
        borderRadius:5,
        backgroundColor:'#FFFFFF',
        zIndex: 111,
        fontSize: 14,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 20,
        paddingTop: 20,
        borderWidth: 0,
    },

    mapContact: {
        position: "absolute",
        bottom : 15,
        right: 17,
        zIndex: 111,
        width: "90%",
        marginLeft: "5%",
        backgroundColor: "transparent",
     },  

    mapContactBottomBtn1:{
        width: "78%",
        borderRadius:35,
        backgroundColor:'#FFFFFF',
        color: "#00D2EF",
        zIndex: 11,
        elevation: 10,
        fontSize: 10,
        alignContent: "center",
        textAlign: "center",
        alignSelf: "flex-start",
        position: "relative",
        top: 55
    },

    mapContactBottomBtn2:{
        width: 57,
        height: 57,
        borderRadius:60,
        backgroundColor:'#FFFFFF',
        color: "#00D2EF",
        zIndex: 111111,
        elevation: 10,
        fontSize: 10,
        alignSelf: "flex-end"
    },
    mapFloatingBtnAction: {
      alignSelf: "center",
      paddingTop: 20
    },


    mapFloatingButtons: {
      position: "absolute",
      bottom : 45,
      right: 17,
      zIndex: 111,
      width: "50%",
      marginLeft: "50%",
      marginRight: "0%",
      backgroundColor: "transparent",
      alignSelf: "flex-end",
    },

    emailFloatingBtn: {
      width: 57,
      height: 57,
      borderRadius:60,
      backgroundColor:'#FFFFFF',
      color: "#00D2EF",
      zIndex: 11,
      elevation: 10,
      fontSize: 10,
      alignSelf: "flex-end",
      position: "relative",
      top: -40
    },
    emailFloatingName: {
      width: 100,
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 16,
      paddingRight: 16,
      borderRadius:5,
      backgroundColor:'#FFFFFF',
      color: "#00D2EF",
      zIndex: 11,
      elevation: 10,
      fontSize: 10,
      alignSelf: "flex-end",
      marginRight:72,
      position: "relative",
      top: 10
    },

    floatingBtn1: {
      position: "relative",
      bottom: -54
    },
    floatingBtn2: {
      position: "relative",
      bottom: -26
    },
    floatingBtn3: {
      position: "relative",
      top: 0
    },
    textFont: {
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    }

});
