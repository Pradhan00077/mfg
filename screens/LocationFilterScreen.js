import React, { Component } from 'react';
import { TouchableOpacity, View, Image, StyleSheet, Platform, ScrollView } from 'react-native';
import { Container, Text, Picker } from 'native-base';
import FilterSchedule from './filterSchedule';
import { LinearGradient } from 'expo-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker'
import { HeaderBackButton } from 'react-navigation'

const Item = Picker.Item;
const monthObj = { '01': 'January', '02': 'February', '03': 'March', '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', '09': 'September', '10': 'October', '11': 'November', '12': 'December' };
setFormatDate = (date) => {
  let returnDate = date;
  var dateArray = date.toString().split('/');
   if (dateArray.length == 3)
    returnDate = monthObj[dateArray[1]] + ' ' + dateArray[0] + ', ' + dateArray[2];
  return returnDate;
}

export default class Location extends Component {
  constructor (props){
    super(props);
    this.params = this.props.navigation.state.params;

    this.state = {
      locationRsp:[],
      filterResponseData: [],
      serviceCategories: [],
      ageGroupFrom: [],
      ageGroupTo: [],
      selectLocations: (this.params.location_Id!=='')?this.params.Location_Name:"Select Location",
      selectedLocation: this.params.location_Id,
      filterAgeGroup1: "From",
      age_group_from: '',
      age_group_to: '',
      filterAgeGroup2: "To",
      filterClassType: (this.params.category_Id!=='')?this.params.Category_Name:"Select Class Type",
      filterClassId: this.params.category_Id,
      item_Id:this.params.item_Id,
      filterCalenderIconClick: false,
      filteredDate: "",
      latitude: null,
      longitude: null,
      firstLoad: false,
      latitude:24.46555020,
      longitude: 54.38153140,
    };

    //Default visible isVisible
    this.isVisibleHeader = false;
    this.validates = this.validates.bind(this);
    this.locationSel = this.locationSel.bind(this);
    this.SelectedClass = this.SelectedClass.bind(this);
  }

  // VALIDATION COMPONENT
  validates = (value, index) => {
    if (index == "filterAgeGroup1") {
      this.setState({ filterAgeGroup1: value, age_group_from: value })
    } else if (index == "filterAgeGroup2") {
      this.setState({ filterAgeGroup2: value, age_group_to: value })
    }

  }

  // LOCATION SELECTION COMPONENT
  locationSel = (value, index, data, selectionType) => {
    
    if(selectionType === 'location'){

      data.map((item, i) => {
        if (i === index) {
          this.setState({ selectLocations: item.value, selectedLocation: item.id })
        }
      })

    }

  }

  // LOCATION SELECTION COMPONENT
  SelectedClass = (value, index, data1, selectionType) => {
    if (selectionType === 'filterClassType') {
      data1.map((item, i) => {
        if (i === index) {
          this.setState({ filterClassType: item.value, filterClassId: item.id })
        }
      });
    }
  }


  //WILL CALL COMPONENT TO GET LOCATIONS
  fetchLocations = async (data) => {
    let locationResponseJSON = []
    let locationRes = []
    try {
      let branchResponse = await fetch("https://konnect.myfirstgym.com/api/customer/locations/nearby?latitude=" +data.let+ "&longitude=" + data.long);
      try {
        locationResponseJSON = await branchResponse.json();

        locationRes.push(locationResponseJSON.data.map((item, i) => {
          return {
            id: item.location_id,
            value: item.name
          }
        }))
        this.setState({ locationRsp: locationRes[0] });

      } catch (error) {
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  //WILL CALL COMPONENT TO GET LOCATIONS
  fetchFilterData = async () => {
    let filterResponseJSON = []
    let serviceCatRsp = []
    let ageGroupfrom = []
    let ageGroupTo = []

    try {
      let filterResponse = await fetch("https://konnect.myfirstgym.com/api/filter-options");
      try {
        filterResponseJSON = await filterResponse.json();

        serviceCatRsp.push(filterResponseJSON.service_categories.map((item, i) => {
          return {
            id: item.id,
            value: item.name
          }
        }))
        ageGroupfrom.push(filterResponseJSON.age_groups.filter_age_group_from.map((item) => {
          return {
            value: item,
          }
        }))
        ageGroupTo.push(filterResponseJSON.age_groups.filter_age_group_to.map((item) => {
          return {
            value: item,
          }
        }))
        this.setState({ filterResponseData: filterResponseJSON, serviceCategories: serviceCatRsp[0], ageGroupFrom: ageGroupfrom[0], ageGroupTo: ageGroupTo[0] });
      } catch (error) {
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }
  }


  //WILL CALL FIRST BEFORE THE RENDER CODE
  componentDidMount() {
    // GETTING CURRENT LOCATION BY FOLLOWING CODE
    if(Platform.OS==='ios'){
      /* navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        this.fetchLocations({ let: latitude, long: longitude });
      },
      error => {},
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );*/

    this.fetchLocations({ let: this.state.latitude, long: this.state.longitude });
    }else if(Platform.OS==='android'){
      navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        this.fetchLocations({ let: latitude, long: longitude });
      },
      error => {},
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    }

    // GETTING DATA BY API THAT'S USING IN FILTER
    this.fetchFilterData();

    this.props.navigation.setParams({ onFullScreen: this.onFullScreen });
  }

  render() {

    let dd = new Date().getDate();
    let mm = new Date().getMonth() + 1;

    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    return (

      <Container style={{ backgroundColor: '#E5E5E5' }} >

        {(this.isVisibleHeader) ? (

          <View></View>

        ) : (
            <LinearGradient
              locations={[0, 1]}
              colors={['#00D2EF', '#0099EF']}
              style={{
                flex: 1,
                ...Platform.select({
                  ios: {
                    maxHeight: 460,
                    minHeight: 200,
                    borderRadius: 15,
                    marginTop: -15
                  },
                  android: {
                    borderBottomLeftRadius: 15, 
                    borderBottomRightRadius: 15,
                    maxHeight: 460,
                    minHeight: 180,
                    // maxHeight: 460,
                    // minHeight: 200,
                  },
                }),
              }}
            >
              <View style={styles.filterContainer}>

                <View style={{ flex: 1, flexDirection: "row", alignContent: "stretch", paddingBottom: 21, maxHeight: 210, paddingLeft: 15, paddingRight: 15, }}>

                  <View style={{ flex: 0.88, flexDirection: "row", zIndex: 9999, elevation: 0, backgroundColor: 'rgba(255, 255, 255, 0.3)', borderRadius: 4, height: 47 }}>

                    <View style={{ flex: 0.08, alignSelf: 'flex-start', paddingTop: 15, }} >
                      <Image style={{ alignSelf: 'flex-end', width: 9, height: 14 }} source={require('../assets/images/location_white.png')} />
                    </View>

                    <View style={{ flex: 0.92, alignSelf: 'flex-end' }} >
                      <Dropdown containerStyle={{ width: '100%', borderRadius: 4, paddingLeft: 8, paddingRight: 8 }}
                        label=""
                        textColor='#ffffff'
                        baseColor='#ffffff'
                        selectedItemColor="rgba(0, 0, 0, 0.34)"
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        labelFontSize={0}
                        value={this.state.selectLocations}
                        data={this.state.locationRsp}
                        keyAttribute={"value"}
                        valueAttribute={"id"}
                        dropdownPosition={0}
                        dropdownOffset={{ top: 8, left: 0 }}
                        onChangeText={(value, index, data) => this.locationSel(value, index, data, "location")}
                      />
                    </View>

                  </View>

                  <View style={{ flex: 0.12 }}>
                    <TouchableOpacity style={{ paddingTop: 11, paddingRight: 5, alignSelf: "flex-end" }} activeOpacity={1} onPress={() => { (this.params.guestSchedule)?this.props.navigation.navigate('GuestSchedules'):this.props.navigation.goBack()}} >
                      <Image style={{ width: 20, height: 20 }} source={require('../assets/images/filter.png')} />
                    </TouchableOpacity>
                  </View>

                </View>



                <View style={{ flex: 1, flexDirection: "row", marginTop: 2, paddingLeft: 45, paddingRight: 35 }}>
                  <Text style={[styles.textFonts, { width: '100%', textAlign: 'center', alignSelf: "center", justifyContent: "center", color: '#ffffff', fontSize: 17, lineHeight: 27 }]}>
                    Filter
                  </Text>
                </View>

                <View style={{ justifyContent: 'center', flex: 1, flexDirection: "row", marginBottom: 10, marginTop: 2, paddingBottom: 16, paddingLeft: 45, paddingRight: 35, }}>

                  <View style={{ flex: 0.45, }}>

                    <Text style={[styles.textFonts, { color: '#ffffff', fontSize: 13 }]}>Age Group</Text>

                    <Dropdown
                      containerStyle={[styles.dropdownSelection, { height: 30 }]}
                      textColor='#ffffff'
                      baseColor='#ffffff'
                      selectedItemColor="rgba(0, 0, 0, 0.34)"
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      labelFontSize={0}
                      fontSize={12}
                      value={this.state.filterAgeGroup1}
                      data={this.state.ageGroupFrom}
                      dropdownPosition={0}
                      dropdownOffset={{ top: 6, left: 0 }}
                      onChangeText={(value) => this.validates(value, 'filterAgeGroup1')}
                    />
                  </View>

                  <View style={{ flex: 0.10, textAlignVertical: 'center', justifyContent: 'center' }}>
                    <Text style={[styles.textFonts, styles.dropdownToSlctText, { marginTop: 24 }]}> To </Text>
                  </View>

                  <View style={{ flex: 0.45, }}>
                    <Text style={[styles.textFonts, { color: '#ffffff', fontSize: 13 }]}></Text>
                    <Dropdown
                      containerStyle={[styles.dropdownSelection, { height: 30 }]}
                      textColor='#ffffff'
                      baseColor='#ffffff'
                      selectedItemColor="rgba(0, 0, 0, 0.34)"
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      labelFontSize={0}
                      fontSize={12}
                      value={this.state.filterAgeGroup2}
                      data={this.state.ageGroupTo}
                      dropdownPosition={0}
                      dropdownOffset={{ top: 6, left: 0 }}
                      onChangeText={(value) => this.validates(value, 'filterAgeGroup2')}
                    />
                  </View>

                </View>

                <View style={{ flex: 1, flexDirection: "row", marginBottom: 10, paddingLeft: 45, paddingRight: 35 }}>

                  <View style={{ width: "100%" }}>

                    <Text style={[styles.textFonts, { color: '#ffffff', fontSize: 13 }]}>Class Type</Text>

                    <Dropdown
                      containerStyle={{ backgroundColor: 'transparent', width: '100%', borderBottomWidth: 1, borderBottomColor: '#FFFFFF', paddingLeft: 5, height: 30, }}
                      // label= "Class Type"
                      textColor='#ffffff'
                      baseColor='#ffffff'
                      selectedItemColor="rgba(0, 0, 0, 0.34)"
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      labelFontSize={0}
                      fontSize={12}
                      value={this.state.filterClassType}
                      data={this.state.serviceCategories}
                      keyAttribute={"value"}
                      valueAttribute={"id"}
                      dropdownPosition={0}
                      dropdownOffset={{ top: 6, left: 0 }}
                      onChangeText={(value, index, data) => this.SelectedClass(value, index, data, 'filterClassType')}
                    />
                  </View>

                </View>

                <View style={{ flex: 1, flexDirection: "row", marginTop: 14, marginBottom: -4, maxHeight: 21, paddingLeft: 45, paddingRight: 35 }}>
                  <Text style={[styles.textFonts, { color: '#ffffff', fontSize: 13 }]}>Date</Text>
                </View>

                <View style={[styles.CalenderContainer, { paddingLeft: 45, paddingRight: 35 }]}>
                  <DatePicker
                    style={[{ width: "100%", borderWidth: 0, backgroundColor: "transparent", }]}
                    date={this.state.filteredDate}
                    mode="date"
                    placeholder='Select Date'
                    format="DD/MM/YYYY"
                    maxDate= {new Date()}
                    iconSource={require('../assets/images/calendar-1.png')}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateInput: {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomWidth: 1,
                        borderBottomColor: "#FFFFFF",
                        backgroundColor: "transparent",
                        textAlign: "left",
                      },
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 7,
                        marginLeft: 0,
                        width: 20,
                        height: 20
                      },
                      placeholderText: {
                        color: "#ffffff",
                        alignSelf: "flex-start"
                      },
                      dateText: {
                        color: "#ffffff",
                        alignSelf: "flex-start"
                      },

                    }}
                    onDateChange={date => {
                      this.setState({ filteredDate: date });
                    }}
                  />
                </View>

                <View style={{ flex: 1, flexDirection: "row", alignContent: "stretch", maxHeight: 80, marginBottom: -15, marginTop: 15, paddingLeft: 25, paddingRight: 15, }}>

                  <View style={{ flex: 0.8, }}>
                    <TouchableOpacity onPress={() => { (this.params.guestSchedule)?this.props.navigation.navigate('GuestSchedules'):this.props.navigation.goBack()}} >
                      <Text style={[styles.textFonts, { textAlign: "right", color: "#ffffff" }]} > CANCEL </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 0.2, }}>
                    <TouchableOpacity onPress={this.onFullScreen} >
                      <Text style={[styles.textFonts, { textAlign: "center", paddingLeft: 5, color: "#ffffff" }]} > OK </Text>
                    </TouchableOpacity>
                  </View>

                </View>

                {/* </ScrollView> */}

              </View>
            </LinearGradient>

          )

        }


        <ScrollView style={styles.container}>
          <View style={styles.mainContainer1}>
            {
              (this.state.firstLoad) ?
                (
                  <View>
                    <FilterSchedule ageFrom={this.state.age_group_from} ageTo={this.state.age_group_to} category_Id={this.state.filterClassId} date={setFormatDate(this.state.filteredDate)} item_Id='' location_Id={this.state.selectedLocation} navigations={this.props.navigation} />
                  </View>
                ) :
                (
                  <FilterSchedule ageFrom={this.state.age_group_from} ageTo={this.state.age_group_to} category_Id={this.state.filterClassId} date={setFormatDate(this.state.filteredDate)} item_Id='' location_Id={this.state.selectedLocation} navigations={this.props.navigation} />
                )
            }


          </View>
        </ScrollView>


      </Container>

    );

  }

  onFullScreen = async () => {
    // Set the params to pass in fullscreen isVisibleHeader to navigationOptions
    this.props.navigation.setParams({ fullscreen: !this.isVisibleHeader });
    this.isVisibleHeader = !this.isVisibleHeader;
    this.setState({ firstLoad: !this.state.firstLoad });
  }

  static navigationOptions = ({ navigation }) => {

    if (navigation.state.params && !navigation.state.params.fullscreen) {

      //Hide Header by returning null
      return { header: null };

    } else {

      return{
          title: 'Filtered Location',
          headerRight: (
            <View style={{ marginRight: 20 }} >
              <TouchableOpacity style={{ borderBottomColor: "rgba(0, 0, 0, 0)", paddingTop: 6}} activeOpacity={1} onPress={navigation.getParam('onFullScreen')} >
                  <Image style={{ width: 20, height: 20 }} source={require('../assets/images/filter.png')} />
              </TouchableOpacity>
            </View>
          ),
          headerLeft:(<HeaderBackButton tintColor='#ffffff' onPress={() => { (navigation.state.params.guestSchedule)?navigation.navigate('GuestSchedules'):navigation.goBack()}}/>),
          headerBackground:(
            <LinearGradient
              colors={['#0099EF', '#00D2EF']}
              style={{ flex: 1, 
                borderBottomLeftRadius: 15, 
                borderBottomRightRadius: 15,
                height:60,
                elevation: 4,
                ...Platform.select({
                  ios:{
                    borderRadius: 15,
                    //height:130,
                    marginTop:-15
                  }
                }),
              }}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
            />
          ),
          headerStyle: {
            backgroundColor: '#E5E5E5',
            height:60,
            elevation: 0,
            borderBottomWidth: 0,
            color:'#ffffff'
          },
          headerTintColor: '#FFFFFF',
          headerTitleStyle: {
            fontWeight: 'normal',
            alignSelf: 'center',
            textAlign:"center", 
            flex:1 ,
            fontSize:20,
            lineHeight:25,
            ...Platform.select({
              ios: {
                fontFamily:'SFProDisplayRegular'
              },
              android: {
                fontFamily:'RobotoRegular_1'
              },
            }),

        }
      }// RETURN CLOSED

    }//ELSE CLOSED

  }
}



  const styles = StyleSheet.create({
      mainContainer:{
        backgroundColor:'#E5E5E5',
        flex:1, 
        justifyContent: 'center',
        marginBottom: 30,
      },
      mainContainer1:{
        backgroundColor:'#E5E5E5',
        flex:1, 
        justifyContent: 'center',
        marginBottom: 20,
      },
      container: {
        flex: 1,
        paddingTop: 10,
        paddingBottom: 50,
        backgroundColor: '#E5E5E5',
      },
      buttonStyle:{
        backgroundColor:'transparent',
        borderRadius:4,
        borderWidth:1,
        flex:0.8,
        width:'90%',
        marginLeft:'5%',
        borderColor:'#000000',
        justifyContent: 'center',
        alignItems: 'center',
      },
        mainEventsClass: {
    marginTop: 10,
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: '#146FA9',
    borderLeftWidth: 4,
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4
  },
      loginButtonText:{
        color:'#000000',
        fontSize:12
      },
      content:{
        textAlign:'center',
        alignItems:'center',
        margin:0,
        flex:1,
        marginTop: 140
      },
      tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
          ios: {
            shadowColor: 'black',
            shadowOffset: { width: 0, height: -3 },
            shadowOpacity: 0.1,
            shadowRadius: 3,
          },
          android: {
            elevation: 20,
          },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
      },
      tabBarInfoText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
      },
      navigationFilename: {
        marginTop: 5,
      },
      filterContainer: {
        flex: 1,
        paddingTop: 40,
        paddingBottom: 15,
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        ...Platform.select({
          ios: {
            maxHeight: 460,
            minHeight: 320,
          },
          android: {
            maxHeight: 460,
            minHeight: 320,
            // maxHeight: 660,
          },
        }),

      },
      CalenderContainer: {
        flex: 1,
        flexDirection: "row",
        color:'#ffffff',
      },
      textFonts:{
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular'
          },
          android: {
            fontFamily:'RobotoRegular_1'
          },
        }),
      },
      dropdownSelection: {
        backgroundColor: 'transparent',
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        paddingLeft: 5,
        textAlignVertical: 'center'
      },
      dropdownToSlctText: {
        textAlignVertical:'center',height:'100%', color: "#ffffff", fontSize:13, textAlign:'center'
      }

  });
