import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity, TouchableHighlight, ScrollView, Platform, AsyncStorage} from 'react-native';
import { Container, Content, Text, Button, Input, Label, Item} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Spacer from '../components/UI/Spacer';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import { LinearGradient } from 'expo-linear-gradient';
import DatePicker from 'react-native-datepicker'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'


class Accounts extends React.Component{
  constructor(props) {
    super(props);
    this.config = {
      apiUrl: "https://konnect.myfirstgym.com/api/customer/my-account"
    }

    
    let currentDate = new Date();

    this.state = {
      Account:[],
      username: '',
      firstname:'',
      lastName:'',
      address:'',
      city:'',
      country:'',
      mobileno:'',
      gender:'',
      relation:'',
      months:'Month',
      days:'Day',
      years:'Year',
      imageId: this.uniqueId(),
      imageSelected: false,
      birthDate: "",
      onChangeMobile: true,
      onChangeCountry: true,
      onChangeCity: true,
      onChangeAddress: true,
      currentDate: currentDate,

      addressError: "",
      cityError: "",
      countryError: "",
      mobilenoError: "",

    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
  }

  async fetchMyAccount(token) {
    try {
  
        let response = await fetch(this.config.apiUrl+"?api_token="+token);
        let responseJson = [];
        try {
            responseJson = await response.json();
        } catch (error) {
            console.log(error);
            responseJson = [];
        }
        
        let accountRsponsJsonData = responseJson.data
        this.setState({ Account: accountRsponsJsonData, mobileno: accountRsponsJsonData.mobile, address: accountRsponsJsonData.address, city: accountRsponsJsonData.city, country: accountRsponsJsonData.country });
        
       // console.log("data - ", this.state.mobileno, this.state.address, this.state.city, this.state.country )

    } catch (error) {
        console.log(error);
    }

  }
  

  componentDidMount() {
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        this.fetchMyAccount(token);
    }    
    });
  }
  
  handleChange = (name, val) =>{
    if(name == "mobileno"){
      
      // if(this.state.mobileno === ""){
      //   this.setState({ [name]: val, onChangeMobile: false, mobilenoError: "Mobile Number can not be blank." })
      // }else if(this.state.mobileno.length < 10){
      //   this.setState({ [name]: val, onChangeMobile: false, mobilenoError: "Minimum 10 digit should be in Mobile Number." })
      // }else {
      //   this.setState({ [name]: val, onChangeMobile: false, mobilenoError: "" })
      // }

      this.setState({ [name]: val, onChangeMobile: false, mobilenoError: "" })

    }else if(name == "country"){
      
      // if(this.state.country === ""){
      //   this.setState({ [name]: val, onChangeCountry: false, countryError: "Country can not be blank." })
      // }else if(this.state.country.length < 3){
      //   this.setState({ [name]: val, onChangeCountry: false, countryError: "Minimum 3 character should be in Country." })
      // }else {
      //   this.setState({ [name]: val, onChangeCountry: false, countryError: "" })
      // }

      this.setState({ [name]: val, onChangeCountry: false, countryError: "" })

    }else if(name == "city"){
      
      // if(this.state.city === ""){
      //   this.setState({ [name]: val, onChangeCity: false, cityError: "City can not be blank." })
      // }else if(this.state.city.length < 3){
      //   this.setState({ [name]: val, onChangeCity: false, cityError: "Minimum 3 character should be in City." })
      // }else {
      //   this.setState({ [name]: val, onChangeCity: false, cityError: "" })
      // }

      this.setState({ [name]: val, onChangeCity: false, cityError: "" })
    }else if(name == "address"){

      // if(this.state.address === ""){
      //   this.setState({ [name]: val, onChangeAddress: false, addressError: "Address can not be blank." })
      // }else if(this.state.address.length < 3){
      //   this.setState({ [name]: val, onChangeAddress: false, addressError: "Minimum 3 character should be in Address." })
      // }else {
      //   this.setState({ [name]: val, onChangeAddress: false, addressError: "" })
      // }

      this.setState({ [name]: val, onChangeAddress: false, addressError: "" })

    }else{
      this.setState({ [name]: val })
    }

  } 

  handleSubmit = () => {
    const { onFormSubmit } = this.props;

    return onFormSubmit(this.state)
      .then(() => setTimeout(() => Actions.pop(), 1000))
      .catch(() => {});
  }
    
  // USING FUNCTION TO ASK PERMISSION FROM USER
  _checkPermissions = async () => {

    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ camera: status });

    // take permission for Gallery, ask a CameraRoll
    const { statusRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({  cameraRoll: statusRoll   });

  };

  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1); // substring return all chars except char at index=0;
  };

  uniqueId = () => {
    // create uniqueId for image as Alphabetical
    return (
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4() +
      "-" +
      this.s4()
    );
  };

  // USING FUNCTION TO CALL PERMISSION WITH DIRECTORY TO CHOOSE IMAGES
  findNewImage = async () => {
      
    this._checkPermissions();

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: "Images",
      allowsEditing: true,
      quality: 1
    });

    if (!result.cancelled) {
      this.setState({
        imageSelected: true,
        imageId: this.uniqueId(),
        uri: result.uri
      });

    } else {
      this.setState({
        imageSelected: false
      });
    }

  };

  profileUpdate = async () => {

    if(this.state.mobileno == ""){

      this.setState({ mobilenoError: "Mobile Number can not be blank." })

    }else if(this.state.mobileno.length < 10){

      this.setState({ mobilenoError: "Minimum 10 digit should be in Mobile Number." })

    }else if(this.state.country == ""){

      this.setState({ countryError: "Country can not be blank." })

    }else if(this.state.country.length < 3){

      this.setState({ countryError: "Minimum 3 character should be in Country." })

    }else if(this.state.city == ""){

      this.setState({ cityError: "City can not be blank." })

    }else if(this.state.city.length < 3){

      this.setState({ cityError: "Minimum 3 character should be in City." })

    }else if(this.state.address == ""){

        this.setState({ addressError: "Address can not be blank." })

    }else if(this.state.address.length < 3){

      this.setState({ addressError: "Minimum 3 character should be in Address." })

    } else{

      this.setState({ mobilenoError: "", countryError: "", cityError: "", addressError: "" })

      var Object = function(state) {
        
        // this.first_name= (state.firstname === "")? state.Account.first_name : state.firstname
        // this.last_name= (state.lastName === "")? state.Account.last_name : state.lastName        
        // this.gender= (state.gender === "")? state.Account.gender : state.gender.toLowerCase()
        // this.relationship= (state.relation === "")? state.Account.relationship : state.relation
        // this.birth_date= (state.birthDate === "")? state.Account.birth_date : state.birthDate
        this.address= (state.address === "")? state.Account.address : state.address
        this.city= (state.city === "")? state.Account.city : state.city
        this.country= (state.country === "")? state.Account.country : state.country
        this.mobile= (state.mobileno === "")? state.Account.mobile : state.mobileno
        
      }
      let updatedFieldsData = new Object(this.state);
      this.props.navigation.navigate('AccountsChild', { profileData : updatedFieldsData})

    }

  }


  render(){
      
    let gender = [
      { value: 'Male' },
      { value: 'Female' }
    ];
    
    let relation = [
      { value: 'Mother' },
      { value: 'Father' },
    ];
    
    let months = [
      { value: 'Jan' },
      { value: 'Feb' },
      { value: 'Mar' },
      { value: 'Apr' },
      { value: 'May' },
      { value: 'Jun' },
      { value: 'Jul' },
      { value: 'Aug' },
      { value: 'Sept' },
      { value: 'Oct' },
      { value: 'Nov' },
      { value: 'Dec' },
    ];

    let days = [];

    for (let i = 1; i < 32; i++) {
        days.push({
            value: i,
        });
    }
    
    let years = [];
    let currentYear = new Date().getFullYear();

    for (let i = currentYear - 50; i <= currentYear; i++) {
        years.push({
            value: i,
        });
    }


   return(
  
  
    <Container>

      <ScrollView style={styles.container}>
        <View style ={styles.mainContainer}>
         {(this.state.Account)
         ?(
           <View style = {{flex:1}}>
            <Spacer size={6} />
            <Text style = {styles.title}>MY INFO</Text>
            <View style ={styles.content}> 
            <Spacer size={12} />
                <View style={styles.notification}>
                  
                  <View style = {styles.firstColumn}>
                      <TextField
                        label='First Name'
                        editable={false}
                        value={this.state.Account.first_name}
                        //value={(this.state.firstname=="")? this.state.Account.first_name:this.state.firstname}
                        //onChangeText={v => this.handleChange('firstname', v)}
                        style={ styles.labelName }
                        labelTextStyle={ styles.labelNameStyle }
                        baseColor={"rgba(0, 0, 0, 0.87)"}
                      />
                  </View>

                  <View style = {styles.secondColumn}>

                    {/* USING FOLLOWING CONDITION TO SHOW SELECTED IMAGE OR BUTTON TO CHOOSE */}
                    {this.state.imageSelected == true ? (

                      //USING CODE TO DISPLAY IMAGE
                      <TouchableOpacity /*  onPress={this.findNewImage} */ underlayColor='tranaparent' >
                        <View style={[ styles.avatar, styles.avatarContainer, { marginBottom: 20 }, ]} >
                          <Image source={{ uri: this.state.uri }} style={styles.avatar} />
                        </View>
                      </TouchableOpacity>

                    ) : (

                      // <TouchableOpacity onPress={this.findNewImage}>
                      <TouchableOpacity>
                        <View style={[ styles.avatar, styles.avatarContainer, { marginBottom: 20 }, ]} >
                          <Image style={styles.avatar} source={{uri:this.state.Account.avatar}}/>
                        </View>
                      </TouchableOpacity>

                    )}
                    {/* CONDITION CLOSED */}

                  </View>

                </View>


                <View style={styles.notification}>
                  
                  <View style = {{ flex: 1, marginTop: -20}}>
                    <TextField
                      label='Last Name'    
                      editable={false}                   
                      value= {this.state.Account.last_name}
                      //value= {(this.state.lastName === "") ? this.state.Account.last_name : this.state.lastName}
                      //onChangeText={v => this.handleChange('lastName', v)}
                      style={ styles.labelName }
                      labelTextStyle={ styles.labelNameStyle }
                      baseColor={"rgba(0, 0, 0, 0.87)"}
                    />
                  </View>

                </View>

                <View style = {styles.itemView}>
                  <Item stackedLabel style={{borderWidth:1, borderColor:'#999999'}}>
                    <Dropdown containerStyle={styles.input}
                      label='Gender'
                      textColor='#000000'
                      editable={false}
                      baseColor='rgba(0, 0, 0, 0.87)'
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      labelFontSize={13}
                      value={this.state.Account.gender}
                      // data={gender}
                      style={ styles.labelName }
                    />
                  </Item>
                </View>

                <View style = {styles.itemView}>
                  <Item stackedLabel style={{borderWidth:1, borderColor:'#999999'}}>
                    <Dropdown containerStyle={styles.input}
                      label='Relationship'
                      textColor='#000000'
                      editable={false}
                      baseColor='rgba(0, 0, 0, 0.87)'
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      labelFontSize={13}
                      value={this.state.Account.relationship}
                      // data={relation}
                      style={ styles.labelName }
                      />
                  </Item>
                </View>

                <Spacer size={15} />


                <View style = {styles.itemView}>

                  <Label style={{ width: "100%", alignSelf: "flex-start", color: "rgba(0, 0, 0, 0.87)", fontSize: 13, lineHeight: 18, backgroundColor:'transparent' }}>Birthday</Label>
                  <DatePicker
                    style={{width: "100%", borderWidth: 0, backgroundColor:'transparent'}}
                    date= {this.state.Account.birth_date}
                    mode="date"
                    disabled={true}
                    placeholder="select date"
                    placeholderTextColor = "#000000"
                    format="YYYY-MM-DD"
                    // minDate="2016-05-01"
                    maxDate= {this.state.currentDate}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                        backgroundColor:'transparent'
                      },
                      dateInput: {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomWidth: 1,
                        textAlign: "left",
                        backgroundColor:'transparent'
                      },
                      placeholderText: {
                        color: "#000000",
                        backgroundColor:'transparent'
                      },
                      backgroundColor:'transparent'
                    }}
                    
                    tintColor = "red"
                    baseColor = "green"    
                    //date= {(this.state.Account.birth_date !== "" && this.state.birthDate == "")? this.state.Account.birth_date : this.state.birthDate }
                    //onDateChange={(date) => {this.setState({birthDate: date})}}
                  />
                    
                </View>

                <View style = {styles.itemView}>
    
                    <TextField
                        label='Address'
                        value={(this.state.onChangeAddress)?this.state.Account.address:this.state.address}
                        onChangeText={v => this.handleChange('address', v)}
                        style={ styles.labelName }
                        minLength={3}
                        labelTextStyle={ styles.labelNameStyle }
                        baseColor={"rgba(0, 0, 0, 0.87)"}
                        error={this.state.addressError}
                    />

                </View>

                <View style = {styles.itemView}>
                  
                    <TextField
                        label='City'
                        value={(this.state.onChangeCity)?this.state.Account.city:this.state.city}
                        onChangeText={v => this.handleChange('city', v)}
                        style={ styles.labelName }
                        minLength={3}
                        labelTextStyle={ styles.labelNameStyle }
                        baseColor={"rgba(0, 0, 0, 0.87)"}
                        error={this.state.cityError}
                    />

                </View>
                <View style = {styles.itemView}>
                  
                    <TextField
                        label='Country'
                        value={(this.state.onChangeCountry)?this.state.Account.country:this.state.country}
                        onChangeText={v => this.handleChange('country', v)}
                        style={ styles.labelName }
                        minLength={3}
                        labelTextStyle={ styles.labelNameStyle }
                        baseColor={"rgba(0, 0, 0, 0.87)"}
                        error={this.state.countryError}
                    />

                </View>
                <View style = {styles.itemView}>
                  
                    <TextField
                        label='Mobile Phone'
                        value={(this.state.onChangeMobile)?this.state.Account.mobile:this.state.mobileno}
                        onChangeText={v => this.handleChange('mobileno', v)}
                        autoCapitalize="none"
                        maxLength={10}
                        minLength={10}
                        keyboardType="numeric"
                        style={ styles.labelName }
                        labelTextStyle={ styles.labelNameStyle }
                        baseColor={"rgba(0, 0, 0, 0.87)"}
                        error={this.state.mobilenoError}
                    />

                </View>
                
              </View>
            </View>
         ):
         (
            <View style ={styles.content}> 
                <Text style = {[styles.text, styles.labelName ]}>All your Account Setting</Text>
              <Spacer size={10} />
              <Text style={[ styles.labelName, {textAlign:'center', flex:0.8, margin:0, color:'#827b7b'}]}>View your go-to classes and book your next workout, instantly</Text>
              <Spacer size={20} />
              <Button block style ={styles.buttonStyle} onPress={Actions.login}>
                  <Text style={[styles.loginButtonText, styles.labelName]}> Login </Text>
              </Button>
            </View>
         )
         }
      </View>
      </ScrollView>
      <KeyboardSpacer topSpacing ={15} />
      {(this.state.Account)
        ?(
        <View style = { styles.cartButtonView } >
          <Button style = { styles.cartButton } onPress={this.profileUpdate} >
              <Image  style = {styles.imageStyle} source = {require('../assets/images/Shape.png')} />
          </Button>
        </View>
        ):
        (
          <View></View>
        )
      }  

    </Container>

  )
}
    
  static navigationOptions =({navigation})=> ({
    title: 'My Account',
    headerRight: (
      <View style={{ backgroundColor: "rgba(0, 0, 0, 0)", marginTop: 20 }} >
        <View style={{ flexDirection: "row", flex: 1, marginRight: 10 }}>

          <TouchableHighlight style={{ flex: 0.5, marginRight: 10 }} onPress = { () => navigation.navigate('Contact')  } underlayColor="transparent" >
              <Image  style={{ width: 20, height: 19 }} source={require('../assets/images/email_call.png')} />
          </TouchableHighlight>

        </View>
      </View>
    ),
    headerStyle: {
      backgroundColor: '#ffffff',
      borderBottomLeftRadius: 15, 
      borderBottomRightRadius: 15,
      height:60,
      borderBottomWidth: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          
          ...Platform.select({
            ios:{
              borderRadius: 15,
              marginTop:-15
            }
          }),

        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:0.8 ,
      fontSize:20,
      lineHeight:25,
      paddingLeft: 20,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }), 
    },
  });
}


const styles = StyleSheet.create({
  avatarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  avatar: {
    backgroundColor:'#0099EF',
    borderRadius: 48,
    width: 96,
    height: 96,
  },
  mainContainer:{
    backgroundColor:'#ffffff',
    flex:1, 
    justifyContent: 'center',
    paddingRight:0,
    paddingLeft:0,
    marginBottom: 100
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#ffffff',
  },
  profileImage:{
    width:95,
    height:95,
    borderRadius:50,
    alignSelf:'center',
    position:'absolute',
    top:-50
  },
    
    title:{
      backgroundColor:'#EEEEEE',
      color:'rgba(0, 0, 0, 0.87)',
      fontWeight:'normal',
      width:'100%',
      textAlign:'center',
      paddingTop:5,
      paddingBottom:5,
      fontSize:16,
      lineHeight:21,
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular',
          fontSize:16,
          lineHeight:21,
          letterSpacing: -0.32
        },
        android: {
          fontFamily:'RobotoRegular_1',
          
          textAlign:'center',
          letterSpacing: -0.32
        },
      }),
    },
   text:{
        fontWeight:'bold',
         fontSize:16,
          textAlign:'center',
           flex:0.8,
          margin:0
      },
  buttonStyle:{
      backgroundColor:'transparent',
      borderRadius:4,
      borderWidth:1,
      flex:0.8,
      width:'80%',
      marginLeft:'10%',
      borderColor:'#000000',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginButtonText:{
      color:'#000000',
      fontSize:12
    },
    content:{
      textAlign:'center',
      alignItems:'center',
      margin:0,
      flex:1,
      paddingLeft:25,
      paddingRight:25,
  },

  notification:{
    flex:1,
    flexDirection:'row',
  },
  firstColumn:{
    flex:0.6
  },
  secondColumn:{
    flex:0.4,
    height: 90,
    textAlign:'center',
    alignItems:'center',
    justifyContent:'center',
    marginTop: 20
  },
  thirdColumn: {
    flex:1,
  },
  birthday:{
    flex:0.33,
    marginBottom:10
  },
  input:{ 
    backgroundColor:'#ffffff', 
    width:'100%',
    paddingLeft:0,
    borderBottomWidth:1,
    borderColor:'rgba(0, 0, 0, 0.12)',
  },

  item:{
    maxHeight:55,
    width:'100%',
    borderBottomWidth:0
  },
  
  itemView:{
    width:'100%',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },

  cartButtonView: {
    position: "absolute",
    bottom: 18,
    right: 18,
    borderRadius:28.5,
    width:57,
    height:57,
  },
 
  cartButton:{
    alignItems:'center',
    width:57,
    height:57, 
    borderRadius:28.5,
    backgroundColor:'#0099EF',
    color:'#ffffff',
    alignSelf:'flex-end',
    zIndex: 11,
    elevation: 10
   },

  imageStyle:{
      position: "relative",
      left: 20,
      top: 1,
      alignSelf:'center', 
      width:18,
      height:18
  },
  labelName: {
    fontSize:17,
    lineHeight:22,
    color: "rgba(0, 0, 0, 0.87)",
    ...Platform.select({
      ios: {
        letterSpacing:-0.41
      },
    }),
  },

  labelNameStyle: {
    fontSize:13,
    lineHeight:18,
    color: "rgba(0, 0, 0, 0.87)",
    ...Platform.select({
      ios: {
        letterSpacing:-0.08
      },
    }),
  }

});

export default Accounts;
  