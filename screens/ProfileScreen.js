import React from 'react';
import { Dimensions, TouchableOpacity, View, Platform, ScrollView, Image, StyleSheet, TouchableHighlight, Text, AsyncStorage } from 'react-native';
import { Button, Container} from 'native-base';
import ActivityIndicator from 'react-native-loading-spinner-overlay';
import { LinearGradient } from 'expo-linear-gradient';

let profile = true;
const window = Dimensions.get('window');

class ProfileScreen extends React.Component{
  constructor(props){
    super(props);
    this.params = this.props.navigation.state.params;
    this.baseURL ='https://konnect.myfirstgym.com/api/customer/profile?api_token=';
    this.state={
      profile: [],
      isLoading: false,
      isLogin:true,
    }
  }
  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}

async fetchProfile(api_token) {
  try {
      let response = await fetch(this.baseURL+api_token);
      let responseJson = [];
      try {
          responseJson = await response.json();
      } catch (error) {
          console.log(error);
          responseJson = [];
      }
      this.setState({ profile: responseJson.data });
     
  } catch (error) {
      console.log(error);
  }
}


componentDidMount() {
  AsyncStorage.getItem('api_token').then((token) => {
    if(token!==null){
      this.setState({
        isLoading: true,
        isLogin:false,
      });
      
      profile = true;
      this.props.navigation.setParams({ profile :true});
      this.fetchProfile(token);
    }
    else{
      profile = false;
      this.props.navigation.setParams({ profile :false});
      this.setState({isLogin:false});
    }
    
  });
}

  statusChange =() =>{
    this.setState({status:!this.state.status});
  }
  render(){
    
    const { navigate } = this.props.navigation;
    
    return (      
    <Container style={{ backgroundColor: '#E5E5E5' }}>
      
      <View style = {[styles.cmpltBoxContainer, this.state.isLoading? styles.logingtime:styles.logouttime]}>
       <LinearGradient colors={['#0099EF', '#00D2EF']} style={[styles.headerBoxContainer, this.state.isLoading? '':{height:215, marginBottom:80, zIndex:9}, { flex: 1 } ]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} >
        
        
          {
         Platform.select({
          android:(
            
              (this.state.isLogin)?
                (
                <View style={styles.loading}>
                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
                </View>
            ):
              (this.state.isLoading)
            ? (
              
              <View style = {{ backgroundColor:'#ffffff', position:'absolute', top: 60, left: 0, zIndex: 2, width:'90%', marginLeft:'5%', alignSelf:'center', borderRadius: 4, elevation: 5 }}>
                  <View style = {styles.profileImage}>
                    <Image  style = {{ width:100, height:100, borderRadius:50, alignSelf:'center' }} source = {{uri:this.state.profile.avatar}} />
                  </View>
                  <View style ={{marginTop:60, flexDirection:'row', flexWrap:'wrap', textAlign:'center', alignSelf:'center'}}>
                    <Text style ={styles.profileName}>{this.state.profile.first_name} {this.state.profile.last_name}</Text>
                    <TouchableHighlight onPress={() => this.props.navigation.navigate('Creditcard')} style={styles.cardIcon} underlayColor="white">
                      <View>
                          <Image style={styles.cardIcon}  source = {require('../assets/images/card.png')}/>
                      </View>
                    </TouchableHighlight>
                  </View>
  
                  <View style ={{alignSelf:'center', textAlign:'center'}}>
                    {
                      (this.state.profile.status)?
                      (<Button style = {styles.buttonStyle}><Text style={styles.buttonText}>Active</Text></Button>):
                      (<Button style = {[styles.buttonStyle, styles.unactivebuttonStyle]}><Text style={styles.buttonText}>Unactive</Text></Button>)
                    }
                    
                  </View>
                  <View style={{flexDirection:'row',flex:1}}>
                    <View style={[styles.flexColumn, styles.bordercolumn]}>
                      <Text style = {[styles.balanceText, { fontSize: 12, lineHeight: 16 }]}>Account Balance</Text>
                      <Text style={styles.balanceBoldText}>AED {this.state.profile.account_balance}</Text>
                    </View>
  
                    <View style={styles.flexColumn}>
                      <Text style = {[styles.balanceText, { fontSize: 12, lineHeight: 16 }]}>Unpaid Classes</Text>
                      <Text style={styles.balanceBoldText}>
                        {
                          (this.state.profile.unpaid_classes)?
                            (this.state.profile.unpaid_classes):
                            0
                        }
                        </Text>
                    </View>
                    </View>
              </View>
              ):(
                <View>
                    <Text></Text>
                </View>
              )
            
          )
        })
       }
       </LinearGradient>
       {
         Platform.select({
          ios:(
            
              (this.state.isLogin)?
                (
                <View style={styles.loading}>
                  <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
                </View>
            ):
              (this.state.isLoading)
            ? (
              
              <View style = {[{ backgroundColor:'#ffffff', position:'absolute', top: 60, left: 0, zIndex: 2, width:'90%', marginLeft:'5%', alignSelf:'center', borderRadius: 4, elevation: 5 }, {shadowOffset:{  width: 0,  height: 2,  },
              shadowColor: 'rgba(0, 0, 0, 0.25)',
              shadowOpacity: 1.0,
              shadowRadius:4
              }]}>
                  <View style = {styles.profileImage}>
                    <Image  style = {{ width:100, height:100, borderRadius:50, alignSelf:'center' }} source = {{uri:this.state.profile.avatar}} />
                  </View>
                  <View style ={{marginTop:60, flexDirection:'row', flexWrap:'wrap', textAlign:'center', alignSelf:'center'}}>
                    <Text style ={styles.profileName}>{this.state.profile.first_name} {this.state.profile.last_name}</Text>
                    <TouchableHighlight onPress={() => this.props.navigation.navigate('Creditcard')} style={styles.cardIcon} underlayColor="white">
                      <View>
                          <Image style={styles.cardIcon}  source = {require('../assets/images/card.png')}/>
                      </View>
                    </TouchableHighlight>
                  </View>
  
                  <View style ={{alignSelf:'center', textAlign:'center'}}>
                    {
                      (this.state.profile.status)?
                      (<Button style = {[styles.buttonStyle, {marginTop:5,paddingTop:0, paddingBottom:0,height:20, shadowOffset:{  width: 2,  height: 4,  },
                        shadowColor: 'rgba(0, 0, 0, 0.25)',
                        shadowOpacity: 0.8,
                        }]} onPress={this.statusChange}><Text style={styles.buttonText}>Active</Text></Button>):
                      (<Button style = {[styles.buttonStyle, styles.unactivebuttonStyle, {marginTop:5,paddingTop:0, paddingBottom:0,height:20}, {shadowOffset:{  width: 2,  height: 4,  },
                        shadowColor: 'rgba(0, 0, 0, 0.25)',
                        shadowOpacity: 0.8,
                        }]} onPress={this.statusChange}><Text style={styles.buttonText}>Unactive</Text></Button>)
                    }
                    
                  </View>
                  <View style={{flexDirection:'row',flex:1}}>
                    <View style={[styles.flexColumn, styles.bordercolumn]}>
                      <Text style = {[styles.balanceText, { fontSize: 12, lineHeight: 16 }]}>Account Balance</Text>
                      <Text style={styles.balanceBoldText}>AED {this.state.profile.account_balance}</Text>
                    </View>
  
                    <View style={styles.flexColumn}>
                      <Text style = {[styles.balanceText, { fontSize: 12, lineHeight: 16 }]}>Unpaid Classes</Text>
                      <Text style={styles.balanceBoldText}>
                        {
                          (this.state.profile.unpaid_classes)?
                            (this.state.profile.unpaid_classes):
                            0
                        }
                        </Text>
                    </View>
                    </View>
              </View>
              ):(
                <View>
                    <Text></Text>
                </View>
              )
            
          )
        })
       }
       
       {
          (this.state.isLogin)?
          (
          <View>
            <Text></Text>
          </View>
      ):
         (this.state.isLoading)?
         (
          <View>
            <Text></Text>
        </View>
         )
         :(
          <View style = {styles.logoutloginbox} >
          <View style = {styles.profileImage}>
            <Image  style = {[styles.profileLogout, {width:100, height:100,borderRadius:50,}]} source = {require('../assets/images/userlogout.png')} />
          </View>
            <TouchableHighlight block style ={[styles.buttonStyleTransparent, {zIndex:999, minHeight:45, marginBottom:20,marginTop:120, justifyContent:'center'}]} onPress={()=>this.props.navigation.navigate('Home') } underlayColor="white" >
              <Text style={styles.transparentButtonText}> Login </Text>
            </TouchableHighlight>
        </View>
         )
       
       
       
       
       }
       
       
       </View> 
        <ScrollView style={[ styles.container, {zIndex:0, position:'relative'} ]}>
          <View style ={[{flex:1, textAlign:'center'}, {zIndex:0}]}>
            {
              (this.state.isLogin)?
              (
              <View style={styles.loading}>
                <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
              </View>
          ):
          (this.state.isLoading)
          ? (

            <View style ={{ marginBottom:50, paddingTop: 120 }}>

            <View style = {{ position:'relative',width:'90%', alignSelf:'center', marginTop:0, marginBottom: 5}}>
              <View style={{flexDirection:'row',flex:1, marginTop:10, marginBottom:0, marginRight:10, marginLeft:10}}>
              <TouchableHighlight style={styles.viewColumnStyle} underlayColor="white" onPress={() => this.props.navigation.navigate('Schedule')}>
                  <View>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-classes.png')}/>
                    <Text style={styles.balanceText} >My Classes</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.viewColumnStyle, {opacity:0.8}]} underlayColor="white">
                  <View>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-waitlist.png')}/>
                    <Text style={styles.balanceText}>My Waitlists</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.viewColumnStyle, {opacity:0.8}]} underlayColor="white">
                  <View>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-attendance.png')}/>
                    <Text style={styles.balanceText}>My Attendance</Text>
                  </View>
                </TouchableHighlight>
              </View>
              <View style={{flexDirection:'row',flex:1, margin:10}}>
              <TouchableHighlight style={styles.viewColumnStyle} underlayColor="white" onPress={() => this.props.navigation.push('Schedule', {purchase:true})}>
                  <View>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-purchase.png')}/>
                    <Text style={styles.balanceText}>My Purchases</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight style={styles.viewColumnStyle} underlayColor="white" onPress={() => this.props.navigation.navigate('MyAccount')}>
                  <View>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-account.png')}/>
                    <Text style={styles.balanceText}>My Account</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.viewColumnStyle, {opacity:0.8}]} underlayColor="white">
                  <View>
                    <Image style={[styles.cardIconImages]}  source = {require('../assets/images/my-points.png')}/>
                    <Text style={styles.balanceText}>My Points</Text>
                  </View>
                </TouchableHighlight>
              </View>
            </View>
            <TouchableHighlight style={[styles.rootView, {borderTopRightRadius:2, borderTopLeftRadius:2, borderBottomWidth:1, borderBottomColor:"rgba(0, 0, 0, 0.1)" },{
        shadowOffset:{  width: 0,  height: 2,  },
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOpacity: 1.0,
        shadowRadius:4
            }]} underlayColor="white" onPress={() => this.props.navigation.navigate('Buyservices')} >
              <View style = {[styles.inlineView]}>
                        <Image style={styles.settingcardIcon}  source = {require('../assets/images/buy-services.png')}/>
                        <Text style ={styles.settingText}>Buy Services</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight style={[styles.rootView, {borderBottomRightRadius:2, borderBottomLeftRadius:2,},{
        shadowOffset:{  width: 0,  height: 2,  },
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOpacity: 1.0,
        shadowRadius:4
            }]} underlayColor="white" onPress={() => this.props.navigation.navigate('Setting')} >
              <View style = {styles.inlineView}>
                  <Image style={styles.settingcardIcon}  source = {require('../assets/images/setting.png')}/>
                  <Text style ={styles.settingText}>Setting</Text>
              </View>
            </TouchableHighlight>
          </View>
          ) : (
            <View style ={{marginBottom:50, paddingTop:0, zIndex:0, position:'relative'}}>
            <View style = {{ position:'relative',width:'90%', alignSelf:'center', marginTop:0, marginBottom: 5}}>
              <View style={{flexDirection:'row',flex:1, marginTop:10, marginBottom:0, marginRight:10, marginLeft:10}}>
                  <View style={styles.viewColumnStyle}>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-classes.png')}/>
                    <Text style={styles.balanceText}>My Classes</Text>
                  </View>
                  <View style={styles.viewColumnStyle}>
                    <Image style={styles.cardIconImages}  source = {require('../assets/images/my-waitlist.png')}/>
                    <Text style={styles.balanceText}>My Waitlists</Text>
                  </View>
                <View style={styles.viewColumnStyle}>
                  <Image style={styles.cardIconImages}  source = {require('../assets/images/my-attendance.png')}/>
                  <Text style={styles.balanceText}>My Attendance</Text>
                </View>
              </View>
              <View style={{flexDirection:'row',flex:1, margin:10}}>
                <View style={styles.viewColumnStyle}>
                  <Image style={styles.cardIconImages}  source = {require('../assets/images/my-purchase.png')}/>
                  <Text style={styles.balanceText}>My Purchases</Text>
                </View>
                <View style={styles.viewColumnStyle}>
                  <Image style={styles.cardIconImages}  source = {require('../assets/images/my-account.png')}/>
                  <Text style={styles.balanceText}>My Account</Text>
                </View>
                <View style={styles.viewColumnStyle}>
                  <Image style={styles.cardIconImages}  source = {require('../assets/images/my-points.png')}/>
                  <Text style={styles.balanceText}>My Points</Text>
                </View>
              </View>
            </View>

            <View style = {[styles.rootView, styles.inlineView, {borderBottomWidth:1, borderBottomColor:"rgba(0, 0, 0, 0.1)"}]}>
                <Image style={styles.settingcardIcon}  source = {require('../assets/images/buy-services.png')}/>
                <Text style ={styles.settingText}>Buy Services</Text>
            </View>

            <View style = {[styles.rootView, styles.inlineView]}>
                <Image style={styles.settingcardIcon}  source = {require('../assets/images/setting.png')}/>
                <Text style ={styles.settingText}>Settings</Text>
            </View>

          </View>
          )
            }
      </View>
    </ScrollView>

    </Container>
    )
  }


  static navigationOptions =({navigation})=> ({
    title: 'Profile',
    headerRight: (
      <View style={{ backgroundColor: "rgba(0, 0, 0, 0)", marginTop: 18 }} >
        <View style={{ flexDirection: "row", flex: 1, marginRight: 10 }}>
        { 
          (profile)?
          (
            
             <TouchableOpacity activeOpacity={1} style={{ flex: 0.5, marginRight: 10, backgroundColor: "transparent", elevation: 0, borderWidth: 0 }} onPress={() => navigation.navigate('MyCard')} underlayColor="transparent" >
              <Image  style={{ width: 24, height: 17, padding: 2}} source={require('../assets/images/card-1.png')} />
             </TouchableOpacity >
          ):
          (
                <Image  style={{marginRight: 10, width: 24, height: 17, padding: 2, opacity:0.3 }} source={require('../assets/images/card-1.png')} />
          )
        }
        </View>
      </View>
    ),
    headerStyle: {
      height:55,
      elevation: 0,
      borderBottomWidth:0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
           }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:24,
      paddingTop: 10,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular',
           paddingLeft: 0,
          letterSpacing: 0.38
        },
        android: {
          fontFamily:'RobotoRegular_1',
          paddingLeft: 55,
        },
      }),  
    },
});
}
const styles = StyleSheet.create({

  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
},
 
  spinnerTextStyle: {
    color: '#FFF',
  },
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor:'#E5E5E5'
  },

  cmpltBoxContainer: {
    backgroundColor: '#E5E5E5',
    height: 175,
    zIndex: 9,
  },
  logingtime:{
    height: 165,
  },
  logouttime:{
    height:250
  },
  headerBoxContainer: {
    // backgroundColor: '#0099EF',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height: 145,
    ...Platform.select({
      ios: {
        marginTop: -15,
        borderRadius: 15,
      }
    }),
  },

  rootView:{
    backgroundColor:'#ffffff',
     position:'relative',
     width:'90%',
     alignSelf:'center',
     elevation: 3
  },
  inlineView:{
    flexDirection:'row',
     flexWrap:'wrap',
     padding:20,
     paddingRight:10,
  },
  profileImage:{
    width:100,
    height:100,
    borderRadius:50,
    alignSelf:'center',
    position:'absolute',
    top:-50,
    elevation: 6,
    borderWidth:1,
    borderColor: "#ffffff",    
  },
  profileLogout:{
    borderWidth:4,
    borderColor:'rgba(255, 255, 255, 0.34)',
    zIndex:9999,
    backgroundColor:'#ffffff'
  },
  profileName:{
    fontWeight:'normal',
    fontSize:22,
    lineHeight:28,
    color: "rgba(0, 0, 0, 0.87)",
    textAlign:'center',
    zIndex: 2,
    marginRight:10,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),

  },
  cardIcon:{
      width:30,
      height:30,
      zIndex:99
  },
  settingcardIcon:{
    width:20,
    height:20,
    zIndex:99,
    marginRight:10
  },
  cardIconImages:{
      width:40,
      height:40,
      resizeMode:'contain',
      alignSelf:'center',
      padding:10
  },
  buttonStyle:{
      backgroundColor:'#25AE88',
      borderWidth:1,
      borderRadius:20,
      height:20,
      width:80,
      borderColor:'#25AE88',
      textAlign:'center',
      fontSize: 11,
      color: "#ffffff",
      alignItems:'center',
      justifyContent:'center',
      elevation: 4,
      padding:0
  },
  unactivebuttonStyle:{
    backgroundColor:'#EB4242'
  },
  buttonText:{
    fontSize:11,
    textAlign:'center',
    color: "#ffffff",
    alignSelf:'center',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  flexColumn:{
    marginTop:20,
    paddingTop:10,
    paddingBottom:10,
     flex:0.5,
     textAlign:'center',
     marginBottom:20,
  },
  bordercolumn:{
    borderColor:'rgba(0, 0, 0, 0.1)',
    borderRightWidth:1,
    borderStyle: 'dotted',
    ...Platform.select({
      ios:{
        borderRightWidth: 2,
        borderColor:'rgba(0, 0, 0, 0.1)',
        borderStyle:'solid'
      }
    })
  },
  balanceText:{
    fontSize:11,
    lineHeight: 13,
    marginTop:5,
    textAlign:'center',
    color: "rgba(0, 0, 0, 0.54)",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  settingText:{
    fontSize:15,
    lineHeight: 20,
    textAlign:'center',
    color: "rgba(0, 0, 0, 0.54)",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  balanceBoldText:{
    fontSize:20,
    lineHeight: 25,
    fontWeight: "normal",
    textAlign:'center',
    textTransform: 'uppercase',
    color: "rgba(0, 0, 0, 0.87)",
    alignSelf:'center',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),

  },
  viewColumnStyle:{
    backgroundColor:'#ffffff',
     flex:0.33,
    alignSelf:'center',
    paddingTop:15,
    paddingBottom:15,
    marginLeft:5,
    marginRight:5,
    borderRadius: 4,
    elevation: 3,
    ...Platform.select({
      ios:{
        shadowOffset:{  width: 0,  height: 2,  },
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOpacity: 1.0,
        shadowRadius:4
      }
    })
  },
  buttonStyleTransparent:{
    backgroundColor:'#ffffff',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.87)',
    width:'90%',
    alignSelf:'center'
  },
  transparentButtonText:{
    color:'rgba(0, 0, 0, 0.87)',
    fontSize:20,
    lineHeight:25,
    alignItems:'center',
    textAlign:'center',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing:0.38,
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  logoutloginbox:
  { 
    backgroundColor:'#ffffff',
     position:'absolute',
      top: 60,
       left: '5%',
        zIndex: 999,
         width:'90%',
          alignSelf:'center',
           borderRadius:4,
           ...Platform.select({
            ios:{
              shadowOffset:{  width: 0,  height: 2,  },
              shadowColor: 'rgba(0, 0, 0, 0.25)',
              shadowOpacity: 1.0,
              shadowRadius:4
            },
            android:{
              elevation:5
            }
          })
}
});

export default ProfileScreen