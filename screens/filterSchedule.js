import React, { Component } from 'react';
import {View, Image, TouchableHighlight, StyleSheet, Platform, Text, AsyncStorage, ScrollView} from 'react-native';
import ActivityIndicator from 'react-native-loading-spinner-overlay';

class FilterSchedule extends Component {
  constructor (props){  
    super(props);
    this.state = {
      services:[],
      isLoading:true,
      apiAttributes:'',
      ageFrom:this.props.ageFrom,
      ageTo:this.props.ageTo,
      category_Id:this.props.category_Id,
      item_Id:this.props.item_Id,
      date:this.props.date,
      location_Id:this.props.location_Id,
    };
  }

async fetchServices(ageFrom, ageTo, category_Id, item_Id, date, location_Id) {
  try {
 //   console.log('https://konnect.myfirstgym.com/api/schedules/filter?to='+ageTo+'&from='+ageFrom+'&service_category_id='+category_Id+'&service_item_id='+item_Id+'&date='+date+'&location_id='+location_Id);
      let response = await fetch('https://konnect.myfirstgym.com/api/schedules/filter?to='+ageTo+'&from='+ageFrom+'&service_category_id='+category_Id+'&service_item_id='+item_Id+'&date='+date+'&location_id='+location_Id);
      let responseJson = [];
      try {
          responseJson = await response.json();
          this.setState({ services: responseJson.data, isLoading:false });
      } catch (error) {
          console.log(error);
          responseJson = [];
      }
     
  } catch (error) {
      console.log(error);
  }
}


componentDidMount() {
  this.fetchServices(this.state.ageFrom, this.state.ageTo, this.state.category_Id, this.state.item_Id, this.state.date, this.state.location_Id);

}

    render(){
      let items=0
      datavalue =[],

      Object.keys(this.state.services).map((item, i) =>{
        let dateLoop = true;
        items = items+1;
  
        datavalue.push(this.state.services[item].map((data, x) => {
            
          let date = null;
          if(dateLoop){
            date = item;
            dateLoop = false;
          }
  
          return(
            <View  key = {x}>
  
            {(date !== null)?
              (
                <View style={{ backgroundColor: "#ffffff", marginBottom: 4, marginTop: 4, elevation:1 }}>
                  <Text style = {[styles.fontFamily, {color:'rgba(0, 0, 0, 0.87)',height:25,justifyContent:'center', fontSize:17, lineHeight:25, backgroundColor:'#ffffff', flex:1, alignSelf:'center', textAlignVertical:'center'}]}>{date}</Text>
                </View>
              ):(
                <View></View>
              )
            }
              <View style = {{ paddingLeft:15, paddingRight:15, }} >
                
                <TouchableHighlight style ={styles.mainEventsClass} underlayColor="white" onPress={() => this.props.navigations.navigate('EventsDescription', {serviceItemId: data.schedule_id, name: data.service_item_name, schedule_date_id: data.schedule_date_id })} >
                  <View style={{flexDirection:'row',flex:1, backgroundColor: "#FFFFFF",paddingTop:15,paddingBottom:15, borderTopRightRadius: 4, borderBottomRightRadius: 4}}>
                    <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.20, alignSelf:'center'}}>
                    <Text style={[styles.fontFamily, {textAlign:'center',lineHeight:20,  fontSize:14, color:'rgba(0, 0, 0, 0.87)', paddingLeft:3, paddingRight:3}]} >{ data.start_time.toUpperCase()}</Text>
                    </View>
                    <View style ={{flex:0.54}}>
                      <View style = {{paddingLeft:10}}>
                        <Text style ={[styles.fontFamily, {fontSize:16, lineHeight:21, color:'rgba(0, 0, 0, 0.87)'}]}>{data.service_item_name}</Text>
                        <Text style ={[styles.fontFamily, {fontSize:13,lineHeight:18, color:'rgba(0, 0, 0, 0.54)'}]} >{data.service_category}</Text>
                      </View>
                    </View>
                    <View style ={{ flex:0.20, justifyContent:'center'}}>
                      <View style ={{flex:1, width:'100%', alignSelf:'center' , textAlign:'center', maxHeight:24, flex:1, justifyContent:'center', backgroundColor:'#25AE88', borderRadius:12,}}>
                        <Text style={[styles.fontFamily, styles.ageGroupbtn, { alignSelf:'center'}]}>{ data.age_group }</Text>
                      </View>
                    </View>
                    <View style ={{ flex:0.06, justifyContent:'center'}}>
                    <View style ={{alignSelf:'center', justifyContent:'center', marginRight:5}}>
                      <Image style={{width:8, height:'100%', resizeMode:'contain'}} source = {require('../assets/images/right_events.png')}/>
                    </View>
                   </View>
                  </View>
                </TouchableHighlight>
              </View>
            </View>
          )
  
        }))
  
      })


      if(this.state.isLoading){
        return(
          <View style={styles.loading}>
            <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
          </View>
        )
      }else{
        return(
          (items === 0)?
          (
            <ScrollView style={[styles.container, {  backgroundColor:'#E5E5E5' }]}>
              <View style ={styles.mainContainer1}>

                <View style={{flex:1, justifyContent:'center', flexDirection:'column'}}>
                  <View style ={styles.mainContainer1}>
                    <View style ={styles.content}>
                      <Text style ={[styles.fontfam, styles.sorrymsg ]}>
                        Sorry, You don't have any Service items.
                      </Text>
                    </View>
                  </View>
                </View>
            
              </View>
            </ScrollView>
          ):(
            <ScrollView style={styles.container}>
              <View style ={styles.mainContainer1}>
                {datavalue}
              </View>
            </ScrollView>
          )
          
        )
      }
  } 
}

  const styles = StyleSheet.create({
    mainContainer:{
      backgroundColor:'#E5E5E5',
      flex:1, 
      justifyContent: 'center',
      paddingRight:15,
      paddingLeft:15,
      paddingTop: 15,
      paddingBottom: 50,
      marginBottom: 30,
    },
    container: {
      flex: 1,
      backgroundColor: '#ffffff',
    },
    mainContainer1:{
      backgroundColor:'#E5E5E5',
      flex:1, 
      justifyContent: 'center',
      marginBottom: 5,
    },
    mainEventsClass:{
      marginTop:4,
      marginBottom:4, 
      borderLeftColor:'#146FA9',
      borderLeftWidth:4,
      borderRadius:4, 
      elevation: 2,
      justifyContent:'center',
      // paddingTop:15,
      // paddingBottom:15
    },
    sorrymsg:{
      alignSelf:'center',
      textAlign:'center', 
      flex:1, 
      margin:0, 
      fontSize:16,
      lineHeight:21,
      paddingBottom: 10, 
      paddingRight: 20, 
      paddingLeft: 20, 
      color:'rgba(0, 0, 0, 0.54)',
    },
    ageGroupbtn:{
      marginLeft:0,
      marginRight:0,
      fontSize:10,
      lineHeight:25, 
      textAlign:'right', 
      color: '#ffffff'
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      opacity: 0.5,
      backgroundColor: 'black',
      justifyContent: 'center',
      alignItems: 'center'
  },
   
    spinnerTextStyle: {
      color: '#FFF',
    },
    buttonStyle:{
      backgroundColor:'transparent',
      borderRadius:4,
      borderWidth:1,
      flex:0.8,
      width:'90%',
      marginLeft:'5%',
      borderColor:'#000000',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginButtonText:{
      color:'#000000',
      fontSize:12
    },
    content:{
      textAlign:'center',
      alignItems:'center',
      margin:0,
      flex:1,
      marginTop: 140
  },
  fontfam:{
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },  
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  });

  export default FilterSchedule;
