import React from 'react';
import { ActivityIndicator, Share, TouchableOpacity, View, StyleSheet, Image, TouchableHighlight, Text, AsyncStorage, ScrollView, Platform } from 'react-native';
import {  Container, Content, Button, ImageBackground  } from 'native-base';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, sliderItemWidth} from '../components/EventDescriptionSlider/Styles_slider';
import { classsliderWidth, classsliderItemWidth} from '../components/EventDescriptionSlider/Styles_slider1';
import SliderCarousel from '../components/EventDescriptionSlider/Setting';
import SliderCarousel1 from '../components/EventDescriptionSlider/Setting1';
import Spacer from '../components/UI/Spacer';
import Modal from "react-native-modal";
import { LinearGradient } from 'expo-linear-gradient';
import axios from 'axios';
import * as Calendar from 'expo-calendar';
import * as Permissions from 'expo-permissions';
import Checkbox from 'react-native-modest-checkbox';
import { StackActions, NavigationActions } from 'react-navigation';
import HTML from 'react-native-render-html';

class EventDescription extends React.Component {
  constructor(props) {
    super(props);
    let fav =false
    this.params = this.props.navigation.state.params;
    this.configSameUrl=this.props.navigations;
    this.config = {
      apiUrl:'https://konnect.myfirstgym.com/api/customer/schedules/'+this.params.serviceItemId+'/date/'+this.params.schedule_date_id,
      AddfevUrl:'https://konnect.myfirstgym.com/api/customer/favourites',
      bookAPI:'https://konnect.myfirstgym.com/api/customer/schedules/'+this.params.serviceItemId+'/date/'+this.params.schedule_date_id+'/book',

      deleteMultipleBookingAPI: 'https://konnect.myfirstgym.com/api/customer/schedules/'+this.params.serviceItemId+'/date/'+this.params.schedule_date_id+'/cancel',
    }
    this.state = {
      scheduleitem:[],
      isLoading:true,
      isHasToken:false,
      booking: true,
      isModalVisible: false,      
      isModalAttention: false,      
      isModalAtToSucs: false,
      isModalLoginFirst: false,
      loggedInUserChilds: [],
      loggedInUserChildsCount: 1,
      isCheckedChilds: false,
      isCheckedAllChilds: false,
      isCheckedChilds: [],
      selectedUserChilds: [],
      isModalUsersChilds: false,
      itemsFavId: 0,
      keyNumberOnLoad: 0,
  }
}

fixedEncodeURIComponent(str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
      return '%' + c.charCodeAt(0).toString(16);
  });
}

redirecctToHome = () =>{
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Explore' })],
  });
  this.props.navigation.dispatch(resetAction);
  this.setState({ isModalLoginFirst: false })
  this.props.navigation.navigate('Home')
}

async fetchServiceItem(token) {
  try {
    let serviceItemResponse = '';

    if(token !== null){
      serviceItemResponse = await fetch(this.config.apiUrl+"?api_token="+token);
    }else{
      serviceItemResponse = await fetch(this.config.apiUrl);
    }

    let serviceItem = [];
    try {
      serviceItem = await serviceItemResponse.json();
    } catch (error) {
        console.log(error);
        serviceItem = [];
    }

    if(serviceItem !== null){

      if(serviceItem.is_booked == true){ this.setState({booking: false }); }

      this.setState({scheduleitem: serviceItem , isLoading:false });
      this.setState({itemsFavId: this.state.scheduleitem.schedule.class.id });

      if(serviceItem.schedule.class.is_favourited === true){
        this.props.navigation.setParams({ favourite: true });
      }else{
        this.props.navigation.setParams({ favourite: false });
      }
      
    }
  } catch (error) {
      console.log(error);
  }
}

interested = (itemID, itemName) =>{
  //this.props.navigation.setParams({ name: itemName, serviceItemId: itemID });
  this.props.navigation.push('ServicesItems', {serviceCatId:itemID, serviceCatName:itemName} )
  //this.setState({isLoading:!this.state.isLoading});
}

   renderListComponent = ({ item }) => (
     <SliderCarousel path={item.path}/>
   );
   renderListComponent1 = ({ item }) => (
    <TouchableHighlight onPress = {() => this.interested(item.id, item.name )} underlayColor="white">
      <SliderCarousel1 title={item.name} path={item.photos[0].path} className={item.age_group}/>
    </TouchableHighlight>
  );

  BookingClickAction =() =>{
    this.setState({ booking: !this.state.booking });
    this.setState({ isModalAtToSucs: !this.state.isModalAtToSucs });
  };

  isModalLoginFirstOpen =() =>{
    this.setState({ isModalLoginFirst: true }); 
  };
  isModalLoginFirstClose =() =>{
    this.setState({ isModalLoginFirst: false }); 
  };

  BookingModalAction =() =>{
    this.setState({ isModalVisible: !this.state.isModalVisible });
      this.onBooking();
    //  this.setState({ booking: !this.state.booking });
  };

  BookingModalCloseAction = async() =>{
    let self = this;

    this.setState({ isModalVisible: !this.state.isModalVisible });
    
    // setTimeout( function (){
            
    //   self.props.navigation.push('EventsDescription', {serviceItemId: self.params.serviceItemId, name: self.params.name, schedule_date_id: self.params.schedule_date_id })

    // }, 1000 )
  };

  //USING COMPONENT TO ADD EVENT IN CALENDAR AS REMINDER
   AddReminderCmpt = async()=> {
    let self =this;
    const  newStatus  = await Permissions.askAsync( Permissions.CALENDAR ); 
    if(newStatus.status === 'granted') {      
      const status  = await Permissions.getAsync( Permissions.CALENDAR );
      if (status.status === 'granted') {
        var d = new Date();
        let result = await Calendar.createEventAsync("100", {
            title : self.state.scheduleitem['schedule'].class.service_category.name,//self.state.scheduleitem['schedule'].class.service_category.name,
            startDate:d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+'T00:00:00.000Z',// self.state.scheduleitem['schedule'].date + self.state.scheduleitem['schedule'].start_time,//self.state.scheduleitem['schedule'].date + self.state.scheduleitem['schedule'].start_time,//'2019-08-09T06:05:00.000Z',
            endDate: d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+'T00:00:00.000Z',//self.state.scheduleitem['schedule'].date + self.state.scheduleitem['schedule'].start_time,//self.state.scheduleitem['schedule'].date + self.state.scheduleitem['schedule'].start_time,//'2019-08-09T06:05:00.000Z',
            location :self.state.scheduleitem['schedule'].branches[0].detail.address,//self.state.scheduleitem['schedule'].branches[0].detail.address,
            notes  : self.state.scheduleitem['schedule'].class.description,//self.state.scheduleitem['schedule'].class.description, 
            timeZone: "GMT+5:30"
        });
        const  CalOpen  = await Calendar.openEventInCalendar(result);
        self.setState({ isModalVisible: !self.state.isModalVisible });
      }else {
      alert("Calendar Event can not be add!")
    }

    }else {
      alert("Calendar Event can not be add!")
    }

  };

  //USING FOLLOWING ACTION TO CANCEL BOOKING
  BookingCancelAction = () => { this.setState({ isModalAttention: !this.state.isModalAttention }); }  

   AttentionToSuccessBtnAction = () => {
    let self = this;
    let responsesQry = ''

    AsyncStorage.getItem('api_token').then((token) => {
      
      if(token!==null){
        responsesQry = axios.delete(this.config.deleteMultipleBookingAPI+"?api_token="+token)
        .then(function (responsesQry) {
          let rspSuccess = responsesQry.data;
          if(rspSuccess["success"] == true){
            self.setState({ isModalAttention: !self.state.isModalAttention, booking:!self.state.booking, isModalAtToSucs: !self.state.isModalAtToSucs });
            self.callbackFunctionForSuccessAtModal();
          }else{
            console.log(rspSuccess["message"]);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    
    });

  }

  BookingClickActionToReload = async() =>{
    if(this.state.booking){
    //  alert("Modal-", this.state.booking, 'true');
      this.setState({ booking: false });
    }
    else{
   //   alert("Modal-", this.state.booking, 'false')
      this.setState({ booking: true });
    }

    if(this.state.isModalAtToSucs){
      this.setState({ isModalAtToSucs: false });
    }
    else{
      this.setState({ isModalAtToSucs: true });
    }

    // setTimeout( function (){

    //   self.props.navigation.push('EventsDescription', {serviceItemId: self.params.serviceItemId, name: self.params.name, schedule_date_id: self.params.schedule_date_id })

    // }, 1000)

  };

  
  // WILL OPEN SHARING POPUP BY ON CLICK OF FOLLOWING FUNCTION 
  componentDidMount() {

    //initialize your function
    this.props.navigation.setParams({ onClickShare: this.onClickShare, onFavRemove: this.onFavRemove , onFavAdd: this.onFavAdd });
    
    // USING ASYNC STORAGE FUNCTION TO SET ITEM CHILDERNS IN STATE    
    AsyncStorage.getItem('@UserChilderns:key').then((children) => {
      let totChildCnt = 1;

      if (children !== null) {
        let ArrayChilderns = JSON.parse(children);
        // We have data!!
        this.setState({ loggedInUserChilds: ArrayChilderns })

        ArrayChilderns[0].map((item, i) =>{
          totChildCnt = totChildCnt+i;
          item.isSelect=false;
        })

        this.setState({ loggedInUserChildsCount: totChildCnt })

      }
      
    });

    // USING ASYNC STORAGE FUNCTION TO GET TOKEN    
    AsyncStorage.getItem('api_token').then((token) => {
      if (token !== null) {
        this.fetchServiceItem(token)
        this.setState({ isHasToken: true })        
      }else{
        this.fetchServiceItem(null)
      }
    });
  }
  onClickShare = async () => {
    let teacherName = ''
    if(this.state.scheduleitem['schedule'].teacher.length > 0){
      teacherName = this.state.scheduleitem['schedule'].teacher[0].name;
    }else{
      teacherName = ''
    }
    try {
      const result = await Share.share({
        message: 'Title - '+this.props.navigation.state.params.name
        
        +"\n\n"+"Description: "+this.state.scheduleitem['schedule'].class.description

        +"\n\n"+"- Scheduled Date&Time: "+this.state.scheduleitem['schedule'].date+", "+this.state.scheduleitem['schedule'].start_time
        +"\n\n"+"- Age Group: "+this.state.scheduleitem['schedule'].class.age_group
        +"\n\n"+"- Teacher: "+teacherName
        +"\n\n"+"- Location: "+this.state.scheduleitem['schedule'].branches[0].detail.address
        ,

      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  onFavRemove =() =>{
    let self = this;
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        axios.delete(this.config.AddfevUrl+'?service_item_id='+ this.state.itemsFavId +'&api_token='+token)
        .then(function (response) {
          self.props.navigation.setParams({ favourite: false });
        })
        .catch(function (error) {
          alert(error)
          console.log(error);
        });
    }
    else{
      alert("Login needed for removing item from Favorite")
    }    
    });
  }

  onFavAdd =() =>{

    let self = this;
    AsyncStorage.getItem('api_token').then((token) => {
      if(token!==null){
        axios.post(this.config.AddfevUrl, {
          service_item_id: this.state.itemsFavId,
          api_token: token,
        })
        .then(function (response) {
          self.props.navigation.setParams({ favourite: true });
        })
        .catch(function (error) {
          alert(error)
          console.log(error);
        });
    }
    else{
      alert("Login needed for Adding item in Favorite")
    }          
    });
  }

  onBooking =() =>{

    let self = this;
    let response = ''
    AsyncStorage.getItem('api_token').then((token) => {
      
      if(token!==null){
              
        if(this.state.loggedInUserChildsCount === 1 ){
          response = axios.post(this.config.bookAPI, {
            api_token: token,
            children: this.state.loggedInUserChilds[0][0].id
          })
          .then(function (response) {
            self.setState({booking:!self.state.booking })
            self.callbackFunctionForSuccessModal();
          })
          .catch(function (error) {
            console.log(error);
          });

        }else{
          response = axios.post(this.config.bookAPI, {
            api_token: token,
            children: this.state.selectedUserChilds.toString()
          })
          .then(function (response) {
            self.setState({booking:!self.state.booking })
            self.callbackFunctionForSuccessModal();
          })
          .catch(function (error) {
            console.log(error);
          });

        }

      }
    });
  }

  //USING COMPONENT TO CHECK OR UNCHECK BY ALL CHECK OPTION
  allCheckedChilds = () => {
    let checkedChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(this.state.isCheckedAllChilds){
        item.isSelect= false;
      }else{
        item.isSelect= true;
      }
    })

    this.setState({ isCheckedChilds: checkedChilds });
    this.setState((prevState) => ({ isCheckedAllChilds: !this.state.isCheckedAllChilds }));
  }

  //USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE CHILDS
  childsCheckbox = (itemID) => {
    let checkedChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(itemID===item.id){
        item.isSelect=!item.isSelect;
      }
    })

    this.setState({ isCheckedChilds: checkedChilds });
    this.setState((prevState) => ({ isCheckedAllChilds: false }));
  }

  // USING COMPONENT ON CLICK OF OK BUTTON TO BOOK CLASS
  selectedUserChilds = () => {
    let selectedUserChilds=[];
    checkedChilds = this.state.loggedInUserChilds[0];

    checkedChilds.map((item, i)=>{
      if(item.isSelect === true){
        selectedUserChilds[item.id] = item.id;
      }
    })
   
    selectedUserChilds = selectedUserChilds.filter(Boolean);
    this.setState({ selectedUserChilds: selectedUserChilds, isModalUsersChilds: !this.state.isModalUsersChilds });
    this.onBooking();
  }


  modalForUserChilds = () => {
    checkedChilds = this.state.loggedInUserChilds[0];
    checkedChilds.map((item, i)=>{
      item.isSelect= false;
    })
    this.setState({ isCheckedChilds: checkedChilds, isCheckedAllChilds: false });

    this.setState({ isModalUsersChilds: !this.state.isModalUsersChilds });
  }

  callbackFunctionForSuccessModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  callbackFunctionForSuccessAtModal = () => {
    this.setState({ isModalAtToSucs: !this.state.isModalAtToSucs })
  }
  
  // componentDidUpdate(){    
  // }

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={styles.loading}>
          <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="large" color="#0000ff"  />
        </View>
      )
    }else{
      
     console.log("Modal 2 - ", this.state.isModalVisible, " == ", this.state.isModalAtToSucs)

      return (
      <Container style={{backgroundColor:'#FFFFFF'}} key={this.state.keyNumberOnLoad} >
        <View style = {[styles.cmpltBoxContainer]}>
            <View style = {[styles.headerBoxContainer]}>
              <LinearGradient colors={['#0099EF', '#00D2EF']} style={{ flex: 1, borderBottomLeftRadius: 15,  borderBottomRightRadius: 15, height: 75,
              
                ...Platform.select({
                  ios:{
                    borderRadius: 15,
                    marginTop:-15,
                    height: 90,
                  }
                }),

              }} start={{x: 0, y: 0}} end={{x: 1, y: 0}} >
              <Image  style={{ width: '100%', height:'100%', zIndex:1,opacity:0.1 }} source = {require('../assets/images/headerbg.png')} />
              </LinearGradient>
            </View> 
          <View style = {styles.firstBoxContainer}>
            <View style = {styles.row}>
              <View style={[styles.column, styles.firstandlastcolumn]}>
                  <Text style = {styles.typeBlock}>Type</Text>
                  <Text style = {styles.nameBlock}>{this.state.scheduleitem['schedule'].class.service_category.name}</Text>
              </View>
              <View style={[styles.column, styles.middlecolumn]}>
                  <Text style = {styles.typeBlock}>Age Group</Text>
                  <Text style = {styles.nameBlock}>{this.state.scheduleitem['schedule'].class.age_group}</Text>
              </View> 
              <View style={[styles.column, styles.firstandlastcolumn, styles.borderRight]}>
                  <Text style = {styles.typeBlock}>Teacher</Text>
                    {
                      (this.state.scheduleitem['schedule'].teacher.length > 0 )?(
                        <Text style = {styles.nameBlock}>
                           {this.state.scheduleitem['schedule'].teacher[0].first_name+' '+this.state.scheduleitem['schedule'].teacher[0].last_name}
                        </Text> 
                      ):(
                        <Text style = {styles.nameBlock}></Text>
                      )

                    }
              </View>
            </View>
            <View style = {styles.row}>
              <View style={[styles.column, styles.firstandlastcolumn, styles.borderBottom]}>
                  <Text style = {styles.typeBlock}>Time</Text>
                  <Text style = {styles.nameBlock}>{this.state.scheduleitem['schedule'].start_time}</Text>
              </View>
              <View style={[styles.column, styles.middlecolumn, styles.borderBottom]}>
                  <Text style = {styles.typeBlock}>Day</Text>
                  <Text style = {styles.nameBlock}>{this.state.scheduleitem['schedule'].date}</Text>
              </View>
              <View style={[styles.column, styles.firstandlastcolumn, styles.borderRight, styles.borderBottom]}>
                  <Text style = {styles.typeBlock}>Duration</Text>
                  <Text style = {styles.nameBlock}>{this.state.scheduleitem['schedule'].duration}</Text>
              </View>
            </View>
          </View>
          
        </View>
        
        <Container style={{backgroundColor:'#FFFFFF'}}>
          <Content>
            <View style={styles.container}>
              <Spacer size={30} />
                  <Carousel
                    data={this.state.scheduleitem['schedule'].class.photos}
                    renderItem={this.renderListComponent}
                    sliderWidth={sliderWidth}
                    itemWidth={sliderItemWidth}
                    activeSlideAlignment={'start'}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={1}
                    loop={true}
                  />
              <Spacer size={5} />
              <View style={styles.listHead}>
                <View style ={{backgroundColor:'#146FA9', borderBottomRightRadius:10, borderTopRightRadius:10, width:3, height:'100%', borderColor:'#146FA9'}}><Text></Text></View>
                <View style={{flex:1, justifyContent:'center', marginLeft:7}}><Text style={styles.eventName}>Class Description</Text></View>
                </View>
              <View style = {styles.description}>
                <View style = {styles.classDescriprion}>
                  <HTML style = {styles.classDescriprionText} html={this.state.scheduleitem['schedule'].class.description}/>
                    {/* <Text style = {styles.classDescriprionText} >{this.state.scheduleitem['schedule'].class.description}</Text> */}
                </View>
              </View>
              <Spacer size={20} />
              <View style={styles.listHead}>
                <View style ={{backgroundColor:'#146FA9', borderBottomRightRadius:10, borderTopRightRadius:10, width:3, height:'100%', borderColor:'#146FA9'}}><Text></Text></View>
                <View style={{flex:1, justifyContent:'center', marginLeft:7}}><Text style={styles.eventName}>You may also be interested in</Text></View>
                {/* <View style={{flex:0.15, justifyContent:'center'}}><Text style={styles.viewStyle}>View All</Text></View>
                <View style={{flex:0.05, justifyContent:'center', textAlign:'right'}}><Image  style = {styles.iconStyle} source = {require('../assets/images/arrow.png')} /></View> */}
            </View>
            <Spacer size={20} />
            <Carousel
              data={this.state.scheduleitem['schedule'].interested_in}
              renderItem={this.renderListComponent1}
              sliderWidth={classsliderWidth}
              itemWidth={classsliderItemWidth}
              activeSlideAlignment={'start'}
              inactiveSlideScale={1}
              inactiveSlideOpacity={1}
              loop={true}
            />
            <Spacer size={15} />
            <View style = {styles.buttonView}>
              { (this.state.booking) ?
                  (
                    <View>
                      
                      { (this.state.loggedInUserChildsCount > 1 ) ?
                        (
                          <View>
                            {(this.state.isHasToken)?
                              (
                                <Button block style ={styles.buttonStyle} onPress = {this.modalForUserChilds}>
                                  <Text style={{ color: "#ffffff" }}>BOOK NOW</Text>
                                </Button>
                              ):(
                                <Button block style={styles.buttonStyle} onPress={this.isModalLoginFirstOpen}>
                                  <Text style={{ color: "#ffffff" }}>BOOK NOW</Text>
                                </Button>
                              )
                            }
                          </View>
                        ):(
                          <View>
                            {(this.state.isHasToken)?
                              (
                                <Button block style ={styles.buttonStyle} onPress = {this.BookingModalAction}>
                                  <Text style={[styles.buttonFontfanm, { color: "#ffffff" }]}>BOOK NOW</Text>
                                </Button>
                              ):(
                                <Button block style={styles.buttonStyle} onPress={this.isModalLoginFirstOpen}>
                                  <Text style={{ color: "#ffffff" }}>BOOK NOW</Text>
                                </Button>
                              )
                            }
                          </View>
                        )
                      }
                      
                    <View>
                        
                        <View>
                            {(this.state.isHasToken)?
                              (
                                <TouchableHighlight onPress = {() => this.props.navigation.navigate('BookMultiple', {
                                  itemID:this.params.serviceItemId, itemDateID:this.params.schedule_date_id, itemName: this.params.name,itemDesc:this.state.scheduleitem['schedule'].class.description,itemLoc:this.state.scheduleitem['schedule'].class.address,itemDate:this.state.scheduleitem['schedule'].date + this.state.scheduleitem['schedule'].start_time} )} underlayColor="transparent" >
                                    <Text style = {[styles.buttonFontfanm, styles.bookMultiple]}>Book Multiple</Text>
                                </TouchableHighlight>
                              ):(
                                <View></View>
                              )
                            }
                        </View>
                        

                      </View>

                    </View>
                  )
                :
                (
                  
                  <Button block style ={[styles.cancelButtonStyle]} onPress = {this.BookingCancelAction}>
                    <Text style={[styles.buttonFontfanm,{ alignSelf:"center", color: "#fff" }]}> CANCEL BOOKING</Text>
                  </Button>
                )
              }

              <Modal isVisible={this.state.isModalVisible} style={[ styles.modal, { paddingLeft: 10, paddingRight: 10 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={{ flex: 1 }}>
                
                <View style={styles.rightImgView} >
                  {
                    Platform.select({
                      ios:(
                        <View style={styles.rightImgSelf}>
                           <Image style = {{ width: 80, height: 80, borderWidth: 5, borderRadius: 40, borderColor: "#ffffff", }} source = {require('../assets/images/success_icon.png')} />
                        </View>
                      ),
                      android:(
                        <Image  style = {styles.rightImgSelf} source = {require('../assets/images/success_icon.png')} />
                      )
                    })
                  }

                </View>

                <View style={ styles.closeModalBtnView }>
                  <Button style={[styles.closeModalBtn ]} onPress={this.BookingModalCloseAction}>
                    <Text style={[ styles.closeModalBtnX ]}>X</Text> 
                  </Button>
                </View>

                <View style={{ flex: 1, position: "relative", top: 70, left: 0 }} >
                  <Text style={[ styles.modalHeading, styles.headingfont] } >SUCCESS!</Text>
                  <View style={{ marginLeft: 35, marginRight: 35 }}>
                    <Text style={[styles.buttonFontfanm, styles.modalMessage ]}>You are now signed up for { this.params.name }.</Text>
                  </View>
                  
                  <TouchableHighlight style={ styles.reminderModalBtn }  onPress={this.AddReminderCmpt} >
                    <Text style={[styles.buttonFontfanm, { alignSelf:"center", color: "#fff" }]}> ADD REMINDER </Text> 
                  </TouchableHighlight>
                </View>

              </View>
              </Modal>


            {/* MODAL USING TO DISPLAY ATTENTION POPUP ON CLICK OF CANCEL BOOKING */}

            <Modal isVisible={this.state.isModalAttention} style={[ styles.modal, { height: 200, padding: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={{ alignContent: "stretch" }}>

                <View style={{ flex: 1, flexDirection: "row"}}>
                  <View style={{ height: 100, }} >

                    <Text style={[styles.headingfont, { color:"#727272", alignSelf:"center", fontSize:20, paddingTop:15 }]}>
                      ATTENTION!
                    </Text>

                    <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                      <Text style={[ styles.modalMessage, styles.buttonFontfanm , { color: "#727272", fontSize: 14 } ]}>
                        Do you really want to cancel { this.params.name }?
                      </Text>
                    </View>
                    
                  </View>
                </View>


                <View style={{ flex: 1, flexDirection: "row", position: "relative", top: 0, marginTop: 75 }} >

                  <View style={{ flex:0.5, marginLeft: 0 , marginRight: 8, marginTop: 0, marginBottom: 0 }} >
                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.BookingCancelAction } >
                      <Text style={[styles.buttonFontfanm, { color: "rgba(0, 0, 0, 0.87)", width: "100%", textAlign:"center", padding: 10, }]}> NO </Text> 
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex:0.5, marginLeft: 8 , marginRight: 0, marginTop: 0, marginBottom: 0 }}>
                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={this.AttentionToSuccessBtnAction } >
                      <Text style={[styles.buttonFontfanm, { color: "rgba(0, 0, 0, 0.87)", width: "100%", textAlign:"center", padding: 10 }]}> YES </Text> 
                    </TouchableOpacity>
                  </View>

                </View>

              </View>
            </Modal>

            {/* MODAL USING TO DISPLAY SUCCESS POPUP FROM ATTENTION ON CLICK OF YES BOOKING */}

            <Modal isVisible={this.state.isModalAtToSucs} style={[ styles.modal, { height: 210, paddingTop: 0 , paddingBottom: 20 , paddingRight: 20 , paddingLeft: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

            <View style={{ alignContent: "stretch" }}>

              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ flex: 1, alignSelf: "flex-end" }} >

                  <TouchableOpacity onPress={ this.BookingClickAction } >
                    <Text style={{ textAlign:"right", color: "#rgba(135, 135, 135, 0.7)" }}> X </Text> 
                  </TouchableOpacity>

                </View>
              </View>

              <View style={{ flex: 1, flexDirection: "row"}}>
                <View style={{ height: 100, }} >

                  <Text style={[styles.headingfont, { color:"#727272", alignSelf:"center", fontSize:20, }]}>
                      SUCCESS!
                  </Text>

                  <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                    <Text style={[ styles.modalMessage , styles.buttonFontfanm, { color: "#727272", fontSize: 14 } ]}>
                      You have successfully canceled { this.params.name }.
                    </Text>
                  </View>
                  
                </View>
              </View>

              <View style={{ flex: 1, flexDirection: "row", marginTop: 60 }} >
                <View style={{ flex: 1 }} >

                  <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.BookingClickActionToReload } >
                    <Text style={[styles.buttonFontfanm, { color: "rgba(0, 0, 0, 0.87)", width: "100%", textAlign:"center", padding: 10, }]}> OK </Text> 
                  </TouchableOpacity>

                </View>
              </View>

            </View>
            </Modal>


            {/* MODAL USING TO DISPLAY LOGIN POPUP TO LOGIN FIRST */}

            <Modal isVisible={this.state.isModalLoginFirst} style={[ styles.modal, { height: 210, paddingTop: 0 , paddingBottom: 20 , paddingRight: 20 , paddingLeft: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

            <View style={{ alignContent: "stretch" }}>

              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ flex: 1, alignSelf: "flex-end" }} >

                  <TouchableOpacity onPress={ this.isModalLoginFirstClose } >
                    <Text style={{ textAlign:"right", color: "#rgba(135, 135, 135, 0.7)" }}> X </Text> 
                  </TouchableOpacity>

                </View>
              </View>

              <View style={{ flex: 1, flexDirection: "row"}}>
                <View style={{ height: 100, }} >

                  <Text style={[styles.headingfont, { color:"#727272", alignSelf:"center", fontSize:20, }]}> LOGIN!</Text>

                  <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                    <Text style={[ styles.modalMessage , styles.buttonFontfanm, { color: "#727272", fontSize: 14 } ]}>
                      You have need to login to book schedule { this.params.name }.
                    </Text>
                  </View>
                  
                </View>
              </View>

              <View style={{ flex: 1, flexDirection: "row", marginTop: 60 }} >
                <View style={{ flex: 1 }} >

                  <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress = {() => this.redirecctToHome()}>
                    <Text style={[styles.buttonFontfanm, { color: "rgba(0, 0, 0, 0.87)", width: "100%", textAlign:"center", padding: 10, }]}> Login  </Text> 
                  </TouchableOpacity>

                </View>
              </View>

            </View>
            </Modal>

            {/* MODAL USING TO DISPLAY ALL USER CHILDS ON CLICK OF BOOK NOW BUTTON */}

            <Modal isVisible= {this.state.isModalUsersChilds} style={[ styles.modal, { width: "80%", marginLeft: "10%", height: 210, padding: 20, paddingTop: 16, paddingBottom: 16 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >


              <View style={[ styles.modalSelectDayViews ]}>
                  <View style={{ flex: 1, alignSelf: "flex-start" }} >
                      <Text style={ styles.selectedDaysTitle }> Select Childerns </Text>
                  </View>
              </View>

                        
              <View style= {[ styles.modalSelectDayViews, { borderBottomWidth: 1, borderBottomColor: "rgba(0, 0, 0, 0.54)", marginBottom: 10, paddingBottom: 5 } ]} >

                  <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                    <Text style={ styles.modalSelectDayTitles }>
                      All Childerns
                    </Text>
                  </View>
                
                  <View style={{flex: 0.13, height: 35, paddingBottom:5, paddingRight: 5, paddingTop:5 }} >
                    <Checkbox 
                      checked={this.state.isCheckedAllChilds}
                      label=""
                      checkedImage={require('../assets/images/box-checked.png')}
                      uncheckedImage={require('../assets/images/box.png')}
                      checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                      onChange={this.allCheckedChilds} 
                    />
                  </View>
              </View>
              <ScrollView style={{ width: "100%", height: 40, alignContent: "stretch" }} >
                {
                 (this.state.loggedInUserChilds && this.state.loggedInUserChilds.length > 0)? ( 
                    this.state.loggedInUserChilds[0].map((item, i) =>{
                      let itemID = item.id
                        
                      return(
                        <View key={i} style={[ styles.modalSelectDayViews ]} >
                            <View style={{flex: 0.87, alignSelf: "flex-start" }} >
                                <Text style={ styles.modalSelectDayTitles }>
                                  {item.name}
                                </Text>
                            </View>

                            <View style={{flex: 0.13, height: 35, paddingBottom:5, paddingRight: 5, paddingTop:5 }} >
                              {
                                (item.isSelect)?
                                (
                                  <Checkbox 
                                    checked={true}
                                    label=""
                                    checkedImage={require('../assets/images/box-checked.png')}
                                    uncheckedImage={require('../assets/images/box.png')}
                                    checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                                    onChange={this.childsCheckbox.bind(this, itemID)}
                                  />
                                ):(
                                  <Checkbox 
                                    checked={false}
                                    label=""
                                    checkedImage={require('../assets/images/box-checked.png')}
                                    uncheckedImage={require('../assets/images/box.png')}
                                    checkboxStyle={{marginLeft: 5,width: 18, height: 18 }}
                                    onChange={this.childsCheckbox.bind(this, itemID)}
                                  />
                                )
                              }
                            </View>
                        </View>
                      )
                    })
                  ):(
                    <View></View>
                  )
                }
              </ScrollView>  
              <View style={[ styles.modalSelectDayViews, { maxHeight: 25, marginTop: 10, paddingTop: 5, alignItems: "center" } ]}>

                  {/* <View style={{ flex: 0.55,alignSelf: "flex-start" }} >
                      
                  </View> */}

                      <TouchableOpacity style={{ paddingRight : 15 }} onPress={ this.modalForUserChilds } >
                        <Text style={[styles.selectedDaysBtns,{textAlign:"right",color:"rgba(0,0,0,0.54)"}]}>
                          CANCEL
                        </Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={{ paddingLeft : 15 }} onPress={ this.selectedUserChilds } >
                        <Text style={[styles.selectedDaysBtns,{textAlign:"left",color:"rgba(0, 0, 0, 0.87)"}]}>
                          OK
                        </Text>
                      </TouchableOpacity>


                  {/* <View style={{ flex: 0.45, alignSelf:"flex-start" }} >
                      
                  </View> */}
                
              </View>


            </Modal>



            </View>
            <Spacer size={20} />
            </View>
          </Content>
        </Container>

      </Container>
      );
    }
  }

  static navigationOptions =({navigation})=> ({
    title: navigation.state.params.name,
    headerRight: (
      <View style={{ backgroundColor: "rgba(0, 0, 0, 0)", marginTop: 20 }} >
        <View style={{ flexDirection: "row", flex: 1, marginRight: 10 }}>
      {
        (!navigation.state.params.favourite)?
        (<TouchableOpacity style={{ flex: 0.5, marginRight: 12 }} onPress ={navigation.getParam('onFavAdd')}>
          <Image  style={{ width: 17, height: 15, padding: 2 }} source={require('../assets/images/favourite.png')} />
      </TouchableOpacity>)
        :
        (<TouchableOpacity style={{ flex: 0.5, marginRight: 12 }} onPress ={navigation.getParam('onFavRemove')} >
          <Image  style={{ width: 17, height: 15, padding: 2 }} source={require('../assets/images/fav.png')} />
      </TouchableOpacity>)
      }
          

          <TouchableOpacity style={{ flex: 0.5, marginRight: 5 }} onPress={navigation.getParam('onClickShare')} >
            <Image  style={{ width: 20, height: 15 }} source = {require('../assets/images/forward.png')} />
          </TouchableOpacity>
        </View>
      </View>
    ),
    headerStyle: {
      height:50,
      elevation: 0,
      borderBottomWidth: 0,
      backgroundColor:'#ffffff'
    },
    headerBackground: (
      <View>
      <LinearGradient
          colors={['#0099EF', '#00D2EF']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
      >
        <Image  style={{ width: '100%', height:'100%', zIndex:1,opacity:0.1 }} source = {require('../assets/images/headerbg.png')} />
        
      </LinearGradient>
      </View>
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      marginRight: 44,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular',
          marginRight:0,
          textAlign:'100%'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  });


}

const styles = StyleSheet.create({
  container:{
    marginRight:15,
    marginLeft:15,
    backgroundColor:'#FFFFFF'
  },
  row:{
      flexDirection:'row',
      flexWrap:'wrap',
      flex:1,
  },

  cmpltBoxContainer: {
    backgroundColor: '#FFFFFF',
    height: 136,
    zIndex: 1
  },

  headerBoxContainer: {
    backgroundColor:'#FFFFFF',
    height: 75,
  },

  firstBoxContainer:{
    shadowOffset: {
      width: 0,
      height: 4,    
    },
    shadowOpacity: 0.30,
    shadowRadius: 4,
    shadowColor:'rgba(0, 0, 0, 0.25)',
    elevation: 4,
    borderColor:'rgba(0, 0, 0, 0.25)',
    borderRadius: 4,
    backgroundColor: "#FFFFFF",
    borderBottomWidth:0,
    flex: 0.9,
    width:'90%',
    marginLeft:'5%',
    zIndex: 2,
    marginTop: 16,
    position: "absolute",
    top: -5,
    left: 0,
    padding: 10
  },
  description:{},
  className:{
    fontSize:17,
    lineHeight:22,
    color:'#146FA9',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  buttonFontfanm:{
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  classDescriprion:{
    marginRight:13,
    marginLeft:13,
    color:'#565656',
    textAlign:'justify'
  },
  classDescriprion1:{
    color:'#565656',
    fontSize:13,
    lineHeight:16,
    textAlign:'justify'
  },
  classDescriprionText:{
    textAlign:"justify",
    color: "#565656",
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  headingfont:{
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  column:{
    justifyContent:'center',
    borderRightColor:'rgba(0, 0, 0, 0.1)',
    borderRightWidth:1,
    borderBottomColor:'rgba(0, 0, 0, 0.1)',
    borderBottomWidth:1,
    alignContent:'center',
    textAlign:'center',
    padding:5
    // paddingTop:5,
    // paddingBottom:5,
    // paddingLeft:5,
    // paddingRight:5
  },
  borderRight:{
    borderRightWidth:0
  },
  borderBottom:{
    borderBottomWidth:0
  },
  firstandlastcolumn:{flex:0.3},
  middlecolumn:{flex:0.4},
  typeBlock:{
    color:'#8F8F8F',
    fontSize:12,
    lineHeight:16,
    alignSelf:'center',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  nameBlock:{
    color:'#000000',
    fontSize:12,
    lineHeight:16,
    alignSelf:'center',
    textAlign: 'center',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  listHead:
  {
    marginLeft:-10,
    flexDirection:'row', 
    flexWrap:'wrap', 
    flex:1,
  },
  eventName:{
    fontSize:17,
    lineHeight:22, 
    color:'#146FA9',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  viewStyle:
  {
    alignSelf:'flex-end', 
    fontSize:12,
    lineHeight:16,
    color:'#565656',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  iconStyle:{
    width:8, 
    height:11, 
    alignSelf:'flex-end'
  },
  buttonView:{
    flex:1
  },
  buttonStyle:{
    paddingBottom:15,
    paddingTop:15,
    backgroundColor:'#25AE88',
    borderRadius:2
  },
  cancelButtonStyle:{
    backgroundColor:'#EB4242',
  },
  buttonText:{
    fontSize:17,
    lineHeight:22,
    color:'#FFFFFF'
  },
  bookMultiple:{
    fontSize:12,
    lineHeight:16,
    textDecorationLine:'underline',
    alignSelf:'flex-end',
    marginTop:5,
    color:'#565656'
  },
  


  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,
    backgroundColor: "#ffffff",
    position: "absolute",
    top:150,
    elevation: 10,
    borderRadius: 5
  },
  modalHeading: {
    fontSize: 26,
    textAlign: "center",    
  },
  modalMessage: {
    textAlign: "center",
    fontSize: 18,
    justifyContent: "center",
    color: "gray",
  },
  closeModalBtnView: {
    position: "absolute",
    top: 5,
    right: 0,
    width: 50,
    paddingRight: 5,
    backgroundColor: "#ffffff",

  },
  
  closeModalBtn: {
    backgroundColor: "#ffffff",
    alignSelf: "center",
    borderWidth: 1,
    borderColor: "#FFFFFF",
    elevation:0,
    paddingLeft: 24
  },

  closeModalBtnX: {
    fontSize:20,
    fontWeight: "bold",
    color: "gray",
    borderWidth: 1,
    borderColor: "#FFFFFF",
    elevation: 0,
    backgroundColor: "#ffffff",
    ...Platform.select({
      ios: {
        paddingTop: 10,
      },
      android: {
        paddingTop: 17,
      },
    }),
  },

  closeModalBtnImg: {
    width: 15,
    marginRight: 5,
    marginLeft: 5,
  },

  modalSelectDayViews: {
    flex: 1,
    flexDirection: "row",
    alignContent: "stretch",
    
  },
  modalSelectDayTitles: {
    textAlign:"left",
    color:"#727272",
    fontSize: 13,
    lineHeight: 18,
    paddingBottom:5,
    paddingTop:5,
    //paddingLeft: 10
  },
  selectedDaysTitle: {
    fontSize: 17,
    lineHeight: 22,
    color: "rgba(0, 0, 0, 0.87)",
    textAlign:"center",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing:-0.41
      },
      android: {
        fontFamily:'RobotoRegular_1',
      },
    }),
  },

  selectedDaysBtns: {
    fontSize: 13,
    lineHeight: 18, 
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing:-0.41
      },
      android: {
        fontFamily:'RobotoRegular_1',
      },
    }),
  },

  rightImgView: {
    position: "absolute",
    top: 0,
    right: 0,
    width: "100%",
    // alignSelf: "center",
    // textAlign: "center"
  },

  rightImgSelf: {
    alignSelf:'center',
    backgroundColor:'transparent',
    position: 'relative',

    ...Platform.select({
      ios:{
        height: 90,
        width: 90,
        top: -40,
      },
      android:{
        height: 90,
        width: 90,
        top: -40,    
        borderWidth: 8,
        borderRadius: 100,
        borderColor: "#ffffff",
      }
    }),
  },
  
  reminderModalBtn: {
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,

    backgroundColor:  "#25AE88",
    padding: 15,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#25AE88"
  }
});
export default EventDescription;
