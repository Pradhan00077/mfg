import React from 'react';
import {Dimensions, TouchableOpacity, Image, Platform, ScrollView,  StyleSheet,  Text,  TouchableHighlight,  View, AsyncStorage} from 'react-native';
import {Button, Container, Content} from 'native-base';
import Spacer from '../components/UI/Spacer';
import Hr from '../components/UI/Hr';
import Modal from "react-native-modal";
import axios from 'axios';
import * as Facebook from 'expo-facebook';
import * as Expo from "expo";
import { LinearGradient } from 'expo-linear-gradient';
import { StackActions, NavigationActions } from 'react-navigation';

const appID = '1319207328248011';
const clientID = "184172311611-gofb7lti6g0favgfv73klt4scaqig8pm.apps.googleusercontent.com";

const window = Dimensions.get('window');

class HomeScreen extends React.Component {
  constructor(props){
    super(props);
    this.params = this.props.navigation.state;
    this.baseURL ='https://konnect.myfirstgym.com/api/customer/profile?api_token=';
    this.state={
      hasToken:false,
      isLoaded:true,
      isModal:false,
      logoutTokenGet: (this.params.params)?true:false           
    }
  }

  PushNotification = () =>{
    this.setState({isModal:false})
  }

  // USING FUNCTION TO FACEBOOK LOGIN
  async loginWithFacebook() {

    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
      appID,{
        permissions: ["public_profile", "email"]
      }
    );

    if (type === 'success') {

      // Get the user's name using Facebook's Graph API
      const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,about,picture`);
      const userInfo = await response.json();

      console.log('USER INFO ', userInfo);

      alert("Hello - "+ userInfo.name);

      this.props.navigation.navigate('Profile');

    }else{
      //alert(type);
    }

  }


  // USING FUNCTION TO GOOGLE LOGIN
  async loginWithGoogle() {

    try {
      const result = await Expo.Google.logInAsync({
        androidClientId: clientID,
        scopes: ['profile', 'email'],
      });
  
      if (result.type === 'success') {

        console.log('USER INFO ', result);
        alert(result.user.name);
        this.props.navigation.navigate('Main', {login:true});

      } else {
        console.log("Cancelled!");
      }
    } catch (e) {
      console.log("Error!", e);
    }

  }


  get fbButton() {
    return (
      
      <TouchableOpacity onPress={() => this.loginWithFacebook() }>
        <Image source ={require('../assets/images/facebook-gym.png')} style ={{width:45, height: 45, marginRight:10}} />
      </TouchableOpacity>

    )
  }

  get googleButton() {
    return (
      
      <TouchableOpacity onPress={() => this.loginWithGoogle() }>
        <Image source ={require('../assets/images/g-gym.png') } style ={{width:45, height: 45, marginLeft:10}} />
      </TouchableOpacity>

    )
  }

  componentDidMount() {
    
    AsyncStorage.getItem('api_token').then((token) => {
     if(token!==null){
       console.log(token);
       axios.get(this.baseURL+token,
        ).then(response => {
           this.setState({ hasToken: true, isLoaded: true }) 
         })
         .catch(error => {
             AsyncStorage.removeItem('api_token');
             this.props.navigation.push('Loading', { logoutToken: true })
         });
         

     }else{
       this.setState({ hasToken: false, isLoaded: true })
     }
   });

   if(this.state.logoutTokenGet){
     const resetAction = StackActions.reset({
       index: 0,
       actions: [NavigationActions.navigate({ routeName: 'Home' })],
     });
     this.props.navigation.dispatch(resetAction);
     
   }
   
 }

  
  render(){
    if (this.state.hasToken ) {
      return (
        this.props.navigation.navigate('Main', {login:true})        
      )
    } else {
      return (
        <ScrollView style={styles.container}>
          <View style ={styles.mainContainer}>
                
                <Spacer size={window.height/12} />

                <Image style={[ styles.logoAdjustment ]} source ={require('../assets/images/mgflogo1.png')} resizeMode='contain' />

                  
                  {
                    Platform.select({
                      ios: (
                        <Spacer size={window.height/34} />
                      ),
                      android: (
                        <Spacer size={window.height/23} />
                      )
                    })
                  }

                  <Text style ={styles.welcomeText} >Welcome!</Text>
                  <Text style ={styles.normalText} >Sign up and start exploring.</Text>
                
                  {
                    Platform.select({
                      ios: (
                        <Spacer size={window.height/21} />
                      ),
                      android: (
                        <Spacer size={window.height/18} />
                      )
                    })
                  }

                  <Button block style ={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Login')}>
                
                    <Text style={styles.loginButtonText}> Login </Text>

                  </Button>

                  <Spacer size={10} />

                  <Button block style ={styles.buttonStyleTransparent} onPress={() => this.props.navigation.navigate('Main', {login:false})} >

                    <Text style={styles.transparentButtonText}> Continue as a guest </Text>

                  </Button>

                  {
                    Platform.select({
                      ios: (
                        <Spacer size={4} />
                      ),
                      android: (
                        <Spacer size={10} />
                      )
                    })
                  }

                    {
                      Platform.select({
                        android: (

                          <View   style={{flex: 1, alignItems:'center',marginTop: 0,
                          marginRight: 'auto',
                          marginBottom: 0,
                          marginLeft: 'auto',}}>
                              <Hr text="Or" />
                              <Spacer size={10} />
                              <Text style={styles.continuaTextt}> Continue with </Text>
                              <Spacer size={10} />
                              <View  style={{flex: 1, flexDirection: 'row', alignItems:'center',marginTop: 0,
                                  marginRight: 'auto',
                                  marginBottom: 0,
                                  marginLeft: 'auto',}}>
                                  
                                {/* USING FUNCTION TO DISPLAY FACEBOOK LOGIN BUTTON */}
                                  {this.fbButton} 
                                {/* USING FUNCTION TO GOOGLE LOGIN BUTTON */}
                                  {this.googleButton}                    
                              </View>
                              </View>
                              )
                            })
                      }
                  {
                    Platform.select({
                      ios: (
                        <Spacer size={window.height/32} />
                      ),
                      android: (
                        <Spacer size={window.height/24} />
                      )
                    })
                  }
                  

                  <View  style={{flex: 1, flexDirection: 'row', alignItems:'center',marginTop: 10,
                      marginRight: 'auto',
                      marginBottom: 0,
                      marginLeft: 'auto',}}>
                    <Text style={styles.signUpText}> Don't have an account? </Text>
                    <TouchableHighlight onPress={() => this.props.navigation.navigate('Signup')} underlayColor="transparent" >
                      <Text style={[styles.signUpText, styles.signUpTextDecoration]}> Sign up </Text>
                    </TouchableHighlight>
                  </View>

                  <Spacer size={30} />

                  {/* <LinearGradient colors={['#0099EF', '#00D2EF']} style ={{ borderWidth:1, borderColor: "red" }}></LinearGradient> */}

                  <Modal isVisible={this.state.isModal} style={[ styles.modal, {  width:"80%", marginLeft: "10%", marginRight: "10%", height: 210, padding: 24,  flex:1,  justifyContent: 'center' }]} backdropColor="#0099EF" backdropOpacity={1} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

                    <View style={{ alignContent: "stretch" }}>

                      <View style={{ flex: 1, flexDirection: "row"}}>
                        <View style={{ height: 100, }} >

                          <Text style={[styles.fontFamily, styles.popupheading]}>
                            Be in the know
                          </Text>

                          <View style={{ marginBottom: 10 , marginTop: 10 }}>
                            <Text style={[ styles.fontFamily,  styles.modalMessage]}>
                              Enable push notification so you don't miss out special offers, tips & new features
                              just for you
                            </Text>
                          </View>
                          
                        </View>
                      </View>


                      <View style={{ flex: 1, flexDirection: "row", position: "relative", top: 0, marginTop: 30, right:0, alignItems:'flex-end', justifyContent:'flex-end' }} >

                        <View style={[styles.iosAbdroidbutton, {  marginLeft: 0 , marginRight: 8, marginTop: 20, marginBottom: 0 }]} >
                          <TouchableOpacity style={{ borderRadius: 2 }} onPress={ this.PushNotification } >
                            <Text style={[styles.fontFamily, styles.disabletext]}> Not Now </Text> 
                          </TouchableOpacity>
                        </View>

                        <View style={[styles.iosAbdroidbutton, {  marginLeft: 8 , marginRight: 0, marginTop: 20, marginBottom: 0 }]}>
                          <TouchableOpacity style={styles.enablebutton} onPress={this.PushNotification} >
                            <Text style={[styles.fontFamily, styles.enabletext]}> Enable </Text> 
                          </TouchableOpacity>
                        </View>

                      </View>

                    </View>
                  </Modal>

              </View>
        </ScrollView>
        
          
          
      );
    }
  }
}

HomeScreen.navigationOptions = ({navigation}) =>( {
  header: null,
  headerStyle: {
    backgroundColor: '#E5E5E5',
    height:60,
    elevation: 0,
    borderBottomWidth: 0
  },
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:1 ,
    fontSize:20,
    lineHeight:25,
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),

  }
});


const styles = StyleSheet.create({
  fontFamily:{
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
  },

  logoAdjustment: {
    ...Platform.select({
      ios: {
        width:270, height: 150, alignSelf:'center'
      },
      android: {
        width:300, height: 160, alignSelf:'center'
      },
    }),
  },

  mainContainer:{
    backgroundColor:'#2eaaeb',
    // flex:1, 
    justifyContent: 'center',
    alignContent:'center',
    alignSelf:'center',
    paddingRight:30,
    paddingLeft:30,
  },
  container: {
    width: "100%",
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#2eaaeb',
  },
  iosAbdroidbutton:{
    ...Platform.select({
      ios: {
        flex:0.5
      },
      android: {
       
      },
    }),
  },
  enablebutton:{
    ...Platform.select({
      ios: {
        borderRadius: 2, 
    borderWidth:1, 
    borderColor:'#565656',
      },
      android: {
        borderWidth:0, 
        textAlign:'right'
      },
    }),
  },
  enabletext:{
    fontSize:14,
    lineHeight:16,
    ...Platform.select({
      ios: {
        width: "100%", 
        textAlign:"center", 
        padding: 10, 
        color:'#565656' ,
      },
      android: {
        textAlign:'right', 
        paddingTop: 20, 
        paddingBottom: 0, 
        paddingLeft: 10, 
        paddingRight:0,
        color:'#009688' ,
        textTransform:'uppercase',
        fontStyle:'normal',
        fontWeight:'normal'
        
      },
    }),
  },
  disabletext:{
    fontSize:14,
    lineHeight:16,
    ...Platform.select({
      ios: {
        width: "100%", 
        textAlign:"center", 
        padding: 10, 
        color:'#565656' ,
      },
      android: {
        textAlign:'right', 
        paddingTop: 20, 
        paddingBottom: 0, 
        paddingLeft: 10, 
        paddingRight:0,
        color:'rgba(0, 0, 0, 0.54)' ,
        textTransform:'uppercase',
        fontStyle:'normal',
        fontWeight:'normal'
        
      },
    }),
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  welcomeText:{
    fontSize:34,
    lineHeight:41,
    textAlign: 'center',
    flex:1, 
    fontStyle:'normal',
    fontWeight:'normal',
    color:'#FFFFFF',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  normalText:{
    fontSize:16,
    textAlign: 'center',
    flex:1, 
    color:'white',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  buttonStyle:{
    backgroundColor:'#ffffff',
    borderRadius:4,
    elevation:0
  },
  buttonStyleTransparent:{
    backgroundColor:'#2eaaeb',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#ffffff',
    elevation:0
  },
  transparentButtonText:{
    color:'#ffffff',
    fontSize:17,
    lineHeight:22,
    alignItems:'center',
    textAlign:'center',
    fontStyle:'normal',
    fontWeight:'normal',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  continuaTextt:{
    color:'#ffffff',
    fontSize:12,
    lineHeight:16,
    alignItems:'center',
    textAlign:'center',
    fontStyle:'normal',
    fontWeight:'normal', 
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  loginButtonText:{
    color:'#00A6EF',
    fontSize:20,
    lineHeight:25,
    fontStyle:'normal',
    fontWeight:'normal',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        //latterSpacing:0.38,
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  signUpText:{
    color:'white',
    fontSize:13,
    lineHeight:18,
    fontStyle:'normal',
    fontWeight:'normal',
    textAlign:'center',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  signUpTextDecoration:{
    textDecorationLine:'underline',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  modal: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    maxHeight: 240,
    backgroundColor: "#ffffff",
    position: "absolute",
    top:150,
    right:0,
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        fontSize:17, 
        lineHeight:22,
        //latterSpacing:-0.41 ,
        color:'#5E5E5E',
        shadowColor: 'rgba(0, 0, 0, 0.3)',
        shadowOffset: { width: 0, height: 19 },
        shadowOpacity: 0.3,
      },
      android: {
        fontFamily:'RobotoRegular_1',
        borderRadius:2,
        elevation: 12,

      },
    }),
  },
  popupheading:{
     color:"rgba(0, 0, 0, 0.87)", 
     alignSelf:'flex-start', 
     fontSize:20, 
     lineHeight:23, 
     fontStyle:'normal',
     fontWeight:'normal',
     ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        fontSize:17, 
        lineHeight:22,
        //latterSpacing:-0.41 ,
        color:'#5E5E5E'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
  modalMessage: {
    textAlign:'justify',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        fontSize:13, 
        lineHeight:18,
        //letterSpacing: -0.08,
        color:'#5E5E5E'
      },
      android: {
        fontFamily:'RobotoRegular_1',
        fontSize:16, 
        lineHeight:24,
        letterSpacing: 0.20,
        fontStyle:'normal',
        fontWeight:'normal',
        color:'rgba(0, 0, 0, 0.541327)',
      },
    }),
  },
  navigationFilename: {
    marginTop: 5,
  },
});
export default HomeScreen