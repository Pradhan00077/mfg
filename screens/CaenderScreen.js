import React from 'react';
import { TouchableOpacity, View, StyleSheet, ScrollView, Platform, TouchableHighlight, Image, Text  } from 'react-native';
import { Button} from 'native-base';
import Spacer from '../components/UI/Hr';
import { Actions } from 'react-native-router-flux';
import CalendarPicker from 'react-native-calendar-picker';

class CalenderScreen extends React.Component {
  constructor (props){
    super(props);
    this.state = {
      LoopNo:10,
      selectedStartDate: null,
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }

  render(){
    var block = [];
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';

    for(let i=0; i <  this.state.LoopNo; i++) {
      block.push(
          <TouchableHighlight key={i} style ={styles.mainEventsClass} underlayColor="white" onPress={() => this.props.navigation.navigate('EventsDescription')}>
           <View style={{flexDirection:'row',flex:1}}>
              <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.22, alignSelf:'center'}}>
              <Text style={{textAlign:'right', paddingRight: 14, fontSize:15, color:'rgba(0, 0, 0, 0.87)'}} >11:00</Text>
                <Text style={{textAlign:'right', paddingRight: 14, fontSize:13, color:'rgba(0, 0, 0, 0.54)'}} >AM</Text>
              </View>
              <View style ={{flex:0.4}}>
                <View style = {{alignSelf:'center'}}>
                  <Text style ={{fontSize:16,fontWeight:'900', lineHeight:21, color:'rgba(0, 0, 0, 0.87)'}}>Stars Artistic</Text>
                  <Text style ={{fontSize:13, color:'rgba(0, 0, 0, 0.54)'}} >Gymnastic</Text>
                </View>
              </View>
              <View style ={{ flex:0.28, justifyContent:'center'}}>
                  <View style ={{alignSelf:'center', flex:1, justifyContent:'center'}}>
                    <Button style ={{backgroundColor:'#25AE88',borderRadius:50,width:'100%',height:30}}  >
                    <Text style={{marginLeft:0,marginRight:0,fontSize:11, width:'100%', textAlign:'center', color: '#ffffff'}}>3.5Y-8Y</Text>
                    </Button>
                  </View>
              </View>
              <View style ={{ flex:0.1, justifyContent:'center'}}>
                  <View style ={{alignSelf:'center', justifyContent:'center'}}>
                      <Image source = {require('../assets/images/right_events.png')}/>
                  </View>
              </View>
            </View>
         </TouchableHighlight>
      )
    }

    return(
      <ScrollView>
        <View>
          {(1)
                ?(
                  <View>
                    <View style={styles.CalenderContainer}>
                        <CalendarPicker
                          onDateChange={this.onDateChange}
                          weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                          nextTitle = '>'
                          previousTitle = '<'
                          allowRangeSelection={false}
                          selectedDayTextColor="#146FA9"
                          textStyle={{
                            fontFamily: 'SFPro_Text_Regular',
                            color: '#ffffff',
                          }}
                        />
                      </View>
                      <View style = {{backgroundColor:'#ffffff',}}>
                        <Text style ={{alignSelf:'center', color:'rgba(0, 0, 0, 0.87)', fontSize:17, fontFamily:'SFPro_Text_Regular', lineHeight:22}}>{ startDate }</Text>
                      </View>
                      <View style = {{paddingLeft:10, paddingRight:10}}>
                    { block }
                    </View>
                  </View>
            )
            : (
           <View style ={styles.content}> 
                  <Text style = {{fontWeight:'bold', fontSize:16, textAlign:'center', flex:0.8, margin:0}}>All your fitness favourites</Text>
                 <Spacer size={10} />
                 <Text style={{textAlign:'center', flex:0.8, margin:0, color:'#827b7b'}}>View your go-to classes and book your next workout, instantly</Text>
                 <Spacer size={20} />
                 <Button block style ={styles.buttonStyle} onPress={Actions.login}>
                     <Text style={styles.loginButtonText}> Login </Text>
                 </Button>
             </View>
            )
          }
          </View>
        </ScrollView>
      )
    }
}

    CalenderScreen.navigationOptions = {
    title: 'Calender',
    headerLeft: (
      <View>
        <View style={{ marginTop: 30, position: "relative", top: 0, left: 320 }} >
            <TouchableOpacity style={{ borderBottomColor: "rgba(0, 0, 0, 0)" }} onPress={ () => alert("Hello")} >
                <Image style={{ width: 20, height: 20 }} source={require('../assets/images/calendar.png')} />
            </TouchableOpacity>
        </View>
      </View>
    ),
    headerStyle: {
      backgroundColor: '#0099EF',
      height:60,
      elevation:0
    },
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'bold',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      marginTop: -45,
      marginRight: 55
    },
  };

const styles = StyleSheet.create({
  CalenderContainer: {
    flex: 1,
    backgroundColor: '#0099EF',
    color:'#ffffff',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
  },
  mainContainer:{
    backgroundColor:'#ffffff',
    flex:1, 
    justifyContent: 'center',
    paddingRight:15,
    paddingLeft:15,
    marginBottom: 30,
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 50,
    backgroundColor: '#ffffff',
  },
  mainEventsClass:{
    marginTop:10,
    marginBottom:10, 
    paddingTop:10, 
    paddingBottom:10, 
    borderColor:'#146FA9',
    borderLeftWidth:4,
    borderRadius:4, 
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4
  },
  buttonStyle:{
    backgroundColor:'transparent',
    borderRadius:4,
    borderWidth:1,
    flex:0.8,
    width:'80%',
    marginLeft:'10%',
    borderColor:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButtonText:{
    color:'#000000',
    fontSize:12
  },
  content:{
    textAlign:'center',
    alignItems:'center',
    margin:0,
    flex:1,
},
tabBarInfoContainer: {
  position: 'absolute',
  bottom: 0,
  left: 0,
  right: 0,
  alignItems: 'center',
  backgroundColor: '#fbfbfb',
  paddingVertical: 20,
},
tabBarInfoText: {
  fontSize: 17,
  color: 'rgba(96,100,109, 1)',
  textAlign: 'center',
},
navigationFilename: {
  marginTop: 5,
}
});
export default CalenderScreen;
