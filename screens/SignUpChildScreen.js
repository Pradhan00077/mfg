import React from 'react';
import { Container, Item, Button} from 'native-base';
import  {ImageEditor,ImageStore, View, Image, StyleSheet, TouchableHighlight, Platform, Animated, Text , TouchableOpacity, KeyboardAvoidingView, ScrollView, Picker } from 'react-native';
import Spacer from '../components/UI/Spacer';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import { LinearGradient } from 'expo-linear-gradient';
import ActivityIndicator from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import IOSPicker from 'react-native-ios-picker';


const name = /^[a-zA-Z]+(?:(?:|['_\. ])([a-zA-Z]*(\.\s)?[a-zA-Z])+)*$/;

const special = ['Zero','first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth'];
const deca = ['twent', 'thirt', 'fort', 'fift', 'sixt', 'sevent', 'eight', 'ninet'];

const numMonthDays = {
  '1': 31, '2': 28, '3': 31, '4': 30, '5': 31, '6': 30,'7': 31, '8': 31, '9': 30, '10': 31, '11': 30, '12': 31
}

let childImage = new FormData();
let currentDate = new Date();

class SignUpChild extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
       
        childData = [{
          FirstName:'',
          FirstNameValidator:true,
          LastName:'',
          LastNameValidator:true,
          School:'',
          MedicalCondition:'',
          MedicalConditionValidatorr:true,
          SchoolValidator:true,
          Gender:'Male',
          Birthday: '01/01/2000',
          currentDate: currentDate,
          imageId: this.uniqueId(),
          ChildAvatar:null,
          imageSelected: false,
          uri:null,
          month: '',
          day: '',
          year: '',
          numberofChild: '',
          childFirstName:'',
          childLastName:'',
          childSchool:'',
          childMedicalCondition:'',
          BirthdayError:'',
          GenderError:'',
        }],        
        this.state =  {
          childinfo:{
            FirstName:'',
            FirstNameValidator:true,
            LastName:'',
            LastNameValidator:true,
            School:'',
            SchoolValidator:true,
            MedicalCondition:'',
            MedicalConditionValidatorr:true,
            Gender:'Male',
            Birthday: '01/01/2000',
            currentDate: currentDate,
            imageId: this.uniqueId(),
            ChildAvatar:null,
            imageSelected: false,
            uri:null,
            month: '',
            day: '',
            year: '',
            childFirstName:'',
            childLastName:'',
            childSchool:'',
            childMedicalCondition:'',
            numberofChild: '0',
          },
          
          childFirstName:'',
          childLastName:'',
          childSchool:'',

          uri:null,
          childCount:1,
          disabled: false,
          isLoading:true,
          isButtonDisabled :true,
          errormessage:'',
          errorshow:false,
          error:false,
          branch:null,
          errorNumber: 0,

          month: 'Month',
          day: 'Day',
          year: 'Year',
    
          changedDay: "1",
          changedMonth: "1",
          changedYear: new Date().getFullYear() - 50,

          numberOfDays: "31",
          selectedDate: "",

        },
        this.animatedValue = new Animated.Value(0);
        this.validates = this.validates.bind(this);
        this.formsubmit = this.formsubmit.bind(this);
        this.stringifyNumber= this.stringifyNumber.bind(this);
      };

    //CALLING COMPONENT ON BLUR FIELD
    async onBlurValidates(type) {

      let splitedType = type.split("-")
      let typeName = splitedType[0]
      let errorNumberType = splitedType[1]
      if (typeName === 'FirstName') {

        if (childData[errorNumberType]['FirstName'] == '') {

          childData[errorNumberType]['childFirstName'] = "First name can not be blank."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
          
        }else if(childData[errorNumberType]['FirstName'].trim(' ').length < 3){
          
          childData[errorNumberType]['childFirstName'] = "Enter minimum 3 characters."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })

        }else{

          if (childData[errorNumberType]['LastName'] == '') {
            childData[errorNumberType]['childLastName'] = "Last name can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['LastName'].trim(' ').length < 3){            
            childData[errorNumberType]['childLastName'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childLastName'] = ""
          }
          
          if (childData[errorNumberType]['School'] == '') {
            childData[errorNumberType]['childSchool'] = "School can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['School'].trim(' ').length < 3){
            childData[errorNumberType]['childSchool'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childSchool'] = ""
          }

        /*  if (childData[errorNumberType]['MedicalCondition'] == '') {
            childData[errorNumberType]['childMedicalCondition'] = "MedicalCondition can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['MedicalCondition'].trim(' ').length < 3){
            childData[errorNumberType]['childMedicalCondition'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childMedicalCondition'] = ""
          }
          */
          childData[errorNumberType]['childFirstName'] = ""
          this.setState({ errorNumber: errorNumberType })
        }

      } else if (typeName === 'LastName') {

        if (childData[errorNumberType]['LastName'] == '') {
          childData[errorNumberType]['childLastName'] = "Last name can not be blank."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
        }else if(childData[errorNumberType]['LastName'].trim(' ').length < 3){    
          childData[errorNumberType]['childLastName'] = "Enter minimum 3 characters."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
        }else{

          if (childData[errorNumberType]['FirstName'] == '') {    
            childData[errorNumberType]['childFirstName'] = "First name can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['FirstName'].trim(' ').length < 3){
            childData[errorNumberType]['childFirstName'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childFirstName'] = ""
          }
          
          if (childData[errorNumberType]['School'] == '') {
            childData[errorNumberType]['childSchool'] = "School can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['School'].trim(' ').length < 3){
            childData[errorNumberType]['childSchool'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childSchool'] = ""
          }

        /*  if (childData[errorNumberType]['MedicalCondition'] == '') {
            childData[errorNumberType]['childMedicalCondition'] = "MedicalCondition can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['MedicalCondition'].trim(' ').length < 3){
            childData[errorNumberType]['childMedicalCondition'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childMedicalCondition'] = ""
          }*/
          
          childData[errorNumberType]['childLastName'] = ""
          this.setState({ errorNumber: errorNumberType })
        }

      } else if (typeName === 'School') {
        if (childData[errorNumberType]['School'] == '') {
          childData[errorNumberType]['childSchool'] = "School can not be blank."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
        }else if(childData[errorNumberType]['School'].trim(' ').length < 3){
          childData[errorNumberType]['childSchool'] = "Enter minimum 3 characters."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
        }else{

          if (childData[errorNumberType]['FirstName'] == '') {    
            childData[errorNumberType]['childFirstName'] = "First name can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['FirstName'].trim(' ').length < 3){
            childData[errorNumberType]['childFirstName'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childFirstName'] = ""
          }
          
          if (childData[errorNumberType]['LastName'] == '') {
            childData[errorNumberType]['childLastName'] = "Last name can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['LastName'].trim(' ').length < 3){            
            childData[errorNumberType]['childLastName'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childLastName'] = ""
          }

       /*   if (childData[errorNumberType]['MedicalCondition'] == '') {
            childData[errorNumberType]['childMedicalCondition'] = "MedicalCondition can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['MedicalCondition'].trim(' ').length < 3){
            childData[errorNumberType]['childMedicalCondition'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childMedicalCondition'] = ""
          }*/
          
          childData[errorNumberType]['childSchool'] = ""
          this.setState({ errorNumber: errorNumberType })
          
        }
      }
      else if(typeName === 'MedicalCondition') {
        if (childData[errorNumberType]['MedicalCondition'] == '') {
          childData[errorNumberType]['childMedicalCondition'] = "MedicalCondition can not be blank."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
        }else if(childData[errorNumberType]['MedicalCondition'].trim(' ').length < 3){
          childData[errorNumberType]['childMedicalCondition'] = "Enter minimum 3 characters."
          this.setState({ isButtonDisabled: true, errorNumber: errorNumberType })
        }else{

          if (childData[errorNumberType]['FirstName'] == '') {    
            childData[errorNumberType]['childFirstName'] = "First name can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['FirstName'].trim(' ').length < 3){
            childData[errorNumberType]['childFirstName'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childFirstName'] = ""
          }
          
          if (childData[errorNumberType]['LastName'] == '') {
            childData[errorNumberType]['childLastName'] = "Last name can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['LastName'].trim(' ').length < 3){            
            childData[errorNumberType]['childLastName'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childLastName'] = ""
          }

          if (childData[errorNumberType]['School'] == '') {
            childData[errorNumberType]['childSchool'] = "School can not be blank."
            this.setState({ isButtonDisabled: true })
          }else if(childData[errorNumberType]['School'].trim(' ').length < 3){
            childData[errorNumberType]['childSchool'] = "Enter minimum 3 characters."
            this.setState({ isButtonDisabled: true })
          }else{
            childData[errorNumberType]['childSchool'] = ""
          }
          
          childData[errorNumberType]['childMedicalCondition'] = ""
          this.setState({ errorNumber: errorNumberType })
          
        }
      }
      

    }

      async fetchBranch() {
        let self = this;
          let response = axios.get('https://konnect.myfirstgym.com/api/locations')
          .then(function (response) {
            self.setState({branch: response['data']});
          })
          .catch(function (error) {
            console.log(error);
          });
      }

     
      stringifyNumber=(n) =>{
        if (n < 20) return special[n];
        if (n%10 === 0) return deca[Math.floor(n/10)-2] + 'ieth';
        return deca[Math.floor(n/10)-2] + 'y-' + special[n%10];
      }

      IncrementChilds = () => {        
        childData.push({
          FirstName:'',
          FirstNameValidator:true,
          LastName:'',
          LastNameValidator:true,
          School:'',
          SchoolValidator:true,
          MedicalCondition:'',
          MedicalConditionValidator:true,
          Gender:'Male',
          Birthday: '01/01/2000',
          currentDate: currentDate,
          imageId: this.uniqueId(), 
          ChildAvatar:null,
          imageSelected: false,
          uri:null
        });

        this.setState({childCount: this.state.childCount+1, isButtonDisabled: true, childinfo:{
          FirstName:'',
          FirstNameValidator:true,
          LastName:'',
          LastNameValidator:true,
          School:'',
          SchoolValidator:true,
          MedicalCondition:'',
          MedicalConditionValidator:true,
          Gender:'Male',
          Birthday: '01/01/2000',
          currentDate: currentDate,
          imageId: this.uniqueId(),
          ChildAvatar:null,
          imageSelected: false,
          uri:null
        }});
        
        let nextChildCnt = this.state.childCount
        childData[nextChildCnt]['BirthdayError'] = ""
      }

    validates = (text, type, index) =>{

      if(type==='FirstName'){
        if(name.test(text.trim(' ')))
        { 
          this.state.childinfo['FirstName']=text;
          this.state.childinfo['FirstNameValidator']=true;
          childData[index]['FirstName']=this.state.childinfo['FirstName'];
          this.setState({error:false});
        } 
        else { 
          this.state.childinfo['FirstName']= text;
          this.state.childinfo['FirstNameValidator']=false;
          childData[index].FirstName=this.state.childinfo['FirstName'];
          //this.setState({ error:true }) 
          //errormessage='FirstName should be in correct format';
        }
      }
      else if(type==='LastName'){
        if(name.test(text.trim(' ')))
        { 
          this.state.childinfo['LastName']= text;
          this.state.childinfo['LastNameValidator']=true;
          childData[index].LastName=this.state.childinfo.LastName; 
          this.setState({error:false});
        } 
        else { 
          this.state.childinfo['LastName']= text;
          this.state.childinfo['LastNameValidator']=false;
          childData[index].LastName=this.state.childinfo.LastName;
          //this.setState({ error:true }) 
          //errormessage='LastName should be in correct format';
        }
      }
      else if(type==='MedicalCondition'){
        if(name.test(text.trim(' ')))
        { 
          this.state.childinfo['MedicalCondition']= text;
          this.state.childinfo['MedicalConditionValidator']=true;
          childData[index].MedicalCondition=this.state.childinfo.MedicalCondition;
          this.setState({error:false});
        } 
        else { 
          this.state.childinfo['MedicalCondition']= text;
          this.state.childinfo['MedicalConditionValidator']=false;
          childData[index].MedicalCondition=this.state.childinfo.MedicalCondition;
          //this.setState({ error:true }) 
          //errormessage='MedicalCondition Name should be in correct format';
        } 
      }
      else if(type==='School'){
        if(name.test(text.trim(' ')))
        { 
          this.state.childinfo['School']= text;
          this.state.childinfo['SchoolValidator']=true;
          childData[index].School=this.state.childinfo.School;
          this.setState({error:false});
        } 
        else { 
          this.state.childinfo['School']= text;
          this.state.childinfo['SchoolValidator']=false;
          childData[index].School=this.state.childinfo.School;
          //this.setState({ error:true }) 
          //errormessage='School Name should be in correct format';
        } 
      }
      else if(type==='Birthday'){ 
          this.state.childinfo['Birthday']= text;
          childData[index].Birthday=this.state.childinfo.Birthday;
          this.setState({error:false}); 
      }
      else if(type==='Gender'){ 
        this.state.childinfo['Gender']= text;
        childData[index].Gender=this.state.childinfo.Gender;
        this.setState({error:false}); 
      }

       {/*  
      if( childData[index].FirstName !=='' && childData[index].LastName !=='' && childData[index].School !=='' && childData[index].Gender ==='' )
      {
        childData[index].GenderError = 'Please select child gender.'
      }else{
        childData[index].GenderError = ''
      }
      
      if( childData[index].FirstName !=='' && childData[index].LastName !=='' && childData[index].School !=='' && childData[index].Birthday ==='' )
      {
        childData[index].BirthdayError = 'Please select child birthday.'
      }else{
        childData[index].BirthdayError = ''
      }

    */} 
      
      
      if( childData[index].FirstName !=='' &&
          childData[index].LastName !=='' &&
          childData[index].School !=='')
      {
        this.setState({ isButtonDisabled: false});
      }else{
        this.setState({isButtonDisabled:true});
      }

    }

    formsubmit =() =>{
    //  console.log("Child Data:-", childData);
     
    //this.props.navigation.navigate('Signupcrediantial', { childData:childData, Branch:this.state.branch});
    this.props.navigation.navigate('Signupcrediantial', {userinfo:this.params.userdata, childData:childData, Branch:this.state.branch});

    }


      
// USING FUNCTION TO ASK PERMISSION FROM USER
_checkPermissions = async () => {

  const { status } = await Permissions.askAsync(Permissions.CAMERA);
//  console.log("Camera status - ", status);
  this.setState({ camera: status });

  // take permission for Gallery, ask a CameraRoll
  const { statusRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
//  console.log("cameraRoll - ", statusRoll);
  this.setState({  cameraRoll: statusRoll   });

};

s4 = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1); // substring return all chars except char at index=0;
};

uniqueId = () => {
  // create uniqueId for image as Alphabetical
  return (
    this.s4() +
    "-" +
    this.s4() +
    "-" +
    this.s4() +
    "-" +
    this.s4() +
    "-" +
    this.s4()
  );
};

// USING FUNCTION TO CALL PERMISSION WITH DIRECTORY TO CHOOSE IMAGES
findNewImage = async (index) => {
  this._checkPermissions();
  let result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: "Images",
    allowsEditing: true,
    aspect: [1, 1],
    quality: 1,
    isCamera: true,
  });
  // console.log(result);
  if (!result.cancelled) {
    let resizedUri = await new Promise((resolve, reject) => {
    ImageEditor.cropImage(result.uri,
      {
        offset: { x: 0, y: 0 },
        size: { width: result.width, height: result.height },
        displaySize: { width: 100, height: 100 },
        resizeMode: 'contain',
      },
      (uri) => {
            ImageStore.getBase64ForTag(uri, (base64Data) => resolve(base64Data), (error) => reject(error))
          },
      () => reject(),
    );
    });
    //let localUri = resizedUri;
      //let filename = localUri.split('/').pop();      
      //let match = /\.(\w+)$/.exec(filename);
      //let type = match ? `image/${match[1]}` : `image`;
    //childImage = new FormData()
    //childImage.append('file', resizedUri)
   
     this.state.childinfo['ChildAvatar']= resizedUri;//{ uri: resizedUri, name: filename, type };
     this.state.childinfo['imageSelected']= true;
     this.state.childinfo['uri']= result.uri;
     childData[index].ChildAvatar=resizedUri;//this.state.childinfo.ChildAvatar;
     childData[index].imageSelected=this.state.childinfo.imageSelected;
     childData[index].uri=this.state.childinfo.uri;
    this.setState({ imageId: this.uniqueId(),uri : result.uri  });
     //console.log(childData);
  }
};

componentDidMount(){
  this.setState({isLoading:false});
  this.fetchBranch();
}

async onChangeDays(oMonthSel, oDaysSel, oYearSel, numberOfForm) {

  let nDays = ''
  let oDaysSelLgth = ''
  
  if(oMonthSel === "month"){
    if(this.state.month === "") oMonthSel = this.state.changedMonth 
    else oMonthSel = (childData[numberOfForm]['month'] == undefined)?0:childData[numberOfForm]['month']
  }
  if(oYearSel === "year"){
    if(this.state.year === "") oYearSel = this.state.changedYear 
    else oYearSel = (childData[numberOfForm]['year'] == undefined)?0:childData[numberOfForm]['year']
  }
  if(oDaysSel === "day"){
    if(this.state.day === "") oDaysSel = this.state.changedDay 
    else oDaysSel = (childData[numberOfForm]['day'] == undefined)?0:childData[numberOfForm]['day']
  }
  nDays = numMonthDays[oMonthSel];
  
  if (nDays == 28 && oYearSel % 4 == 0){
    nDays = nDays + 1;
  }
  this.setState({ numberOfDays: nDays })

  oDaysSelLgth = this.state.numberOfDays;
  let selectDate = oMonthSel + '/' + oDaysSel + '/' + oYearSel;

  childData[numberOfForm]['day'] = oDaysSel
  childData[numberOfForm]['month'] = oMonthSel
  childData[numberOfForm]['year'] = oYearSel
  childData[numberOfForm]['numberofChild'] = numberOfForm
  

  this.state.childinfo.day = oDaysSel
  this.state.childinfo.month = oMonthSel
  this.state.childinfo.year = oYearSel
  this.state.childinfo.numberofChild = numberOfForm

  this.setState({ day: oDaysSel, month: oMonthSel, year: oYearSel, selectedDate: selectDate })
  
  if( (oMonthSel != 0 && oMonthSel != "Month") && (oDaysSel != 0 && oDaysSel != "Day") && (oYearSel != 0 && oYearSel != "Year") ){
    
      this.state.childinfo.Birthday = selectDate
      childData[numberOfForm]['Birthday'] = this.state.childinfo.Birthday
      childData[numberOfForm].BirthdayError = ''

      if( childData[numberOfForm].FirstName !=='' &&
          childData[numberOfForm].LastName !=='' &&
          childData[numberOfForm].School !=='' )
      {
        this.setState({ isButtonDisabled: false});
      }else{
        this.setState({isButtonDisabled:true});
      }

  }else{
    this.setState({ isButtonDisabled: true });
  }
}


    render(){

      var chilDataView=[];
        let Gender = [
          { value: 'Male' },
          { value: 'Female' }
        ];

        let year = [];
        let currentYear = new Date().getFullYear();
        for (let i = currentYear - 50; i <= currentYear; i++) {
          year.push({
            value: i,
          });
        }
    
    
        let days = [];
        let labelVal = ''
        let daysLength = ''
        
        if(this.state.numberOfDays == undefined)
          daysLength = "31"
        else
          daysLength = this.state.numberOfDays
    
        for (let i = 0; i < daysLength; i++) {
          days.push({
            value: i+1,
          });
        }
        
        const totYears = year.map((item, i) => {
          return (
            <Picker.Item label={(item.value).toString()} value={item.value} key={i} />
          )
        })


      const options = {
        title: 'Select Avatar',
        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };


      for(let i=0; i<this.state.childCount;i++ ){
        
        let eachChildDays = []
        let numOfDays = '31'

        numOfDays = numMonthDays[childData[i]['month']];
        if (numOfDays == 28 && childData[i]['year'] % 4 == 0){
          numOfDays = numOfDays + 1;
        }

        for (let i = 0; i < numOfDays; i++) {
          eachChildDays.push({
            value: i+1,
          });
        }

        chilDataView.push(
          <View key = {i}>
                {
                  (i===(this.state.childCount-1))?
                    (this.state.error)?
                      (<Text style = {[ styles.fontFamily, {alignSelf:'center'}]}>{errormessage}</Text>):
                      (<Text> </Text>)
                      :
                      (<Text> </Text>)
                }
                
          <Spacer size={6} />
          <Text style = {[ styles.fontFamily, styles.title]}>{ (this.state.childCount===1)? ( ' CHILD INFO'):((this.stringifyNumber(i+1)) +' CHILD INFO') }</Text>
          <View style ={styles.content}> 
          <Spacer size={12} />
            <View style={styles.notification}>
              <View style = {styles.firstColumn}>
                <TextField
                  style={styles.labelName}
                  label='First Name'
                  onChangeText={text => this.validates(text, 'FirstName', i)}
                  onBlur={() => this.onBlurValidates('FirstName-'+i)}
                  error={(this.state.errorNumber == i)?childData[i]['childFirstName']:""}
                />
              </View>
              <View style = {styles.secondColumn}>
                  {/* USING FOLLOWING CONDITION TO SHOW SELECTED IMAGE OR BUTTON TO CHOOSE */}
                  {
                    (childData[i].imageSelected)? (
                  //USING CODE TO DISPLAY IMAGE
                    <View>
                      <TouchableOpacity onPress={()=>this.findNewImage(i)}>
                        <View style={[ styles.avatar, styles.avatarContainer, { marginTop: 10, marginBottom: 15 } ]} >
                          <Image source={{ uri: childData[i].uri }} style={styles.avatar} />
                      </View>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View>
                      <TouchableOpacity onPress={()=>this.findNewImage(i)}>
                        <View style={[ styles.avatar, styles.avatarContainer, {marginTop: 10, marginBottom: 15} ]} >
                          <Image style={{width:15,height:15}} source={require('../assets/images/edit.png')}/>
                        </View>
                      </TouchableOpacity>
                    </View>
                  )
                }
                {/* CONDITION CLOSED */}
              </View>
            </View>
            <View style={styles.notification}>
              <View style = {{ flex: 1, marginTop: -20}}>
                  <TextField
                    style={ styles.labelName }
                    label='Last Name'                       
                    onChangeText={text => this.validates(text, 'LastName', i)}
                    onBlur={() => this.onBlurValidates('LastName-'+i)}
                    error={(this.state.errorNumber == i)?childData[i]['childLastName']:""}
                  />
              </View>
            </View>
            <View style = {styles.itemView}>
              <Item stackedLabel style={{borderWidth:1, borderColor:'#999999'}}>
                <Dropdown containerStyle={[styles.input]}
                  label='Gender'
                  textColor='#000000'
                  baseColor='rgba(0, 0, 0, 0.54)'
                  inputContainerStyle={{ borderBottomColor: 'transparent' }}
                  labelFontSize={17}
              //   value={this.state.childinfo.Gender}
                  data={Gender}
                  style={ styles.labelName }
                  onChangeText={text => this.validates(text, 'Gender', i)}
                  
                  />
              </Item>
            </View>
            <Spacer size={20} />
           

            <View style={styles.notification}>
              <View style={[styles.birthday, { flex: 1, alignSelf: "flex-start" }]}>
                <Text style={(childData[i]['BirthdayError'] !== "")?{ color: "rgb(213, 0, 0)", fontSize: 12, lineHeight: 16 }: { color: "rgba(0, 0, 0, 0.87)", fontSize: 12, lineHeight: 16 }} >
                  Birthday
                </Text>
              </View>
            </View>


            <View style={  [styles.notification,(childData[i]['BirthdayError'] !== "")?{
              ...Platform.select({
                ios:{
                  borderBottomColor:"rgb(213, 0, 0)",
                  borderBottomWidth:1,
                  paddingBottom: 5
                },
                android: {
                  borderBottomColor:"rgb(213, 0, 0)",
                  borderBottomWidth:1
                }
              })
              
            }:{
              ...Platform.select({
                ios:{
                  borderBottomColor:"rgba(0, 0, 0, 0.12)",
                  borderBottomWidth:1,
                  paddingBottom: 5
                },
                android: {
                  borderBottomColor:"rgba(0, 0, 0, 0.12)",
                  borderBottomWidth:1
                }
              })

            }]}>

              <View style={[styles.birthday, { flex: 0.43, borderRightWidth:1, borderRightColor: "rgba(0, 0, 0, 0.12)", marginTop: 8, marginBottom: 8, }]}>

              {
                Platform.select({
                  ios:(

                    <IOSPicker 
                      mode='modal'
                      selectedValue={
                        (childData[i]['month'] < 10)?(
                          (parseInt(childData[i]['month']) == 0 || childData[i]['month'] == "")?(
                            "Month"
                          ):(
                            "0"+childData[i]['month']
                          )
                        ):(
                          childData[i]['month']
                        )
                      }
                      style={[styles.labelName, { height:30, width: "100%", paddingRight: 0, marginRight: 0, borderTopColor: "#FFFFFF"  }]}
                      onValueChange={(value) => this.onChangeDays(value,"day","year", i) }
                    >
                      <Picker.Item label="Month" value="0" />
                      <Picker.Item label="01" value="1" />
                      <Picker.Item label="02" value="2" />
                      <Picker.Item label="03" value="3" />
                      <Picker.Item label="04" value="4" />
                      <Picker.Item label="05" value="5" />
                      <Picker.Item label="06" value="6" />
                      <Picker.Item label="07" value="7" />
                      <Picker.Item label="08" value="8" />
                      <Picker.Item label="09" value="9" />
                      <Picker.Item label="10" value="10" />
                      <Picker.Item label="11" value="11" />
                      <Picker.Item label="12" value="12" />

                    </IOSPicker>

                  ),
                  android: (

                    <Picker
                      selectedValue={childData[i]['month']}
                      style={[styles.labelName, { height:30, width: "100%", paddingRight: 0, marginRight: 0 }]}
                      onValueChange={(value) => this.onChangeDays(value,"day","year", i) }
                    >
                      <Picker.Item label="Month" value="0" />
                      <Picker.Item label="01" value="1" />
                      <Picker.Item label="02" value="2" />
                      <Picker.Item label="03" value="3" />
                      <Picker.Item label="04" value="4" />
                      <Picker.Item label="05" value="5" />
                      <Picker.Item label="06" value="6" />
                      <Picker.Item label="07" value="7" />
                      <Picker.Item label="08" value="8" />
                      <Picker.Item label="09" value="9" />
                      <Picker.Item label="10" value="10" />
                      <Picker.Item label="11" value="11" />
                      <Picker.Item label="12" value="12" />

                    </Picker>

                  )
                })
              }

              </View>

              <View style={[styles.birthday, { flex: 0.38, borderRightWidth:1, borderRightColor: "rgba(0, 0, 0, 0.12)", marginTop: 8, marginBottom: 8 }]}>
              {
                Platform.select({
                  ios:(
                    <IOSPicker
                      mode='modal'
                      selectedValue={
                        (childData[i]['day'] < 10)?(
                          (childData[i]['day'] == 0)?(
                            "Day"
                          ):(
                            "0"+childData[i]['day']
                          )
                        ):(
                          childData[i]['day']
                        )
                      }
                      style={[styles.labelName, {height:30, borderTopColor: "#FFFFFF"}]}
                      onValueChange={(value) => this.onChangeDays("month",value,"year", i) }
                    >
                      <Picker.Item label="Day" value="0" />

                      {
                        eachChildDays.map((item, i) => {
                          let labelValue = ''
                          if((item.value).toString() < 10){
                            labelValue =  "0"+(item.value).toString()
                          }else{
                            labelValue =  (item.value).toString()
                          }
                          return (
                            <Picker.Item label={labelValue} value={item.value} key={i} />
                          )
                        })
                      }

                    </IOSPicker>
                  ),
                  android:(
                    <Picker
                      selectedValue={childData[i]['day']}
                      style={[styles.labelName, {height:30, borderTopColor: "#FFFFFF"}]}
                      onValueChange={(value) => this.onChangeDays("month",value,"year", i) }
                    >
                      <Picker.Item label="Day" value="0" />

                      {
                        eachChildDays.map((item, i) => {
                          let labelValue = ''
                          if((item.value).toString() < 10){
                            labelValue =  "0"+(item.value).toString()
                          }else{
                            labelValue =  (item.value).toString()
                          }
                          return (
                            <Picker.Item label={labelValue} value={item.value} key={i} />
                          )
                        })
                      }

                    </Picker>

                  )
                })
              }
                

              </View>

              <View style={[styles.birthday, { flex: 0.39, marginTop: 8, marginBottom: 8 }]}>

              {
                Platform.select({
                  ios:(
                    <IOSPicker
                      mode='modal'
                      selectedValue={
                        (childData[i]['year'] == "0" || childData[i]['year'] == "" )?(
                          "Year"
                        ):(
                          childData[i]['year']
                        )
                      }
                      style={[styles.labelName,{ height:30, borderTopColor: "#FFFFFF" }]}
                      onValueChange={(value) => this.onChangeDays("month","day",value, i) }
                    >
                      <Picker.Item label="Year" value="0" />
                      {totYears}
                    </IOSPicker>
                  ),
                  android: (
                    <Picker
                      selectedValue={childData[i]['year']}
                      style={[styles.labelName,{ height:30 }]}
                      onValueChange={(value) => this.onChangeDays("month","day",value, i) }
                    >
                      <Picker.Item label="Year" value="0" />
                      {totYears}
                    </Picker>
                  )
                })
              }

              </View>

            </View>

            <View style = {styles.itemView}>
                <TextField
                  style={ styles.labelName }
                  label='School'
                  onChangeText={text => this.validates(text, 'School', i)}
                  onBlur={() => this.onBlurValidates('School-'+i)}
                  error={(this.state.errorNumber == i)?childData[i]['childSchool']:""}
                />
            </View>
            <View style = {styles.itemView}>
                <TextField
                  style={ styles.labelName }
                  label='MedicalCondition'
                  onChangeText={text => this.validates(text, 'MedicalCondition', i)}
                  onBlur={() => this.onBlurValidates('MedicalCondition-'+i)}
                  error={(this.state.errorNumber == i)?childData[i]['childMedicalCondition']:""}
                />
            </View>
          </View>
        </View>
        );
      }

return(
  <Container>
      
      <ScrollView keyboardDismissMode='on-drag'>
      <View style ={styles.mainContainer}>
         {(!this.state.isLoading)?
         (
            <View style = {{flex:1}}>
                <View>
                  {chilDataView}
                </View>
              <View>
              <Spacer size={0} />

                <TouchableHighlight style ={styles.headingView} onPress={!this.state.isButtonDisabled?this.IncrementChilds:null} underlayColor="white">
                  <View style ={styles.headingView} >
                    <Text style={[ styles.fontFamily, styles.textHeading]} >Add Children</Text>
                    <View style = {{width:32, height:32, borderRadius:16, backgroundColor:'#0099EF', marginLeft:10,position: "absolute", right: "20%"}}>
                      <Image  style = {{width:16, height: 16,  alignSelf:'center', marginTop:8 }} source = {require('../assets/images/plusimage.png')} />
                    </View>
                  </View>
                </TouchableHighlight>
              </View>
              <KeyboardSpacer topSpacing ={15} />
            </View>

         ):
         (
          <View style ={styles.content}> 
             <View style={styles.loading}>
                <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
              </View>
          </View>
         )
         }
      </View>
    </ScrollView>
    {(!this.state.isLoading)
        ?(
          <View style = { styles.cartButtonView } >
          <Button style = {[ styles.cartButton, this.state.isButtonDisabled ? styles.buttonDisabled : ''] } onPress={this.formsubmit} disabled = {this.state.isButtonDisabled } >
              <Image  style = {styles.imageStyle} source = {require('../assets/images/Shape.png')} />
          </Button>
        </View>
        ):
        (
          <View style={styles.loading}>
            <ActivityIndicator visible={true} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} size="small" color="#0000ff"  />
          </View>
        )
      }

      
    </Container>
    
    )
  } 
};
  SignUpChild.navigationOptions = {
    title: 'Sign Up',
    headerStyle: {
      backgroundColor: '#0099EF',
      borderBottomLeftRadius: 15, 
      borderBottomRightRadius: 15,
      borderBottomWidth: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          ...Platform.select({
            ios:{
              borderRadius: 15,
              marginTop:-15
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      paddingRight: 42,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };

  const styles = StyleSheet.create({
    fontFamily:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    fontFamilyDisplay:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      opacity: 0.5,
      backgroundColor: 'black',
      justifyContent: 'center',
      alignItems: 'center'
  },
   
    spinnerTextStyle: {
      color: '#FFF',
    },
    avatarContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      
    },
    buttonDisabled:{
      backgroundColor:'#cccccc',
     },
    avatar: {
      backgroundColor:'#0099EF',
      borderRadius: 48,
      width: 94,
      height: 94,
    },
    mainContainer:{
      backgroundColor:'#ffffff',
      flex:1, 
      justifyContent: 'center',
      paddingRight:0,
      paddingLeft:0,
      marginBottom: 80
    },
    container: {
      flex: 1,
      paddingTop: 15,
      paddingBottom: 50,
      backgroundColor: '#ffffff',
    },
    profileImage:{
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center',
      position:'absolute',
      top:-50
    },
    title:{
      backgroundColor:'#EEEEEE',
      color:'rgba(0, 0, 0, 0.87)',
      fontSize:13,
      lineHeight:18,
      fontWeight:'normal',
      width:'100%',
      textAlign:'center',
      paddingTop:5,
      paddingBottom:5,
      textTransform: 'uppercase',
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular',
        },
        android: {
          fontFamily:'RobotoRegular_1',
        },
      }),
    },
    text:{
        fontWeight:'bold',
          fontSize:16,
          textAlign:'center',
            flex:0.8,
          margin:0
    },
    buttonStyle:{
      backgroundColor:'transparent',
      borderRadius:4,
      borderWidth:1,
      flex:0.8,
      width:'80%',
      marginLeft:'10%',
      borderColor:'#000000',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginButtonText:{
      color:'#000000',
      fontSize:12
    },
    content:{
      textAlign:'center',
      alignItems:'center',
      margin:0,
      flex:1,
      paddingLeft:25,
      paddingRight:25,
      paddingTop:15,
    },

    notification:{
      flex:1,
      flexDirection:'row',
    },
    firstColumn:{
      flex:0.6
    },
    secondColumn:{
      flex:0.4,
      height: 90,
      textAlign:'center',
      alignItems:'center',
      justifyContent:'center',
    },
    Birthday:{
      flex:0.33,
      marginBottom:10
    },
    input:{
      backgroundColor:'#ffffff', 
      width:'100%',
      paddingLeft:0,
      borderBottomWidth:1,
      borderColor:'rgba(0, 0, 0, 0.12)',
    },
    item:{
      maxHeight:55,
      width:'100%',
      borderBottomWidth:0
    },
    itemView:{
      width:'100%',
    },
    headingView:{
      flexDirection:'row', 
      flexWrap:'wrap', 
      alignContent:'center',
      alignItems:'center',
      marginTop:10,
      flex:1,
      marginBottom:10,
      marginRight: 15
    },
    textHeading:{
      width:'100%',
      textAlign:'center',
      fontSize:17,
      lineHeight:22,
      alignContent:'center',
      alignSelf:'center',
      margin:0,
      position:'relative',
      color:'rgba(0, 0, 0, 0.87)'
    },
    tabBarInfoContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { width: 0, height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      alignItems: 'center',
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
    tabBarInfoText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      textAlign: 'center',
    },
    navigationFilename: {
      marginTop: 5,
    },

    cartButtonView: {
      position: "absolute",
      bottom: 18,
      right: 18,
      borderRadius:33,
      width:66,
      height:66,
    },
   
    cartButton:{
      alignItems: 'center',
      width: 57,
      height: 57,
      borderRadius: 33,
      backgroundColor: '#0099EF',
      color: '#ffffff',
      alignSelf: 'flex-end',
      zIndex: 11,
      elevation: 10
    },
  
    imageStyle:{
        position: "relative",
        left: 20,
        top: 1,
        alignSelf:'center', 
        width:21,
        height:21
    },

    labelName: {
      fontSize:17,
      lineHeight:22,
      color: "rgba(0, 0, 0, 0.87)",
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular',
          letterSpacing:-0.41
        },
        android: {
          fontFamily:'RobotoRegular_1',
        },
      }),
    },

  });
export default SignUpChild;
