import React from 'react';
import {  TouchableOpacity, View, StyleSheet, Text, ScrollView } from 'react-native';
import { Container} from 'native-base';

import Modal from "react-native-modal";

import axios from 'axios';

class PushNotification extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
          isModal: true,
      }
    }

    PushNotification = () =>{
      this.setState({isModal:false})
      this.props.navigation.navigate('Home', replace={true});
    }

    render(){
        return(
            <Container>
               <ScrollView style={styles.container}>
          <View style ={styles.mainContainer}>
        <View style = {styles.cmpltBoxContainer}>
            <Modal isVisible={this.state.isModal} style={[ styles.modal, { height: 200, padding: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

              <View style={{ alignContent: "stretch" }}>

                <View style={{ flex: 1, flexDirection: "row"}}>
                  <View style={{ height: 100, }} >

                    <Text style={{ color:"#000000", alignSelf:'flex-start', fontSize:14, padding:10 }}>
                    Be in the know
                    </Text>

                    <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                      <Text style={[ styles.modalMessage , { color: "#727272", fontSize: 14 } ]}>
                        Enable push notification so you don't miss out special offers, tips & new features
                        just for you!
                      </Text>
                    </View>
                    
                  </View>
                </View>


                <View style={{ flex: 1, flexDirection: "row", position: "relative", top: 0, marginTop: 75 }} >

                  <View style={{ flex:0.5, marginLeft: 0 , marginRight: 8, marginTop: 0, marginBottom: 0 }} >
                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.PushNotification } >
                      <Text style={{ width: "100%", textAlign:"center", padding: 10, }}> Not Now </Text> 
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex:0.5, marginLeft: 8 , marginRight: 0, marginTop: 0, marginBottom: 0 }}>
                    <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={this.PushNotification} >
                      <Text style={{ width: "100%", textAlign:"center", padding: 10 }}> Enable </Text> 
                    </TouchableOpacity>
                  </View>

                </View>

              </View>
            </Modal>
            </View>
            </View>
        </ScrollView>
            </Container>
        )
    }
    

}
PushNotification.navigation = {
  header:null
}
const styles = StyleSheet.create({
  mainContainer:{
    backgroundColor:'#2eaaeb',
    flex:1, 
    justifyContent: 'center',
    paddingRight:30,
    paddingLeft:30
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#2eaaeb',
  },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 250,
        backgroundColor: "#ffffff",
        position: "absolute",
        top:100,
        right:0,
      },
      modalHeading: {
        fontSize: 26,
        textAlign: "center",    
      },
      modalMessage: {
        textAlign: "center",
        fontSize: 18,
        justifyContent: "center",
        color: "gray",
      },
      closeModalBtnView: {
        position: "absolute",
        top: 5,
        right: 0,
        width: 50,
        paddingRight: 5,
        backgroundColor: "#ffffff",
      },
      closeModalBtn: {
        backgroundColor: "#ffffff",
      },
      closeModalBtnImg: {
        width: 15,
        marginRight: 5,
        marginLeft: 5,
      }
})
export default PushNotification;