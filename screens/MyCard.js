import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, StyleSheet, Platform,  AsyncStorage  } from 'react-native';
import Barcode from 'react-native-barcode-builder';
import {
  Container, Content,Text
} from 'native-base';
import Spacer from '../components/UI/Spacer';
import { LinearGradient } from 'expo-linear-gradient';

import { QRCode } from 'react-native-custom-qr-codes';

class Mycard extends React.Component{
  constructor(props){
    super(props);
    this.state={
        CardName: "MFGCard",
        Points: 200,
        BarCode: '3638596',
        borcodes:''
    }
  }

   componentWillMount() {
    AsyncStorage.getItem('member_id').then((member) => {
      if(member!==null){
       this.setState({
        BarCode: member
       });
    }
    });
  }

  render(){
    console.log(this.state.borcodes);
    return(
      <Container>
      <View style ={{flex:1, textAlign:'center'}}>
      <Content>
          <LinearGradient
          colors={['#0099EF', '#00D2EF']}
          style={[styles.rootView, { flex: 1, 
            borderBottomLeftRadius: 15, 
            borderBottomRightRadius: 15,
            }]}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
    >
            <View style = {{margin:15}}>
            <Text style ={styles.profileName}>{this.state.CardName}</Text>
              <View style ={{flexDirection:'row', flexWrap:'wrap', flex:1, justifyContent:'center', marginTop:20}}>
                  <View style={{alignContent:'center', textAlign:'center',flex:0.5, justifyContent:'center'}}>
                     <Text style = {styles.balanceText}>Total Points</Text>
                     <Text style = {styles.balanceBoldText}>{this.state.Points}</Text>
                  </View>
                <View style={{flex:0.5, alignSelf:'center', justifyContent:'center', textAlign:'center'}}>
                  <QRCode style = {{alignSelf:'center', textAlign:'center'}} size={100} value={this.state.BarCode} linearGradient={['rgb(255,0,0)','rgb(0,255,255)']} />
                </View>
              </View>
          </View>
          </LinearGradient>
      </Content>
      </View>
    </Container>
  
    )
  }
}

Mycard.navigationOptions = {
  title: 'My Card',
  headerStyle: {
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 15, 
    borderBottomRightRadius: 15,
    height:60,
    borderBottomWidth: 0
  },
  headerBackground: (
    <LinearGradient
      colors={['#0099EF', '#00D2EF']}
      style={{ flex: 1, 
        borderBottomLeftRadius: 15, 
        borderBottomRightRadius: 15,
        ...Platform.select({
          ios:{
            borderRadius: 15,
            marginTop:-15
          }
        }),
      }}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  ),
  headerTintColor: '#FFFFFF',
  headerTitleStyle: {
    fontWeight: 'normal',
    alignSelf: 'center',
    textAlign:"center", 
    flex:0.8,
    fontSize:20,
    lineHeight:25,
    color: "#ffffff",
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }), 
  },
};
  

const styles = StyleSheet.create({
rootView:{
    backgroundColor:'#2eaaeb',
    minHeight:200,
    borderRadius:18,
    position:'relative',
    width:'90%',
    alignSelf:'center',
    marginTop:50,
    paddingBottom:15
},
profileName:{
    fontSize:24,
    lineHeight: 28,
    textAlign:'left',
    color:'#ffffff',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 2
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }), 
},  
cardColor:{
    color:'#ddd9d9',
    fontStyle: 'italic'
},
balanceText:{
    fontSize:17,
    lineHeight: 22,
    textAlign:'center',
    color:'#ffffff',
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular',
        letterSpacing: 1
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
  },
balanceBoldText:{
    fontSize:34,
    lineHeight: 41,
    fontWeight:'normal',
    textAlign:'center',
    textTransform: 'uppercase',
    color:'#ffffff',
    ...Platform.select({
      ios: {
        fontFamily:'SFProDisplayRegular',
        letterSpacing: 2
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }), 

  },
});

export default Mycard;
