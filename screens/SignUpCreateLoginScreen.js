import React from 'react';
import { Form, Button, Container, CheckBox} from 'native-base';
import { View, StyleSheet, ScrollView, Platform, Text, TouchableHighlight, Picker, AsyncStorage } from 'react-native';
// import { CheckBox } from 'react-native-elements'
import Spacer from '../components/UI/Spacer';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { TextField } from 'react-native-material-textfield';
import axios from 'axios';
const Loading=() =><View><Text>Loading...</Text></View>;
import HTML from 'react-native-render-html';
import SignaturePad from 'react-native-signature-pad';
import Modal from "react-native-modal";
import { StackActions, NavigationActions } from 'react-navigation';
import { LinearGradient } from 'expo-linear-gradient';

import IOSPicker from 'react-native-ios-picker';

let Email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ; 
let pwd= /^(?=[^\d_].*?\d)\w(\w|[!@#$%]){7,20}/;
let errormessage = ''
 
class SignUpCreateLogin extends React.Component {

  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.config={
      apiUrl:'https://konnect.myfirstgym.com/api/terms-and-conditions',
      signinApiURL:'https://konnect.myfirstgym.com/api/customer/auth/register'
    }
    this.state = {
      Apiresponse :[],
      location: this.params.Branch['data'][0].id,
      selectedLocationForIos: "",
      maxheightterms:100,
      usercred:{
        Email: '',
        EmailValidator:false,
        Password: '',
        PasswordValidator:false,
        confirmPassword: '',
        confirmPasswordValidator:false,
        Passwordmatched:false,
        Branch_Id:1,
        OtherInfo:'',
        OtherInfoValidator:false,
        DigitalSignature:'',
      },
      errorshow:false,
      error:false,
      isLoading:true,
      howlearn: '',
      checked:false,
      isButtonDisabled :true,

      userEmailError: "",
      userPasswordError: "",
      usercPasswordError: "",
      userOtherInfoError: "",
    }
    this.validates = this.validates.bind(this);
    this.formsubmit = this.formsubmit.bind(this);
  }

  //CALLING COMPONENT ON BLUR FIELD
  async onBlurValidates(type) {
    
    if (type === 'Email') {
      if (this.state.usercred.Email == '') {
        this.setState({ userEmailError: "Email can not be blank.", isButtonDisabled: true })
      }else if((/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.usercred.Email.trim(' '))) == false ){
        this.setState({ userEmailError: "E-mail should be in correct format.", isButtonDisabled: true })
      }else{
        this.setState({ userEmailError: "" })
      }
    } else if (type === 'Password') {
      if (this.state.usercred.Password == '') {
        this.setState({ userPasswordError: "Password can not be blank.", isButtonDisabled: true })
      }else if(this.state.usercred.Password.trim(' ').length < 8){
        this.setState({ userPasswordError:"Password must be at least 8 characters long.", isButtonDisabled: true})
      }else{
        this.setState({ userPasswordError: "" })
      }
    } else if (type === 'cPassword') {
      if (this.state.usercred.confirmPassword == '') {
        this.setState({ usercPasswordError: "Confirm Password can not be blank.", isButtonDisabled: true })
      }else if(this.state.usercred.confirmPassword.trim(' ').length < 8){
        this.setState({ usercPasswordError: "Password must be at least 8 characters long.", isButtonDisabled: true })
      }else if (this.state.usercred.Password != this.state.usercred.confirmPassword ) {
          this.setState({ usercPasswordError: "Password and Confirm Password not matched." })
      }else{
        this.setState({ usercPasswordError: "" })
      }
    } 
    // else if (type === 'OtherInfo') {
    //   if (this.state.usercred.OtherInfo == '') {
    //     this.setState({ userOtherInfoError: "Last name can not be blank.", isButtonDisabled: true })
    //   }else if(this.state.usercred.OtherInfo.trim(' ').length < 10){
    //     this.setState({ userOtherInfoError: "Enter minimum 10 characters about us.", isButtonDisabled: true })
    //   }else{
    //     this.setState({ userOtherInfoError: "" })
    //   }
    // }

  }
    
  validates = (text, type) => { 
    if(type==='Email'){
      if(Email.test(text.trim(' ')))
      { 
           this.state.usercred.Email=text;
           this.state.usercred.EmailValidator=true;
          this.setState({error:false});
      } 
      else { 
           this.state.usercred.Email=text;
           this.state.usercred.EmailValidator=false; 
          //this.setState({ error:true }) 
          //errormessage='E-mail should be in correct format';
      } 
    }
    else if(type==='Password'){
      if(pwd.test(text.trim(' ')))
      { 
           this.state.usercred.Password=text;
           this.state.usercred.PasswordValidator=true; 
          this.setState({error:false});
      } 
      else { 
        this.state.usercred.Password=text;
           this.state.usercred.PasswordValidator=false; 
          //this.setState({ error:true }) 
          //errormessage='Password should be in correct format';
      } 
    }
    else if(type==='cPassword'){
      if(pwd.test(text.trim(' ')))
      { 
           this.state.usercred.confirmPassword=text;
           this.state.usercred.confirmPasswordValidator=true; 
           if(this.state.usercred.Password===this.state.usercred.confirmPassword)
           {
              this.state.usercred.Passwordmatched=true; 
               this.setState({error:false});
           }
           else{
            this.state.usercred.Passwordmatched=false; 
           // this.setState({ error:true }) 
            //errormessage=' Confirm Password should be matched with Password';
           }
            
      } 
      else {  
           this.state.usercred.confirmPassword=text;
           this.state.usercred.confirmPasswordValidator=false; 
          //this.setState({ error:true }) 
          //errormessage=' Confirm Password should be in correct format';
      } 
    }
    else if(type==='branch'){ 
      
      if(text.trim(' ').length>0){
        this.state.usercred.Branch_Id=text;
        this.setState({error:false});
      }
      else{
          this.setState({ error:true }) 
          errormessage=' Please select Brach';
      }
    }
    // else if(type==='OtherInfo'){  
    //   if(text.trim(' ').length>0){
    //     this.state.usercred.OtherInfo=text;
    //     this.state.usercred.OtherInfoValidator=true; 
    //       this.setState({error:false});
    //   }
    //   else{
    //     this.state.usercred.OtherInfo=text;
    //     this.state.usercred.OtherInfoValidator=false; 
    //    this.setState({ error:true }) 
    //    errormessage='Other info should be mandatory';
    //   }
    // }


    if(this.state.usercred.Email !=='' &&
      this.state.usercred.EmailValidator &&
     this.state.usercred.Password !=='' &&
     this.state.usercred.PasswordValidator &&
      this.state.usercred.confirmPassword !=='' &&
      this.state.usercred.confirmPasswordValidator !=='' &&
      this.state.usercred.Passwordmatched
      //  this.state.usercred.OtherInfo !=='' &&
      //  this.state.usercred.OtherInfoValidator 

    ){
     
      this.setState({isButtonDisabled:false}); 
    }
    else{
      this.setState({isButtonDisabled:true});
    }
  }

  formsubmit = async () =>{
    this.setState({isButtonDisabled:true});
    await this.fetchsign();
  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue); 
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  renderViewMore=()=>{
    if(this.state.maxheightterms===100){
      this.setState({maxheightterms:null});
    }else{
      this.setState({maxheightterms:100});
    }
    
  }

  fetchsign() 
  {
    let self = this;
    console.log(self.params);
    axios.post("https://konnect.myfirstgym.com/api/customer/register/email/validate",
      {"email":self.state.usercred.Email}
      , {headers : {
        'Accept':'application/json',
        'Content-Type':'application/json'
      }}).then(data2=>{
        //console.log(data2.data.success);
        if(data2.data.success)
        {
    let childernsResp = [];
    return new Promise(async (resolve)=>{     
      
      let data={};
      data.Branch_Id=self.state.location;
      data.FirstName=self.params.userinfo.FirstName;
      data.LastName=self.params.userinfo.LastName;
      data.Gender=self.params.userinfo.Gender;
      data.Relationship=self.params.userinfo.Relationship;
      data.Birthday=self.params.userinfo.Birthday;
      data.Address=self.params.userinfo.Address;
      data.City=self.params.userinfo.City;
      data.Country=self.params.userinfo.Country;
      data.Mobile=self.params.userinfo.Mobile;
      data.Email=self.state.usercred.Email;
      data.Password=self.state.usercred.Password;
      data.DigitalSignature=self.state.usercred.DigitalSignature;
      data.OtherInfo=self.state.usercred.OtherInfo;
      data.Avatar=self.state.usercred.Avatar;
        let ChildInfo=[];
        for(i=0;i<self.params.childData.length;i++)
        {
          ChildInfo.push(
              {
                  FirstName:self.params.childData[i].FirstName,
                  LastName:self.params.childData[i].LastName,
                  Gender:self.params.childData[i].Gender,
                  Birthday:self.params.childData[i].Birthday,
                  School:self.params.childData[i].School,
                  ChildAvatar:self.params.childData[i].ChildAvatar,
                  MedicalCondition:self.params.childData[i].MedicalCondition 
                });
        }
      data.ChildInfo=ChildInfo;
      axios.post("https://konnect.myfirstgym.com/api/customer/auth/register",
      data
      , {headers : {
        'Accept':'application/json',
        'Content-Type':'application/json'
      }}).then(data3=>{
       //  console.log('API Request Data:-', data3);
          resolve(data3);
          responseJson = data3['data']['data'];
          let childToBeSaved = responseJson.children;
          if(childToBeSaved == ""){
            childernsResp.push(0)
          }else{
            childernsResp.push( childToBeSaved )
          } 
          self.setState({Apiresponse: responseJson, isdisabled: false});
          self.saveItem('api_token', self.state.Apiresponse.api_token);
          self.saveItem('member_id', self.state.Apiresponse.member_id);
          self.saveItem('@UserChilderns:key', JSON.stringify(childernsResp) );
           const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
          });
          this.props.navigation.dispatch(resetAction);
          self.props.navigation.navigate('Thankssignup');
        }).catch(function(error){
          resolve({});
          alert(error.response.data);
        })   
      })
        }
        else
        {
          alert("This email already exist!")
        }
      });
    
}


  async fetchTermsandConditions() {
    let self = this;
        let response = axios.get(this.config.apiUrl)
        .then(function (response) {
          self.setState({Apiresponse: response['data'], isLoading:false});
        })
        .catch(function (error) {
          console.log(error);
        });
  }

  onSelectLocation = (data) =>{

    this.params.Branch['data'].map((item) =>{
      if(data == item.id){
        this.setState({ selectedLocationForIos: item.name});
      }
    })
    
    this.setState({location: data});

  };

  changeCheckboxValue=() => {
    this.setState({checked: !this.state.checked})
    if (this.state.checked === false) {
      this.setState({ isModalVisible: !this.state.isModalVisible })
    }
  }

  
  closeModalVisible = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }

  componentDidMount(){
    this.fetchTermsandConditions()
  }

  _signaturePadError = (error) => {
    console.error(error);
  }

  _signaturePadChange = ({base64DataUrl}) => {
    let baseSrc=base64DataUrl.replace(/^data:image\/(png|jpg);base64,/, '');

    //console.log("Got new signature: " + base64DataUrl);
    //console.log("Got new signature: " + baseSrc);
     this.state.usercred.DigitalSignature=baseSrc;
   //this.setState({DigitalSignature:baseSrc}); 
  }

  render(){

        return(
          <Container>
            <ScrollView keyboardDismissMode='on-drag' style={styles.container} >
          <View style ={styles.mainContainer}>
           <View style = {{flex:1}}>
              <Spacer size={6} />
              <Text style = {[styles.fontFamily, styles.title]}>CREATE YOUR LOGIN</Text>
              <View> 

                <Spacer size={3} />
                <View>
                {
                  (this.state.error)?
                  (<Text style = {[styles.fontFamily, {alignSelf:'center'}]}>{errormessage}</Text>):
                  (<View></View>)
                }
                
              </View>
                <Spacer size={1} />

                <Form style ={styles.fontFamily}>

                 <View style={{ width: "90%", marginLeft: "5%" }}>

                  <TextField
                    label='Email'
                    keyboardType="email-address"
                    onChangeText={(value) => this.validates(value, 'Email')}
                    style={ styles.labelName }
                    onBlur={() => this.onBlurValidates('Email')}
                    error={this.state.userEmailError}
                  />
                  
                  <TextField
                    label='Enter Password'
                     secureTextEntry
                    onChangeText={(value) => this.validates(value, 'Password')}
                    style={ styles.labelName }
                    onBlur={() => this.onBlurValidates('Password')}
                    error={this.state.userPasswordError}
                  />

                  <TextField
                    label='Confirm Password'
                     secureTextEntry
                    onChangeText={(value) => this.validates(value, 'cPassword')}
                    style={ styles.labelName }
                    onBlur={() => this.onBlurValidates('cPassword')}
                    error={this.state.usercPasswordError}
                  />
                </View>

                  <Spacer size={20} />
                  <Text style = {[styles.fontFamily, styles.title]}>OTHER INFO</Text>
                  <Spacer size={20} />

                  <View style={{ width: "90%", marginLeft: "5%", borderBottomWidth:1, borderBottomColor:"#999999" }}>

                  {
                    Platform.select({
                      ios:(
                        <IOSPicker
                          mode='modal'
                          selectedValue={ 
                            (this.state.location  == this.params.Branch['data'][0].id)?(
                              this.params.Branch['data'][0].name
                            ):(
                              this.state.selectedLocationForIos
                            )
                          }
                          style={[ styles.labelName, {width:"100%", color: "#999999", paddingLeft:10, borderTopColor: "#FFFFFF"}]}
                          onValueChange={(value) => this.onSelectLocation(value) }>
                          {
                            
                            this.params.Branch['data'].map((item) =>{
                              return(
                              <Picker.Item  label={item.name} value={item.id} key={item.name}/>
                              );
                            })
                          }
                        </IOSPicker>
                      ),
                      android: (
                        <Picker
                          selectedValue={ this.state.location }                      
                          style={[ styles.labelName, {width:"100%", color: "#999999", paddingLeft:10}]}
                          onValueChange={(value) => this.onSelectLocation(value) }>
                          {
                            
                            this.params.Branch['data'].map((item) =>{
                              return(
                              <Picker.Item  label={item.name} value={item.id} key={item.name}/>
                              );
                            })
                          }
                        </Picker>
                      )
                    })
                  }

                  </View>

                <View style={{ width: "90%", marginLeft: "5%" }}>

                  <TextField
                    label='How did you learn about us?'
                    keyboardType="default"
                    onChangeText={(value) => this.validates(value, 'OtherInfo')}
                    style={[ styles.labelName, styles.fontFamily]}
                    onBlur={() => this.onBlurValidates('OtherInfo')}
                    error={this.state.userOtherInfoError}
                  />

                </View>
                
                  <Spacer size={10} />
                  <Text style = {[styles.fontFamily, styles.title]}>LIABILITY RELEASE</Text>

                  <Spacer size={20} />
                  <View style={{marginRight:20, marginLeft:20, padding:15, borderWidth:1, borderColor:'#C4C4C4', borderRadius:4}}>
                    <Text style={[styles.fontFamily, {fontWeight:'bold', fontSize:14, lineHeight: 16}]}>Refund Policy</Text>
                     {
                      (this.state.isLoading)?
                      (
                        <View style={[styles.loading, {backgroundColor:'transparent'} ]}>
                          <Loading style ={{backgroundColor:'#ffffff'}}/>
                        </View>
                      )
                      :
                      ( 
                        
                        <View>
                          <ScrollView style = {[ { maxHeight: this.state.maxheightterms }]}>
                            <HTML html={this.state.Apiresponse['data']} /> 
                          </ScrollView>
                          <Text onPress={() => this.renderViewMore()} style={{fontSize:40, fontWeight:'900', color:'#000000', alignItems:'flex-end', alignSelf:'flex-end'}}>...</Text>
                        </View>
                      )
                    } 
                    
                  </View>
                  <Spacer size={15} />
                  <View style ={{flex:1, flexDirection:'row',  width: "90%", marginLeft: "5%" }}>
                    <View style = {{flex:0.85}}>
                        <Text>I agree to the My First Gym liability waiver</Text>
                    </View>
                    <View style = {{flex:0.15, alignSelf:'flex-end'}}>
                      <CheckBox
                      containerStyle={[{ backgroundColor: "#ffffff", borderColor: "#ffffff", borderWidth:1,marginTop:-12}]}
                      center
                      iconRight
                      checkedColor='#147BDF'
                      checked={this.state.checked}
                      onPress={this.changeCheckboxValue}
                      checkedIcon='check-square'
                      uncheckedIcon='square'
                    />
                    </View>
                  </View>

                  <Spacer size={15} />

                  <Button block style={[styles.submition, this.state.isButtonDisabled ? styles.buttonDisabled : null]}  onPress={this.formsubmit} disabled = {this.state.isButtonDisabled} >
                    <Text style = {[styles.fontFamily, { fontSize: 15, lineHeight: 24, color:'#ffffff'}]}>SUBMIT</Text>
                  </Button>

                  <Spacer size={25} />
                </Form>
              </View>
            </View>
          </View>
          

      <Modal isVisible={this.state.isModalVisible} style={[ styles.modal ]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

        <View style={{ flex: 1,flexDirection: 'column', alignItems: 'stretch', }}>
          
          <View style={{ height:50, borderBottomWidth: 1, borderBottomColor: "#999999", textAlign: "center" }}>
              <Text style={[styles.fontFamily, { fontSize: 22, lineHeight: 28, paddingTop:14, paddingBottom:14, alignSelf: "center", color: "rgba(0, 0, 0, 0.87)" }]}>Digital Signature</Text>
          </View>

          <View style={{ height: 180 }}>
              <SignaturePad onError={this._signaturePadError} onChange={this._signaturePadChange} style={{ backgroundColor: '#efefeffa', width:"100%" }}/>
          </View>


          <View style={{ flex: 1, flexDirection: 'row', height: 50 }}>
              
              <View style={{ flex: 0.5 }}>
                  <TouchableHighlight style={{ borderBottomLeftRadius: 10, borderTopWidth: 1, borderTopColor: "rgba(0, 0, 0, 0.12)", paddingTop: 11, paddingBottom: 11 }} onPress={this.closeModalVisible} underlayColor="transparent"  >
                  <Text style={[styles.fontFamily, {fontSize: 22, lineHeight: 28,alignSelf:"center", color: "rgba(0, 0, 0, 0.54)" }]}> Clear </Text> 
                  </TouchableHighlight>    
              </View>
              
              <View style={{ flex: 0.5 }} >
                  <TouchableHighlight style={[styles.submitButton]} onPress={this.closeModalVisible} underlayColor="transparent" >
                      <Text style={[styles.fontFamily, { fontSize: 22, lineHeight: 28, alignSelf:"center", color: "#fff" }]}> Submit </Text>  
                  </TouchableHighlight>    
              </View>

          </View>

        </View>
      </Modal>
      </ScrollView>
      <KeyboardSpacer topSpacing ={15} />
      </Container>
        )
    }
  };

  SignUpCreateLogin.navigationOptions = {
    title: 'Sign Up',
    headerStyle: {
      backgroundColor: '#0099EF',
      borderBottomLeftRadius: 15, 
      borderBottomRightRadius: 15,
      borderBottomWidth: 0
    },
    headerBackground: (
      <LinearGradient
        colors={['#0099EF', '#00D2EF']}
        style={{ flex: 1, 
          borderBottomLeftRadius: 15, 
          borderBottomRightRadius: 15,
          ...Platform.select({
            ios:{
              borderRadius: 15,
              marginTop:-15
            }
          }),
        }}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
      />
    ),
    headerTintColor: '#FFFFFF',
    headerTitleStyle: {
      fontWeight: 'normal',
      alignSelf: 'center',
      textAlign:"center", 
      flex:1 ,
      fontSize:20,
      lineHeight:25,
      paddingRight: 42,
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
  };

  const styles = StyleSheet.create({
    fontFamily:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    fontFamilyDisplay:{
      ...Platform.select({
        ios: {
          fontFamily:'SFProDisplayRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
    },
    loading: {
      opacity: 0.5,
      backgroundColor: 'black',
      justifyContent: 'center',
      alignItems: 'center'
  },
   
    spinnerTextStyle: {
      color: '#FFF',
    },
    submitButton:{
      backgroundColor: "#0099EF", 
      paddingTop: 11, 
      paddingBottom: 11, 
      borderBottomRightRadius: 10,
    },
    submition:{
      width:'80%', 
      alignSelf:'center',
      backgroundColor:'#147BDF', 
      borderRadius:25,
      borderRadius: 161,
      elevation: 3
    },
    buttonDisabled:{
      backgroundColor:'#cccccc',
      color: "#FFFFFF"
     },
    mainContainer:{
      backgroundColor:'#ffffff',
      flex:1, 
      justifyContent: 'center',
      paddingBottom: 20
    },
    container: {
      flex: 1,
      paddingTop: 15,
      paddingBottom: 50,
      backgroundColor: '#ffffff',
    },
      title:{
        backgroundColor:'#EEEEEE',
        color:'rgba(0, 0, 0, 0.87)',
        fontSize:12,
        lineHeight:16,
        fontWeight:'normal',
        width:'100%',
        textAlign:'center',
        paddingTop:5,
        paddingBottom:5,
        alignSelf:'center',
        flex:1,
        ...Platform.select({
          ios: {
            fontFamily:'SFProTextRegular',
          },
          android: {
            fontFamily:'RobotoRegular_1',
          },
        }),
      },
      modal: {
        height: 280,
        width: "90%",
        marginLeft: "5%",
        backgroundColor: "#ffffff",
        position: "absolute",
        top:100,
        right:0,
        borderRadius: 10
      },    
    buttonStyle:{
        backgroundColor:'transparent',
        borderRadius:4,
        borderWidth:1,
        flex:0.8,
        width:'80%',
        marginLeft:'10%',
        borderColor:'#000000',
        justifyContent: 'center',
        alignItems: 'center',
      },
      loginButtonText:{
        color:'#000000',
        fontSize:12
      },
    input:{
      backgroundColor:'#ffffff', 
      width:'100%',
      paddingLeft:0,
      borderBottomWidth:1,
      borderColor:'rgba(0, 0, 0, 0.12)',
    },
    tabBarInfoContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      ...Platform.select({
        ios: {
          shadowColor: 'black',
          shadowOffset: { width: 0, height: -3 },
          shadowOpacity: 0.1,
          shadowRadius: 3,
        },
        android: {
          elevation: 20,
        },
      }),
      alignItems: 'center',
      backgroundColor: '#fbfbfb',
      paddingVertical: 20,
    },
    tabBarInfoText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      textAlign: 'center',
    },
    navigationFilename: {
      marginTop: 5,
    }
  });
export default SignUpCreateLogin;
