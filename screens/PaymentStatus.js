import React from 'react';
import {AsyncStorage, AppRegistry, ListView, TouchableWithoutFeedback,TouchableOpacity, View, StyleSheet , Image, ScrollView, Platform, TouchableHighlight  } from 'react-native';
import { WebView } from 'react-native-webview';

// ...
class PaymentStatus extends React.Component {
    constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    //  datasource rerendered when change is made (used to set swipeout to active)
    console.log(this.params.payment_url);
    paymentStatus=()=>{
        self.props.navigation.navigate('PaymentStatus', { replace: true });
    }  
    }
  render() {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={{ paddingLeft : 15 }} onPress={ this.paymentStatus } >
                        <Text style={[styles.selectedDaysBtns,{textAlign:"left",color:"rgba(0, 0, 0, 0.87)"}]}>
                          Once payment done. You need to click Here
                        </Text>
                      </TouchableOpacity>
            <WebView source={{ uri: this.params.payment_url }} style={styles.webView}/>
        </View>
    );
  }
}
var styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor:  '#ff00ff'
   },
   webView :{
     height: 320,
     width : 200
   }
});
export default PaymentStatus;