import React from 'react';
import { Scene, Tabs, Stack } from 'react-native-router-flux';
/*
import DefaultProps from '../constants/navigation';
import AppConfig from '../../constants/config';

import RecipesContainer from '../../containers/Recipes';
import RecipeListingComponent from '../components/Recipe/Listing';
import RecipeSingleComponent from '../components/Recipe/Single';

import SignUpContainer from '../../containers/SignUp';
import SignUpComponent from '../components/User/SignUp';
import SignUpChildComponent from '../components/User/SignUpChild';
import SignUpCreateLoginComponrnt from '../components/User/SignUpCreateLogin';



import LoginContainer from '../../containers/Login';
import LoginComponent from '../components/User/Login';

import ForgotPasswordContainer from '../../containers/ForgotPassword';
import ForgotPasswordComponent from '../components/User/ForgotPassword';

import UpdateProfileContainer from '../../containers/UpdateProfile';
import UpdateProfileComponent from '../components/User/UpdateProfile';

import MemberContainer from '../../containers/Member';
import ProfileComponent from '../components/User/Profile';*/

import HomeScreen from '../../screens/HomeScreen';
import ExploreScreen from '../../screens/ExploreScreen';
import FavouriteScreen from '../../screens/FavouriteScreen';
import ProfileScreen from '../../screens/ProfileScreen';
import ScheduleScreen from '../../screens/ScheduleScreen';

/*import MainComponent from '../components/User/Main';

import ExploreComponent from '../components/User/Explore';
import ScheduleComponent from '../components/User/Schedule';
import FavouriteComponent from '../components/User/Favourite';
import EventDescriptionComponent from '../components/User/EventsDescription';
import BookingMultipleComponent from '../components/User/BookMultiple';


import Mycard from '../components/User/MyCard';
import CreditcardComponent from '../components/User/CreditCard';


import UpdateCreditCardComponent from '../components/User/UpdateCreditCard';
import NewCreditCardAddComponent from '../components/User/NewCreditCardAdd';
import MyPoints from '../components/User/MyPoints';

import AccountComponent from '../components/User/Accounts';
import SettingComponent from '../components/User/Setting';

import BuyServicesComponent from '../components/User/BuyServices';
import CheckoutComponent from '../components/User/Checkout';

import PaymentMethodComponent from '../components/User/Payment';
import PaymentCardComponent from '../components/User/PaymentCard'*/

const Index = (
  <Stack >
    <Scene >
    <Scene key="home" component={ HomeScreen } />
      <Tabs
        key="tabbar"
        swipeEnabled
        type="replace"
        style = {{textAlign:'center',borderBottomLeftRadius:4,borderBottomRightRadius:4}}
        showLabel={false}

      >
          <Scene
              key="explore"
              component={ExploreScreen}   
            />
            <Scene
              key="favourite"
              component={FavouriteScreen}   
            />
            
            <Scene
              key="schedule"
              component={ScheduleScreen}   
            />
            <Scene
              key="profile"
              component={ProfileScreen}   
            />
      </Tabs>
    </Scene>
  </Stack>
);

export default Index;
