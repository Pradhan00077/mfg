import React from 'react';
import { View, Text, ImageBackground, StyleSheet, Platform } from 'react-native'; 
import { sliderItemWidth, sliderItemHorizontalMargin, slideWidth } from './Styles_slider';



const Setting = ({ title , path, className}) => (

  <View
    style={{

      flex:1,
      flexDirection: "row",      
      paddingHorizontal: sliderItemHorizontalMargin,
      alignItems: 'center',
      justifyContent: 'center',
      
    }}
  >
    <View
      style={{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:4,
        
      }}
    >
      <ImageBackground
        source={{uri:path}}
        style={{
          height: "100%",
          width: "100%",
          position: 'relative', // because it's parent
          borderRadius:4,
          backgroundColor: "#E5E5E5",
        }}
        resizeMode="cover"
        imageStyle={{ borderRadius: 4, borderWidth:0 }}
      >

        <View style={styles.classes}><Text style={styles.classsesColor}>{title}</Text></View>

      </ImageBackground>
    </View>
  </View>
);
const styles = StyleSheet.create({

  location:{ color: 'white', position:'absolute', bottom:10, left:10, fontSize:16, lineHeight:21, width:'80%',
  borderBottomLeftRadius:8,
  borderBottomRightRadius:8,
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1',
    },
  }),
},
  classes:{  borderBottomLeftRadius:4,  borderBottomRightRadius:4,  textTransform: 'uppercase', color: 'white', position:'absolute', bottom:0, left:0, backgroundColor:'rgba(0, 0, 0, 0.3)', width:'100%', justifyContent:'center', textAlign:'center', },
  classsesColor:{
  textTransform: 'uppercase',
  fontSize:11,
  lineHeight:12,
  color:'#ffffff',
  textAlign:'center',
  paddingTop: 13,
  paddingBottom: 12,
  paddingLeft: 5,
  paddingRight: 5,
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular'
    },
    android: {
      fontFamily:'RobotoRegular_1',
    },
  }),
}
});
export default Setting;
