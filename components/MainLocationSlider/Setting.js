import React from 'react';
import { View, Text, ImageBackground, StyleSheet, Platform } from 'react-native'; 
import { sliderItemWidth, sliderItemHorizontalMargin, slideWidth } from './Styles_slider';



const Setting = ({ title , path, className}) => (

  <View
    style={{
      width: sliderItemWidth,
      height: 160,
      paddingHorizontal: sliderItemHorizontalMargin,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#FFFFFF',
    }}
    onPress = {() => this.props.navigation.navigate('Location')}
  >
    <View
      style={{
        width: slideWidth,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'purple',
        borderRadius:4,
        borderWidth:1,
        borderColor:'#FFFFFF',
        marginRight:0
      }}
    >
      
      <ImageBackground
                source={{ uri:path}}
                style={[{
                  height: 160,
                  width: '100%',
                  position: 'relative', // because it's parent
                  textAlign:'center',
                  alignContent:'center',
                  alignItems:'center',
                  backgroundColor: (path==='')?('#eeeeee'):('#FFFFFF'),
                }]}
                imageStyle={{ borderRadius: 4 }}
              >
              <Text style={styles.location}>{title}</Text>
      </ImageBackground>
    </View>
  </View>
);
const styles = StyleSheet.create({
  location:{ color: 'white', 
  position:'absolute', 
  bottom:15, 
  left:15, 
  fontSize:20, 
  lineHeight:25, 
  ...Platform.select({
    ios: {
      fontFamily:'SFProTextRegular',
      letterSpacing:0.38
    },
    android: {
      fontFamily:'RobotoRegular_1'
    },
  }),
},
});
export default Setting;
