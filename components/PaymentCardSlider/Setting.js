import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native'; 
import { sliderItemWidth, sliderItemHorizontalMargin, slideWidth } from './Styles_slider';
import Spacer from '../UI/Spacer';



const Setting = ({ title , path, className}) => (

  <View
    style={{
      width: sliderItemWidth,
      height: '100%',
      paddingHorizontal: sliderItemHorizontalMargin,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ffffff',
      borderRadius:0
    }}
  >
    <View
      style={{
        width: slideWidth,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        borderRadius:8,
        borderWidth:1,
        borderColor:'#ffffff',
        marginRight:0
      }}
    >
      <ImageBackground
                source={path}
                style={{
                  flex:1,
                  backgroundColor:'transparent',
                  width: '100%', 
                  height: '100%',
                  position: 'relative', // because it's parent
                  textAlign:'center',
                  alignContent:'center',
                  alignItems:'center',
                  backgroundPosition:'100%',
                  resizeMode: 'contain'
                }}
                imageStyle={{ borderRadius: 10 }}
              >
                <Spacer size ={110} />
              <View style ={{marginLeft:-30, flexDirection:'row',flexWrap:'wrap' , flex:1,}}>
                    <View style ={{flexDirection:'row' , flex:1, position:'absolute'}}>
                        <Text style ={[styles.ExpdateText]} >Exp 06/23</Text>
                    </View>
                    <Text style ={[styles.cardNumberText]} >.... .... ....</Text>
                    <Text style ={[styles.balanceText]} >0212</Text>
                  </View>
      </ImageBackground>
    </View>
  </View>
);
const styles = StyleSheet.create({
  ExpdateText:{
    fontSize:18,
    textAlign:'right',
    color:'#ffffff',
  },
  balanceText:{
    fontSize:18,
    textAlign:'right',
    color:'#ffffff',
    paddingTop:24
  },
  cardNumberText:{
    fontSize:37,
    textAlign:'right',
    color:'#ffffff',
    marginRight:10,
    marginBottom:0
  },
});
export default Setting;
