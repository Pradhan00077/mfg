import React from 'react';
import { View, Text, Image, StyleSheet, Platform } from 'react-native'; 
import { classsliderItemWidth, classsliderItemHorizontalMargin, classslideWidth} from './Styles_slider1';

const Setting1 = ({ title , path, className}) => (
  <View
    style={{
      width: classsliderItemWidth,
      paddingHorizontal: classsliderItemHorizontalMargin,
      // alignItems: 'center',
      // justifyContent: 'center',
    }}
  >
    <View
      style={{
        width: classslideWidth,
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        borderRadius:4
      }}
    >
             <Image
               source={{uri:path}}
                style={{
                  width: '100%',
                  position: 'relative', // because it's parent
                  alignContent:'center',
                  alignItems:'center',
                  borderRadius:4,
                  minHeight:70
                }}
              >
          </Image>
          {(title) ?
            (
                <View>
                    <Text style={styles.className}>{title}</Text>
                    <Text  style={styles.classTextName}>{className}</Text>
                </View>
            ):
             ( <View><Text></Text></View>)
          }
    </View>
  </View>
);
const styles = StyleSheet.create({
  className:{
      fontSize:15,
      lineHeight:20,
      textAlign: "left",
      color:'rgba(0, 0, 0, 0.87)',
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
  },  
  classTextName:{
    fontSize:13,
    lineHeight:16,
    color:'#727272',
    textAlign: "left",
    ...Platform.select({
      ios: {
        fontFamily:'SFProTextRegular'
      },
      android: {
        fontFamily:'RobotoRegular_1'
      },
    }),
},  
});
export default Setting1;

