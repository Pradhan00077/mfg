import React from 'react';
import { View, Text, Image, StyleSheet, Platform,} from 'react-native'; 
import { sliderItemWidth, sliderItemHorizontalMargin, slideWidth} from './Styles_slider';
import Lightbox from 'react-native-lightbox';

class Setting extends React.Component{
  constructor(props){
      super(props);
      this.state = {
        display: false
      }
  }
  render(){
    return(
      <View
    style={{
      width: sliderItemWidth,
      paddingHorizontal: sliderItemHorizontalMargin,
    }}
  >
    <View
      style={{
        width: slideWidth,
        flex: 1,
        borderRadius:4
      }}
    >
        <Lightbox>
             <Image
               source={{uri: this.props.path}}
                style={{
                  resizeMode : "contain",
                  width: '100%',
                  height:'100%',
                  alignContent:'center',
                  alignItems:'center',
                  borderRadius:4,
                  minHeight:slideWidth-(sliderItemHorizontalMargin*4),
                }}
              >
          </Image>
          </Lightbox>
    </View>
  </View>
    )
  }
}
const styles = StyleSheet.create({
  classTextName:{
      fontSize:15,
      lineHeight:20,
      color:'rgba(0, 0, 0, 0.87)',
      textAlign:'left',
      alignSelf:'flex-start',
      justifyContent:'flex-start',
      ...Platform.select({
        ios: {
          fontFamily:'SFProTextRegular'
        },
        android: {
          fontFamily:'RobotoRegular_1'
        },
      }),
  },  
});
export default Setting;