import React from 'react';
import { Ionicons } from '@expo/vector-icons';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  return (
    <Ionicons
      name={props.name}
      size={26}
      style={props.focused?{
        marginBottom: 3,
        paddingTop:5,
        width:'100%',
        borderTopWidth:2,
        borderTopColor:'#014863',
        textAlign:'center',
        backgroundColor:'#ffffff'
      }:
      {
        width:'100%',
        textAlign:'center',
        backgroundColor:'#FFFFFF'
      }
    }
      color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
    />
  );
}
