import React, { Component } from "react";

import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Body,
  List,
  ListItem,
  Left,
  Button,
  Right,
  Icon,
  Font
} from "native-base";

import styles from "./styles";

class ServiceObj extends Component {

  constructor(props) {
    super(props);

    this.config = {
      apiUrl: "https://konnect.myfirstgym.com/api/customer/auth/login"
    }
    this.state = {
      serviceObjs: [],
      rowHasChanged: false,
      fontLoaded: false
    };
  }

  fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
      return '%' + c.charCodeAt(0).toString(16);
    });
  }

  toFormData(serviceObj) {
    let array = [];
    for (let prop in serviceObj) {
      array.push(`${prop}=${this.fixedEncodeURIComponent(serviceObj[prop])}`);
    }
    return array.join('&');
  }

  async fetchServiceObjs() {
    console.log(this.config.apiUrl);
    try {

      let response = await fetch(this.config.apiUrl);
      let responseJson = [];
      try {
        responseJson = await response.json();
      } catch (error) {
        console.log(error);
        responseJson = [];
      }
      this.setState({ serviceObjs: responseJson });
    } catch (error) {
      console.log(error);
    }
  }

  async putServiceObj(serviceObj) {
    let formData = this.toFormData(serviceObj);
      let response = await fetch(this.config.apiUrl, {
        method: 'PUT',
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: formData
      });
     let responseJson = [];
      try {
        responseJson = await response.json();
        console.log(responseJson);
        this.setState({ serviceObjs: responseJson });
      } catch (error) {
        console.log(error);
        responseJson = [];
      }      
    
  }

  async postServiceObj(serviceObj) {
    try {
      await fetch(this.config.apiUrl, {
        method: 'POST',
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: this.toFormData(serviceObj)
      });
      this.fetchServiceObjs();
    } catch (error) {
      console.log(error);
    }
  }

  async deleteServiceObj(serviceObj) {
    try {
      await fetch(this.config.apiUrl, {
        method: 'DELETE',
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: this.toFormData(serviceObj)
      });
      this.fetchServiceObjs();
    } catch (error) {
      console.log(error);
    }
  }

  componentDidMount() {
    this.fetchServiceObjs();
  }

  updateServiceObj = (serviceObj) => {
    // new or update
    if (!serviceObj._id) {
      this.postServiceObj(serviceObj);
    } else {
      this.putServiceObj(serviceObj);
    }
  }

  handleDeleteServiceObj = (serviceObj) => {
    this.deleteServiceObj(serviceObj);
  }

  render() {

    return (
      <Container style={styles.container}>
        <Header>
          <Body>
            <Title>ServiceObjs</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.fetchServiceObjs()}>
              <Text>Reload</Text>
            </Button>
            <Button transparent onPress={() => this.props.navigation.navigate('ServiceObjDetail', {
                serviceObj: {
                  key: null,
                  Name: '',
                  Phone: ''
                },
                updateServiceObj: this.updateServiceObj
              })}>
              <Text>New</Text>
            </Button>
          </Right>

        </Header>

        <Content>
          <List
            dataArray={this.state.serviceObjs}
            rowHasChanged={() => true}
            renderRow={serviceObj =>
              <ListItem button onPress={() => this.props.navigation.navigate('ServiceObjDetail', {
                serviceObj: serviceObj,
                updateServiceObj: this.updateServiceObj,
                deleteServiceObj: this.handleDeleteServiceObj
              })}>
                <Text>{serviceObj.Name}</Text>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}


export default ServiceObj;