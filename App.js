import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import AppNavigator from './navigation/AppNavigator';

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  console.disableYellowBox = true; 
  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <AppNavigator />
      </View>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([

    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app


      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      
      /* Fonts for Android*/ 
      RobotoRegular_1: require('./assets/fonts/Android/Roboto-Regular_1.ttf'),
      RobotoMedium_1: require('./assets/fonts/Android/Roboto-Medium_1.ttf'),
      RobotoBlackItalic_1: require('./assets/fonts/Android/Roboto-BlackItalic_1.ttf'),




      /* Fonts for IOS*/
      SFProDisplayRegular: require('./assets/fonts/iOS/SF-Pro-Display-Regular.otf'),
      SFProTextRegular: require('./assets/fonts/iOS/SF-Pro-Text-Regular.otf'),



      SFProDisplay_Light: require('./assets/fonts/SFProDisplay_Light.ttf'),
      SFProDisplay_Medium: require('./assets/fonts/SFProDisplay_Medium.ttf'),
      SFProDisplay_Regular: require('./assets/fonts/SFProDisplay_Regular.ttf'),
      SFPro_Text_Semibold: require('./assets/fonts/SFPro_Text_Semibold.ttf'),
      SFPro_Text_Medium: require('./assets/fonts/SFPro_Text_Medium.ttf'),
      SFPro_Text_Regular: require('./assets/fonts/SFProText_Regular.ttf'),
      SFPro_Text_Light: require('./assets/fonts/SFProText_Light.ttf'),
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ]); 
}

function handleLoadingError(error: Error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
