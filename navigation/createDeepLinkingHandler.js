import { createDeepLinkingHandler } from 'react-native-deep-link';

/**
 * This function receives a result of url parsing,
 * you can find the structure of this object in the API docs below, and returns a function.
 * The returned function receives component props.
 */
const handleInvitationToChannel = ({ params: { channelId } }) => ({ dispatch }) => {
    // addCurrentUserToChannel is a redux-thunk action,
    // which was defined somewhere in the code.
    dispatch(addCurrentUserToChannel(channelId));
}

const schemes = [
    {
        name: 'example:',
        routes: [
            {
                expression: '/channels/:channelId',
                callback: handleInvitationToChannel
            }
        ]
    }
];

const withDeepLinking = createDeepLinkingHandler(schemes);