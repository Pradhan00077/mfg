import React from 'react';
import { Platform, Image, ImageBackground , View, AsyncStorage, Text } from 'react-native';
import Colors from '../constants/Colors';
import {Entypo, AntDesign } from '@expo/vector-icons';

import { createStackNavigator,  createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';

//import PushNotificationScreen from '../screens/PushNotification';
import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import MainScreen from '../screens/MainScreen';
import SignUpScreen from '../screens/SignUpScreen';
import SignUpChildScreen from '../screens/SignUpChildScreen';
import SignUpCreateLoginScreen from '../screens/SignUpCreateLoginScreen';
import ForgotPasswordScreen from '../screens/ForgotPassword';
import ThanksScreen from '../screens/ThankYouSignup';

import MainSliderSetting from '../components/MainLocationSlider/Setting';

import FavouriteScreen from '../screens/FavouriteScreen';
import LoadingScreen from '../screens/Loading';


import GuestSchedulesScreen from '../screens/GuestSchedulesScreen';

import ServicesItemsScreen from '../screens/ServicesItemsScreen';
import FavServicesItemsScreen from '../screens/FavServicesItemsScreen';

import CalenderScreen from '../screens/CaenderScreen';

import ExploreScreen from '../screens/ExploreScreen';
import ExploreViewAllScreen from '../screens/ExploreViewAllScreen';
import ExploreServicesScreen from '../screens/ExploreServicesScreen';

import EventsDescriptionScreen from '../screens/EventsDescription';
import BookMultipleScreen from '../screens/BookMultiple';


import LocationScreen from '../screens/Location';
import LocationFilterScreen from '../screens/LocationFilterScreen';
import FilterSchedule from '../screens/filterSchedule';

import ProfileScreen from '../screens/ProfileScreen';
import SettingScreen from '../screens/Setting';
import BuyServicesScreen from '../screens/BuyServices';
import CheckoutScreen from '../screens/Checkout';
import PaymentScreen from '../screens/Payment';
import PaymentCardScreen from '../screens/PaymentCard';
import CreditCardScreen from '../screens/CreditCard';
import UpdateCreditCardScreen from '../screens/UpdateCreditCard';
import NewCreditCardScreen from '../screens/NewCreditCardAdd';
import MypointsScreen from '../screens/MyPoints';
import AccountsScreen from '../screens/Accounts';
import MyCardScreen from '../screens/MyCard';
import ThankYouScreen from '../screens/ThankYou';
import AccountsChildScreen from '../screens/AccountsChild';
import ContactScreen from '../screens/Contact';
import ScheduleScreen from '../screens/ScheduleScreen';
import MainProfileScreen from '../screens/MainProfileScreen';
import payByexistingcardScreen from '../screens/paybyExistingCard';
import WebViewIframe from '../screens/WebViewIframe';
import PaymentStatus from '../screens/PaymentStatus';
const HomeStack = createStackNavigator({

  //LocationFilter: LocationFilterScreen,

  Home: HomeScreen,
  Main:MainScreen,
  Login: LoginScreen,
  Signup:SignUpScreen,
  Signupchild:SignUpChildScreen,
  Signupcrediantial:SignUpCreateLoginScreen,
  ForgotPassword:ForgotPasswordScreen,
  slidersetting:MainSliderSetting,
  Thankssignup:ThanksScreen

});

let numberofKey = 1;

HomeStack.navigationOptions = ({ navigation }) => {

  let focusedVar = '';
  let tabBarVisible;
  let routName = '';
  
  //USING FOLLOWING CONDITION TO BACK CALL MAIN SCREEN (IN FIRST TIME LOGIN)
  if ( navigation.state.hasOwnProperty('params') ){
    routName = navigation.state.routes;

    AsyncStorage.getItem('api_token').then((token) => {

      let keys = Object.keys(routName);
      let last = keys.slice(-1)[0];
      let lastRouteName = routName[last].routeName;
      if(token!== null){
        if( lastRouteName === "Home" ){
          navigation.replace('Main', {login:true})
        }
      }
    })

  }

  navigation.state.routes.map(route => {

    if (route.routeName === "Push" || route.routeName === "Home" ||route.routeName === "ForgotPassword" || route.routeName === "Login" || route.routeName === "Signup" || route.routeName === "Signupchild" || route.routeName === "Thankssignup" ) {
      tabBarVisible = false;
    } else {
      tabBarVisible = true;
    }

  });

  return {
    tabBarVisible,
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused }) => (
      <View
        style={focused?{
        marginBottom: 3,
        paddingTop:5,
          width:'100%',
          borderTopWidth:2,
          borderTopColor:'#014863',
          textAlign:'center',
          backgroundColor:'#FFFFFF',
          alignContent:'center'
        }:
        {
          width:'100%',
          textAlign:'center',
          backgroundColor:'#FFFFFF'
        }}
      >
        <ImageBackground
          source={
            focused
              ? require('../assets/images/home_active.png')
              : require('../assets/images/home.png')
          }
          style={{
            width: 26,
            height:26,
            alignSelf:'center'
          }}
        />
      </View>
    ),
    tabBarOptions: ({focused}) => {
    
      (focused)?{
        tabStyle: {
          backgroundColor: '#FFFFFF',
        }, 
        inactiveTintColor: {
          backgroundColor: '#F1F1F1',
        },
        activeTintColor: {
          backgroundColor: '#FFFFFF',
        },

      }:{

        tabStyle:{
          backgroundColor: '#F1F1F1',  
        },   
        inactiveTintColor: {
          backgroundColor: '#F1F1F1',
        },
        activeTintColor: {
          backgroundColor: '#FFFFFF',
        },

      }

    }
    
  };
};



const ExploreStack = createStackNavigator({
  
  Explore: ExploreScreen,
  Calender: CalenderScreen,
  EventsDescription: EventsDescriptionScreen,
  ExploreViewAll: ExploreViewAllScreen,
  ExploreServices: ExploreServicesScreen,
  ServicesItems: ServicesItemsScreen,
  BookMultiple: BookMultipleScreen,
  Location: LocationScreen,
  LocationFilterScreen: LocationFilterScreen,
  FilterSchedule:FilterSchedule

});

ExploreStack.navigationOptions = {
  tabBarLabel: 'Explore',
  tabBarIcon: ({ focused }) => (
    <View
        style={focused?{
            marginBottom: 3,
            paddingTop:5,
              width:'100%',
              borderTopWidth:2,
              borderTopColor:'#014863',
              textAlign:'center',
              backgroundColor:'#FFFFFF',
              alignContent:'center'
            }:
            {
              width:'100%',
              textAlign:'center',
              backgroundColor:'#FFFFFF'
            }}
        >
          <ImageBackground
        source={
          focused
            ? require('../assets/images/explore_active.png')
            : require('../assets/images/explore.png')
        }
        style={{
          width: 26,
          height:26,
          alignSelf:'center'
        }}

        />
    </View>
  ),
  tabBarOptions : {

    activeBackgroundColor: {
      backgroundColor: '#FFFFFF',
    },
    inactiveBackgroundColor: {
      backgroundColor: '#F1F1F1',
    },

  },
};

const ScheduleStack = createStackNavigator({
  GuestSchedules:GuestSchedulesScreen,
});

ScheduleStack.navigationOptions = {
  tabBarLabel: 'Schedule',
  tabBarIcon: ({ focused }) => (
    <View
        style={focused?{
            marginBottom: 3,
            paddingTop:5,
              width:'100%',
              borderTopWidth:2,
              borderTopColor:'#014863',
              textAlign:'center',
              backgroundColor:'#FFFFFF',
              alignContent:'center'
            }:
            {
              width:'100%',
              textAlign:'center',
              backgroundColor:'#FFFFFF'
            }}
        >
          <ImageBackground
        source={
          focused
            ? require('../assets/images/schedule_active.png')
            : require('../assets/images/schedule.png')
        }
        style={{
          width: 26,
          height:26,
          alignSelf:'center'
        }}

        />
    </View>
  ),
  tabBarOptions : {
    
    activeBackgroundColor: {
      backgroundColor: '#FFFFFF',
    },
    inactiveBackgroundColor: {
      backgroundColor: '#F1F1F1',
    },

  },
};

const FavouriteStack = createStackNavigator({
  Favourite: FavouriteScreen,
  FavServicesItems: FavServicesItemsScreen,
  Loading: LoadingScreen,
});

FavouriteStack.navigationOptions = {
  tabBarLabel: 'Favourite',
  tabBarIcon: ({ focused }) => (
    <View
        style={focused?{
            marginBottom: 3,
            paddingTop:5,
              width:'100%',
              borderTopWidth:2,
              borderTopColor:'#014863',
              textAlign:'center',
              backgroundColor:'#FFFFFF',
              alignContent:'center'
            }:
            {
              width:'100%',
              textAlign:'center',
              backgroundColor:'#FFFFFF'
            }}
        >
          <ImageBackground
        source={
          focused
            ? require('../assets/images/favorite_active.png')
            : require('../assets/images/favorite.png')
        }
        style={{
          width: 26,
          height:26,
          alignSelf:'center'
        }}

        />
    </View>

    // <TabBarIcon
    //   focused={focused}
    //   name={Platform.OS === 'ios' ? 'ios-star' : 'md-star'}
    // />
  ),
  tabBarOptions : {
    
    activeBackgroundColor: {
      backgroundColor: '#FFFFFF',
    },
    inactiveBackgroundColor: {
      backgroundColor: '#F1F1F1',
    },

  },

};
const ProfileStack = createStackNavigator({
  
  

  Profile: ProfileScreen,
  MainProfile:MainProfileScreen,
  Setting: SettingScreen,
  Buyservices: BuyServicesScreen,
  Checkout:CheckoutScreen,  
  WebViewIframe:WebViewIframe,
  PaymentStatus:PaymentStatus,
  Payment:PaymentScreen,
  Paymentcard:PaymentCardScreen,
  Creditcard:CreditCardScreen,
  UpdateCreditcard:UpdateCreditCardScreen,
  payByexistingcard:payByexistingcardScreen,
  NewCreditcard:NewCreditCardScreen,
  Schedule: ScheduleScreen,
  MyPoints:MypointsScreen,
  MyAccount:AccountsScreen,
  AccountsChild: AccountsChildScreen,
  MyCard:MyCardScreen,
  Contact:ContactScreen,
  ThankYou:ThankYouScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  header:true,
  tabBarIcon: ({ focused }) => (
    <View
    style={focused?{
         marginBottom: 3,
         paddingTop:5,
          width:'100%',
          borderTopWidth:2,
          borderTopColor:'#014863',
          textAlign:'center',
          backgroundColor:'#FFFFFF',
          alignContent:'center'
        }:
        {
          width:'100%',
          textAlign:'center',
          backgroundColor:'#FFFFFF'
        }}
    >
      <ImageBackground
        source={
          focused
            ? require('../assets/images/profile_active.png')
            : require('../assets/images/profile.png')
        }
        style={{
          width: 34,
          height:22,
          alignSelf:'center'
        }}
      />
      </View>
  ),
  tabBarOptions : {
    
    activeBackgroundColor: {
      backgroundColor: '#FFFFFF',
    },
    inactiveBackgroundColor: {
      backgroundColor: '#F1F1F1',
    },

  },
};

export default createBottomTabNavigator({
  
  HomeStack,
  ExploreStack,  
  ScheduleStack,
  FavouriteStack,
  ProfileStack,
});
